<?php

namespace Tests\AppBundle\AuditReaderService;

use AppBundle\Service\AuditReaderService;
use PHPUnit\Framework\TestCase;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadataInfo;

/**
 * Class UserTest
 * @package Tests\AppBundle\Service
 */
class AuditReaderServiceTest extends TestCase
{
    public function testEditCurrentUserNull()
    {
        $mockEntityManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getConnection', 'getDatabasePlatform', 'getConfiguration'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $getConnection = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getConnection', 'getDatabasePlatform'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $getConfiguration = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getConnection', 'getQuoteStrategy'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $mockEntityManager->expects($this->once())
            ->method('getConnection')
            ->will($this->returnValue($getConnection));

        $mockEntityManager->expects($this->once())
            ->method('getConfiguration')
            ->will($this->returnValue($getConfiguration));

        $config = $this->createMock('SimpleThings\EntityAudit\AuditConfiguration');
        $getAllClassNames = ["test"];

        $factory = $this->getMockBuilder('SimpleThings\EntityAudit\Metadata\MetadataFactory')
            ->setMethods(array('getAllClassNames', 'isAudited'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $factory->expects($this->any())
            ->method('getAllClassNames')
            ->will($this->returnValue($getAllClassNames));

        $factory->expects($this->any())
            ->method('isAudited')
            ->will($this->returnValue(true));

        $AuditReaderService = new AuditReaderService($mockEntityManager, $config, $factory);
        $AuditReaderService->isLoadAuditedCollections();
    }


    public function mockEntityManager()
    {
        $mockEntityManager = $this
            ->getMockBuilder('Doctrine\ORM\EntityManagerInterface')
            ->setMethods(array('getConnection', 'getDatabasePlatform', 'getConfiguration'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $mockEntityManager->expects($this->any())
            ->method('getConnection')
            ->will($this->returnValue($this->getConnection()));

        $mockEntityManager->expects($this->any())
            ->method('getConfiguration')
            ->will($this->returnValue($this->getConfiguration()));

        return $mockEntityManager;
    }

    public function getConnection()
    {
        $getConnection = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getConnection', 'getDatabasePlatform'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        return $getConnection;
    }

    public function getConfiguration()
    {
        $getConfiguration = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getConnection', 'getQuoteStrategy'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        return $getConfiguration;
    }

    public function config()
    {
        $config = $this->createMock('SimpleThings\EntityAudit\AuditConfiguration');

        return $config;
    }

    public function factory()
    {
        $factory = $this->createMock('SimpleThings\EntityAudit\Metadata\MetadataFactory');

        return $factory;
    }

    public function testIsLoadNativeEntities()
    {
        $AuditReaderService = new AuditReaderService($this->mockEntityManager(), $this->config(), $this->factory());
        $AuditReaderService->isLoadNativeEntities();
    }

    public function testGetConnection()
    {
        $AuditReaderService = new AuditReaderService($this->mockEntityManager(), $this->config(), $this->factory());
        $AuditReaderService->getConnection();
    }

    public function testGetConfiguration()
    {
        $AuditReaderService = new AuditReaderService($this->mockEntityManager(), $this->config(), $this->factory());
        $AuditReaderService->getConfiguration();
    }

    public function testSetLoadNativeEntities()
    {
        $AuditReaderService = new AuditReaderService($this->mockEntityManager(), $this->config(), $this->factory());
        $AuditReaderService->setLoadNativeEntities(true);
    }

    public function testClearEntityCache()
    {
        $AuditReaderService = new AuditReaderService($this->mockEntityManager(), $this->config(), $this->factory());
        $AuditReaderService->clearEntityCache();
    }

    public function testSetLoadAuditedCollections()
    {
        $AuditReaderService = new AuditReaderService($this->mockEntityManager(), $this->config(), $this->factory());
        $AuditReaderService->setLoadAuditedCollections(true);
    }

    public function testIsLoadAuditedEntities()
    {
        $AuditReaderService = new AuditReaderService($this->mockEntityManager(), $this->config(), $this->factory());
        $AuditReaderService->isLoadAuditedEntities(true);
    }

    public function testSetLoadAuditedEntities()
    {
        $AuditReaderService = new AuditReaderService($this->mockEntityManager(), $this->config(), $this->factory());
        $AuditReaderService->setLoadAuditedEntities(true);
    }

    public function testIsLoadNativeCollections()
    {
        $AuditReaderService = new AuditReaderService($this->mockEntityManager(), $this->config(), $this->factory());
        $AuditReaderService->isLoadNativeCollections(true);
    }

    public function TestSetLoadNativeCollections()
    {
        $AuditReaderService = new AuditReaderService($this->mockEntityManager(), $this->config(), $this->factory());
        $AuditReaderService->setLoadNativeCollections(true);
    }

    public function TestFind()
    {
        $AuditReaderService = new AuditReaderService($this->mockEntityManager(), $this->config(), $this->factory());
        $AuditReaderService->find("AppBundle\Entity\User", 1, 1, []);
    }

    public function testFindRevisionHistory()
    {
        $AuditReaderService = new AuditReaderService($this->mockEntityManager(), $this->config(), $this->factory());
//        $AuditReaderService->findRevisionHistory(1,10,"");
    }

    public function testFindEntitesChangedAtRevision()
    {
        $AuditReaderService = new AuditReaderService($this->mockEntityManager(), $this->config(), $this->factory());
//        $AuditReaderService->findEntitiesChangedAtRevision(1);
    }

    public function testGetEntityValues()
    {
        $AuditReaderService = new AuditReaderService($this->mockEntityManager(), $this->config(), $this->factory());
        $entity = $this->createMock('SimpleThings\EntityAudit\Metadata\MetadataFactory');
        $className = 'SimpleThings\EntityAudit\Metadata\MetadataFactory';
        //        $AuditReaderService->getEntityValues($className,$entity);
    }

    public function testDiff()
    {
        $getAllClassNames = ["test"];

        $factory = $this->getMockBuilder('SimpleThings\EntityAudit\Metadata\MetadataFactory')
            ->setMethods(array('getAllClassNames', 'isAudited'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $factory->expects($this->any())
            ->method('getAllClassNames')
            ->will($this->returnValue($getAllClassNames));

        $factory->expects($this->any())
            ->method('isAudited')
            ->will($this->returnValue(true));

        $TableName = new ClassMetadataInfo($this->mockEntityManager());

        $config = $this->getMockBuilder('SimpleThings\EntityAudit\AuditConfiguration')
            ->setMethods(array('getTableName'))
            ->disableOriginalConstructor()
            ->getMock();

        $config->expects($this->any())
            ->method('getTableName')
            ->will($this->returnValue($TableName));

        $AuditReaderService = new AuditReaderService($this->mockEntityManager(), $config, $factory);
        $className = 'AppBundle\Entity\User';

//        $AuditReaderService->diff($className,1,1,1);
    }
}