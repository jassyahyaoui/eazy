<?php

namespace Tests\AppBundle\Service;


use AppBundle\Entity\Company;
use AppBundle\Entity\ServiceRole;
use AppBundle\Entity\User;
use AppBundle\Entity\UserCompany;
use AppBundle\Entity\UserCompanyService;
use AppBundle\Entity\UserCompanyServiceRoles;
use AppBundle\Service\OneUp;
use PHPUnit\Framework\TestCase;

class OneUpTest extends TestCase
{
    public function testFetchMeParam()
    {
        $container = $this
            ->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface')
            ->disableOriginalConstructor()
            ->getMock();


        $oneUp = new OneUp($container);

        $serviceRole = new ServiceRole();
        $serviceRole->setExternalId('external');

        $userCompanyServiceRoles = new UserCompanyServiceRoles();
        $userCompanyServiceRoles->setServiceRole($serviceRole);

        $userCompanyService = new UserCompanyService();
        $userCompanyService->addUserCompanyServiceRole($userCompanyServiceRoles);

        $userCompany = $this
            ->getMockBuilder('\AppBundle\Entity\UserCompany')
            ->disableOriginalConstructor()
            ->getMock();
        $userCompany
            ->expects($this->any())
            ->method('getUserCompanyService', 'getValues')
            ->willReturn($userCompanyService);

        $roles = array('ROLE_ADMIN', 'ROLE_USER', 'ROLE_OWNER');

        $userCompany
            ->expects($this->any())
            ->method('getRole')
            ->willReturn($roles);

        $user = new User();
        $user->setUsername('test username');
        $user->setUsernameCanonical('test username');
        $user->setEmail('test@test.com');
        $user->setCreatedAt(new \DateTime('now'));

        $company = $this
            ->getMockBuilder('\AppBundle\Entity\Company')
            ->disableOriginalConstructor()
            ->getMock();

        $oneUp->fetchMe($user, $company, 1, $userCompany, $userCompanyService);

        $this->assertNotNull($user);
        $this->assertInternalType('object', $user);
        $this->assertNotNull($company);
        $this->assertInternalType('object', $company);
        $this->assertNotNull($userCompany);
        $this->assertInternalType('object', $userCompany);
        $this->assertNotNull($userCompanyService);
        $this->assertInternalType('object', $userCompanyService);
    }

    public function testFetchMeRoleOwner()
    {
        $container = $this
            ->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $oneUp = new OneUp($container);

        $serviceRole = new ServiceRole();
        $serviceRole->setExternalId('external');

        $userCompanyServiceRoles = new UserCompanyServiceRoles();
        $userCompanyServiceRoles->setServiceRole($serviceRole);

        $userCompanyService = new UserCompanyService();
        $userCompanyService->addUserCompanyServiceRole($userCompanyServiceRoles);

        $userCompany = $this
            ->getMockBuilder('\AppBundle\Entity\UserCompany')
            ->disableOriginalConstructor()
            ->getMock();
        $userCompany
            ->expects($this->any())
            ->method('getUserCompanyService', 'getValues')
            ->willReturn($userCompanyService);

        $user = new User();
        $user->setUsername('test username');
        $user->setUsernameCanonical('test username');
        $user->setEmail('test@test.com');
        $user->setCreatedAt(new \DateTime('now'));

        $company = $this
            ->getMockBuilder('\AppBundle\Entity\Company')
            ->disableOriginalConstructor()
            ->getMock();

        $roles = array('ROLE_ADMIN', 'ROLE_USER', 'ROLE_OWNER');

        $userCompany
            ->expects($this->any())
            ->method('getRole')
            ->willReturn($roles);


        $response1 = $oneUp->fetchMe($user, $company, 1, $userCompany, $userCompanyService);

        $this->assertNotNull($response1);
        $this->assertEquals('full', $response1['user_group_role']);
        $this->assertEquals('admin', $response1['user_role']);
    }

    public function testFetchMeRoleNotOwner()
    {
        $container = $this
            ->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $oneUp = new OneUp($container);

        $serviceRole = new ServiceRole();
        $serviceRole->setExternalId('external');

        $userCompanyServiceRoles = new UserCompanyServiceRoles();
        $userCompanyServiceRoles->setServiceRole($serviceRole);

        $userCompanyService = new UserCompanyService();
        $userCompanyService->addUserCompanyServiceRole($userCompanyServiceRoles);

        $userCompany = $this
            ->getMockBuilder('\AppBundle\Entity\UserCompany')
            ->disableOriginalConstructor()
            ->getMock();
        $userCompany
            ->expects($this->any())
            ->method('getUserCompanyService', 'getValues')
            ->willReturn($userCompanyService);

        $user = new User();
        $user->setUsername('test username');
        $user->setUsernameCanonical('test username');
        $user->setEmail('test@test.com');
        $user->setCreatedAt(new \DateTime('now'));

        $company = $this
            ->getMockBuilder('\AppBundle\Entity\Company')
            ->disableOriginalConstructor()
            ->getMock();

        $roles = array('ROLE_ADMIN', 'ROLE_USER');

        $userCompany
            ->expects($this->any())
            ->method('getRole')
            ->willReturn($roles);


        $response1 = $oneUp->fetchMe($user, $company, 1, $userCompany, $userCompanyService);

        $this->assertNotNull($response1);
        $this->assertNotEquals('full', $response1['user_group_role']);
        $this->assertNotEquals('admin', $response1['user_role']);
    }

    public function testFetchMeRoleEnabled()
    {
        $container = $this
            ->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $oneUp = new OneUp($container);

        $serviceRole = new ServiceRole();
        $serviceRole->setExternalId('external');

        $userCompanyServiceRoles = new UserCompanyServiceRoles();
        $userCompanyServiceRoles->setServiceRole($serviceRole);

        $userCompanyService = new UserCompanyService();
        $userCompanyService->addUserCompanyServiceRole($userCompanyServiceRoles);
        $userCompanyService->setEnabled(true);

        $userCompany = $this
            ->getMockBuilder('\AppBundle\Entity\UserCompany')
            ->disableOriginalConstructor()
            ->getMock();
        $userCompany
            ->expects($this->any())
            ->method('getUserCompanyService', 'getValues')
            ->willReturn($userCompanyService, $userCompany);
        $userCompany
            ->expects($this->any())
            ->method('getEnabled')
            ->willReturn(true);

        $user = new User();
        $user->setUsername('test username');
        $user->setUsernameCanonical('test username');
        $user->setEmail('test@test.com');
        $user->setCreatedAt(new \DateTime('now'));
        $user->setEnabled(true);

        $company = $this
            ->getMockBuilder('\AppBundle\Entity\Company')
            ->disableOriginalConstructor()
            ->getMock();

        $roles = array('ROLE_ADMIN', 'ROLE_USER');

        $userCompany
            ->expects($this->any())
            ->method('getRole')
            ->willReturn($roles);


        $response1 = $oneUp->fetchMe($user, $company, 1, $userCompany, $userCompanyService);


        $this->assertNotNull($response1);
        $this->assertEquals('enable', $response1['user_status']);
        $this->assertNotEquals('disable', $response1['user_status']);
    }

    public function testFetchMeRoleNotEnabled()
    {
        $container = $this
            ->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $oneUp = new OneUp($container);

        $serviceRole = new ServiceRole();
        $serviceRole->setExternalId('external');

        $userCompanyServiceRoles = new UserCompanyServiceRoles();
        $userCompanyServiceRoles->setServiceRole($serviceRole);

        $userCompanyService = new UserCompanyService();
        $userCompanyService->addUserCompanyServiceRole($userCompanyServiceRoles);
        $userCompanyService->setEnabled(false);

        $userCompany = $this
            ->getMockBuilder('\AppBundle\Entity\UserCompany')
            ->disableOriginalConstructor()
            ->getMock();
        $userCompany
            ->expects($this->any())
            ->method('getUserCompanyService', 'getValues')
            ->willReturn($userCompanyService, $userCompany);
        $userCompany
            ->expects($this->any())
            ->method('getEnabled')
            ->willReturn(false);

        $user = new User();
        $user->setUsername('test username');
        $user->setUsernameCanonical('test username');
        $user->setEmail('test@test.com');
        $user->setCreatedAt(new \DateTime('now'));
        $user->setEnabled(false);

        $company = $this
            ->getMockBuilder('\AppBundle\Entity\Company')
            ->disableOriginalConstructor()
            ->getMock();

        $roles = array('ROLE_ADMIN', 'ROLE_USER');

        $userCompany
            ->expects($this->any())
            ->method('getRole')
            ->willReturn($roles);


        $response1 = $oneUp->fetchMe($user, $company, 1, $userCompany, $userCompanyService);


        $this->assertNotNull($response1);
        $this->assertEquals('disable', $response1['user_status']);
        $this->assertNotEquals('enable', $response1['user_status']);
    }


    public function testEventOneUp()
    {
        $container = $this
            ->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface')
            ->disableOriginalConstructor()
            ->getMock();
        $container
            ->expects($this->any())
            ->method('getParameter');

        $user = $this
            ->getMockBuilder('\AppBundle\Entity\User')
            ->disableOriginalConstructor()
            ->getMock();

        $company = $this
            ->getMockBuilder('\AppBundle\Entity\Company')
            ->disableOriginalConstructor()
            ->getMock();


        $oneUp = new OneUp($container);
        $eventType = 'DISABLE_USER';
        $oneUp->eventOneUp($user, $company, $eventType);

        $this->assertNotNull($user);
        $this->assertNotNull($company);
        $this->assertNotNull($eventType);
        $this->assertInternalType('object', $user);
        $this->assertInternalType('object', $company);
        $this->assertInternalType('string', $eventType);
    }

    public function testHash()
    {
        $container = $this
            ->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface')
            ->disableOriginalConstructor()
            ->getMock();
        $container
            ->expects($this->any())
            ->method('getParameter');

        $user = $this
            ->getMockBuilder('\AppBundle\Entity\User')
            ->disableOriginalConstructor()
            ->getMock();
        $user
            ->expects($this->any())
            ->method('getId')
            ->willReturn(2);

        $company = $this
            ->getMockBuilder('\AppBundle\Entity\Company')
            ->disableOriginalConstructor()
            ->getMock();
        $company
            ->expects($this->any())
            ->method('getId')
            ->willReturn(1);


        $oneUp = new OneUp($container);
        $data = array(
            'event_type' => 'ENABLE_USER',
            'company_id' => $company->getId(),
            'user_id' => $user->getId(),
        );
        $oneUp->hash($data, 'SECRET');

        $this->assertNotNull($data);
        $this->assertInternalType('array', $data);
        $this->assertInternalType('string', $data['event_type']);
        $this->assertNotNull($data['company_id']);
        $this->assertNotNull($data['user_id']);
    }

    public function testHeader()
    {

        $container = $this
            ->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface')
            ->disableOriginalConstructor()
            ->getMock();
        $container
            ->expects($this->any())
            ->method('getParameter');

        $user = $this
            ->getMockBuilder('\AppBundle\Entity\User')
            ->disableOriginalConstructor()
            ->getMock();
        $user
            ->expects($this->any())
            ->method('getId')
            ->willReturn(2);

        $company = $this
            ->getMockBuilder('\AppBundle\Entity\Company')
            ->disableOriginalConstructor()
            ->getMock();
        $company
            ->expects($this->any())
            ->method('getId')
            ->willReturn(1);


        $oneUp = new OneUp($container);
        $data = array(
            'event_type' => 'ENABLE_USER',
            'company_id' => $company->getId(),
            'user_id' => $user->getId(),
        );
        $response = $oneUp->header($data, 'SECRET');

        $this->assertNotNull($data);
        $this->assertInternalType('array', $data);
        $this->assertNotNull($response);
        $this->assertNotEmpty($response);
        $this->assertInternalType('array', $response);


    }

}