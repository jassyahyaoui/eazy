<?php

namespace Tests\AppBundle\Service;

use AppBundle\Entity\User;
use AppBundle\Service\Company;
use PHPUnit\Framework\TestCase;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Component\DependencyInjection\Container;


/**
 * Class CompanyTest
 * @package Tests\AppBundle\Service
 */
class CompanyTest extends TestCase
{
    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    public function entityManagerMock()
    {
        $entityManager = $this->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository', 'persist','flush'))
            ->disableOriginalConstructor()
            ->getMock();

        return $entityManager;
    }

    public function testSetCurrentEmptyCompany()
    {
        $companyRepository = $this->createMock(ObjectRepository::class);
        $companyRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn(null);
        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($companyRepository);

        $companyService = new Company($objectManager);
        $user = new User();
        $user->setUsername("test");

        $result = $companyService->setCurrentCompany($user, 'test');
        $this->assertNotEmpty($result);
        $this->assertEquals($result["message"], "Error");
    }

    public function testSetCurrentCompany()
    {
        $companyRepository = $this->createMock(ObjectRepository::class);
        $companyMock = $this->createMock('\AppBundle\Entity\Company');
        $companyMock->expects($this->any())
            ->method('getId')
            ->willReturn(1);

        $companyRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn($companyMock);

        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($companyRepository);

        $companyService = new Company($objectManager);
        $user = new User();
        $user->setUsername("testUser");
        $result = $companyService->setCurrentCompany($user, 'test');

        $this->assertNotEmpty($result);
        $this->assertEquals($result["company"], 1);
        $this->assertEquals($result["message"], "Your current company has changed");


    }

    public function testGetCurrentCompany()
    {
        $userRepository = $this->createMock(ObjectRepository::class);
        
        $companyMock = $this->createMock('\AppBundle\Entity\Company');
        $companyMock->expects($this->any())
            ->method('getId')
            ->willReturn(1);
        $userMock = $this->createMock('\AppBundle\Entity\User');
        $userMock->expects($this->any())
            ->method('getCurrentCompany')
            ->willReturn($companyMock);

        
        $userRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn($userMock);
        
        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($userRepository);
        
       $company = new Company($objectManager);
       $result = $company->getCurrentCompany("test");
       
       $this->assertNotNull($result);
       $this->assertEquals($result->getId(), 1);

    }

    public function testGetCurrentCompanyWithNullUser()
    {
        $userRepository = $this->createMock(ObjectRepository::class);

        $companyMock = $this->createMock('\AppBundle\Entity\Company');
        $companyMock->expects($this->any())
            ->method('getId')
            ->willReturn(1);

        $userMock = $this->createMock('\AppBundle\Entity\User');
        $userMock->expects($this->any())
            ->method('getCurrentCompany')
            ->willReturn($companyMock);

        $userRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn(null);

        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($userRepository);
       $company = new Company($objectManager);
       $result = $company->getCurrentCompany("test");
       $this->assertNull($result);
    }
}