<?php

namespace Tests\AppBundle\ServiceService;

use PHPUnit\Framework\TestCase;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectRepository;
use AppBundle\Entity\Company;
use AppBundle\Entity\User;
use AppBundle\Service\ServicesService as ServicesService;

class ServiceServiceTest extends TestCase
{

    public function testGtUserCompanyServicesNotNull()
    {
        $userRepository = $this
            ->getMockBuilder(ObjectRepository::class)
            ->setMethods(array('getUserCompanyServices', 'findOneByUsername'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $companyMock = new Company();
        $companyMock->setName('test');
        $userMock = new User();
        $userMock->setUsername('test');
        $userMock->setCurrentCompany($companyMock);
        $UserCompanyService = $this
            ->getMockBuilder(ObjectRepository::class)
            ->setMethods(array('getUserCompanyServices', 'findOneByUsername'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $UserCompanyService->expects($this->any())
            ->method('getUserCompanyServices')
            ->willReturn(null);

        $userRepository->expects($this->any())
            ->method('findOneByUsername')
            ->willReturn($userMock);

        $objectManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->withAnyParameters()
            ->willReturn($userRepository);

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->withAnyParameters()
            ->willReturn($UserCompanyService);

        $service = new ServicesService($objectManager);
        $result = $service->getUserCompanyServices("test");
        $this->assertNotNull($result);
    }

    public function testGtUserCompanyServicesWithUserNull()
    {
        $userRepository = $this
            ->getMockBuilder(ObjectRepository::class)
            ->setMethods(array('getUserCompanyServices', 'findOneByUsername'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $userRepository->expects($this->any())
            ->method('findOneByUsername')
            ->willReturn(null);

        $objectManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->withAnyParameters()
            ->willReturn($userRepository);

        $service = new ServicesService($objectManager);
        $result = $service->getUserCompanyServices("test");
        $this->assertNull($result);
    }

    public function testGetUserCompanyServicesCurrentCompanyNull()
    {
        $userRepository = $this
            ->getMockBuilder(ObjectRepository::class)
            ->setMethods(array('getUserCompanyServices', 'findOneByUsername'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $userMock = new User();
        $userMock->setUsername('test');

        $userRepository->expects($this->any())
            ->method('findOneByUsername')
            ->willReturn($userMock);

        $objectManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->withAnyParameters()
            ->willReturn($userRepository);

        $service = new ServicesService($objectManager);
        $result = $service->getUserCompanyServices("test");
        $this->assertNull($result);
    }
}