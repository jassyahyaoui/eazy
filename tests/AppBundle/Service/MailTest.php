<?php

namespace Tests\AppBundle\Service;


use AppBundle\Service\Mail;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Templating\EngineInterface;


class MailTest extends TestCase
{
    public function testSendMailPassword()
    {
        $mailer = $this
            ->getMockBuilder('\Swift_Mailer')
            ->disableOriginalConstructor()
            ->getMock();
        $mailer
            ->expects($this->once())
            ->method('send');
        $templating = $this
            ->getMockBuilder('Symfony\Component\Templating\EngineInterface')
            ->disableOriginalConstructor()
            ->getMock();
        $templating
            ->expects($this->once())
            ->method('render');

        $container = $this
            ->getMockBuilder('Symfony\Component\DependencyInjection\Container')
            ->disableOriginalConstructor()
            ->getMock();

        $mail = new Mail($mailer, $templating,$container);

        $subject = 'the mail subject';
        $from = 'test@test.test';
        $senderName = "test";
        $to = "test@test.fr";
        $uniqPassword = 'uniq password test';
        $baseResettingUrl = "www.mazars.fr";

        //  $subject, $from, $senderName, $to, $uniqPassword,$baseResettingUrl)

        $mail->SendMailPassword($subject, $from,$senderName, $to,$uniqPassword,$baseResettingUrl);
        $this->assertNotNull($subject);
        $this->assertNotNull($to);
        $this->assertNotNull($uniqPassword);
    }

    public function testSendJson()
    {
        $mailer = $this
            ->getMockBuilder('\Swift_Mailer')
            ->disableOriginalConstructor()
            ->getMock();
        $mailer
            ->expects($this->any())
            ->method('send');
        $templating = $this
            ->getMockBuilder('Symfony\Component\Templating\EngineInterface')
            ->disableOriginalConstructor()
            ->getMock();
        $templating
            ->expects($this->any())
            ->method('render');
        $container = $this
            ->getMockBuilder('Symfony\Component\DependencyInjection\Container')
            ->disableOriginalConstructor()
            ->getMock();

        $mail = new Mail($mailer, $templating,$container);
        $subject = 'the mail subject';
        $to = 'test@test.test';
        $password = 'uniq password test';

        $mail->sendJson($subject, $to, $password);
        $this->assertNotNull($subject);
        $this->assertNotNull($to);
        $this->assertNotNull($password);
    }



}