<?php

namespace Tests\AppBundle\Service;

use AppBundle\Entity\Notification as NotificationEntity;
use AppBundle\Repository\UserRepository;
use PHPUnit\Framework\TestCase;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use AppBundle\Entity\Office;
use Mgilet\NotificationBundle\Manager\NotificationManager;
use AppBundle\Entity\User;
use AppBundle\Service\Notification as NotificationService;
use Doctrine\ORM\EntityManager;
use PhpunitBundle\Repository\CustomerRepository;
use Mgilet\NotificationBundle\Entity\NotifiableNotification;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\AbstractQuery;

class NotificationTest extends TestCase
{
    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    public function entityManagerMock()
    {
        $entityManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository', 'persist', 'flush'))
            ->disableOriginalConstructor()
            ->getMock();

        return $entityManager;
    }

    public function testCreateNotificationOwnerActionNull()
    {
        $userRepository = $this->createMock(UserRepository::class);
        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->with('AppBundle:User')
            ->willReturn($userRepository);

        $createNotification = $this->createMock('Mgilet\NotificationBundle\Entity\Notification');
        $notificationMock = $this->createMock('\Mgilet\NotificationBundle\Manager\NotificationManager');
        $notificationMock->expects($this->any())
            ->method('createNotification')
            ->willReturn($createNotification);

        $service = new NotificationService($objectManager,$notificationMock);
        $notificationData = ["companyId" => "companyId" ,"sujet" => "sujet test", "message" => "message test","company" => "company", "link" => "link", "user" => 1];
        $result = $service->createNotificationOwnerAction($notificationData);
    }

    public function testCreateNotificationOwnerActionEmptyArguments()
    {
        $userRepository = $this->createMock(UserRepository::class);
        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->with('AppBundle:User')
            ->willReturn($userRepository);

        $serviceRole = $this->getMockForAbstractClass('\AppBundle\Entity\ServiceRole');
        $userCompanyServiceRoles = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyServiceRoles');
        $userCompanyServiceRoles->setServiceRole($serviceRole);
        $entity = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyService');
        $entity->addUserCompanyServiceRole($userCompanyServiceRoles);
        // Data preparation End

        $createNotification = $this->createMock('Mgilet\NotificationBundle\Entity\Notification');
        $notificationMock = $this->createMock('\Mgilet\NotificationBundle\Manager\NotificationManager');
        $notificationMock->expects($this->any())
            ->method('createNotification')
            ->willReturn($createNotification);

        $service = new NotificationService($objectManager,$notificationMock);
        $notificationData = ["companyId" => "companyId" ,"sujet" => "", "message" => "message test","company" => "company", "link" => "link", "user" => 1];
        $result = $service->createNotificationOwnerAction($notificationData);

    }

    public function testCreateNotificationUsersFromOfficeActionIsNotNull()
    {
        $office = new Office();
        $office->setName("test");

        $userOne = new User();
        $userTwo = new User();
        $users = array($userOne,$userTwo);

        $repositoryMock = $this
            ->getMockBuilder(ObjectRepository::class)
            ->setMethods(array('getUsersOffice'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $repositoryMock->expects($this->any())
            ->method('getUsersOffice')
            ->willReturn($users);

        $repositoryMock->expects($this->any())
            ->method('find')
            ->willReturn($office);

        $objectManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->withAnyParameters()
            ->willReturn($repositoryMock);

        $createNotification = $this->createMock('Mgilet\NotificationBundle\Entity\Notification');
        $notificationMock = $this->createMock('\Mgilet\NotificationBundle\Manager\NotificationManager');
        $notificationMock->expects($this->any())
            ->method('createNotification')
            ->willReturn($createNotification);
        $service = new NotificationService($objectManager,$notificationMock);

        $notificationData = ["companyId" => "companyId" ,"sujet" => "sujet test", "message" => "message test","company" => "company", "link" => "link", "office" => "1"];
        $service->createNotificationUsersFromOfficeAction($notificationData);
        $this->assertNotNull($office);
        $this->assertNotNull($users);

    }
  public function testCreateNotificationUsersFromOfficeActionWithUsersNull()
    {
        $office = new Office();
        $office->setName("test");
        $repositoryMock = $this
            ->getMockBuilder(ObjectRepository::class)
            ->setMethods(array('getUsersOffice'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $repositoryMock->expects($this->any())
            ->method('getUsersOffice')
            ->willReturn(null);
        $repositoryMock->expects($this->any())
            ->method('find')
            ->willReturn($office);
        $objectManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->withAnyParameters()
            ->willReturn($repositoryMock);

        $createNotification = $this->createMock('Mgilet\NotificationBundle\Entity\Notification');
        $notificationMock = $this->createMock('\Mgilet\NotificationBundle\Manager\NotificationManager');
        $notificationMock->expects($this->any())
            ->method('createNotification')
            ->willReturn($createNotification);
        $service = new NotificationService($objectManager,$notificationMock);

        $notificationData = ["companyId" => "companyId" ,"sujet" => "sujet test", "message" => "message test","company" => "company", "link" => "link", "office" => "1"];
        $service->createNotificationUsersFromOfficeAction($notificationData);
    }

public function testCreateNotificationUsersFromOfficeActionWithOfficeNull()
    {
        $repositoryMock = $this
            ->getMockBuilder(ObjectRepository::class)
            ->setMethods(array('getUsersOffice'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $repositoryMock->expects($this->any())
            ->method('find')
            ->willReturn(null);

        $objectManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->withAnyParameters()
            ->willReturn($repositoryMock);

        $createNotification = $this->createMock('Mgilet\NotificationBundle\Entity\Notification');
        $notificationMock = $this->createMock('\Mgilet\NotificationBundle\Manager\NotificationManager');
        $notificationMock->expects($this->any())
            ->method('createNotification')
            ->willReturn($createNotification);
        $service = new NotificationService($objectManager,$notificationMock);

        $notificationData = ["companyId" => "companyId" ,"sujet" => "sujet test", "message" => "message test","company" => "company", "link" => "link", "office" => "1"];
        $service->createNotificationUsersFromOfficeAction($notificationData);
    }


public function testCreateNotificationUsersFromOfficeActionWithEmptyArguments()
    {
        $repositoryMock = $this
            ->getMockBuilder(ObjectRepository::class)
            ->setMethods(array('getUsersOffice'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $repositoryMock->expects($this->any())
            ->method('find')
            ->willReturn(null);

        $objectManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->withAnyParameters()
            ->willReturn($repositoryMock);

        $createNotification = $this->createMock('Mgilet\NotificationBundle\Entity\Notification');
        $notificationMock = $this->createMock('\Mgilet\NotificationBundle\Manager\NotificationManager');
        $notificationMock->expects($this->any())
            ->method('createNotification')
            ->willReturn($createNotification);
        $service = new NotificationService($objectManager,$notificationMock);

        $notificationData = ["sujet" => "", "message" => "", "link" => "", "office" => "1"];
        $service->createNotificationUsersFromOfficeAction($notificationData);
    }

    public function testCreateNotificationUserNotNull()
    {
        $user = new User();
        $user->setUsername("test");


        $userRepository = $this->createMock(UserRepository::class);
        $userRepository->expects($this->any())
            ->method('find')
            ->willReturn($user);

        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->with('AppBundle:User')
            ->willReturn($userRepository);


        $serviceRole = $this->getMockForAbstractClass('\AppBundle\Entity\ServiceRole');
        $userCompanyServiceRoles = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyServiceRoles');
        $userCompanyServiceRoles->setServiceRole($serviceRole);
        $entity = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyService');
        $entity->addUserCompanyServiceRole($userCompanyServiceRoles);
        // Data preparation End

        $createNotification = $this->createMock('Mgilet\NotificationBundle\Entity\Notification');
        $notificationMock = $this->createMock('\Mgilet\NotificationBundle\Manager\NotificationManager');
        $notificationMock->expects($this->any())
            ->method('createNotification')
            ->willReturn($createNotification);
        $service = new NotificationService($objectManager,$notificationMock);
        $notificationData = ["companyId" => "companyId" ,"sujet" => "sujet test", "message" => "message test","company" => "company", "link" => "link", "user" => 1];
        $result = $service->createNotificationUserAction($notificationData);
    }


    public function testCreateNotificationUserEmptyArguments ()
    {
        $user = new User();
        $user->setUsername("test");
        $userRepository = $this->createMock(UserRepository::class);
        $userRepository->expects($this->any())
            ->method('find')
            ->willReturn($user);

        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->with('AppBundle:User')
            ->willReturn($userRepository);


        $serviceRole = $this->getMockForAbstractClass('\AppBundle\Entity\ServiceRole');
        $userCompanyServiceRoles = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyServiceRoles');
        $userCompanyServiceRoles->setServiceRole($serviceRole);
        $entity = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyService');
        $entity->addUserCompanyServiceRole($userCompanyServiceRoles);
        // Data preparation End

        $createNotification = $this->createMock('Mgilet\NotificationBundle\Entity\Notification');
        $notificationMock = $this->createMock('\Mgilet\NotificationBundle\Manager\NotificationManager');
        $notificationMock->expects($this->any())
            ->method('createNotification')
            ->willReturn($createNotification);
        $service = new NotificationService($objectManager,$notificationMock);
        $notificationData = ["companyId" => "companyId" ,"sujet" => "", "message" => "","company" => "company", "link" => "", "user" => ""];
        $result = $service->createNotificationUserAction($notificationData);
    }

    public function testCreateNotificationUserNull()
    {
        $userRepository = $this->createMock(UserRepository::class);
        $userRepository->expects($this->any())
            ->method('find')
            ->willReturn(null);


        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->with('AppBundle:User')
            ->willReturn($userRepository);

        $serviceRole = $this->getMockForAbstractClass('\AppBundle\Entity\ServiceRole');
        $userCompanyServiceRoles = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyServiceRoles');
        $userCompanyServiceRoles->setServiceRole($serviceRole);
        $entity = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyService');
        $entity->addUserCompanyServiceRole($userCompanyServiceRoles);
        $createNotification = $this->createMock('Mgilet\NotificationBundle\Entity\Notification');
        $notificationMock = $this->createMock('\Mgilet\NotificationBundle\Manager\NotificationManager');
        $notificationMock->expects($this->any())
            ->method('createNotification')
            ->willReturn($createNotification);
        $service = new NotificationService($objectManager,$notificationMock);
        $notificationData = ["companyId" => "companyId" ,"sujet" => "sujet test", "message" => "message test","company" => "company", "link" => "link", "user" => 1];
        $result = $service->createNotificationUserAction($notificationData);
    }

    public function testNotifiableNotifications()
    {
        $result = [];

        $manager  = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $repository = $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $manager->expects($this->once())
            ->method('getRepository')
            ->with('MgiletNotificationBundle:NotifiableNotification')
            ->will($this->returnValue($repository));

        $queryBuilder = $this->getMockBuilder(QueryBuilder::class)
            ->disableOriginalConstructor()
            ->getMock();

        $repository->expects($this->once())
            ->method('createQueryBuilder')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(0))
            ->method('select')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(1))
            ->method('join')
            ->will($this->returnValue($queryBuilder));
        $queryBuilder->expects($this->at(2))
            ->method('join')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(3))
            ->method('AndWhere')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(4))
            ->method('setParameter')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(5))
            ->method('andWhere')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(6))
            ->method('setParameter')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(7))
            ->method('addSelect')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(8))
            ->method('addSelect')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(9))
            ->method('orderBy')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(10))
            ->method('setMaxResults')
            ->will($this->returnValue($queryBuilder));

        $getQuery = $this->getMockBuilder(AbstractQuery::class)
            ->setMethods(array('getResult'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $queryBuilder->expects($this->at(11))
            ->method('getQuery')
            ->will($this->returnValue($getQuery));

        $getQuery->expects($this->once())
            ->method('getResult')
            ->will($this->returnValue($result));

        $serviceRole = $this->getMockForAbstractClass('\AppBundle\Entity\ServiceRole');
        $userCompanyServiceRoles = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyServiceRoles');
        $userCompanyServiceRoles->setServiceRole($serviceRole);
        $entity = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyService');
        $entity->addUserCompanyServiceRole($userCompanyServiceRoles);
        $createNotification = $this->createMock('Mgilet\NotificationBundle\Entity\Notification');
        $notificationMock = $this->createMock('\Mgilet\NotificationBundle\Manager\NotificationManager');
        $notificationMock->expects($this->any())
            ->method('createNotification')
            ->willReturn($createNotification);

        $service = new NotificationService($manager,$notificationMock);
        $userId = 1;
        $max = 10;
        $service->notifiableNotifications($userId,$max);
    }

 public function testNbNotifiableNotifications()
    {
        $result = 1;

        $manager  = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $repository = $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $manager->expects($this->once())
            ->method('getRepository')
            ->with('MgiletNotificationBundle:NotifiableNotification')
            ->will($this->returnValue($repository));

        $queryBuilder = $this->getMockBuilder(QueryBuilder::class)
            ->disableOriginalConstructor()
            ->getMock();

        $repository->expects($this->once())
            ->method('createQueryBuilder')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(0))
            ->method('select')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(1))
            ->method('join')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(2))
            ->method('join')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(3))
            ->method('AndWhere')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(4))
            ->method('setParameter')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(5))
            ->method('andWhere')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(6))
            ->method('setParameter')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(7))
            ->method('andWhere')
            ->will($this->returnValue($queryBuilder));

        $getQuery = $this->getMockBuilder(AbstractQuery::class)
            ->setMethods(array('getSingleScalarResult'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $queryBuilder->expects($this->at(8))
            ->method('getQuery')
            ->will($this->returnValue($getQuery));

        $getQuery->expects($this->once())
            ->method('getSingleScalarResult')
            ->will($this->returnValue(1));

        $serviceRole = $this->getMockForAbstractClass('\AppBundle\Entity\ServiceRole');
        $userCompanyServiceRoles = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyServiceRoles');
        $userCompanyServiceRoles->setServiceRole($serviceRole);
        $entity = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyService');
        $entity->addUserCompanyServiceRole($userCompanyServiceRoles);
        $createNotification = $this->createMock('Mgilet\NotificationBundle\Entity\Notification');
        $notificationMock = $this->createMock('\Mgilet\NotificationBundle\Manager\NotificationManager');
        $notificationMock->expects($this->any())
            ->method('createNotification')
            ->willReturn($createNotification);

        $service = new NotificationService($manager,$notificationMock);
        $userId = 1;
        $res = $service->nbNotifiableNotifications($userId);
        $this->assertEquals($res,1);
    }



    public function testgetNotifications()
    {
        $result = [];

        $manager  = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $repository = $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $manager->expects($this->once())
            ->method('getRepository')
            ->with('MgiletNotificationBundle:NotifiableNotification')
            ->will($this->returnValue($repository));

        $queryBuilder = $this->getMockBuilder(QueryBuilder::class)
            ->disableOriginalConstructor()
            ->getMock();

        $repository->expects($this->once())
            ->method('createQueryBuilder')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(0))
            ->method('leftJoin')
            ->will($this->returnValue($queryBuilder));
        $queryBuilder->expects($this->at(1))
            ->method('leftJoin')

            ->will($this->returnValue($queryBuilder));
        $queryBuilder->expects($this->at(2))
            ->method('AndWhere')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(3))
            ->method('setParameter')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(4))
            ->method('AndWhere')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(5))
            ->method('setParameter')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(6))
            ->method('AndWhere')
            ->will($this->returnValue($queryBuilder));

        $getQuery = $this->getMockBuilder(AbstractQuery::class)
            ->setMethods(array('getResult'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $queryBuilder->expects($this->at(7))
            ->method('getQuery')
            ->will($this->returnValue($getQuery));

        $getQuery->expects($this->once())
            ->method('getResult')
            ->will($this->returnValue($result));

        $serviceRole = $this->getMockForAbstractClass('\AppBundle\Entity\ServiceRole');
        $userCompanyServiceRoles = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyServiceRoles');
        $userCompanyServiceRoles->setServiceRole($serviceRole);
        $entity = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyService');
        $entity->addUserCompanyServiceRole($userCompanyServiceRoles);
        $createNotification = $this->createMock('Mgilet\NotificationBundle\Entity\Notification');
        $notificationMock = $this->createMock('\Mgilet\NotificationBundle\Manager\NotificationManager');
        $notificationMock->expects($this->any())
            ->method('createNotification')
            ->willReturn($createNotification);

        $service = new NotificationService($manager,$notificationMock);
        $userId = 1;
        $service->getNotifications($userId);
    }

    public function testSetSeenByList()
    {
        $result = [];

        $manager  = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $repository = $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $manager->expects($this->once())
            ->method('getRepository')
            ->with('MgiletNotificationBundle:NotifiableNotification')
            ->will($this->returnValue($repository));

        $queryBuilder = $this->getMockBuilder(QueryBuilder::class)
            ->disableOriginalConstructor()
            ->getMock();

        $repository->expects($this->once())
            ->method('createQueryBuilder')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(0))
            ->method('update')
            ->will($this->returnValue($queryBuilder));
        $queryBuilder->expects($this->at(1))
            ->method('set')

            ->will($this->returnValue($queryBuilder));
        $queryBuilder->expects($this->at(2))
            ->method('where')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(3))
            ->method('setParameter')
            ->will($this->returnValue($queryBuilder));

        $getQuery = $this->getMockBuilder(AbstractQuery::class)
            ->setMethods(array('execute'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $queryBuilder->expects($this->at(4))
            ->method('getQuery')
            ->will($this->returnValue($getQuery));

        $getQuery->expects($this->once())
            ->method('execute')
            ->will($this->returnValue($result));

        $serviceRole = $this->getMockForAbstractClass('\AppBundle\Entity\ServiceRole');
        $userCompanyServiceRoles = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyServiceRoles');
        $userCompanyServiceRoles->setServiceRole($serviceRole);
        $entity = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyService');
        $entity->addUserCompanyServiceRole($userCompanyServiceRoles);
        $createNotification = $this->createMock('Mgilet\NotificationBundle\Entity\Notification');
        $notificationMock = $this->createMock('\Mgilet\NotificationBundle\Manager\NotificationManager');
        $notificationMock->expects($this->any())
            ->method('createNotification')
            ->willReturn($createNotification);

        $service = new NotificationService($manager,$notificationMock);
        $list = "test";
        $service->setSeenByList($list);
    }

     public function testSetSeenById()
    {
        $result = [];

        $manager  = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $repository = $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $manager->expects($this->once())
            ->method('getRepository')
            ->with('MgiletNotificationBundle:NotifiableNotification')
            ->will($this->returnValue($repository));

        $queryBuilder = $this->getMockBuilder(QueryBuilder::class)
            ->disableOriginalConstructor()
            ->getMock();

        $repository->expects($this->once())
            ->method('createQueryBuilder')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(0))
            ->method('update')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(1))
            ->method('set')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(2))
            ->method('andWhere')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->at(3))
            ->method('setParameter')
            ->will($this->returnValue($queryBuilder));

        $getQuery = $this->getMockBuilder(AbstractQuery::class)
            ->setMethods(array('execute'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $queryBuilder->expects($this->at(4))
            ->method('getQuery')
            ->will($this->returnValue($getQuery));

        $getQuery->expects($this->once())
            ->method('execute')
            ->will($this->returnValue($result));

        $serviceRole = $this->getMockForAbstractClass('\AppBundle\Entity\ServiceRole');
        $userCompanyServiceRoles = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyServiceRoles');
        $userCompanyServiceRoles->setServiceRole($serviceRole);
        $entity = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyService');
        $entity->addUserCompanyServiceRole($userCompanyServiceRoles);
        $createNotification = $this->createMock('Mgilet\NotificationBundle\Entity\Notification');
        $notificationMock = $this->createMock('\Mgilet\NotificationBundle\Manager\NotificationManager');
        $notificationMock->expects($this->any())
            ->method('createNotification')
            ->willReturn($createNotification);

        $service = new NotificationService($manager,$notificationMock);
        $id = 1;
        $service->setSeenById($id);
    }

}