<?php

namespace Tests\AppBundle\GdprUserOptin;

use PHPUnit\Framework\TestCase;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\User;
use PhpunitBundle\Repository\CustomerRepository;
use AppBundle\Service\GdprUserOptin;

class GdprUserOptinTest extends TestCase
{

    public function testGetGdprUserOptin()
    {
        $objectManager = $this->createMock(EntityManager::class);
        $sdprUserOptin = new GdprUserOptin($objectManager);
        $user = new User();
        $user->setGdprUserOptin(true);
        $result = $sdprUserOptin->getGdprUserOptin($user);
        $this->assertNotNull($result);
    }

    public function testGetGdprUserOptinNull()
    {
        $objectManager = $this->createMock(EntityManager::class);
        $sdprUserOptin = new GdprUserOptin($objectManager);
        $user = new User();
        $user->setGdprUserOptin(false);
        $result = $sdprUserOptin->getGdprUserOptin($user);
        $this->assertNotNull($result);
    }

    public function testSetGdprUserOptinWithUserNull()
    {
        $objectManager = $this->createMock(EntityManager::class);
        $sdprUserOptin = new GdprUserOptin($objectManager);
        $result = $sdprUserOptin->setGdprUserOptin(null,"127.0.0.1");
        $this->assertNull($result);
    }

    public function testSetGdprUserOptin()
    {
        $objectManager = $this->createMock(EntityManager::class);
        $sdprUserOptin = new GdprUserOptin($objectManager);
        $user = new User();

        $ip = "127.0.0.1";

        $result = $sdprUserOptin->setGdprUserOptin($user,"127.0.0.1");
        $this->assertNotNull($result);
    }
}