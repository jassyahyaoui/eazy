<?php

namespace Tests\AppBundle\Service;


use AppBundle\Entity\Company;
use AppBundle\Entity\ServiceRole;
use AppBundle\Entity\UserCompany;
use AppBundle\Entity\UserCompanyServiceRoles;
use AppBundle\Service\UserCompanyService;
use PHPUnit\Framework\TestCase;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Component\HttpFoundation\Request;

class UserCompanyServiceTest extends TestCase
{
    public function testFetchChosenCompany()
    {
        $entityManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMock();

        $userCompanyService = new UserCompanyService($entityManager);
        $data = array();
        $response = $userCompanyService->fetchChosenCompany($data);

        $this->assertInternalType('array', $data);
        $this->assertInternalType('array', $response);
        $this->assertNotEmpty($response);
        $this->assertInternalType('array', $response['chosenCompany']);
        $this->assertInternalType('array', $response['userCompanyService']);
    }

    public function testFetchUserCompanyServiceParamNull()
    {
        $entityManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMock();

        $userCompanyService = new UserCompanyService($entityManager);
        $userCompanyServiceData = array();
        $chosenCompany = array('company_id' => null);
        $response = $userCompanyService->fetchUserCompanyService($userCompanyServiceData, $chosenCompany);
        $this->assertEmpty($response);
    }

    public function testFetchUserCompanyServiceParamNotNull()
    {
        $entityManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMock();

        $userCompanyService = new UserCompanyService($entityManager);
        $entityServiceRole = new ServiceRole();
        $entityServiceRole->setShortDesc('short desc');
        $data = array(0 => $entityServiceRole);
        $serviceRole = $this
            ->getMockBuilder('\AppBundle\Entity\ServiceRole')
            ->setMethods(['getValues'])
            ->disableOriginalConstructor()
            ->getMock();
        $serviceRole
            ->expects($this->any())
            ->method('getValues')
            ->willReturn($data);
        $company = $this
            ->getMockBuilder('\AppBundle\Entity\Company')
            ->disableOriginalConstructor()
            ->getMock();
        $company
            ->expects($this->any())
            ->method('getId')
            ->willReturn(1);
        $service = $this
            ->getMockBuilder('\AppBundle\Entity\Service')
            ->disableOriginalConstructor()
            ->getMock();
        $service
            ->expects($this->any())
            ->method('getId')
            ->willReturn(1);
        $service
            ->expects($this->any())
            ->method('getServiceRole')
            ->willReturn($serviceRole);

        $userCompany = new UserCompany();
        $userCompany->setCompany($company);


        $entity = new \AppBundle\Entity\UserCompanyService();
        $entity->setUserCompany($userCompany);
        $entity->setService($service);

        $userCompanyServiceData = array(
            0 => array(
                $entity,
            ),

        );
        $chosenCompany = array('company_id' => 1);
        $response = $userCompanyService->fetchUserCompanyService($userCompanyServiceData, $chosenCompany);

        $this->assertNotEmpty($response);

    }

    public function testFetchUserCompanyServiceReturnServiceRole()
    {
        $entityManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMock();

        $userCompanyService = new UserCompanyService($entityManager);
        $entityServiceRole = new ServiceRole();
        $entityServiceRole->setShortDesc('short desc');
        $data = array(0 => $entityServiceRole);
        $serviceRole = $this
            ->getMockBuilder('\AppBundle\Entity\ServiceRole')
            ->setMethods(['getValues'])
            ->disableOriginalConstructor()
            ->getMock();
        $serviceRole
            ->expects($this->any())
            ->method('getValues')
            ->willReturn($data);
        $company = $this
            ->getMockBuilder('\AppBundle\Entity\Company')
            ->disableOriginalConstructor()
            ->getMock();
        $company
            ->expects($this->any())
            ->method('getId')
            ->willReturn(1);
        $service = $this
            ->getMockBuilder('\AppBundle\Entity\Service')
            ->disableOriginalConstructor()
            ->getMock();
        $service
            ->expects($this->any())
            ->method('getId')
            ->willReturn(1);
        $service
            ->expects($this->any())
            ->method('getServiceRole')
            ->willReturn($serviceRole);

        $userCompany = new UserCompany();
        $userCompany->setCompany($company);


        $entity = new \AppBundle\Entity\UserCompanyService();
        $entity->setUserCompany($userCompany);
        $entity->setService($service);

        $userCompanyServiceData = array(
            0 => array(
                $entity,
            ),

        );
        $chosenCompany = array('company_id' => 1);
        $response = $userCompanyService->fetchUserCompanyService($userCompanyServiceData, $chosenCompany);

        $this->assertNotEmpty($response[0]['serviceRole'][0]['label']);
        $this->assertNotNull($response[0]['serviceRole'][0]['label']);

    }

    public function testFetchUserCompanyServiceReturnSelectedRoleSingle()
    {
        $entityManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMock();

        $userCompanyService = new UserCompanyService($entityManager);
        $entityServiceRole = new ServiceRole();
        $entityServiceRole->setShortDesc('short desc');
        $data = array(0 => $entityServiceRole);
        $serviceRole = $this
            ->getMockBuilder('\AppBundle\Entity\ServiceRole')
            ->setMethods(['getValues'])
            ->disableOriginalConstructor()
            ->getMock();
        $serviceRole
            ->expects($this->any())
            ->method('getValues')
            ->willReturn($data);
        $serviceRole->setShortDesc('short');
        $company = $this
            ->getMockBuilder('\AppBundle\Entity\Company')
            ->disableOriginalConstructor()
            ->getMock();
        $company
            ->expects($this->any())
            ->method('getId')
            ->willReturn(1);
        $service = $this
            ->getMockBuilder('\AppBundle\Entity\Service')
            ->disableOriginalConstructor()
            ->getMock();
        $service
            ->expects($this->any())
            ->method('getId')
            ->willReturn(1);
        $service
            ->expects($this->any())
            ->method('getRoleType')
            ->willReturn('single');
        $service
            ->expects($this->any())
            ->method('getServiceRole')
            ->willReturn($serviceRole);


        $userCompany = new UserCompany();
        $userCompany->setCompany($company);

        $entity = new \AppBundle\Entity\UserCompanyService();

        $userCompanyServiceRoles = new UserCompanyServiceRoles();
        $userCompanyServiceRoles->setServiceRole($serviceRole);
        $userCompanyServiceRoles->setUserCompanyService($entity);

        $entity->setUserCompany($userCompany);
        $entity->setService($service);
        $entity->addUserCompanyServiceRole($userCompanyServiceRoles);

        $userCompanyServiceData = array(
            0 => array(
                $entity,
            ),
        );

        $chosenCompany = array('company_id' => 1);
        $response = $userCompanyService->fetchUserCompanyService($userCompanyServiceData, $chosenCompany);
        $this->assertNotNull($response[0]['selectedRole']['label']);
    }

    public function testFetchUserCompanyServiceReturnSelectedRoleNotSingle()
    {
        $entityManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMock();

        $userCompanyService = new UserCompanyService($entityManager);
        $entityServiceRole = new ServiceRole();
        $entityServiceRole->setShortDesc('short desc');
        $data = array(0 => $entityServiceRole);
        $serviceRole = $this
            ->getMockBuilder('\AppBundle\Entity\ServiceRole')
            ->setMethods(['getValues'])
            ->disableOriginalConstructor()
            ->getMock();
        $serviceRole
            ->expects($this->any())
            ->method('getValues')
            ->willReturn($data);
        $serviceRole->setShortDesc('short');
        $company = $this
            ->getMockBuilder('\AppBundle\Entity\Company')
            ->disableOriginalConstructor()
            ->getMock();
        $company
            ->expects($this->any())
            ->method('getId')
            ->willReturn(1);
        $service = $this
            ->getMockBuilder('\AppBundle\Entity\Service')
            ->disableOriginalConstructor()
            ->getMock();
        $service
            ->expects($this->any())
            ->method('getId')
            ->willReturn(1);
        $service
            ->expects($this->any())
            ->method('getRoleType')
            ->willReturn('NOT single');
        $service
            ->expects($this->any())
            ->method('getServiceRole')
            ->willReturn($serviceRole);


        $userCompany = new UserCompany();
        $userCompany->setCompany($company);

        $entity = new \AppBundle\Entity\UserCompanyService();

        $userCompanyServiceRoles = new UserCompanyServiceRoles();
        $userCompanyServiceRoles->setServiceRole($serviceRole);
        $userCompanyServiceRoles->setUserCompanyService($entity);

        $entity->setUserCompany($userCompany);
        $entity->setService($service);
        $entity->addUserCompanyServiceRole($userCompanyServiceRoles);

        $userCompanyServiceData = array(
            0 => array(
                $entity,
            ),
        );

        $chosenCompany = array('company_id' => 1);
        $response = $userCompanyService->fetchUserCompanyService($userCompanyServiceData, $chosenCompany);
        $this->assertNotNull($response[0]['selectedRole'][0]['label']);
    }

    public function testEditUserCompanyServiceUserCompanyServiceNotNull()
    {
        $userCompanyService = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('setUserCompanyService', 'addAdditionalService', 'editUserCompanyService'))
            ->disableOriginalConstructor()
            ->getMock();

        $entityIds = [];
        $userCompanyService
            ->expects($this->any())
            ->method('editUserCompanyService')
            ->willReturn($entityIds);

        $request = [];
        $data = [];
        $dataUserCompanyService = [0 => ['id' => 1]];

        $response = $userCompanyService->editUserCompanyService($request, $data, $dataUserCompanyService);
        $this->assertNotEmpty($dataUserCompanyService);
        $this->assertInternalType('array', $response);
    }

    public function testEditUserCompanyServiceUserCompanyServiceNull()
    {
        $userCompanyService = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('addUserCompanyService', 'addAdditionalService', 'editUserCompanyService'))
            ->disableOriginalConstructor()
            ->getMock();

        $entityIds = [];
        $userCompanyService
            ->expects($this->any())
            ->method('editUserCompanyService')
            ->willReturn($entityIds);

        $request = [];
        $data = [];
        $dataUserCompanyService = [];

        $response = $userCompanyService->editUserCompanyService($request, $data, $dataUserCompanyService);
        $this->assertEmpty($dataUserCompanyService);
        $this->assertInternalType('array', $response);
    }

    public function testSetUserCompanyService()
    {
        $userRepository = $this->createMock(ObjectRepository::class);
        $entityUserCompanyService = new \AppBundle\Entity\UserCompanyService();
        $param = [0 => $entityUserCompanyService];
        $userRepository->expects($this->any())
            ->method('findBy')
            ->willReturn($param);
        $userRepository->expects($this->any())
            ->method('find')
            ->willReturn($entityUserCompanyService);

        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($userRepository);

        $userCompanyService = new UserCompanyService($objectManager);

        $data = ['value' => ['user_company_id' => 1]];
        $dataUserCompanyService = [
            0 => [
                'enabled' => 1,
                'defaultStatus' => 1,
                'externalId' => 'external',
                'defaultStatus' => 1,
            ],
        ];
        $counter = 0;
        $userCompanyService->setUserCompanyService($data, $dataUserCompanyService, $counter);
        $this->assertNotEmpty($data);
        $this->assertNotNull($data['value']['user_company_id']);
        $this->assertNotNull($dataUserCompanyService[0]['enabled']);
        $this->assertInternalType('array', $dataUserCompanyService);
        $this->assertInternalType('array', $data);
        $this->assertInternalType('array', $param);
        $this->assertInternalType('object', $param[0]);
    }

    public function testSetUserCompanyServiceExternalId()
    {
        $userRepository = $this->createMock(ObjectRepository::class);
        $entityUserCompanyService = new \AppBundle\Entity\UserCompanyService();
        $entityUserCompanyService->setEnabled(1);
        $param = [0 => $entityUserCompanyService];
        $userRepository->expects($this->any())
            ->method('findBy')
            ->willReturn($param);
        $userRepository->expects($this->any())
            ->method('find')
            ->willReturn($entityUserCompanyService);

        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($userRepository);

        $userCompanyService = new UserCompanyService($objectManager);

        $data = ['value' => ['user_company_id' => 1]];
        $dataUserCompanyService = [
            0 => [
                'enabled' => 1,
                'defaultStatus' => 1,
                'defaultStatus' => 1,
            ],
        ];
        $counter = 0;
        $userCompanyService->setUserCompanyService($data, $dataUserCompanyService, $counter);
        $this->assertArrayNotHasKey('externalId', $dataUserCompanyService);
    }

    public function testAddUserCompanyService()
    {
        $userRepository = $this->createMock(ObjectRepository::class);
        $entityUserCompany = new UserCompany();
        $entityService = new \AppBundle\Entity\Service();

        $userRepository->expects($this->any())
            ->method('find')
            ->willReturn($entityUserCompany, $entityService);

        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($userRepository);

        $userCompanyService = new UserCompanyService($objectManager);
        $dataUserCompanyService = [
            0 => [
                'serviceId' => 1,
                'enabled' => 1,
                'defaultStatus' => 1,
                'externalId' => 'external',
            ],
        ];
        $data = [
            'value' => [
                'user_company_id' => 1,
                'started_at' => '2018-01-01',
                'end_at' => '2018-01-01',
            ],
        ];
        $response = $userCompanyService->addUserCompanyService($dataUserCompanyService, $data);
        $this->assertInternalType('array', $data);
        $this->assertNotEmpty($data['value']['user_company_id']);
        $this->assertNotEmpty($dataUserCompanyService[0]['serviceId']);
        $this->assertInternalType('array', $response);
    }

    public function testAddAdditionalService()
    {
        $userRepository = $this->createMock(ObjectRepository::class);
        $entityUserCompany = new UserCompany();
        $entityService = new \AppBundle\Entity\Service();

        $userRepository->expects($this->any())
            ->method('find')
            ->willReturn($entityUserCompany, $entityService);

        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($userRepository);

        $userCompanyService = new UserCompanyService($objectManager);
        $request = new Request();
        $values = [
            'value' => [
                'user_company_id' => 1,
                'id' => 1,
                'enabled' => 1,
                'defaultStatus' => 1,
            ],
        ];
        $data = [
            'value' => [
                'started_at' => '2018-01-01',
                'end_at' => '2018-01-01',
            ],
        ];

        $request = Request::create(
            '/',
            'POST',
            array('restServices' => $values)
        );
        $userCompanyService->addAdditionalService($request, $data);
        $this->assertInternalType('object', $request);
        $this->assertInternalType('array', $data);
        $this->assertNotEmpty($request->request->get('restServices'));
        $this->assertNotEmpty($data['value']);
    }

    public function testDeleteRoles()
    {
        $entityManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('createQuery', 'execute'))
            ->disableOriginalConstructor()
            ->getMock();

        $entityManager->expects($this->any())
            ->method('execute');

        $userCompanyService = new UserCompanyService($entityManager);
        $optionId = [];
        $userCompanyService->deleteRoles($optionId);
    }

    public function testFormatOptionsMultiple()
    {
        $optionId = [0 => 1];
        $selectedOptions = [0 => ['selectedRole' => [0 => ['id' => 1]]]];
        $entityManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMock();

        $userCompanyService = new UserCompanyService($entityManager);

        $response = $userCompanyService->formatOptions($selectedOptions, $optionId);
        $this->assertNotEmpty($response);
        $this->assertInternalType('array', $response);
        $this->assertEquals(1, $response[0]['id']);
        $this->assertEquals(1, $response[0]['user_company_service_id']);
    }

    public function testFormatOptionsSingle()
    {
        $optionId = [0 => 1];
        $selectedOptions = [0 => ['selectedRole' => ['label' => 1, 'id' => 1]]];
        $entityManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMock();

        $userCompanyService = new UserCompanyService($entityManager);

        $response = $userCompanyService->formatOptions($selectedOptions, $optionId);
        $this->assertNotEmpty($response);
        $this->assertInternalType('array', $response);
        $this->assertEquals(1, $response[0]['id']);
        $this->assertEquals(1, $response[0]['user_company_service_id']);
    }

    public function testFormatOptionsNull()
    {
        $optionId = [0 => 1];
        $selectedOptions = [0 => []];

        $entityManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMock();

        $userCompanyService = new UserCompanyService($entityManager);

        $response = $userCompanyService->formatOptions($selectedOptions, $optionId);

        $this->assertNotEmpty($response);
        $this->assertInternalType('array', $response);
        $this->assertEquals(null, $response[0]['id']);
        $this->assertEquals(1, $response[0]['user_company_service_id']);
    }

    public function testSaveRolesDefinedId()
    {
        $data = [0 => ['user_company_service_id' => 1, 'id' => 1]];
        $userRepository = $this->createMock(ObjectRepository::class);
        $entityUserCompanyService = new \AppBundle\Entity\UserCompanyService();
        $entityService = new \AppBundle\Entity\ServiceRole();

        $userRepository->expects($this->any())
            ->method('find')
            ->willReturn($entityUserCompanyService, $entityService);

        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($userRepository);

        $userCompanyService = new UserCompanyService($objectManager);

        $userCompanyService->saveRoles($data);
        $this->assertInternalType('array', $data);
        $this->assertNotEmpty($data[0]['user_company_service_id']);
        $this->assertNotEmpty($data[0]['id']);
    }

    public function testSaveRolesUndefinedId()
    {
        $data = [0 => ['user_company_service_id' => 1]];
        $userRepository = $this->createMock(ObjectRepository::class);
        $entityUserCompanyService = new \AppBundle\Entity\UserCompanyService();
        $entityService = new \AppBundle\Entity\ServiceRole();

        $userRepository->expects($this->any())
            ->method('find')
            ->willReturn($entityUserCompanyService, $entityService);

        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($userRepository);

        $userCompanyService = new UserCompanyService($objectManager);

        $userCompanyService->saveRoles($data);

        $this->assertInternalType('array', $data);
        $this->assertNotEmpty($data[0]['user_company_service_id']);
        $this->assertArrayNotHasKey('id', $data[0]);

    }

    public function testSaveUserCompanyService()
    {
        $userCompanyServiceData = [
            0 => [
                'serviceId' => 1,
                'enabled' => 1,
                'defaultStatus' => 1,
                'externalId' => 'external',
            ],
        ];
        $userCompanyId = 1;
        $userRepository = $this->createMock(ObjectRepository::class);
        $entityUserCompany = new \AppBundle\Entity\UserCompany();
        $entityService = new \AppBundle\Entity\Service();

        $userRepository->expects($this->any())
            ->method('find')
            ->willReturn($entityUserCompany, $entityService);

        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($userRepository);

        $userCompanyService = new UserCompanyService($objectManager);
        $userCompanyService->saveUserCompanyService($userCompanyServiceData, $userCompanyId);
        $this->assertInternalType('array', $userCompanyServiceData);
        $this->assertInternalType('int', $userCompanyId);
    }

    public function testSaveUserCompanyServiceUndefinedExternal()
    {
        $userCompanyServiceData = [
            0 => [
                'serviceId' => 1,
                'enabled' => 1,
                'defaultStatus' => 1,
            ],
        ];
        $userCompanyId = 1;
        $userRepository = $this->createMock(ObjectRepository::class);
        $entityUserCompany = new \AppBundle\Entity\UserCompany();
        $entityService = new \AppBundle\Entity\Service();

        $userRepository->expects($this->any())
            ->method('find')
            ->willReturn($entityUserCompany, $entityService);

        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($userRepository);

        $userCompanyService = new UserCompanyService($objectManager);
        $userCompanyService->saveUserCompanyService($userCompanyServiceData, $userCompanyId);
        $this->assertInternalType('array', $userCompanyServiceData);
        $this->assertInternalType('int', $userCompanyId);
        $this->assertArrayNotHasKey('externalId', $userCompanyServiceData[0]);
    }

    public function testFetchId()
    {
        $userRepository = $this->createMock(ObjectRepository::class);
        $entity = new \AppBundle\Entity\UserCompany();
        $entityUserCompany = [0 => $entity];
        $userRepository->expects($this->any())
            ->method('findBy')
            ->willReturn($entityUserCompany);

        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($userRepository);

        $userCompanyService = new UserCompanyService($objectManager);

        $userCompanyId = 1;
        $response = $userCompanyService->fetchId($userCompanyId);
        $this->assertInternalType('int', $userCompanyId);
        $this->assertInternalType('array', $response);
        $this->assertNotEmpty($response);
    }

    public function testFetchServiceRolesSingleRole()
    {
        $userCompanyServiceRoles = [
            0 => [
                'selectedRole' => [
                    'label' => 'ROLE_OWNER',
                    'id' => 1,
                ],
            ],
        ];
        $userCompanyServiceId = [0 => 1];
        $entityManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMock();

        $userCompanyService = new UserCompanyService($entityManager);
        $response = $userCompanyService->fetchServiceRoles($userCompanyServiceRoles, $userCompanyServiceId);
        $this->assertNotEmpty($response);
        $this->assertInternalType('array', $response);
        $this->assertEquals(1, $response[0]['serviceRoleId'][0]);
        $this->assertEquals(1, $response[0]['userCompanyServiceId']);
    }

    public function testFetchServiceRolesSingleRoleEmpty()
    {
        $userCompanyServiceRoles = [
            0 => [
                'selectedRole' => [],
            ],
        ];
        $userCompanyServiceId = [0 => 1];
        $entityManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMock();

        $userCompanyService = new UserCompanyService($entityManager);
        $response = $userCompanyService->fetchServiceRoles($userCompanyServiceRoles, $userCompanyServiceId);
        $this->assertNotEmpty($response);
        $this->assertInternalType('array', $response);
        $this->assertEquals(1, $response[0]['userCompanyServiceId']);
    }

    public function testFetchServiceRolesUndefinedRole()
    {
        $userCompanyServiceRoles = [
            0 => [
            ],
        ];
        $userCompanyServiceId = [0 => 1];
        $entityManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMock();

        $userCompanyService = new UserCompanyService($entityManager);
        $response = $userCompanyService->fetchServiceRoles($userCompanyServiceRoles, $userCompanyServiceId);
        $this->assertNotEmpty($response);
        $this->assertInternalType('array', $response);
        $this->assertEquals(null, $response[0]['serviceRoleId'][0]);
        $this->assertEquals(1, $response[0]['userCompanyServiceId']);
    }

    public function testSaveServiceRoleUndefinedServiceRoleId()
    {
        $userRepository = $this->createMock(ObjectRepository::class);
        $entityServiceRole = new ServiceRole();
        $entityUserCompanyService = new \AppBundle\Entity\UserCompanyService();
        $userRepository->expects($this->any())
            ->method('find')
            ->willReturn($entityUserCompanyService, $entityServiceRole);

        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($userRepository);

        $userCompanyService = new UserCompanyService($objectManager);

        $data = [0 => ['serviceRoleId' => 1, 'userCompanyServiceId' => 1]];
        $response = $userCompanyService->saveServiceRole($data);
        $this->assertNull($response);
    }

    public function testSaveServiceRoleDefinedServiceRoleId()
    {
        $userRepository = $this->createMock(ObjectRepository::class);
        $entityServiceRole = new ServiceRole();
        $entityUserCompanyService = new \AppBundle\Entity\UserCompanyService();
        $userRepository->expects($this->any())
            ->method('find')
            ->willReturn($entityServiceRole, $entityUserCompanyService);

        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($userRepository);

        $userCompanyService = new UserCompanyService($objectManager);

        $data = [0 => ['serviceRoleId' => [0 => 1], 'userCompanyServiceId' => 1]];
        $response = $userCompanyService->saveServiceRole($data);

        $this->assertNull($response);
    }

    public function testFetchByUserCompany()
    {
        $userRepository = $this->createMock(ObjectRepository::class);
        $entityServiceRole = new ServiceRole();
        $entityUserCompanyService = new \AppBundle\Entity\UserCompanyService();
        $userRepository->expects($this->any())
            ->method('find')
            ->willReturn($entityServiceRole, $entityUserCompanyService);

        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($userRepository);

        $userCompanyService = new UserCompanyService($objectManager);

        $userCompanyServiceMock = $this->getMockBuilder('\AppBundle\Entity\UserCompanyService')
            ->setMethods(array('getValues'))
            ->disableOriginalConstructor()
            ->getMock();
        $userCompanyServiceMock->expects($this->any())
            ->method('getValues')
            ->willReturn([1,2]);
        $userCompanyMock = $this->getMockBuilder('\AppBundle\Entity\UserCompany')
            ->disableOriginalConstructor()
            ->getMock();
        $userCompanyMock->expects($this->any())
            ->method('getUserCompanyService')
            ->willReturn($userCompanyServiceMock);

//        $userCompanyServiceMock->expects($this->any())
//            ->method('getValues');
//        $userCompany->expects($this->any())
//            ->method('getUserCompanyService')
//            ->willReturn($userCompanyServiceMock);
//        $userCompanyService->fetchByUserCompany($userCompanyMock);
    }
}
