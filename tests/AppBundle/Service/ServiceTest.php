<?php

namespace Tests\AppBundle\Service;

use PHPUnit\Framework\TestCase;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectRepository;
use AppBundle\Entity\UserCompanyService;
use AppBundle\Entity\UserCompanyServiceRoles;
use AppBundle\Entity\ServiceRole;
use PhpunitBundle\Repository\CustomerRepository;
use AppBundle\Service\Service as ServiceService;

class ServiceTest extends TestCase
{

    private $customerRepositoryMock;

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    public function entityManagerMock()
    {
        $entityManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository', 'persist', 'flush'))
            ->disableOriginalConstructor()
            ->getMock();

        return $entityManager;
    }


    public function testGetAllRolesByServicesNotNull()
    {
        $userRepository = $this->createMock(ObjectRepository::class);
//        $userMock = new User();
//        $userMock->setUsername('test');
        $userRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn(null);
        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->with('AppBundle:User')
            ->willReturn($userRepository);



        // Data preparation Start
        $service1 = $this->getMockForAbstractClass('\AppBundle\Entity\Service');
        $service1->setName("test");
        $service1->getId(1);

        $serviceRole = $this->getMockForAbstractClass('\AppBundle\Entity\ServiceRole');
        $serviceRole->setService($service1);

        $userCompanyServiceRoles = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyServiceRoles');
        $userCompanyServiceRoles->setServiceRole($serviceRole);

        $entity = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyService');
        $entity->addUserCompanyServiceRole($userCompanyServiceRoles);
        // Data preparation End


       // $serCompanyService
        $service = new ServiceService($objectManager);
        $result = $service->getAllRolesByService($entity);
        $this->assertNotNull($result);
    }

    public function testGetAllRolesByServicesNull()
    {
        $userRepository = $this->createMock(ObjectRepository::class);
        $userRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn(null);
        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->with('AppBundle:User')
            ->willReturn($userRepository);

        // Data preparation Start
        $service1 = $this->getMockForAbstractClass('\AppBundle\Entity\Service');
        $service1->setName("test");
        $service1->getId(1);

        $serviceRole = $this->getMockForAbstractClass('\AppBundle\Entity\ServiceRole');
        $serviceRole->setService($service1);

        $userCompanyServiceRoles = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyServiceRoles');
//        $userCompanyServiceRoles->setServiceRole($serviceRole);

        $entity = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyService');
        $entity->addUserCompanyServiceRole($userCompanyServiceRoles);
        // Data preparation End

        $service = new ServiceService($objectManager);
        $result = $service->getAllRolesByService($entity);
        $this->assertNotNull($result);
    }


    public function testFetchServicesNotNull()
    {

        $serviceMock1 = $this->createMock('\AppBundle\Entity\Service');
        $serviceMock1->expects($this->any())
            ->method('getId')
            ->willReturn(1);


        $serviceRepository = $this->createMock(ObjectRepository::class);
        $serviceRepository->expects($this->any())
            ->method('findBy')
            ->willReturn([$serviceMock1]);

        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->with('AppBundle:Service')
            ->willReturn($serviceRepository);




        $serviceMock2 = $this->createMock('\AppBundle\Entity\Service');
        $serviceMock2->expects($this->any())
            ->method('getId')
            ->willReturn(2);


        // Data preparation Start
        $service1 = $this->getMockForAbstractClass('\AppBundle\Entity\Service');
        $service2 = $this->getMockForAbstractClass('\AppBundle\Entity\Service');
        $servicesEnabled = array($service1, $service2,$serviceMock1);


        $service1->setName("service1");
        $service2->setName("service2");


        $service1->id = 1;
        $service2->id = 1;

        $service3 = $this->getMockForAbstractClass('\AppBundle\Entity\Service');
        $service3->setName("service3");

        $serviceRole = $this->getMockForAbstractClass('\AppBundle\Entity\ServiceRole');
        $serviceRole->setService($serviceMock1);

        $userCompanyServiceRoles = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyServiceRoles');
        $userCompanyServiceRoles->setServiceRole($serviceRole);

        $userCompanyService = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyService');
        $userCompanyService->addUserCompanyServiceRole($userCompanyServiceRoles);
        $userCompanyService->setService($service3);

        $userCompany = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompany');
        $userCompany->addUserCompanyService($userCompanyService);
        // Data preparation End

        // $serCompanyService
        $service = new ServiceService($objectManager);
        $result = $service->fetchServices($userCompany,$servicesEnabled);
        $this->assertNotNull($result);
    }


    public function testFetchServiceRoleNotEmpty()
    {
        $objectManager = $this->createMock(EntityManager::class);

        // Data preparation Start
        $serviceRole['id'] = 1 ;
        $serviceRole ['service']['id'] = 1;
        $serviceRole ['service']['name'] = "tes" ;
        $serviceRoles = array($serviceRole);
        // Data preparation End

        $service = new ServiceService($objectManager);
        $result = $service->fetchServiceRole($serviceRoles);
        $this->assertNotNull($result);
    }

    public function testFetchOneServiceRoleNotEmpty()
    {
        $objectManager = $this->createMock(EntityManager::class);
        // Data preparation Start
        $data = array();

        $service1 = $this->getMockForAbstractClass('\AppBundle\Entity\Service');
        $service1->setName("test");
        $service1->id = 1;

        $sr = $this->getMockForAbstractClass('\AppBundle\Entity\ServiceRole');
        $sr->setService($service1);
        $sr->setShortDesc("short desc test");

        $serviceRole = [$sr];
        // Data preparation End
        $service = new ServiceService($objectManager);
        $result = $service->fetchOneServiceRole($serviceRole);
        $this->assertNotNull($result);
    }




    public function testFetchServiceRoleOptionsNotNull()
    {
        $serviceMock1 = $this->createMock('\AppBundle\Entity\Service');
        $serviceMock1->expects($this->any())
            ->method('getId')
            ->willReturn(1);

        $serviceRepository = $this->createMock(ObjectRepository::class);
        $serviceRepository->expects($this->any())
            ->method('findBy')
            ->willReturn([$serviceMock1]);

        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->with('AppBundle:Service')
            ->willReturn($serviceRepository);

        $serviceMock2 = $this->createMock('\AppBundle\Entity\Service');
        $serviceMock2->expects($this->any())
            ->method('getId')
            ->willReturn(2);

        $service1 = $this->getMockForAbstractClass('\AppBundle\Entity\Service');
        $service2 = $this->getMockForAbstractClass('\AppBundle\Entity\Service');
        $servicesEnabled = array($service1, $service2,$serviceMock1);

        $service1->setName("service1");
        $service2->setName("service2");

        $service1->id = 1;
        $service2->id = 1;

        $service3 = $this->getMockForAbstractClass('\AppBundle\Entity\Service');
        $service3->setName("service3");

        $serviceRole = $this->getMockForAbstractClass('\AppBundle\Entity\ServiceRole');
        $serviceRole->setService($serviceMock1);

        $userCompanyServiceRoles = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyServiceRoles');
        $userCompanyServiceRoles->setServiceRole($serviceRole);

        $userCompanyService = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyService');
        $userCompanyService->addUserCompanyServiceRole($userCompanyServiceRoles);
        $userCompanyService->setService($service3);

        $userCompany = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompany');
        $userCompany->addUserCompanyService($userCompanyService);

        $service = $this->getMockForAbstractClass('\AppBundle\Entity\Service');
        $service->id = 1;
        $service->setName("Nom service");

        $serviceRole = $this->getMockForAbstractClass('\AppBundle\Entity\ServiceRole');
        $serviceRole->setService($service);

        $userCompanyServiceRoles = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyServiceRoles');
        $userCompanyServiceRoles->setServiceRole($serviceRole);

        $userCompanyServiceRoles2 = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyServiceRoles');

        $userCompanyService = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyService');
        $userCompanyService->addUserCompanyServiceRole($userCompanyServiceRoles);
        $userCompanyService->addUserCompanyServiceRole($userCompanyServiceRoles2);

        $userCompanyService->setService($service);

        $serviceRoles = [$userCompanyService];
        $this->customerRepositoryMock = $this->getMockBuilder(CustomerRepository::class)
            ->setMethods(array('findBy'))
            ->disableOriginalConstructor()
            ->getMock();
        $this->customerRepositoryMock
            ->method('findBy')
            ->with(array('userCompany' => 1))
            ->willReturn($serviceRoles);

        $entity = $this->customerRepositoryMock->findBy(
            array('userCompany' => 1)
        );
        // Data preparation End

        $service = new ServiceService($objectManager);
        $result = $service->fetchServiceRoleOptions($entity);
        $this->assertNotNull($result);
    }



    public function testEnableOneServiceNotNull()
    {
        $userRepository = $this->createMock(ObjectRepository::class);
        $service1 = $this->getMockForAbstractClass('\AppBundle\Entity\Service');
        $service1->setName("test");
        $service1->getId(1);
        $userRepository->expects($this->any())
            ->method('find')
            ->willReturn($service1);
        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->with('AppBundle:Service')
            ->willReturn($userRepository);
        $serviceRole = $this->getMockForAbstractClass('\AppBundle\Entity\ServiceRole');
        $serviceRole->setService($service1);
        $userCompanyServiceRoles = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyServiceRoles');
        $userCompanyServiceRoles->setServiceRole($serviceRole);
        $entity = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyService');
        $entity->addUserCompanyServiceRole($userCompanyServiceRoles);
        // Data preparation End

        $service = new ServiceService($objectManager);
        $serviceId = 1 ;
        $result = $service->enableOneService($serviceId);
        $this->assertNotNull($result);
    }

    public function testDisableOneServiceNotNull()
    {
        $userRepository = $this->createMock(ObjectRepository::class);
        $service1 = $this->getMockForAbstractClass('\AppBundle\Entity\Service');
        $service1->setName("test");
        $service1->getId(1);
        $userRepository->expects($this->any())
            ->method('find')
            ->willReturn($service1);
        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->with('AppBundle:Service')
            ->willReturn($userRepository);
        $serviceRole = $this->getMockForAbstractClass('\AppBundle\Entity\ServiceRole');
        $serviceRole->setService($service1);
        $userCompanyServiceRoles = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyServiceRoles');
        $userCompanyServiceRoles->setServiceRole($serviceRole);
        $entity = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyService');
        $entity->addUserCompanyServiceRole($userCompanyServiceRoles);
        // Data preparation End

        $service = new ServiceService($objectManager);
        $serviceId = 1 ;
        $result = $service->disableOneService($serviceId);
        $this->assertNotNull($result);
    }

    /********************/


    public function testTetAllRolesByServiceIsNotNull()
    {
        // Data preparation Start
        $service1 = $this->getMockForAbstractClass('\AppBundle\Entity\Service');
        $service1->setName("test");
        $service1->getId(1);

        $serviceRole = $this->getMockForAbstractClass('\AppBundle\Entity\ServiceRole');
        $serviceRole->setService($service1);

        $userCompanyServiceRoles = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyServiceRoles');
        $userCompanyServiceRoles->setServiceRole($serviceRole);

        $entity = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyService');
        $entity->addUserCompanyServiceRole($userCompanyServiceRoles);
        // Data preparation End

        $userCompanyServiceRoles = $entity->getUserCompanyServiceRoles()->getValues();

        $data = [];
        for ($i = 0; $i < sizeof($userCompanyServiceRoles); $i++) {
            $this->assertNotEmpty($userCompanyServiceRoles[$i]->getServiceRole());
            $data[$i]['label'] = $userCompanyServiceRoles[$i]->getServiceRole()->getService()->getName();

            $data[$i]['value'] = array(
                'serviceRoleId' => $userCompanyServiceRoles[$i]->getServiceRole()->getId(),
                'serviceId' => $userCompanyServiceRoles[$i]->getServiceRole()->getService()->getId(),
                'serviceName' => $userCompanyServiceRoles[$i]->getServiceRole()->getService()->getName(),
            );
        }

        $this->assertInternalType('array', $data);
        $this->assertNotNull($data);

    }

    public function testFetchServices()
    {
        // Data preparation Start
        $service1 = $this->getMockForAbstractClass('\AppBundle\Entity\Service');
        $service2 = $this->getMockForAbstractClass('\AppBundle\Entity\Service');
        $servicesEnabled = array($service1, $service2);

        $service1->id = 1;
        $service2->id = 1;

        $service3 = $this->getMockForAbstractClass('\AppBundle\Entity\Service');
        $service3->setName("service3");

        $serviceRole = $this->getMockForAbstractClass('\AppBundle\Entity\ServiceRole');
        $serviceRole->setService($service3);

        $userCompanyServiceRoles = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyServiceRoles');
        $userCompanyServiceRoles->setServiceRole($serviceRole);

        $userCompanyService = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyService');
        $userCompanyService->addUserCompanyServiceRole($userCompanyServiceRoles);
        $userCompanyService->setService($service1);

        $userCompany = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompany');
        $userCompany->addUserCompanyService($userCompanyService);
        // Data preparation End

        $service = array();
        $userCompanyService = array();
        $data = array();

        for ($j = 0; $j < sizeof($servicesEnabled); $j++) {
            for ($i = 0; $i < count($userCompany->getUserCompanyService()); $i++) {
                if (array_values($userCompany->getUserCompanyService())[$i]->getService(
                    )->id
                    === $servicesEnabled[$j]->id) {
                    array_push($service, $servicesEnabled[$j]->getId());
                } else {
                    if (!in_array($servicesEnabled[$j]->id, $userCompanyService)) {
                        array_push($userCompanyService, $servicesEnabled[$j]->id);
                    }
                }

            }
        }

        $this->customerRepositoryMock = $this->getMockBuilder(CustomerRepository::class)
            ->setMethods(array('findById'))
            ->disableOriginalConstructor()
            ->getMock();

        $this->customerRepositoryMock
            ->method('findById')
            ->with(1)
            ->willReturn($service);

        $key = 0;

        foreach (array_diff($userCompanyService, $service) as $value) {
            $data[$key] = array(
                'user_company_id' => $userCompany->getId(),
                'id' => $this->customerRepositoryMock->findById(1),
                'service' => $this->customerRepositoryMock->findById(1),
                'defaultStatus' => 'Enable',
                'enabled' => true,
            );
            $key++;
        }
        $this->assertInternalType('array', $data);
        $this->assertNotNull($data);
    }

    public function testFetchServiceRole()
    {
        $data = array();
        $serviceRole = [['id' => 1, "service" => ["id" => 1, "name" => "test"]]];
        for ($i = 0; $i < sizeof($serviceRole); $i++) {
            $data[$i] = array(
                'serviceRoleId' => $serviceRole[$i]['id'],
                'serviceId' => $serviceRole[$i]['service']['id'],
                'serviceName' => $serviceRole[$i]['service']['name'],
            );
        }
        $this->assertInternalType('array', $data);
        $this->assertNotEmpty($data);
    }


    public function testFetchOneServiceRole()
    {
        // Data preparation Start
        $data = array();

        $service1 = $this->getMockForAbstractClass('\AppBundle\Entity\Service');
        $service1->setName("test");
        $service1->id = 1;

        $sr = $this->getMockForAbstractClass('\AppBundle\Entity\ServiceRole');
        $sr->setService($service1);
        $sr->setShortDesc("short desc test");

        $serviceRole = [$sr];
        // Data preparation End

        // Test if the function parameters are a table and not null
        $this->assertInternalType('array', $serviceRole);
        $this->assertNotEmpty($serviceRole);

        for ($i = 0; $i < sizeof($serviceRole); $i++) {
            // Test the array contain a object of service role
            $this->assertNotEmpty($serviceRole[$i]);
            $data[$i] = array(
                'serviceRoleId' => $serviceRole[$i]->getId(),
                'serviceId' => $serviceRole[$i]->getService()->id,
                'serviceName' => $serviceRole[$i]->getShortDesc(),
            );
        }
        $this->assertInternalType('array', $data);
        $this->assertNotEmpty($data);
    }


    public function testFetchServiceRoleOptions()
    {
        // Data preparation Start
        $service = $this->getMockForAbstractClass('\AppBundle\Entity\Service');
        $service->id = 1;
        $service->setName("Nom service");

        $serviceRole = $this->getMockForAbstractClass('\AppBundle\Entity\ServiceRole');
        $serviceRole->setService($service);

        $userCompanyServiceRoles = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyServiceRoles');
        $userCompanyServiceRoles->setServiceRole($serviceRole);

        $userCompanyService = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompanyService');
        $userCompanyService->addUserCompanyServiceRole($userCompanyServiceRoles);
        $userCompanyService->setService($service);

        $serviceRoles = [$userCompanyService];
        $this->customerRepositoryMock = $this->getMockBuilder(CustomerRepository::class)
            ->setMethods(array('findBy'))
            ->disableOriginalConstructor()
            ->getMock();
        $this->customerRepositoryMock
            ->method('findBy')
            ->with(array('userCompany' => 1))
            ->willReturn($serviceRoles);

        $entity = $this->customerRepositoryMock->findBy(
            array('userCompany' => 1)
        );
        // Data preparation End

        $data = array();
        $this->assertInternalType('array', $data);
        $this->assertEquals(sizeof($entity), 1);
        for ($i = 0; $i < sizeof($entity); $i++) {
            $this->assertNotNull($entity[$i]->getService());
            $data[$i] = array(
                'ServiceId' => $entity[$i]->getService()->id,
                'ServiceName' => $entity[$i]->getService()->getName(),
            );
            $this->assertEquals(count($entity[$i]->getUserCompanyServiceRoles()), 1);
            for ($j = 0; $j < count($entity[$i]->getUserCompanyServiceRoles()); $j++) {
                $this->assertNotNull($entity[$i]->getUserCompanyServiceRoles());
                $data[$i]['selectedRole'][$j] = [
                    'label' => $entity[$i]->getUserCompanyServiceRoles()->getValues()[$j]->getServiceRole()
                        ->getService()->getName(),
                    'value' => [
                        'serviceId' => $entity[$i]->getUserCompanyServiceRoles()->getValues()[$j]
                            ->getServiceRole()->getService()->getId(),
                        'serviceRoleId' => $entity[$i]->getUserCompanyServiceRoles()->getValues()[$j]
                            ->getServiceRole()->getId(),
                        'serviceName' => $entity[$i]->getUserCompanyServiceRoles()->getValues()[$j]
                            ->getServiceRole()->getService()->getName(),
                    ],
                ];

            }

        }


        $this->assertNotEmpty($data);
    }



    public function testEnableOneService()
    {
        // Data preparation Start
        $serviceId = 1;
        $service = $this->getMockForAbstractClass('\AppBundle\Entity\Service');
        $service->id = 1;
        $service->setName("Nom service");
        $service->setEventManager("eventManagerTest");

        $this->customerRepositoryMock = $this->getMockBuilder(CustomerRepository::class)
            ->setMethods(array('find'))
            ->disableOriginalConstructor()
            ->getMock();
        $this->customerRepositoryMock
            ->method('find')
            ->with(1)
            ->willReturn($service);
        // Data preparation End

        $service = $this->customerRepositoryMock->find($serviceId);
        $eventManager = $service->getEventManager();
        $this->assertEquals($eventManager, "eventManagerTest");
        $method = "Enable".$eventManager."UserFromCompany";
        $this->assertEquals($method, "EnableeventManagerTestUserFromCompany");
        $this->assertNotEmpty($method);
    }

        public function testDisableOneService()
    {
        // Data preparation Start
        $serviceId = 1;
        $service = $this->getMockForAbstractClass('\AppBundle\Entity\Service');
        $service->id = 1;
        $service->setName("Nom service");
        $service->setEventManager("eventManagerTest");

        $this->customerRepositoryMock = $this->getMockBuilder(CustomerRepository::class)
            ->setMethods(array('find'))
            ->disableOriginalConstructor()
            ->getMock();
        $this->customerRepositoryMock
            ->method('find')
            ->with(1)
            ->willReturn($service);
        // Data preparation End

        $service = $this->customerRepositoryMock->find($serviceId);
        $eventManager = $service->getEventManager();
        $this->assertEquals($eventManager, "eventManagerTest");
        $method = "Disable".$eventManager."UserFromCompany";
        $this->assertEquals($method, "DisableeventManagerTestUserFromCompany");
        $this->assertNotEmpty($method);
    }



}