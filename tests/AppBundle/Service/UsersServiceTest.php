<?php

namespace Tests\AppBundle\UsersService;

use PHPUnit\Framework\TestCase;
use PhpunitBundle\Repository\CustomerRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectRepository;
use AppBundle\Entity\User;
use AppBundle\Entity\Company;
use AppBundle\Entity\UserCompany;
use AppBundle\Service\UsersService;
use tests\bootstrap;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\Container;

/**
 * Class ContactTest
 * @package Tests\AppBundle\Service
 */
class UsersServiceTest extends TestCase
{

    public function testGetCompanyUsersNotNull()
    {
        $repositoryMock = $this
            ->getMockBuilder(ObjectRepository::class)
            ->setMethods(array('getCompanyUsers'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $companyMock = new Company();
        $companyMock->setName('test');

        $userMock = new User();
        $userMock->setUsername('test');
        $userMock->setCurrentCompany($companyMock);

        $firstUserCompany = new UserCompany();
        $companyUsers = array($firstUserCompany);

        $repositoryMock->expects($this->any())
            ->method('findOneBy')
            ->willReturn($userMock);

        $repositoryMock->expects($this->any())
            ->method('getCompanyUsers')
            ->willReturn($companyUsers);

        $objectManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->withAnyParameters()
            ->willReturn($repositoryMock);

        $container = new Container();
        $usersService = new UsersService($objectManager, $container);
        $res = $usersService->getCompanyUsers("test");
        $this->assertNotEmpty($res);
    }

    public function testGetCompanyUsersWithNullUser()
    {
        $userRepository = $this
            ->getMockBuilder(ObjectRepository::class)
            ->setMethods(array('getCompanyUsers'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $userRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn(null);

        $objectManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->withAnyParameters()
            ->willReturn($userRepository);

        $container = new Container();
        $usersService = new UsersService($objectManager, $container);
        $res = $usersService->getCompanyUsers("test");
        $this->assertEmpty($res);
    }

    public function testGetCompanyUsersWithNullCompany()
    {
        $userRepository = $this
            ->getMockBuilder(ObjectRepository::class)
            ->setMethods(array('getCompanyUsers'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $userMock = new User();
        $userMock->setUsername('test');

        $userRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn($userMock);

        $objectManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->withAnyParameters()
            ->willReturn($userRepository);

        $container = new Container();
        $usersService = new UsersService($objectManager, $container);
        $res = $usersService->getCompanyUsers("test");
        $this->assertEmpty($res);
    }
}