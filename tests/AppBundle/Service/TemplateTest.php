<?php

namespace Tests\AppBundle\Service;


use AppBundle\Entity\Service;
use AppBundle\Entity\ServiceRole;
use AppBundle\Entity\TemplateService;
use AppBundle\Service\Template;
use PHPUnit\Framework\TestCase;

class TemplateTest extends TestCase
{
    public function testFetchTemplateServices()
    {
        $template = new Template();
        $entity = new \AppBundle\Entity\Template();
        $entityTemplateService = new TemplateService();
        $entityService = $this->getMockBuilder('\AppBundle\Entity\Service')
            ->setMethods(array('isEmpty', 'getValues', 'count'))
            ->disableOriginalConstructor()
            ->getMock();
        $entityTemplateService->setService($entityService);
        $entity->addTemplateService($entityTemplateService);

        $response = $template->fetchTemplateServices($entity);
        $this->assertNotNull($entity);
        $this->assertNotEmpty($entity);
        $this->assertNotEmpty($entity);
        $this->assertNotNull($response);
        $this->assertInternalType('array', $response);
    }

}