<?php

namespace Tests\AppBundle\AbstractService;

use PHPUnit\Framework\TestCase;
use Doctrine\ORM\EntityManager;
use AppBundle\Service\AbstractService;

class AbstractServiceTest extends TestCase
{

    public function testUniqPwd()
    {
        $manager = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $abstractService = new AbstractService($manager);
        $uniqPwd = $abstractService->uniqPwd();
        $this->assertNotEmpty($uniqPwd);
    }
}