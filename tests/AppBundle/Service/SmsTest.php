<?php
/**
 * Created by PhpStorm.
 * User: MAZ_USER3
 * Date: 12/07/2018
 * Time: 09:22
 */

namespace Tests\AppBundle\Service;


use AppBundle\Entity\User;
use AppBundle\Service\Sms;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class SmsTest extends TestCase
{
    public function testBeginAuthenticationIsEmailAuthEnabled()
    {
        $container = $this
            ->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface')
            ->disableOriginalConstructor()
            ->getMock();
        $templating = $this
            ->getMockBuilder('Symfony\Bundle\FrameworkBundle\Templating\EngineInterface')
            ->disableOriginalConstructor()
            ->getMock();
        $context = $this
            ->getMockBuilder('Scheb\TwoFactorBundle\Security\TwoFactor\AuthenticationContextInterface')
            ->disableOriginalConstructor()
            ->getMock();
        $userEntity = new User();
        $context
            ->expects($this->any())
            ->method('getUser')
            ->willReturn($userEntity);
        $userEntity->setTwoFactorAuthentication(1);
        $sms = new Sms($container, $templating);


        $response = $sms->beginAuthentication($context);
        $this->assertNotEmpty($response);
        $this->assertTrue($response);
        $this->assertInternalType('bool', $response);
    }

    public function testBeginAuthenticationIsNotEmailAuthEnabled()
    {
        $container = $this
            ->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface')
            ->disableOriginalConstructor()
            ->getMock();
        $templating = $this
            ->getMockBuilder('Symfony\Bundle\FrameworkBundle\Templating\EngineInterface')
            ->disableOriginalConstructor()
            ->getMock();
        $context = $this
            ->getMockBuilder('Scheb\TwoFactorBundle\Security\TwoFactor\AuthenticationContextInterface')
            ->disableOriginalConstructor()
            ->getMock();
        $userEntity = new User();
        $context
            ->expects($this->any())
            ->method('getUser')
            ->willReturn($userEntity);
        $userEntity->setTwoFactorAuthentication(0);
        $sms = new Sms($container, $templating);


        $response = $sms->beginAuthentication($context);
//        $this->assertNotEmpty($response);
        $this->assertInternalType('bool', $response);
        $this->assertFalse($response);
    }

    public function testRequestAuthenticationCode()
    {
        $container = $this
            ->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface')
            ->disableOriginalConstructor()
            ->getMock();
        $templating = $this
            ->getMockBuilder('Symfony\Bundle\FrameworkBundle\Templating\EngineInterface')
            ->disableOriginalConstructor()
            ->getMock();
        $context = $this
            ->getMockBuilder('Scheb\TwoFactorBundle\Security\TwoFactor\AuthenticationContextInterface')

            ->disableOriginalConstructor()
            ->getMock();
        $userEntity = new User();
        $request = new Request();

        $request = Request::create(
            '/hello-world',
            'POST',
            array('_auth_code' => null)
        );

        $context
            ->expects($this->any())
            ->method('getUser')
            ->willReturn($userEntity);
        $context
            ->expects($this->any())
            ->method('getRequest')
            ->willReturn($request);

        $sms = new Sms($container, $templating);
        $sms->requestAuthenticationCode($context);

    }

    public function testSendSmsCode()
    {
        $container = $this
            ->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface')
            ->disableOriginalConstructor()
            ->getMock();
        $templating = $this
            ->getMockBuilder('Symfony\Bundle\FrameworkBundle\Templating\EngineInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $sms = new Sms($container, $templating);
        $response = $sms->sendSmsCode('50452880', '216', 'sms');
        $this->assertNotEmpty($response);
    }

    public function testVerificationSmsCode()
    {
        $container = $this
            ->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface')
            ->disableOriginalConstructor()
            ->getMock();
        $templating = $this
            ->getMockBuilder('Symfony\Bundle\FrameworkBundle\Templating\EngineInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $sms = new Sms($container, $templating);
        $response = $sms->verificationSmsCode('50452880', '216', '0000');
        $this->assertFalse($response);
        $this->assertInternalType('bool', $response);

    }
}