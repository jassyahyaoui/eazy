<?php

namespace Tests\AppBundle\Service;

use AppBundle\Service\UserAgent;
use PHPUnit\Framework\TestCase;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\User;
use AppBundle\Entity\Service;

class UserAgentTest extends TestCase
{

    public function testBlackListDevices()
    {
        $_SERVER['HTTP_USER_AGENT'] = "test";
        $serviceId = 1;
        $user = new User();

        $service = new Service();
        $service->setUserAgentDeny(['test']);

        $entityManagerMock = $this->getMockBuilder(EntityManager::class)
            ->setMethods(array('find'))
            ->disableOriginalConstructor()
            ->getMock();

        $entityManagerMock
            ->method('find')
            ->willReturn($service);

        $tokenStorageMock = $this->getMockBuilder(
            'Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface'
        )
            ->setMethods(['getUser', 'getToken'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $tokenStorageMock->expects($this->at(1))
            ->method('getUser')
            ->will($this->returnValue($user));

        $tokenStorageMock->expects($this->at(0))
            ->method('getToken')
            ->will($this->returnValue($tokenStorageMock));

        $userAgent = new UserAgent($entityManagerMock, $tokenStorageMock);
       $result =  $userAgent->checkDevice($serviceId);
       //$this->assertTrue($result);
       $this->assertNotEmpty($result);
    }

}
