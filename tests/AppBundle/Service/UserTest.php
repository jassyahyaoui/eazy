<?php

namespace Tests\AppBundle\Service;

use AppBundle\Service\User;
use AppBundle\Service\UserCompany;
use PHPUnit\Framework\TestCase;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectRepository;
use AppBundle\Entity\User as UserEntity;

/**
 * Class UserTest
 * @package Tests\AppBundle\Service
 */
class UserTest extends TestCase
{
    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    public function entityManagerMock()
    {
        $entityManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository', 'persist', 'flush','findOneBy'))
            ->disableOriginalConstructor()
            ->getMock();

        return $entityManager;
    }

    public function testFetchUser()
    {
        $user = new User($this->entityManagerMock());
        $entity = $this->getMockForAbstractClass('\AppBundle\Entity\User');

        $data = $user->fetchUser($entity);

        $this->assertInstanceOf('\AppBundle\Entity\User', $entity);
        $this->assertInternalType('array', $data);
        $this->assertCount(10, $data);
        $this->assertNotEmpty($data);

    }

    public function testFetchUserIsNull()
    {
        $userCompanyEntity = new UserCompany($this->entityManagerMock());

        $user = $this->getMockForAbstractClass('\AppBundle\Entity\User');

        $data = $userCompanyEntity->fetchUserCompany($user);

        $this->assertInstanceOf('\AppBundle\Entity\User', $user);
        $this->assertInternalType('array', $data);
        $this->assertNull($data['userCompany']);
        $this->assertNull($data['userCompanyService']);
    }

    public function testFetchUserCompanyIsNotNull()
    {
        $userCompanyEntity = new UserCompany($this->entityManagerMock());

        $user = $this->getMockForAbstractClass('\AppBundle\Entity\User');

        $template = $this->getMockForAbstractClass('\AppBundle\Entity\Template');

        $company = $this->getMockForAbstractClass('\AppBundle\Entity\Company');
        $company->setName('TEST COMPANY');
        $company->setTemplate($template);

        $userCompany = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompany');
        $userCompany->setStartedAt(new \DateTime('now'));
        $userCompany->setEndAt(new \DateTime('now'));
        $userCompany->setRole('ROLE_USER');
        $userCompany->setNotifySms(1);
        $userCompany->setNotifyEmail(1);
        $userCompany->setCompany($company);

        $user->addUserCompany($userCompany);

        $data = $userCompanyEntity->fetchUserCompany($user);

        $this->assertInstanceOf('\AppBundle\Entity\User', $user);
        $this->assertInternalType('array', $data);
        $this->assertNotNull($data['userCompany']);
        $this->assertNotNull($data['userCompanyService']);

    }

    public function testEditUserNotEmptyParam()
    {
        $userData = array(
            'email' => 'eazy@by.mazars',
            'enabled' => true,
            'first_name' => 'eazy',
            'last_name' => 'mazars',
            'mobile' => '+00 33 55 22 55 66',
        );

        $user = new User($this->entityManagerMock());

        $entity = $this->getMockForAbstractClass('\AppBundle\Entity\User');

        $user->editUser($entity, $userData, 'MAZARS');

        $this->assertInstanceOf('\AppBundle\Entity\User', $entity);
        $this->assertInternalType('array', $userData);
        $this->assertNotEmpty($userData);
        $this->assertInternalType('object', $user);
    }

    public function testEditUserEmptyParam()
    {
        $userData = array(
            'email' => null,
            'enabled' => null,
            'first_name' => null,
            'last_name' => null,
            'mobile' => null,
        );

        $user = new User($this->entityManagerMock());

        $entity = $this->getMockForAbstractClass('\AppBundle\Entity\User');
        $data = $user->editUser($entity, $userData, 'MAZARS_PSW');

        $this->assertEquals(null, $data->getEmail());

        $this->assertInternalType('array', $userData);
        $this->assertEquals(null, $userData['email']);
    }

    public function testEditCurrentUserNotEmpty()
    {
        $userData = array(
            'email' => 'eazy@by.mazars',
            'enabled' => true,
            'firstName' => 'eazy',
            'lastName' => 'mazars',
            'mobile' => '+00 33 55 22 55 66',
            'companyNotifyEmail'=>true,
            'companyNotifySms'=>true,
            'locale'=>'fr',
            'mobileCountry'=>'+33',
            'title'=>'Manager',
            'companyId'=>1
        );
        $userCompany = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompany');

        $userRepository = $this
            ->getMockBuilder(ObjectRepository::class)
            ->setMethods(array('findOneBy'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $userRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn($userCompany);

        $objectManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository', 'persist', 'flush'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->withAnyParameters()
            ->willReturn($userRepository);

        $UserEntity = $this->getMockForAbstractClass('\AppBundle\Entity\User');
        $user = new User($objectManager);
        $user->editCurrentUser($UserEntity, $userData, 'MAZARS');
    }

    public function testEditCurrentUserNull()
    {
        $userData = array(
            'email' => 'eazy@by.mazars',
            'enabled' => true,
            'firstName' => 'eazy',
            'lastName' => 'mazars',
            'mobile' => '+00 33 55 22 55 66',
            'companyNotifyEmail'=>true,
            'companyNotifySms'=>true,
            'locale'=>'fr',
            'mobileCountry'=>'+33',
            'title'=>'Manager',
            'companyId'=>1
        );

        $mockRepository = $this
            ->getMockBuilder(ObjectRepository::class)
            ->setMethods(array('findOneBy'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $mockRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn(null);

        $mockEntityManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository', 'persist', 'flush'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $mockEntityManager->expects($this->any())
            ->method('getRepository')
            ->withAnyParameters()
            ->willReturn($mockRepository);

        $entityUser = $this->getMockBuilder('\AppBundle\Entity\User')
            ->disableOriginalConstructor()
            ->getMock();

        $user = new User($mockEntityManager);
        $user->editCurrentUser($entityUser, $userData);
    }

}