<?php

namespace Tests\AppBundle\Service;

use AppBundle\Service\UserCompany;
use PHPUnit\Framework\TestCase;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectRepository;

class UserCompanyTest extends TestCase
{
    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    public function entityManagerMock()
    {
        $entityManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository', 'persist', 'flush'))
            ->disableOriginalConstructor()
            ->getMock();

        return $entityManager;
    }

    public function testFetchSettingsIsNotNull()
    {
        $userCompany = new UserCompany($this->entityManagerMock());
        $entity = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompany');
        $entity->setRole(array('ROLE_USER'));
        $entity->setNotifySms(0);
        $entity->setNotifyEmail(1);

        $this->assertInstanceOf('\AppBundle\Entity\UserCompany', $entity);

        $data = $userCompany->fetchSettings($entity);
        $this->assertNotEmpty($data);
        $this->assertNotNull($entity->getRole());
    }

    public function testFetchSettingsIsNull()
    {
        $userCompany = new UserCompany($this->entityManagerMock());
        $entity = $this->getMockForAbstractClass('\AppBundle\Entity\UserCompany');

        $this->assertInstanceOf('\AppBundle\Entity\UserCompany', $entity);

        $data = $userCompany->fetchSettings($entity);
        $this->assertNotEmpty($data);
    }

    public function testEditSettings()
    {
        $userCompany = new UserCompany($this->entityManagerMock());

        $entity = $this->getMockBuilder('\AppBundle\Entity\UserCompany')
            ->disableOriginalConstructor()
            ->getMock();

        $data = array(
            'roles' => 'ROLE_USER',
            'email' => 1,
            'sms' => 0,
        );

        $data = $userCompany->editSettings($entity, $data);
        $this->assertNotEmpty($data);
    }

    public function editUserCompany()
    {
        $userCompany = new UserCompany($this->entityManagerMock());
        $entity = $this->getMockBuilder('\AppBundle\Entity\User')
            ->disableOriginalConstructor()
            ->getMock();
        $dataUserCompany = array();
        $userCompany->editUserCompany($entity, $dataUserCompany);
        $this->assertInternalType('object', $entity);
    }

    public function testSetUserCompany()
    {
        $userCompany = new UserCompany($this->entityManagerMock());

        $entityUser = $this->getMockBuilder('\AppBundle\Entity\User')
            ->disableOriginalConstructor()
            ->getMock();

        $entityUserCompany = $this->getMockBuilder('\AppBundle\Entity\UserCompany')
            ->disableOriginalConstructor()
            ->getMock();
        $data = array($entityUserCompany);
        $dataUserCompany = array(
            0 => array(
                'value' => array(
                    'started_at' => '08/08/2018',
                    'end_at' => '08/08/2018',
                ),
            ),
        );
        $counter = 0;
        $userCompany->setUserCompany($entityUser, $data, $dataUserCompany, $counter);

        $this->assertInternalType('object', $entityUser);
        $this->assertNotNull($entityUser);
        $this->assertInternalType('array', $data);
        $this->assertNotNull($data);
        $this->assertInternalType('int', $counter);
        $this->assertNotNull($counter);
        $this->assertInternalType('string', $dataUserCompany[$counter]['value']['started_at']);
        $this->assertNotNull($dataUserCompany[$counter]['value']['started_at']);
        $this->assertNotNull($dataUserCompany[$counter]['value']['end_at']);
    }

    public function testAddUserCompany()
    {
        $dataUserCompany = array(
            0 => array(
                'value' => array(
                    'started_at' => '08/08/2018',
                    'end_at' => '08/08/2018',
                    'enabled' => true,
                    'company_id' => 1,
                ),
            ),
        );
        $counter = 0;

        $user = $this->getMockBuilder('\AppBundle\Entity\User')
            ->disableOriginalConstructor()
            ->getMock();
        $company = $this->getMockBuilder('\AppBundle\Entity\Company')
            ->disableOriginalConstructor()
            ->getMock();

        $userRepository = $this->createMock(ObjectRepository::class);

        $userRepository->expects($this->any())
            ->method('find')
            ->willReturn($user, $company);

        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($userRepository);

        $userCompany = new UserCompany($objectManager);
        $userCompany->addUserCompany($user, $dataUserCompany, $counter);
        $this->assertNotNull($user);
        $this->assertNotNull($dataUserCompany);
        $this->assertNotNull($counter);
        $this->assertInternalType('object', $user);
        $this->assertInternalType('array', $dataUserCompany);
        $this->assertInternalType('int', $counter);
    }

    public function testEditCurrentCompany()
    {
        $user = $this->getMockBuilder('\AppBundle\Entity\User')
            ->disableOriginalConstructor()
            ->getMock();
        $company = $this->getMockBuilder('\AppBundle\Entity\Company')
            ->disableOriginalConstructor()
            ->getMock();

        $userRepository = $this->createMock(ObjectRepository::class);

        $userRepository->expects($this->any())
            ->method('find')
            ->willReturn($user, $company);

        $objectManager = $this->createMock(EntityManager::class);

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($userRepository);
        $userCompanyData = array(
            'uctab' => array(
                0 => array(
                    'companyId' => 1,
                ),
            ),
        );
        $userId = 1;
        $userCompany = new UserCompany($objectManager);
        $userCompany->editCurrentCompany($userId, $userCompanyData);
        $this->assertInternalType('int', $userId);
        $this->assertNotNull($userId);
        $this->assertInternalType('array', $userCompanyData);
        $this->assertNotNull($userCompanyData);
    }

    public function testSaveUserCompany()
    {

        $user = $this->getMockBuilder('\AppBundle\Entity\User')
            ->disableOriginalConstructor()
            ->getMock();
        $company = $this->getMockBuilder('\AppBundle\Entity\Company')
            ->disableOriginalConstructor()
            ->getMock();

        $userRepository = $this->createMock(ObjectRepository::class);

        $userRepository->expects($this->any())
            ->method('find')
            ->willReturn($user, $company);

        $objectManager = $this->createMock(EntityManager::class);

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($userRepository);

        $userId = 1;
        $data = array(
            0 => array(
                'uctab' => array(
                    0 => array(
                        'startedAt' => '08/08/2018',
                        'endAt' => '09/08/2018',
                        'enabled' => true,
                        'companyId' => 1,
                    ),
                ),
                'roles' => ['ROLE_ADMIN', 'ROLE_CONTACT'],
                'sms' => true,
                'email' => 'test@test.com',
            ),
        );

        $userCompany = new UserCompany($objectManager);
        $response = $userCompany->saveUserCompany($userId, $data);
        $this->assertInternalType('array', $data);
        $this->assertNotNull($data[0]['uctab'][0]['enabled']);
        $this->assertNotNull($data[0]['sms']);
        $this->assertNotNull($data[0]['email']);
        $this->assertInternalType('array', $response);
        $this->assertNotNull($response);
    }

    public function testChosenCompany()
    {
        $userCompanyService = $this->getMockBuilder('\AppBundle\Entity\UserCompanyService')
            ->setMethods(array('isEmpty', 'getValues'))
            ->disableOriginalConstructor()
            ->getMock();
        $userCompany = new UserCompany($this->entityManagerMock());
        $entity = array($userCompanyService);
        $data = $userCompany->chosenCompany($entity);
        $this->assertNotNull($entity);
        $this->assertInternalType('array',$entity);
        $this->assertNull($data);
    }
}
