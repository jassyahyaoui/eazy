<?php

namespace Tests\AppBundle\Service;

use PHPUnit\Framework\TestCase;
use Doctrine\ORM\EntityManager;
use PhpunitBundle\Repository\CustomerRepository;
use AppBundle\Service\BigIp;

class BigIpTest extends TestCase
{

    public function testClientIp()
    {
        $_SERVER['REMOTE_ADDR'] = "127.0.0.1";
        $_SERVER['HTTP_X_FORWARDED_FOR'] = "127.0.0.1";

        $bigIp = new BigIp();
        $result = $bigIp->clientIp();
        $this->assertNotNull($result);
    }
    public function testClientIpExtern()
    {
        $_SERVER['REMOTE_ADDR'] = "41.33.22.11";
        $_SERVER['HTTP_X_FORWARDED_FOR'] = "127.0.0.1";

        $bigIp = new BigIp();
        $result = $bigIp->clientIp();
        $this->assertNotNull($result);
    }
}