<?php

namespace Tests\AppBundle\Service;

use PHPUnit\Framework\TestCase;
use PhpunitBundle\Repository\CustomerRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectRepository;
use AppBundle\Entity\User;
use AppBundle\Entity\UserCompany;
use AppBundle\Entity\Company;
use AppBundle\Service\Contact;
use tests\bootstrap;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class ContactTest
 * @package Tests\AppBundle\Service
 */
class ContactTest extends TestCase
{
    public function entityManagerMock()
    {
        $entityManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMock();

        return $entityManager;
    }

    public function testGetCompanyContactsNotNull()
    {
        $userRepository = $this
            ->getMockBuilder(ObjectRepository::class)
            ->setMethods(array('getCompanyContacts'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $companyMock = new Company();
        $companyMock->setName('test');

        $userMock = new User();
        $userMock->setUsername('test');
        $userMock->setCurrentCompany($companyMock);
        $contacts = [$userMock];
        $companyRepository = $this
            ->getMockBuilder(ObjectRepository::class)
            ->setMethods(array('getCompanyContacts'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $companyRepository
            ->expects($this->any())
            ->method('getCompanyContacts')
            ->willReturn(null);

        $userRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn($userMock);

        $userRepository->expects($this->any())
            ->method('getCompanyContacts')
            ->willReturn($userMock);

        $objectManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->withAnyParameters()
            ->willReturn($userRepository);

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->withAnyParameters()
            ->willReturn($companyRepository);
           $contact = new Contact($objectManager);
           $res = $contact->getCompanyContacts("test");
           $this->assertNotEmpty($res);
    }

    public function testGetCompanyContactsWithNoCurrentCompany()
    {
        $userRepository = $this
            ->getMockBuilder(ObjectRepository::class)
            ->setMethods(array('getCompanyContacts'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $userMock = new User();
        $userMock->setUsername('test');
        $userRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn($userMock);
        $objectManager = $this
            ->getMockBuilder(EntityManager::class)
            ->setMethods(array('getRepository'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->withAnyParameters()
            ->willReturn($userRepository);
        $contact = new Contact($objectManager);
        $res = $contact->getCompanyContacts("test");
        $this->assertNull($res);
    }


    public function testGetCompanyContactsNull()
    {
        // $userRepository = $this->createMock(ObjectRepository::class);

        $userMock = new User();
        $userMock->setUsername('test');

        $userRepository = $this->getMockBuilder(CustomerRepository::class)
            ->setMethods(array('findOneBy', 'getCompanyContacts'))
            ->disableOriginalConstructor()
            ->getMock();


        $userRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn(null);
        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->with('AppBundle:User')
            ->willReturn($userRepository);
        $contact = new Contact($objectManager);
        $result = $contact->getCompanyContacts("test");
        $this->assertNull($result);
    }


    public function testGetCompanyContacts()
    {
        // Data preparation Start
        $company = $this->getMockForAbstractClass('\AppBundle\Entity\Company');
        $company->id = 1;
        $userParam = $this->getMockForAbstractClass('\AppBundle\Entity\User');
        $userParam->setUsername('david');
        $userParam->setCurrentCompany($company);
        $username = 'david';
        $contact1 = $this->getMockForAbstractClass('\AppBundle\Entity\User');
        $contact1->setUsername('First contact');
        $contact2 = $this->getMockForAbstractClass('\AppBundle\Entity\User');
        $contact2->setUsername('Second contact');
        $contacts = [$contact1, $contact2];
        // Data preparation End

        $customerRepositoryMock = $this->getMockBuilder(CustomerRepository::class)
            ->setMethods(array('findOneByUsername', 'getCompanyContacts'))
            ->disableOriginalConstructor()
            ->getMock();

        $customerRepositoryMock
            ->method('findOneByUsername')
            ->with("david")
            ->willReturn($userParam);

        $customerRepositoryMock->method('getCompanyContacts')
            ->with(1)
            ->willReturn($contacts);

        $user = $customerRepositoryMock->findOneByUsername($username);
        if ($user) {
            //Test if findOneByUsername will return the correct user with the username "David"
            $this->assertSame($userParam, $user);
            $currentCompany = $user->getCurrentCompany();
            // Test if the user have a current company and  the getCurrentCompany() will return a valid company object
            $this->assertInstanceOf('AppBundle\Entity\Company', $currentCompany);
            // Test if getCurrentCompany will return the correct company
            $this->assertSame($currentCompany, $company);
            if ($currentCompany) {
                $companyContacts = $customerRepositoryMock->getCompanyContacts($currentCompany->id);
                // Test if the companyContacts is an array
                $this->assertInternalType('array', $companyContacts);
                // Test if the companyContacts is not empty
                $this->assertNotEmpty($companyContacts);
            }
        }

    }
}