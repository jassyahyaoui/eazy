<?php

namespace Tests\AppBundle\GdprUserOptin;

use PHPUnit\Framework\TestCase;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Company;
use AppBundle\Entity\Service as ServiceEntity;
use AppBundle\Service\Export;

class ExportTest extends TestCase
{

    public function testExport()
    {
        $mailer = $this
            ->getMockBuilder('\Swift_Mailer')
            ->disableOriginalConstructor()
            ->getMock();
        $mailer
            ->expects($this->once())
            ->method('send');

        $company = new Company();
        $company->setName("Test");
        $companies = array($company);
        $service = new ServiceEntity();
        $service->setName("Test");
        $service->setExternalId("Test");
        $services = [$service];
        $userCompanyServices = [
            [
                'role' => 'a:2:{i:0;s:12:"ROLE_CONTACT";i:1;s:9:"ROLE_USER";}',
                'id' => 1,
                'firstName' => "test",
                'lastName' => "test",
                'email' => "test@domain.com",
            ],
        ];

        $manager = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $companyRepository = $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $companyRepository->expects($this->once())
            ->method('findAll')
            ->withAnyParameters()
            ->willReturn($companies);

        $serviceRepository = $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $serviceRepository->expects($this->any())
            ->method('findAll')
            ->withAnyParameters()
            ->willReturn($services);

        $userCompanyServiceRepository = $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['getUserFromUserCompanyService'])
            ->getMock();

        $userCompanyServiceRepository->expects($this->any())
            ->method('getUserFromUserCompanyService')
            ->withAnyParameters()
            ->willReturn($userCompanyServices);

        $manager->expects($this->at(0))
            ->method('getRepository')
            ->with('AppBundle:Service')
            ->will($this->returnValue($serviceRepository));

        $manager->expects($this->at(1))
            ->method('getRepository')
            ->with('AppBundle:Company')
            ->will($this->returnValue($companyRepository));

        $manager->expects($this->at(2))
            ->method('getRepository')
            ->with('AppBundle:UserCompanyService')
            ->will($this->returnValue($userCompanyServiceRepository));

        $export = new Export($manager, $mailer);
        $export->export("test@gmail.com");
    }
}