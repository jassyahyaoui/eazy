<?php

namespace AppBundle\Api\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\UserCompany;
use AppBundle\Entity\User;

/**
 * UserCompanyService controller.
 *
 * @Route("/api/backend/user_company")
 */
class UserCompanyController extends AbstractController
{
    /**
     * @Route("/{id}/get/settings", options={"expose"=true}, name="get_user_company_settings")
     * @Method("GET")
     * @param UserCompany $userCompany
     * @return JsonResponse
     */
    public function getUserCompanySettingsAction(UserCompany $userCompany)
    {
        return new JsonResponse($this->container->get('user_company.service')->fetchSettings($userCompany));
    }

    /**
     * Lists All User Companies
     *
     * @Route("/get/all",options={"expose"=true}, name="get_user_companies_all")
     * @Method("GET")
     * @return JsonResponse
     */
    public function getUserCompaniesAllAction()
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new JsonResponse(array('userCompanies' => null));
        }
        if (!$this->getUser()) {
            return new JsonResponse('User is not Found');
        }

        $userCompanies = $this->getDoctrine()->getRepository('AppBundle:UserCompany')->getUserCompaniesAll(
            $this->getUser()->getId()
        );

        return new JsonResponse($userCompanies);
    }

    /**
     * @Route("/{id}/get/services",options={"expose"=true}, name="get_rest_of_services")
     * @Method("GET")
     * @ParamConverter("userCompany",class="AppBundle:UserCompany")
     * @param UserCompany $userCompany
     * @return JsonResponse
     */
    public function getServicesAction(UserCompany $userCompany)
    {
        $servicesEnabled = $this->getDoctrine()->getRepository('AppBundle:Service')->findBy(array('enable' => true));
        $response = $this->container->get('service.service')->fetchServices($userCompany, $servicesEnabled);

        return new JsonResponse($response);
    }

    /**
     * @Route("/{id}/set/settings", options={"expose"=true}, name="set_user_company_settings")
     * @Method("PUT")
     * @param Request $request
     * @param UserCompany $userCompany
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setUserCompanySettingsAction(Request $request, UserCompany $userCompany)
    {
        $data = $request->request->all();
        $this->container->get('user_company.service')->editSettings($userCompany, $data);

        return new JsonResponse('Edit settings [Email-SMS-Roles] with success');
    }

    /**
     * @Route("/delete/{id}", options={"expose"=true}, name="delete_user_company")
     * @Method("DELETE")
     * @ParamConverter("userCompany",class="AppBundle:UserCompany")
     * @param UserCompany $userCompany
     * @return JsonResponse
     */
    public function deleteUserCompanyAction(UserCompany $userCompany)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($userCompany);
        $em->flush($userCompany);

        return new JsonResponse('Deleted', 200);
    }

    /**
     * @Route("/user/{id}/company", options={"expose"=true}, name="set_user_company")
     * @Method("PUT")
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setUserCompanyAction(Request $request, User $user)
    {
        $dataUserCompany = $request->request->get('userCompany');
        $this->container->get('user_company.service')->editUserCompany($user, $dataUserCompany);
        $this->container->get('notification.service')->notificateUserNewCompany($user, $dataUserCompany);

        return new JsonResponse('Update Success', 200);
    }

    /**
     * @Route("/user/company/post", options={"expose"=true}, name="post_backend_user_company")
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postUserCompanyAction(Request $request)
    {
        $userId = $request->request->get('userId');
        $userCompanyData = $request->request->get('userCompany');
        $data = $request->request->all();
        $this->container->get('user_company.service')->editCurrentCompany($userId, $userCompanyData);
        $userCompanyIds = $this->container->get('user_company.service')->saveUserCompany($userId, $data);

        $serializer = $this->get('jms_serializer');
        $response = $serializer->serialize($userCompanyIds, 'json');

        return new JsonResponse($response, 200);
    }
}