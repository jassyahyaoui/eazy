<?php
/**
 * Created by PhpStorm.
 * User: MAZ_USER3
 * Date: 12/06/2018
 * Time: 11:24
 */

namespace AppBundle\Api\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\UserCompany;

/**
 * UserCompanyService controller.
 *
 * @Route("/api/backend/user_company_service")
 */
class UserCompanyServiceController extends AbstractController
{
    /**
     * @Route("/all", name="get_company_services_all")
     * @Method("GET")
     * @return JsonResponse
     */
    public function getCompanyServicesAllAction()
    {
        $token = $this->get('security.token_storage')->getToken();
        if (!$this->getUser()) {
            return new JsonResponse('User Not Found');
        }
        if (!$this->getUser()->getCurrentCompany()) {
            return new JsonResponse('Company Not Found');
        }
        $services = $this->getDoctrine()->getRepository('AppBundle:CompanyService')->getUserCompanyServicesAll(
            $token->getUser()->getCurrentCompany()->getId()
        );

        return new JsonResponse(array('services' => $services));

    }

    /**
     * @Route("/{id}/get/service",options={"expose"=true}, name="get_user_company_service_by_UserCompany_id")
     * @Method("GET")
     * @param UserCompany $userCompany
     * @return JsonResponse
     */
    public function getUserCompanyServiceByUserCompanyIdAction(UserCompany $userCompany)
    {
        return new JsonResponse(
            $this->container->get('user_company_service.service')->fetchByUserCompany($userCompany)
        );
    }

    /**
     * @Route("/post", options={"expose"=true}, name="post_backend_user_company_service")
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postUserCompanyServiceAction(Request $request)
    {
        $userCompanyServiceId = $this->container->get('user_company_service.service')->saveUserCompanyService(
            $request->request->get('userCompanyService'),
            $request->request->get('userCompanyId')
        );

        return new JsonResponse($userCompanyServiceId, 200);
    }

    /**
     * @Route("/post/roles", options={"expose"=true}, name="post_backend_user_company_service_roles")
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postUserCompanyServiceRolesAction(Request $request)
    {
        $userCompanyId = $request->request->get('userCompanyId');
        $userCompanyServiceRoles = $request->request->get('userCompanyServicRoles');
        $container = $this->container->get('user_company_service.service');
        $userCompanyServiceId = $container->fetchId($userCompanyId);
        $data = $container->fetchServiceRoles(
            $userCompanyServiceRoles,
            $userCompanyServiceId
        );
        $container->saveServiceRole($data);

        return new JsonResponse("Success", 200);
    }

    /**
     * @Route("/user_company_service", options={"expose"=true}, name="set_user_company_service")
     * @Method("PUT")
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     */
    public function setUserCompanyServiceAction(Request $request)
    {
        $data = $request->request->get('selectInfos');
        $dataUserCompanyService = $request->request->get('userCompanyService');

        $entityIds = $this->container->get('user_company_service.service')->editUserCompanyService(
            $request,
            $data,
            $dataUserCompanyService
        );

        return new JsonResponse($entityIds, 200);
    }

    /**
     * @Route("/user_company_service/roles", options={"expose"=true}, name="set_user_company_service_roles")
     * @Method("PUT")
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setUserCompanyServiceRolesAction(Request $request)
    {
        $optionId = $request->request->get('user_company_service_id_option');
        $selectedOptions = $request->request->get('selectedRoleOptions');
        $container = $this->container->get('user_company_service.service');
        $container->deleteRoles($optionId);
        $data = $container->formatOptions($selectedOptions, $optionId);
        $this->container->get('user_company_service.service')->saveRoles($data);

        return new JsonResponse('UserCompanyService was updated', 200);
    }

}