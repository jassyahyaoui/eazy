<?php

namespace AppBundle\Api\Backend;

use AppBundle\Entity\Company;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * UserCompanyService controller.
 *
 * @Route("/api/backend")
 */
class OneUpController extends AbstractController
{
    /**
     * @Route("/me/{token}",options={"expose"=true}, name="me_action")
     * @Method({"GET", "POST"})
     * @param $token
     * @return JsonResponse
     */
    public function meAction($token)
    {
        $serverToken = $this->container->get('fos_oauth_server.access_token_manager.default');
        $accessToken = $serverToken->findTokenBy(array('token' => $token));

        if (!$accessToken) {
            return new JsonResponse(['status' => 400, 'message' => 'Missing or invalid access token'], 400);
        }

        if ($accessToken->hasExpired()) {
            return new JsonResponse(['status' => 400, 'message' => 'Access token has expired'], 400);
        }

        $accessToken = $serverToken->findTokenBy(
            array('user' => $accessToken->getUser()->getId(), 'company' => $accessToken->getCompany()->getId())
        );

        $service = $this->getDoctrine()->getRepository('AppBundle:Service')
            ->findOneBy(
                array('connectionIdentifier' => $accessToken->getClient()->getId(), 'connectionType' => 'Oauth')
            );

        return $this->getMeAction(
            $accessToken->getUser(),
            $accessToken->getCompany(),
            $service->getId()
        );

        $logger->addInfo(
            var_export($this->getMeAction($request, $user, $company, $service_id, $accessToken), true)
        );
    }

    /**
     * @Route("/me/{user_id}/{company_id}",options={"expose"=true}, name="get_me_action")
     * @Method("GET")
     * @ParamConverter("user",class="AppBundle:User", options={"id" = "user_id"})
     * @ParamConverter("company",class="AppBundle:Company", options={"id" = "company_id"})
     * @param User $user
     * @param Company $company
     * @param $service_id
     * @return JsonResponse
     */
    public function getMeAction(User $user, Company $company, $service_id)
    {
        $userCompany = $this->getDoctrine()->getRepository('AppBundle:UserCompany')->findOneBy(
            array('user' => $user->getId(), 'company' => $company->getId())
        );
        if (!$userCompany) {
            return new JsonResponse('userCompany not found');
        }
        $userCompanyService = $this->getDoctrine()->getRepository('AppBundle:UserCompanyService')->findOneBy(
            array('userCompany' => $userCompany->getId(), 'service' => $service_id)
        );
        if (!$userCompanyService) {
            return new JsonResponse('userCompany not found');
        }
        $response = $this->container->get('one_up.service')->fetchMe(
            $user,
            $company,
            $service_id,
            $userCompany,
            $userCompanyService
        );

        return new JsonResponse($response);
    }
}