<?php
/**
 * Created by PhpStorm.
 * User: MAZ_USER3
 * Date: 12/06/2018
 * Time: 11:23
 */

namespace AppBundle\Api\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * UserCompanyService controller.
 *
 * @Route("/api/backend/company")
 */
class CompanyController extends AbstractController
{
    /**
     * Lists All User Companies
     *
     * @Route("/all",options={"expose"=true}, name="get_backend_companies_all")
     * @Method("GET")
     * @return JsonResponse
     */
    public function getCompaniesAllAction()
    {
        return new JsonResponse($this->getDoctrine()->getRepository('AppBundle:Company')->getCompaniesAll());
    }

    /**
     * @Route("/services", options={"expose"=true}, name="get_company_services")
     * @Method("GET")
     * @return JsonResponse
     */
    public function getCompanyServicesAction()
    {
        if (!$this->getUser() || !$this->getUser()->getCurrentCompany()) {
            return new JsonResponse('Undefined User or currentCompany of connected User');
        }

        $services = $this->getDoctrine()->getRepository('AppBundle:CompanyService')->getCompanyServices(
            $this->getUser()->getCurrentCompany()->getId()
        );

        return new JsonResponse(array('services' => $services));
    }





}