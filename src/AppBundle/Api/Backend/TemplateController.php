<?php
/**
 * Created by PhpStorm.
 * User: MAZ_USER3
 * Date: 12/06/2018
 * Time: 12:08
 */

namespace AppBundle\Api\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Template;

/**
 * UserCompanyService controller.
 *
 * @Route("/api/backend/template")
 */
class TemplateController extends AbstractController
{
    /**
     * @Route("/service/{id}", options={"expose"=true}, name="get_backend_template_services")
     * @Method("GET")
     * @param Template $template
     * @return JsonResponse
     */
    public function getTemplateServicesAction(Template $template)
    {
        return new JsonResponse($this->container->get('template.service')->fetchTemplateServices($template));
    }
}