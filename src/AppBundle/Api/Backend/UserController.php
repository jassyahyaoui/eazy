<?php
/**
 * Created by PhpStorm.
 * User: MAZ_USER3
 * Date: 12/06/2018
 * Time: 10:24
 */

namespace AppBundle\Api\Backend;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;


/**
 * UserCompanyService controller.
 *
 * @Route("/api/backend/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/get/{id}",options={"expose"=true}, name="get_backend_user_by_id")
     * @Method("GET")
     * @param User $user
     * @return JsonResponse
     */
    public function getUserAction(User $user)
    {
        $data['user'] = $this->container->get('user.service')->fetchUser($user);
        $data['user_company'] = $this->container->get('user_company.service')->fetchUserCompany($user)['userCompany'];

        $userCompanyService = $this->container->get('user_company.service')->fetchUserCompany(
            $user
        )['userCompanyService'];
        $chosenCompany = $this->container->get('user_company.service')->chosenCompany($userCompanyService);
        $data['choosen_company'] = $this->container->get('user_company_service.service')->fetchChosenCompany(
            $chosenCompany
        )['chosenCompany'];
        $userCompanyServiceData = $this->container->get('user_company_service.service')->fetchChosenCompany(
            $chosenCompany
        )['userCompanyService'];
        $data['user_company_service'] = $this->container->get('user_company_service.service')->fetchUserCompanyService(
            $userCompanyServiceData,
            $data['choosen_company']
        );

        return new JsonResponse($data);
    }

    /**
     * @Route("/set/{id}", options={"expose"=true}, name="set_user")
     * @Method("PUT")
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     */
    public function setUserAction(Request $request, User $user)
    {
        $dataUser = $request->request->get('user');
        $user->setEmail($dataUser['email']);
        $user->setUsername($dataUser['email']);
        $user->setEnabled($dataUser['enabled']);
        $user->setFirstName($dataUser['first_name']);
        $user->setLastName($dataUser['last_name']);
        $user->setMobile($dataUser['mobile']);
        $user->setMobileCountry($dataUser['mobile_country']);
        $user->setTitle($dataUser['title']);
        $user->setTwoFactorAuthentication($dataUser['twoFactorAuthentication']);
        $this->get('fos_user.user_manager')->updateUser($user);

        return new JsonResponse('user updated', 200);
    }

    /**
     * @Route("/post", options={"expose"=true}, name="post_backend_user")
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     */
    public function postUserAction(Request $request)
    {
        $userData = $request->request->get('user');
        $userManager = $this->get('fos_user.user_manager');
        $uniqPwd = $this->container->get('absract.service')->uniqPwd();
        $sendPwd = $this->container->get('mail');

        $userEntity = $userManager->createUser();
        $user = $this->container->get('user.service')->editUser($userEntity, $userData, $uniqPwd);

        $from = $this->container->getParameter('ADDRESS_MAIL');
        $senderName = $this->container->getParameter('SENDER_NAME');
        $baseResettingUrl = $request->getScheme().'://'.$request->getHttpHost().$request->getBasePath(
            ).'/resetting/request';

        $sendPwd->SendMailPassword(
            'Création de votre compte',
            $from,
            $senderName,
            $userData['email'],
            $uniqPwd,
            $baseResettingUrl
        );

        $exceptionData = $this->getDoctrine()->getRepository('AppBundle:User')
            ->findOneBy(array('email' => (string)$userData['email']));

        if ($exceptionData==null) {
            $userManager->updateUser($user);
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            $response = $serializer->serialize($user, 'json');

            return new JsonResponse($response, 200);
        } else {

            $exceptionArray = [];
            $exceptionArray['id'] = $exceptionData->getId();
            $exceptionArray['firstName'] = $exceptionData->getFirstName();
            $exceptionArray['lastName'] = $exceptionData->getLastName();
            $exceptionArray['email'] = $exceptionData->getEmail();

            return new JsonResponse($exceptionArray, 500);
        }
    }

    /**
     * @Route("/disable/TwoFA", name="disable_user_TwoFA",options={"expose"=true})
     * @Method("PUT")
     * @param Request $request
     * @return JsonResponse
     */
    public function setTwoFA(Request $request)
    {
        $TwoFA = ($request->query->get('TwoFA') === 'true') ? true : false;
        $id = $request->query->get('id');
        $entity = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);
        $entity->setTwoFactorAuthentication((int)$TwoFA);
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush($entity);

        return new JsonResponse('TwoFA disabled', 200);
    }


    /**
     * @Route("/change-password/{id}", options={"expose"=true}, name="user_change_password")
     * @Method("PUT")
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     */
    public function changePasswordAction(Request $request, User $user)
    {
        if (!$user) {
            return new JsonResponse("Error");
        }
        $password = $request->request->get('password');
        $result = ['message' => null];
        $password = $request->request->get('password');
        $user->setPlainPassword($password);
        $this->get('fos_user.user_manager')->updateUser($user, false);
        $this->getDoctrine()->getManager()->flush();
        $result['message'] = "Password changed successfully";

        return new JsonResponse($result);
    }
}