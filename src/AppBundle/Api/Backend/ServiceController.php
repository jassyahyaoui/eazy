<?php
/**
 * Created by PhpStorm.
 * User: MAZ_USER3
 * Date: 12/06/2018
 * Time: 10:57
 */

namespace AppBundle\Api\Backend;

use AppBundle\Entity\Company;
use AppBundle\Entity\Service;
use AppBundle\Entity\User;
use AppBundle\Entity\UserCompanyService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * UserCompanyService controller.
 *
 * @Route("/api/backend/service")
 */
class ServiceController extends AbstractController
{
    /**
     * @Route("/role/all",options={"expose"=true}, name="get_service_role_all")
     * @Method("GET")
     * @return JsonResponse
     */
    public function getServiceRoleAllAction()
    {
        $serviceRole = $this->getDoctrine()->getRepository('AppBundle:ServiceRole')->getServiceRoleAll();
        $response = $this->container->get('service.service')->fetchServiceRole($serviceRole);

        return new JsonResponse($response);
    }

    /**
     * @Route("/role/{id}",options={"expose"=true}, name="get_service_role")
     * @Method("GET")
     * @param $id
     * @return JsonResponse
     */
    public function getServiceRoleAction($id)
    {
        $serviceRole = $this->getDoctrine()->getRepository('AppBundle:ServiceRole')->findBy(array('service' => $id));
        $response = $this->container->get('service.service')->fetchOneServiceRole($serviceRole);

        return new JsonResponse($response);
    }

    /**
     * @Route("/role/options/{id}",options={"expose"=true}, name="get_service_role_options")
     * @Method("GET")
     * @param $id
     * @return JsonResponse
     */
    public function getServiceRoleOptionsAction($id)
    {
        $entity = $this->getDoctrine()->getRepository('AppBundle:UserCompanyService')->findBy(
            array('userCompany' => $id)
        );
        $response = $this->container->get('service.service')->fetchServiceRoleOptions($entity);

        return new JsonResponse($response);
    }

    /**
     * @Route("/option/{id}",options={"expose"=true}, name="get_selected_option")
     * @Method("GET")
     * @ParamConverter("id",class="AppBundle:UserCompanyService")
     * @param UserCompanyService $userCompanyService
     * @return JsonResponse
     */
    public function getSelectedOptionAction(UserCompanyService $userCompanyService)
    {
        return new JsonResponse($this->container->get('service.service')->getAllRolesByService($userCompanyService));
    }

    /**
     * @Route("/disable/{service_id}/{user_id}/{company_id}",options={"expose"=true}, name="disable_service_user_from_company")
     * @Method("PUT")
     * @ParamConverter("service",class="AppBundle:Service", options={"id" = "service_id"})
     * @ParamConverter("user",class="AppBundle:User", options={"id" = "user_id"})
     * @ParamConverter("company",class="AppBundle:Company", options={"id" = "company_id"})
     * @param $serviceId
     * @param User $user
     * @param Company $company
     * @return mixed
     */
    public function DisableServiceUserFromCompany($service, User $user, Company $company)
    {
        $method = $this->container->get('service.service')->disableOneService($service);

        return $this->$method($user, $company);
    }

    /**
     * @Route("/enable/{service_id}/{user_id}/{company_id}",options={"expose"=true}, name="enable_service_user_from_company")
     * @Method("PUT")
     * @ParamConverter("service",class="AppBundle:Service", options={"id" = "service_id"})
     * @ParamConverter("user",class="AppBundle:User", options={"id" = "user_id"})
     * @ParamConverter("company",class="AppBundle:Company", options={"id" = "company_id"})
     * @param $service
     * @param User $user
     * @param Company $company
     * @return mixed
     */
    public function EnableServiceUserFromCompany($service, User $user, Company $company)
    {
        $method = $this->container->get('service.service')->enableOneService($service);

        return $this->$method($user, $company);
    }


    /**
     * @Route("/multiple/disable/{user_id}/{company_id}",options={"expose"=true}, name="disable_services_user_from_company")
     * @Method("PUT")
     * @ParamConverter("user",class="AppBundle:User", options={"id" = "user_id"})
     * @ParamConverter("company",class="AppBundle:Company", options={"id" = "company_id"})
     * @param User $user
     * @param Company $company
     * @return JsonResponse
     */
    public function DisableServicesUserFromCompany(User $user, Company $company)
    {
        $userCompany = $this->getDoctrine()->getRepository('AppBundle:UserCompany')
            ->findOneBy(array('user' => $user->getId(), 'company' => $company->getId()));

        if (empty($userCompany)) {
            return new JsonResponse('Empty UserCompany for this user and company', 400);
        }

        $userCompanyService = $userCompany->getUserCompanyService()->getValues();
        for ($i = 0; $i < sizeof($userCompanyService); $i++) {
            if ($userCompanyService[$i]->getService()->getEventEnable() == true) {
                $service = $this->getDoctrine()->getRepository('AppBundle:Service')->find(
                    $userCompanyService[$i]->getService()->getId()
                );
                $this->DisableServiceUserFromCompany($service, $user, $company);
            } else {
                continue;
            }
        }

        return new JsonResponse('DisableServicesUserFromCompany Success', 200);
    }

    /**
     * @Route("/multiple/enable/{user_id}/{company_id}",options={"expose"=true}, name="enable_services_user_from_company")
     * @Method("PUT")
     * @ParamConverter("user",class="AppBundle:User", options={"id" = "user_id"})
     * @ParamConverter("company",class="AppBundle:Company", options={"id" = "company_id"})
     * @param User $user
     * @param Company $company
     * @return JsonResponse
     */
    public function EnableServicesUserFromCompany(User $user, Company $company)
    {
        $userCompany = $this->getDoctrine()->getRepository('AppBundle:UserCompany')
            ->findOneBy(array('user' => $user->getId(), 'company' => $company->getId()));

        if (empty($userCompany)) {
            return new JsonResponse('Empty UserCompany for this user and company', 400);
        }
        $userCompanyService = $userCompany->getUserCompanyService()->getValues();
        for ($i = 0; $i < sizeof($userCompanyService); $i++) {
            if ($userCompanyService[$i]->getService()->getEventEnable() == true) {
                $this->EnableServiceUserFromCompany($userCompanyService[$i]->getService()->getId(), $user, $company);
            } else {
                continue;
            }
        }

        return new JsonResponse('EnableServicesUserFromCompany Success', 200);
    }


    /**
     * @Route("/disable/user/{user_id}/{company_id}",options={"expose"=true}, name="disable_ONEUP_user_from_company")
     * @Method("POST")
     * @param User $user
     * @param Company $company
     * @return JsonResponse
     */
    public function DisableONEUPUserFromCompany(User $user, Company $company)
    {
        $this->container->get('one_up.service')->eventOneUp($user, $company, 'DISABLE_USER');

        return new JsonResponse('ONEUP User From Company disabled', 200);
    }

    /**
     * @Route("/enable/user/{user_id}/{company_id}",options={"expose"=true}, name="enable_ONEUP_user_from_company")
     * @Method("POST")
     * @param User $user
     * @param Company $company
     * @return JsonResponse
     */
    public function EnableONEUPUserFromCompany(User $user, Company $company)
    {
        $this->container->get('one_up.service')->eventOneUp($user, $company, 'ENABLE_USER');

        return new JsonResponse('ONEUP User From Company enabled', 200);
    }

    /**
     * @Route("/delete/user/{user_id}/{company_id}",options={"expose"=true}, name="delete_ONEUP_user_from_company")
     * @Method("DELETE")
     * @param User $user
     * @param Company $company
     * @return JsonResponse
     */
    public function DeleteONEUPUserFromCompany(User $user, Company $company)
    {
        $this->container->get('one_up.service')->eventOneUp($user, $company, 'DELETE_USER');

        return new JsonResponse('ONEUP User From Company deleted', 200);
    }

    /**
     * @Route("/delete/company/{user_id}/{company_id}",options={"expose"=true}, name="delete_ONEUP_company")
     * @Method("DELETE")
     * @param User $user
     * @param Company $company
     * @return JsonResponse
     */
    public function DeleteONEUPCompany(User $user, Company $company)
    {
        $this->container->get('one_up.service')->eventOneUp($user, $company, 'DELETE_ACCOUNT');

        return new JsonResponse('ONEUP User From Company deleted', 200);
    }

}