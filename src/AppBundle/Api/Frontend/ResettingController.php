<?php

namespace AppBundle\Api\Frontend;

use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\ResettingController as BaseController;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseNullableUserEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Mailer\MailerInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\User\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ResettingController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Lists All User Companies
     *
     * @Route("/resetting-password", name="resetting_password",options={"expose"=true})
     * @Method("POST")
     */
    public function sendEmailAction(Request $request)
{
    $result = ['message' => null];

    $username = $request->request->get('username');

    /** @var $user UserInterface */
    $user = $this->get('fos_user.user_manager')->findUserByUsernameOrEmail($username);

    /** @var $dispatcher EventDispatcherInterface */
    $dispatcher = $this->get('event_dispatcher');

    /* Dispatch init event */
    $event = new GetResponseNullableUserEvent($user, $request);
    $dispatcher->dispatch(FOSUserEvents::RESETTING_SEND_EMAIL_INITIALIZE, $event);

    if (null !== $event->getResponse()) {
        return $event->getResponse();
    }

    if (null === $user) {
        return new JsonResponse(
            'User not recognised',
            JsonResponse::HTTP_FORBIDDEN
        );
    }

    $event = new GetResponseUserEvent($user, $request);
    $dispatcher->dispatch(FOSUserEvents::RESETTING_RESET_REQUEST, $event);

    if (null !== $event->getResponse()) {
        return $event->getResponse();
    }

    if ($user->isPasswordRequestNonExpired($this->container->getParameter('fos_user.resetting.token_ttl'))) {
        return new JsonResponse(
            $this->get('translator')->trans('resetting.password_already_requested', [], 'FOSUserBundle'),
            JsonResponse::HTTP_FORBIDDEN
        );
    }

    if (null === $user->getConfirmationToken()) {
        /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
        $tokenGenerator = $this->get('fos_user.util.token_generator');
        $user->setConfirmationToken($tokenGenerator->generateToken());
    }

    /* Dispatch confirm event */
    $event = new GetResponseUserEvent($user, $request);
    $dispatcher->dispatch(FOSUserEvents::RESETTING_SEND_EMAIL_CONFIRM, $event);

    if (null !== $event->getResponse()) {
        return $event->getResponse();
    }

    $this->get('fos_user.mailer')->sendResettingEmailMessage($user);
    $user->setPasswordRequestedAt(new \DateTime());
    $this->get('fos_user.user_manager')->updateUser($user);

    /* Dispatch completed event */
    $event = new GetResponseUserEvent($user, $request);
    $dispatcher->dispatch(FOSUserEvents::RESETTING_SEND_EMAIL_COMPLETED, $event);

    if (null !== $event->getResponse()) {
        return $event->getResponse();
    }

    $result['message'] = $this->get('translator')->trans(
        'resetting.check_email',
        [ '%tokenLifetime%' => floor($this->container->getParameter('fos_user.resetting.token_ttl') / 3600) ],
        'FOSUserBundle'
    );
        return new JsonResponse($result,JsonResponse::HTTP_OK);
    }
}
