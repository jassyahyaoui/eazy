<?php

namespace AppBundle\Api\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Api\Frontend\AbstractController;

/**
 * UserCompanyService controller.
 *
 * @Route("/api/frontend")
 */
class NotificationController extends AbstractController
{
    /**
     * @Route("/notification", options={"expose"=true}, name="get_notification_user")
     * @Method("GET")
     */
    public function getNotificationAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $firstResult = $request->query->get('p', 1);
        $maxResults = 3;
        $notificationService = $this->get("notification.service");
        $notifiableNotifications = $notificationService
            ->notifiableNotifications($this->getUser()->getId(), $maxResults * $firstResult);
        $nbNotifiableNotifications = $notificationService
            ->nbNotifiableNotifications($this->getUser());

        return new JsonResponse([
            'notifications' => $notifiableNotifications,
            'numberNotifications' => $nbNotifiableNotifications
        ]);
    }

    /**
     * @Route("/notificationn/allseen", options={"expose"=true}, name="set_notification_user_all_seen")
     * @Method("PUT")
     */
    public function setNotificationAllSeenAction()
    {
        $user = $this->getUser();
        if (!$user) {
            return new JsonResponse('error');
        }

        $notificationService = $this->get("notification.service");
        $notifications = $notificationService->getNotifications($user->getId());

        if (count($notifications) <= 0) {
            return new JsonResponse('error');
        }

        $list = "";
        foreach ($notifications as $value) {
            $list .= $value["id"].",";
        }
        $list = rtrim($list, ",");
        $notificationService->setSeenByList($list);

        return new JsonResponse([
            'notifications' => 'OK'
        ]);
    }

    /**
     * @Route("/notificationn/seen", options={"expose"=true}, name="set_notification_user_seen")
     * @Method("PUT")
     */
    public function setNotificationSeenAction(Request $request)
    {
        $user = $this->getUser();
        if (!$user) {
            return new JsonResponse(['notifications' => null]);
        }

        $notificationService = $this->get("notification.service");
        $notification = $notificationService->setSeenById($request->request->get('id'));

        return new JsonResponse(['notifications' => $notification]);
    }
}
