<?php

namespace AppBundle\Api\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Api\Frontend\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Thumbnail;

/**
 * UserCompanyService controller.
 *
 * @Route("/api/frontend")
 */
class UserController extends AbstractController
{
    /**
     * Lists All User Companies
     *
     * @Route("/user/updateimage", name="set_user_image",options={"expose"=true})
     * @Method("POST")
     */
    public function setUserImage(Request $request)
    {
        $submittedToken = $request->request->get('_csrf_token');
        if (!$this->isCsrfTokenValid('file-upload', $submittedToken)) {
            return ERROR;
        }
        $result = ['message' => null];

        $Thumbnail = new Thumbnail();
        $form = $this->createForm('AppBundle\Form\ThumbnailType', $Thumbnail);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {

            $validator = $this->get('validator');
            $errors = $validator->validate($Thumbnail);
            if ($errors->count() > 0) {
                return new JsonResponse("Image n'est pas valide", 400);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($Thumbnail);
            $user = $this->getUser();
            $user->setThumbnail($Thumbnail);
            $em->persist($user);
            $em->flush();
            $result['message'] = "New Thumbnail was created successfully";
        }

        return new JsonResponse($result);
    }

    /**
     * @Route("/user", name="get_user",options={"expose"=true})
     * @Method("GET")
     */
    public function getUserAction()
    {
        $user = $this->getDoctrine()->getManager()->getRepository('AppBundle:User')->getUser($this->getUser()->getId());
        $u = [];
        $u['firstName'] = $user["firstName"];
        $u['lastName'] = $user["lastName"];
        $u['locale'] = $user["locale"];
        $u['mobile'] = $user["mobile"];
        $u['mobileCountry'] = $user["mobileCountry"];
        $u['thumbnail'] = $user["thumbnail"]['link'];
        $u['currentCompany']['id'] = $user["currentCompany"]['id'];
        $u['twoFactorAuthentication'] = $user["twoFactorAuthentication"];
        $u['title'] = $user["title"];
        $u['email'] = $user["email"];
        $userCompany = [];
        $u['userCompany'] = [];
        foreach ($user["userCompany"] as $key => $userCompanyArray) {
            $userCompany["id"] = $userCompanyArray["id"];
            $userCompany["notifySms"] = $userCompanyArray["notifySms"];
            $userCompany["notifyEmail"] = $userCompanyArray["notifyEmail"];
            $userCompany["company"]["name"] = $userCompanyArray["company"]["name"];
            $userCompany["company"]["id"] = $userCompanyArray["company"]["id"];
            array_push($u['userCompany'], $userCompany);
        }

        return new JsonResponse(['user' => $u]);
    }

    /**
     * @Route("/update-user", name="post_current_user",options={"expose"=true})
     * @Method("PUT")
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postUserAction(Request $request)
    {
        $result = ['message' => "Error"];
        if (!$this->getUser()) {
            return new JsonResponse($result);
        }

        // Get the form data from the the request
        $data = $request->request->all();
        // call the service to save data for the current user
        $this->container->get('user.service')->editCurrentUser($this->getUser(), $data);
        $result['message'] = "Your informations has changed";
        $result['user'] = $this->getUser()->getId();

        return new JsonResponse($result);
    }

    /**
     * @Route("/user/password/reset", options={"expose"=true}, name="user_reset_password")
     * @Method("PUT")
     */
    public function resetPasswordAction(Request $request)
    {
        $result = ['message' => null];
        $email = $request->request->get('email');
        $result['email'] = $email;

        if (!$this->getUser()) {
            return new JsonResponse(array('email' => null, 'password' => null), 404);
        }

        $user = $this->getUser();

        if ($email != $user->getEmail()) {
            return new JsonResponse("You can only change your own email address", 404);
        }

        $uniqPassword = strtoupper('$_mazars_'.substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 16).'_$');
        $result['password'] = $uniqPassword;
        $user->setPlainPassword($uniqPassword);
        $send_mail_password_service = $this->container->get('mail');
        $send_mail_password_service->SendMailPassword('Votre nouveau mot de passe', $email, $uniqPassword);
        $this->get('fos_user.user_manager')->updateUser($user, false);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse($result);
    }

    /**
     * @Route("/user/phone/validation", name="user_phone_validation",options={"expose"=true})
     * @Method("PUT")
     * @param Request $request
     * @return JsonResponse
     */
    public function phoneValidationAction(Request $request)
    {


        return new JsonResponse('SMS sended', 200);
    }

    /**
     * @Route("/user/set/TwoFA", name="user_set_TwoFA",options={"expose"=true})
     * @Method("PUT")
     * @param Request $request
     * @return JsonResponse
     */
    public function setTwoFA(Request $request)
    {
        $TwoFA = ($request->query->get('TwoFA') === 'true') ? true : false;
        $entity = $this->getUser();
        $entity->setTwoFactorAuthentication((int)$TwoFA);
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush($entity);

        return new JsonResponse('TwoFA updated', 200);
    }

    /**
     * @Route("/send/sms/code", name="send_sms_code",options={"expose"=true})
     * @Method("POST")
     */
    public function sendSmsCode()
    {
        $this->container->get('twilio.sms')->sendSmsCode(
            $this->getUser()->getMobile(),
            $this->getUser()->getMobileCountry(),
            'sms'
        );

        return new JsonResponse('SMS sended', 200);
    }

    /**
     * @Route("/validate/sms/code", name="validate_sms_code",options={"expose"=true})
     * @Method("POST")
     */
    public function validateSmsCode(Request $request)
    {

        $response = $this->container->get('twilio.sms')->verificationSmsCode(
            $this->getUser()->getMobile(),
            $this->getUser()->getMobileCountry(),
            $request->query->get('code')
        );

        return new JsonResponse($response);
    }
}
