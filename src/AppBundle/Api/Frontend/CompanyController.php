<?php

namespace AppBundle\Api\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Thumbnail;
use AppBundle\Api\Frontend\AbstractController;
use AppBundle\Entity\Company;

/**
 * UserCompanyService controller.
 *
 * @Route("/api/frontend")
 */
class CompanyController extends AbstractController
{
    /**
     * Lists All User Companies
     *
     * @Route("/company/contacts", name="get_company_contacts",options={"expose"=true})
     * @Method("GET")
     */
    public function getCompanyContacts()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $result = ["CompanyContacts" => null];
        $user = $this->getUser();

        if ($user->getCurrentCompany()) {
            $em = $this->getDoctrine()->getManager();
            $result['CompanyContacts'] = $em->getRepository('AppBundle:UserCompany')
                ->getCompanyContacts($user->getCurrentCompany()->getId());
        }

        return new JsonResponse($result);
    }

    /**
     * Lists All User Companies
     *
     * @Route("/company/set", name="set_current_company",options={"expose"=true})
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     */
    public function setCurrentCompany(Request $request)
    {
        if (!$this->getUser()) {
            return new JsonResponse(['message' => "Error"]);
        }
        $idCompany = $request->request->get('company');
        $result = $this->container->get('company.service')->setCurrentCompany($this->getUser(), $idCompany);
        return new JsonResponse($result);
    }

    /**
     * Lists All User Companies
     *
     * @Route("/company/updateimage", name="set_current_company_image",options={"expose"=true})
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     */
//    public function setCurrentCompanyImage(Request $request)
//    {
//        $result = ['message' => null];
//        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
//            $Thumbnail = new Thumbnail();
//            $form = $this->createForm('AppBundle\Form\ThumbnailType', $Thumbnail);
//            $form->handleRequest($request);
//            if ($form->isSubmitted()) {
//                $em = $this->getDoctrine()->getManager();
//                $em->persist($Thumbnail);
//                $user = $this->getUser();
//                $company = $user->getCurrentCompany();
//                if (null != $company) {
//                    $user->getCurrentCompany()->setThumbnail($Thumbnail);
//                    $em->persist($company);
//                    $result['message'] = "New Thumbnail was created successfully";
//                }
//                $em->flush();
//
//                $result['message'] = "You don't have a company";
//            }
//        }
//
//        return new JsonResponse($result);
//    }

    /**
     * @Route("/company/{id}/edit", options={"expose"=true}, name="post_frontend_edit_company")
     * @Method("PUT")
     * @param Request $request
     * @return JsonResponse
     */
    public function putCompanyAction(Request $request,Company $company)
    {
        if(!$this->getUser())
            return new JsonResponse("Form not valid 1", 401);

        if(!$this->getUser()->getCurrentCompany())
            return new JsonResponse("Form not valid 2", 401);

        if($this->getUser()->getCurrentCompany()->getId()!= $company->getId())
            return new JsonResponse("Form not valid 3 ", 401);

        $companyData = $request->query->get('company');
        if((!array_key_exists('phone', $companyData)) || (!array_key_exists('fax', $companyData)) || (!array_key_exists('email', $companyData))){
            return new JsonResponse("Form not valid 4", 401);
        }

        if($companyData['phone'] != "")
        {
            $company->setPhone($companyData['phone']);
        }

        if($companyData['fax'] != "")
        {
            $company->setFax($companyData['fax']);
        }

        if($companyData['email'] != "")
        {
            $company->setEmail($companyData['email']);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($company);
        $em->flush();
        return new JsonResponse("Data Saved Successfully", 200);
    }

}
