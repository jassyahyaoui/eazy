<?php

namespace AppBundle\Api\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Api\Frontend\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Thumbnail;

/**
 * UserCompanyService controller.
 *
 * @Route("/api/frontend")
 */
class GdprUserOptin extends AbstractController
{
    /**
     * @Route("/user/gdpr", name="get_user_gdpr",options={"expose"=true})
     * @Method("GET")
     */
    public function getGdprUserOptinAction()
    {

        $result = ['gdpr' => null];
        if (!$this->getUser()) {
            return new JsonResponse(array('gdpr' => null), 404);
        }
        $result['gdpr'] = $this->container->get('gdpr_user_optin.service')->getGdprUserOptin($this->getUser());

        return new JsonResponse($result);
    }

    /**
     * @Route("/user/set-gdpr", name="set_user_gdpr",options={"expose"=true})
     * @Method("POST")
     */
    public function setGdprUserOptinAction(Request $request)
    {
        if ($request->request->has('accept')) {
            if ($request->request->get('accept') === "on") {
                $result['gdpr'] = $this->container->get('gdpr_user_optin.service')->setGdprUserOptin($this->getUser(),$this->container->get('big.ip.service')->clientIp());

                return $this->redirectToRoute('frontend_homepage');
            }
        }

        return $this->redirectToRoute('frontend_homepage');
    }
}
