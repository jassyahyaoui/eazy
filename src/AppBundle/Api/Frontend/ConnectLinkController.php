<?php

namespace AppBundle\Api\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Api\Frontend\AbstractController;

/**
 * UserCompanyService controller.
 *
 * @Route("/api/frontend")
 */
class ConnectLinkController extends AbstractController
{
    /**
     * Lists All User Companies
     *
     * @Route("/link/cegid/connect", name="get_cegid_connect_link",options={"expose"=true})
     * @Method("GET")
     */
    public function GetCEGIDConnectLink($idservice, $idUsercompanyservice)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $em = $this->getDoctrine()->getManager();
        $usercompanyservice = $em->getRepository('AppBundle:UserCompanyService')->find($idUsercompanyservice);

        if (!$usercompanyservice || !$usercompanyservice->getService()
            || empty($usercompanyservice->getService()->getConnectionTarget())
            || !$usercompanyservice->getExternalId() || !$usercompanyservice->getService()->getExternalId()) {
            return "";
        }

        $service = $usercompanyservice->getService();
        $link = $service->getConnectionTarget();
        $key = "";

        if (!$this->container->hasParameter('CEGID_PORTAL_ID') ||
            empty($this->getParameter('CEGID_PORTAL_ID'))) {
            return "";
        }

        $portailNumber = $this->getParameter('CEGID_PORTAL_ID');

        // Portail 
        $link = $link."?portail=".$portailNumber;
        $key = $key.$portailNumber;
        $userCompanyServicEexternalId = $usercompanyservice->getExternalId();
        $key = $key.$userCompanyServicEexternalId;
        $link = $link."&identifiant=".$userCompanyServicEexternalId;

        // Produit
        $serviceExternalId = $service->getExternalId();
        $key = $key.$serviceExternalId;
        $link = $link."&produit=".$serviceExternalId;

        // Timestamp
        $cegidWebhook = $this->getParameter('CEGID_WEBHOOK_TIMESTAMP_CHECK');
        if (!$this->container->hasParameter('CEGID_WEBHOOK_TIMESTAMP_CHECK') || empty($cegidWebhook)) {
            return "";
        }
        try {
            $contentRemoteTimestamp = @file_get_contents($cegidWebhook);
            if (!$contentRemoteTimestamp) {
                return "";
            }
        } catch (Exception $e) {
            return "";
        }

        $remoteTimestamp = json_decode($contentRemoteTimestamp)->ticket;
        if (!$remoteTimestamp) {
            return "";
        }

        $link = $link."&ticket=".$remoteTimestamp;

        //cle
        $CegidWebHookPk = $this->getParameter('CEGID_WEBHOOK_PK');
        if (!$this->container->hasParameter('CEGID_WEBHOOK_PK') || empty($CegidWebHookPk)) {
            return "";
        }

        $key = $key.$remoteTimestamp.$CegidWebHookPk;
        $Hashedkey = md5($key);
        $link = $link."&cle=".$Hashedkey;
        $this->LogConnect($service);

        return $link;
    }

    /**
     * Lists All User Companies
     *
     * @Route("/link/oneup/connect", name="get_oneup_connect_link",options={"expose"=true})
     * @Method("GET")
     */
    public function GetONEUPConnectLink($idservice, $idUsercompanyservice)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $link = "";
        $em = $this->getDoctrine()->getManager();
        $service = $em->getRepository('AppBundle:Service')->find($idservice);
        if ($service) {
            if ($service->getConnectionTarget()) {
                $link = $service->getConnectionTarget();
            }
        }
        $this->LogConnect($service);

        return $link;
    }

    /**
     * Lists All User Companies
     *
     * @Route("/connect/service/user", name="connect_service_user_from_company",options={"expose"=true})
     * @Method("GET")
     */
    public function ConnectServiceUserFromCompany(Request $request)
    {
        $whiteListUserCompanyService = [];
        $whiteListService = [];
        foreach ($this->getUser()->getUserCompany()->toArray() as $userCompany) {
            foreach ($userCompany->getUserCompanyService()->toArray() as $userCompanyService) {
                array_push($whiteListUserCompanyService, $userCompanyService->getId());
                array_push($whiteListService, $userCompanyService->getService()->getId());
            }
        }
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $result = ['link' => null];
        $em = $this->getDoctrine()->getManager();
        $idUsercompanyservice = $request->query->get('id');
        $usercompanyservice = $em->getRepository('AppBundle:UserCompanyService')->find($idUsercompanyservice);

        $service = $usercompanyservice->getService();
        $manager = $service->getEventManager();
        if (!$usercompanyservice
            || !$usercompanyservice->getService()
            || !in_array(
                $idUsercompanyservice,
                $whiteListUserCompanyService
            ) || !in_array(
                $service->getId(),
                $whiteListService
            )) {
            return new JsonResponse($result);
        }
        if ($manager === "ONEUP") {
            $link = $this->GetONEUPConnectLink($service->getId(), $idUsercompanyservice);
            $result['link'] = $link ? $link : "Settings for connecting to service undefined";
        } elseif ($manager === "CEGID") {
            $CEGIDlink = $this->GetCEGIDConnectLink($service->getId(), $idUsercompanyservice);
            $result['link'] = $CEGIDlink ? $CEGIDlink : "Settings for connecting to service undefined";
        }

        return new JsonResponse($result);
    }

    public function LogConnect($service)
    {
        $this->get('session')->set('service', $service);
        $logger = $this->container->get('monolog.logger.connect');
        $logger->pushProcessor(
            function ($entry) {
                $user = $this->getUser();
                $service = $this->get('session')->get('service');

                $entry['extra']['user_id'] = $user->getId();
                $entry['extra']['username'] = $user->getUsername();

                if (!empty($user->getCurrentCompany())) {
                    $entry['extra']['company_id'] = $user->getCurrentCompany()->getId();
                    $entry['extra']['company_name'] = $user->getCurrentCompany()->getName();
                }

                if (!empty($service)) {
                    $entry['extra']['service_id'] = $service->getId();
                    $entry['extra']['service_name'] = $service->getName();
                }

                return $entry;
            }
        );

        $logger->addInfo('  ');
    }
}
