<?php

namespace AppBundle\Api\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Api\Frontend\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * UserCompanyService controller.
 *
 * @Route("/api/frontend")
 */
class UserCompanyController extends AbstractController
{
    /**
     * Lists all UserCompany entities.
     *
     * @Route("/user/companies", name="get_user_companies",options={"expose"=true})
     * @Method("GET")
     */
    public function getUserCompaniesAction()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $result = ['userCompanies' => null];

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $result['userCompanies'] = $em->getRepository('AppBundle:Company')->getUserCompanies($user);

        return new JsonResponse($result);
    }

    /**
     * Lists All User Companies
     *
     * @Route("/company/get", name="get_current_company",options={"expose"=true})
     * @Method("GET")
     */
    public function getCurrentCompany()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $user = $this->getUser();
        $currentCompany = $user->getCurrentCompany();
        if (null == $currentCompany) {
            return new JsonResponse(['currentCompany' => 'No company found']);
        }

        $em = $this->getDoctrine()->getManager();
        $this->container->get('session')->set('current_company', $currentCompany);

        return new JsonResponse(
            [
                'currentCompany' => $em->getRepository('AppBundle:Company')->getCompany($currentCompany->getId()),
            ]
        );
    }

    /**
     * @Route("/user/company/services", name="get_user_company_services",options={"expose"=true})
     * @Method("GET")
     */
    public function getUserCompanyServicesAction()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $currentCompanyId = $user->getCurrentCompany()->getId();
        $services = $em->getRepository('AppBundle:UserCompanyService')->getUserCompanyServices(
            $currentCompanyId,
            $user->getId()
        );


        foreach ($services as $key => $service) {
            $entity = $em->find('AppBundle:Service', $service['id']);
            $entity->setTranslatableLocale($user->getLocale());
            $em->refresh($entity);
            $services[$key]['name'] = $entity->getName();
            $services[$key]['shortDesc'] = $entity->getShortDesc();
            $services[$key]['longDesc'] = $entity->getLongDesc();
            $services[$key]['isDenied'] = $this->container->get('user.agent.service')->checkDevice($service['id']);
        }

        return new JsonResponse(array('services' => $services));
    }

    /**
     * @Route("/users", options={"expose"=true}, name="get_company_users")
     * @Method("GET")
     */
    public function getCompanyUsersAction()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $currentCompany = $user->getCurrentCompany();
        $users = $em->getRepository('AppBundle:User')->getCompanyUsers($currentCompany->getId());
        for ($j = 0; $j < sizeOf($users); $j++) {
            $r = [];
            $roles = explode('"', $users[$j]['role']);
            for ($i = 1; $i < sizeOf($roles); $i += 2) {
                array_push($r, $roles[$i]);
            }
            $users[$j]['role'] = $r;
            $users[$j]['locale'] = $this->getUser()->getLocale();
        }

        return new JsonResponse(['users' => $users]);
    }
}
