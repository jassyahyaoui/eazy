<?php

namespace AppBundle\EventListener;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Entity\User;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class RedirectUserListener
{
    private $tokenStorage;
    private $router;

    public function __construct(TokenStorageInterface $t, RouterInterface $r)
    {
        $this->tokenStorage = $t;
        $this->router = $r;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
//       if ($this->isUserLogged() && $event->isMasterRequest()) {
//            $currentRoute = $event->getRequest()->attributes->get('_route');
//           // get the current user
//          $user = $this->tokenStorage->getToken()->getUser();
//           // check if the user is not an admin
//            if (!in_array("ROLE_SUPER_ADMIN", $user->getRoles())) {
//                // check if the user is trying to access the protected route
//                if (!$this->isAuthenticatedUserOnAnonymousPage($currentRoute)) {
//                    // check if the user had already see and accept this
//                    if ($user->getGdprUserOptin() != true) {
//                        // force the user ti redirect the page and accept the term
//                        $response = new RedirectResponse($this->router->generate('gdpr'));
//                        $event->setResponse($response);
//                    }
//                }
//            }
// ce code rederige l'utilisateur à une boucle infinie
//            if ($this->isAuthenticatedUserOnAnonymousPage($currentRoute)) {
//                $response = new RedirectResponse($this->router->generate('frontend_homepage'));
//                $event->setResponse($response);
//            }


//        }
    }

    private function isUserLogged()
    {
        $user = $this->tokenStorage->getToken()->getUser();

        return $user instanceof User;
    }

    private function isAuthenticatedUserOnAnonymousPage($currentRoute)
    {
        return in_array(
            $currentRoute,
            [
                'fos_user_security_login',
                'fos_user_resetting_request',
                'fos_user_registration_register',
                'gdpr',
                'set_user_gdpr',
                '_twig_error_test'
            ]
        );
    }


}
