<?php

namespace AppBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\GetResponseUserEvent;
use Prophecy\Argument\Token\TokenInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use ReCaptcha\ReCaptcha;
use FOS\UserBundle\Event\FilterUserResponseEvent;

/**
 * Listener responsible to change the redirection at the end of the password resetting
 */
class PasswordResettingListener implements EventSubscriberInterface
{
    private $router;

    private $key;

    private $container;

    public function __construct(UrlGeneratorInterface $router, $key, ContainerInterface $container)
    {
        $this->router = $router;
        $this->key = $key;
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::RESETTING_RESET_REQUEST => 'onResettingResetRequest',
            FOSUserEvents::RESETTING_RESET_COMPLETED => 'onResettingResetCompleted',
        );
    }

    public function onResettingResetRequest(GetResponseUserEvent $event)
    {
        $request = $event->getRequest();

        $recaptcha = new ReCaptcha($this->key);

        $resp = $recaptcha->verify($request->request->get('g-recaptcha-response'), $request->getClientIp());
        if (!$event->getUser()) {
            if (!$resp->isSuccess() && $request->isMethod('POST')) {
                $request->getSession()->getFlashBag()->add(
                    'recapErrorReset',
                    'CAPTCHA validation failed, try again.'
                );
                $event->setResponse(new RedirectResponse($this->router->generate('fos_user_resetting_request')));
            }
        }
        if (!$event->getUser()->isAccountNonLocked()) {
            $event->setResponse(new RedirectResponse($this->router->generate('fos_user_resetting_request    ')));
        }
    }

    public function onResettingResetCompleted(FilterUserResponseEvent $event)
    {
        /*Notice owners by email*/
        if ($this->container->get('notice.service')->notice($event->getUser())) {
            $this->newPasswordSecurityNotice(
                $event->getUser(),
                $this->container->get('notice.service')->getOwners($event->getUser()),
                $this->container->get('big.ip.service')->clientIp()
            );
        }
    }


    private function newPasswordSecurityNotice($user, $owners, $ip)
    {
        $location = [];
        if (!empty($user->getUserCompany()->toArray())) {
            $location['country_name'] = $user->getUserCompany()->toArray()[0]->getLastLoginCountryName();
            $location['region_name'] = $user->getUserCompany()->toArray()[0]->getLastLoginRegionName();
        }

        foreach ($owners as $owner) {
            $locale = $owner['locale'];
            $subject = 'Information de sécurité concernant votre compte EAZY by MAZARS';
            if ($locale == null) {
                $locale = 'fr';
            }
            if ($locale == 'en') {
                $subject = 'Security notice about your account EAZY by MAZARS';
            }


            $this->container->get('mail')->passwordSecurityNotice(
                $subject,
                $owner['email'],
                $locale,
                $user,
                $ip,
                $owner,
                $location
            );
        }
    }
}
