<?php
/**
 * Created by PhpStorm.
 * User: MAZ_USER3
 * Date: 10/04/2018
 * Time: 11:47
 */

namespace AppBundle\EventListener;

use FOS\OAuthServerBundle\Event\OAuthEvent;

class OAuthEventListener
{
    public function onPreAuthorizationProcess(OAuthEvent $event)
    {
        $event->setAuthorizedClient(TRUE);
    }
}
