<?php

namespace AppBundle\Repository;

use Doctrine\ORM\Query;

/**
 * CompanyRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class NotificationRepository extends \Doctrine\ORM\EntityRepository
{
  public function getList()
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('c')
            ->from('AppBundle:Company', 'c')
            ->leftJoin('c.userCompany', 'uc')
            ->leftJoin('uc.user', 'u')
            ->where('u.id = :user ')
            ->setParameter('user', $user)
            ->orderBy('uc.id')
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
    }
   

}
