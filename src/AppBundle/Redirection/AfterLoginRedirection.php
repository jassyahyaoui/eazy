<?php

namespace AppBundle\Redirection;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\DependencyInjection\Container;
use ReCaptcha\ReCaptcha;

class AfterLoginRedirection implements AuthenticationSuccessHandlerInterface
{

    /**
     * @var \Symfony\Component\Routing\RouterInterface
     */
    private $router;

    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    private $container;

    /**
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router, Container $container)
    {
        $this->router = $router;
        $this->container = $container;
    }


//onAuthenticationFailure

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $em = $this->container->get('doctrine.orm.default_entity_manager');
        $recaptcha = new ReCaptcha($this->container->getParameter('CAPTACHA_SECRET_KEY'));
        $resp = $recaptcha->verify(
            $request->request->get('g-recaptcha-response'),
            $this->container->get('big.ip.service')->clientIp()
        );

        if ($token->getUser()->getByPassRecaptcha() != true) {
            if (!$resp->isSuccess() && $request->isMethod('POST')) {
                $request->getSession()->getFlashBag()->add('recapError', 'CAPTCHA validation failed, try again.');
                $request->getSession()->remove('byPassRecaptcha');

                return new RedirectResponse($this->router->generate('fos_user_security_login'));
            }
        } else {
            if ((!$resp->isSuccess()) && ($request->isMethod('POST')) && (!$request->getSession()->get(
                    'byPassRecaptcha'
                ))) {
                $request->getSession()->getFlashBag()->add('recapError', 'CAPTCHA validation failed, try again.');
                $request->getSession()->set('byPassRecaptcha', 'true');

                return new RedirectResponse($this->router->generate('fos_user_security_login'));
            }
        }
        $request->getSession()->remove('byPassRecaptcha');


        /*Log cconnection*/
        $logger = $this->container->get('monolog.logger.security');
        $logger->addInfo('USER-ID : '.$token->getUser()->getId());
        $logger->addInfo('USERNAME : '.$token->getUser()->getUsername());
        //   $logger->addInfo('COMPANY-ID : '.$token->getUser()->getCurrentCompany()->getId());
//        $logger->addInfo('COMPANY-NAME : '.$token->getUser()->getCurrentCompany()->getName());
        /*END Log cconnection*/

        $user = $token->getUser();
        $roles = $token->getRoles();
        $rolesTab = array_map(
            function ($role) {
                return $role->getRole();
            },
            $roles
        );

        //Ip Stack block
        if ($this->isNewIp($token, $this->container->get('big.ip.service')->clientIp())) {
            $this->container->get('ip_stack.service')->menageIpStack(
                $token->getUser()->getId(),
                $this->container->get('big.ip.service')->clientIp()
            );
        }

        //ADFS Mazars
        if ($this->container->get('adfs.mazars.service')->isInAllowedDomain($request->request->get('_username'))) {
            return new RedirectResponse($this->router->generate('lightsaml_sp.login'));
        }

        if (in_array('ROLE_ADMIN', $rolesTab, true) || in_array('ROLE_SUPER_ADMIN', $rolesTab, true)) {
            $redirection = new RedirectResponse($this->router->generate('backend_homepage'));
        } else {
            if (
                $this->isNewIp($token, $this->container->get('big.ip.service')->clientIp())
                && $this->container->get('notice.service')->notice($token->getUser())
            ) {
                $this->newIpSecurityNotice(
                    $token->getUser(),
                    $this->container->get('notice.service')->getOwners($token->getUser()),
                    $this->container->get('big.ip.service')->clientIp()
                );
            }
            $this->setLastLoginDate($token, $em);
            $this->setLastLoginIp($token, $em, $this->container->get('big.ip.service')->clientIp());


            /* Check Gdpr*/
            // check if the user had already see the page and accept the terms and condition
            if ($user->getGdprUserOptin() != true) {
                // redirect the user to the gdpr page
                $redirection = new RedirectResponse($this->router->generate('gdpr'));

                return $redirection;
            }
            $redirection = new RedirectResponse($this->router->generate('frontend_homepage'));
        }

        return $redirection;
    }

    private function setLastLoginDate($token, $em)
    {
        foreach ($token->getUser()->getUserCompany()->toArray() as $userCompany) {
            $userCompany->setLastLoginDate(new \DateTime('now'));
        }

        $em->persist($userCompany);
        $em->flush();
    }

    private function setLastLoginIp($token, $em, $ip)
    {
        foreach ($token->getUser()->getUserCompany()->toArray() as $userCompany) {
            $userCompany->setLastLoginIp($ip);
        }
        $em->persist($userCompany);
        $em->flush();
    }

    private function isNewIp($token, $newIp)
    {
        $oldIp = null;
        foreach ($token->getUser()->getUserCompany()->toArray() as $userCompany) {
            $oldIp = $userCompany->getLastLoginIp();
        }
        if ($oldIp !== $newIp) {
            return true;
        }

        return false;
    }


    private function newIpSecurityNotice($user, $owners, $ip)
    {
        $location = [];
        if (!empty($user->getUserCompany()->toArray())) {
            $location['country_name'] = $user->getUserCompany()->toArray()[0]->getLastLoginCountryName();
            $location['region_name'] = $user->getUserCompany()->toArray()[0]->getLastLoginRegionName();
        }

        foreach ($owners as $owner) {
            $locale = $owner['locale'];
            $subject = 'Information de sécurité concernant votre compte EAZY by MAZARS';
            if ($locale == null) {
                $locale = 'fr';
            }
            if ($locale == 'en') {
                $subject = 'Security notice about your account EAZY by MAZARS';
            }


            $this->container->get('mail')->ipSecurityNotice(
                $subject,
                $owner['email'],
                $locale,
                $user,
                $ip,
                $owner,
                $location
            );
        }
    }


}
