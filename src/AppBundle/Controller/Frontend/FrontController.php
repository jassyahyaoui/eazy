<?php

namespace AppBundle\Controller\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class FrontController extends Controller
{
    /**
     * @Route("/gdpr", name="gdpr", options={"expose"=true})
     */
    public function gdprAction(Request $request)
    {

        return $this->render('frontend/gdpr.html.twig');
    }

    /**
     * @Route("/bureau-digital/", name="bureau_digital", options={"expose"=true})
     */
    public function bureauDigitalAction()
    {
        return $this->redirectToRoute('frontend_homepage');
    }

    /**
     * @Route("/", name="frontend_homepage", options={"expose"=true})
     */
    public function homepageAction(Request $request)
    {
        if ($request->query->has('bureau-digital')) {
            return $this->redirectToRoute('frontend_homepage');
        }

        if ($this->getUser() == null) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        if ($this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            return $this->redirectToRoute('backend_homepage');
        }

        return $this->render('frontend/homepage.html.twig');
    }

    /**
     * @Route("/user/security", name="user_security", options={"expose"=true})
     */
    public function securityAction()
    {
        return $this->render('frontend/user/security.html.twig');
    }

    /**
     * @Route("/user", name="frontend_user")
     */
    public function userAction()
    {
        return $this->render('frontend/user/edit.html.twig');
    }

    /**
     * @Route("/company-setting", name="frontend_company_setting")
     */
    public function companyAction()
    {
        return $this->render('frontend/company/show.html.twig');
    }

    /**
     * @Route("/user-list", name="frontend_userlist")
     */
    public function userListAction()
    {
        return $this->render('frontend/user/index.html.twig');
    }

}
