<?php

namespace AppBundle\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\View\TwitterBootstrap3View;

use AppBundle\Entity\Company;

/**
 * Class CompanyController
 * @package AppBundle\Controller\Backend
 * @Route("/backend/company")
 */
class CompanyController extends Controller
{
    /**
     * Lists all Company entities.
     *
     * @Route("/", name="company")
     * @Method("GET")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('AppBundle:Company')->createQueryBuilder('e');

        list($filterForm, $queryBuilder) = $this->filter($queryBuilder, $request);
        list($companies, $pagerHtml) = $this->paginator($queryBuilder, $request);

        $totalOfRecordsString = $this->getTotalOfRecordsString($queryBuilder, $request);

        return $this->render(
            'backend/company/index.html.twig',
            array(
                'companies' => $companies,
                'pagerHtml' => $pagerHtml,
                'filterForm' => $filterForm->createView(),
                'totalOfRecordsString' => $totalOfRecordsString,

            )
        );
    }

    /**
     * Create filter form and process filter request.
     *
     * @param $queryBuilder
     * @param Request $request
     * @return array
     */
    protected function filter($queryBuilder, Request $request)
    {
        $session = $request->getSession();
        $filterForm = $this->createForm('AppBundle\Form\CompanyFilterType');

        // Reset filter
        if ($request->get('filter_action') == 'reset') {
            $session->remove('CompanyControllerFilter');
        }
        // Filter action
        if ($request->get('filter_action') == 'filter') {
            // Bind values from the request
            $filterForm->handleRequest($request);

            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('CompanyControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('CompanyControllerFilter')) {
                $filterData = $session->get('CompanyControllerFilter');

                foreach ($filterData as $key => $filter) { //fix for entityFilterType that is loaded from session
                    if (is_object($filter)) {
                        $filterData[$key] = $queryBuilder->getEntityManager()->merge($filter);
                    }
                }
                $filterForm = $this->createForm('AppBundle\Form\CompanyFilterType', $filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }


    /**
     * Get results from paginator and get paginator view.
     *
     * @param $queryBuilder
     * @param Request $request
     * @return array
     */
    protected function paginator($queryBuilder, Request $request)
    {
        //sorting
        $sortCol = $queryBuilder->getRootAlias().'.'.$request->get('pcg_sort_col', 'id');
        $queryBuilder->orderBy($sortCol, $request->get('pcg_sort_order', 'desc'));
        // Paginator
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage($request->get('pcg_show', 10));
        try {
            $pagerfanta->setCurrentPage($request->get('pcg_page', 1));
        } catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $ex) {
            $pagerfanta->setCurrentPage(1);
        }
        $entities = $pagerfanta->getCurrentPageResults();
        // Paginator - route generator
        $me = $this;
        $routeGenerator = function ($page) use ($me, $request) {
            $requestParams = $request->query->all();
            $requestParams['pcg_page'] = $page;

            return $me->generateUrl('company', $requestParams);
        };
        // Paginator - view
        $view = new TwitterBootstrap3View();
        $pagerHtml = $view->render(
            $pagerfanta,
            $routeGenerator,
            array(
                'proximity' => 3,
                'prev_message' => 'previous',
                'next_message' => 'next',
            )
        );

        return array($entities, $pagerHtml);
    }


    /*
     * Calculates the total of records string
     *
     * @param $queryBuilder
     * @param $request
     * @return string
     */
    protected function getTotalOfRecordsString($queryBuilder, $request)
    {
        $totalOfRecords = $queryBuilder->select('COUNT(e.id)')->getQuery()->getSingleScalarResult();
        $show = $request->get('pcg_show', 10);
        $page = $request->get('pcg_page', 1);
        $startRecord = ($show * ($page - 1)) + 1;
        $endRecord = $show * $page;
        if ($endRecord > $totalOfRecords) {
            $endRecord = $totalOfRecords;
        }

        return "Showing $startRecord - $endRecord of $totalOfRecords Records.";
    }


    /**
     * Displays a form to create a new Company entity.
     *
     * @Route("/new", name="company_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $company = new Company();
        $form = $this->createForm('AppBundle\Form\CompanyType', $company);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($company);
            $em->flush();
            $editLink = $this->generateUrl('company_edit', array('id' => $company->getId()));
            $this->get('session')->getFlashBag()->add(
                'success',
                "<a href='$editLink'>New company was created successfully.</a>"
            );
            $nextAction = $request->get('submit') == 'save' ? 'company' : 'company_new';

            return $this->redirectToRoute($nextAction);
        }

        return $this->render(
            'backend/company/new.html.twig',
            array(
                'company' => $company,
                'form' => $form->createView(),
            )
        );
    }

    private function isValidEmail($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        $allowedDomainsExport = $this->getParameter('ALLOWED_DOMAINS');

        $array = explode("@", $email);
        if (!is_array($array)) {
            return false;
        }
        $domain = $array[1];
        if (!in_array($domain, $allowedDomainsExport[0])) {
            return false;
        }

        return true;
    }

    /**
     * Finds and displays a User entity.
     *
     * @Route("/export", name="export_company")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function exportAction(Request $request)
    {
        $form = $request->request->get("export");
        $email = $form['email'];

        if ($this->isValidEmail($email) == false) {
            $this->get('session')->getFlashBag()->add('error', 'This e-mail is not valid !');

            return $this->redirect($this->generateUrl('company'));
        }
        $this->get('export.service')->export($email);

        return $this->redirect($this->generateUrl('company'));
    }

    /**
     * Finds and displays a Company entity.
     *
     * @Route("/{id}", name="company_show")
     * @Method("GET")
     * @param Company $company
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public
    function showAction(
        Company $company
    ) {
        $deleteForm = $this->createDeleteForm($company);

        return $this->render(
            'backend/company/show.html.twig',
            array(
                'company' => $company,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }


    /**
     * Displays a form to edit an existing Company entity.
     *
     * @Route("/{id}/edit", name="company_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Company $company
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public
    function editAction(
        Request $request,
        Company $company
    ) {
        $deleteForm = $this->createDeleteForm($company);
        $editForm = $this->createForm('AppBundle\Form\CompanyType', $company);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($company);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'Edited Successfully!');

            return $this->redirectToRoute('company_edit', array('id' => $company->getId()));
        }

        return $this->render(
            'backend/company/edit.html.twig',
            array(
                'company' => $company,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }


    /**
     * Deletes a Company entity.
     *
     * @Route("/{id}", name="company_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param Company $company
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public
    function deleteAction(
        Request $request,
        Company $company
    ) {
        $form = $this->createDeleteForm($company);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($company);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The Company was deleted successfully');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the Company');
        }

        return $this->redirectToRoute('company');
    }

    /**
     * Creates a form to delete a Company entity.
     *
     * @param Company $company
     * @return \Symfony\Component\Form\FormInterface
     */
    private
    function createDeleteForm(
        Company $company
    ) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('company_delete', array('id' => $company->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Delete Company by id
     *
     * @Route("/delete/{id}", name="company_by_id_delete")
     * @Method("GET")
     * @param Company $company
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public
    function deleteByIdAction(
        Company $company
    ) {
        $em = $this->getDoctrine()->getManager();
        try {
            $em->remove($company);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The Company was deleted successfully');
        } catch (Exception $ex) {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the Company');
        }

        return $this->redirect($this->generateUrl('company'));

    }


    /**
     * Bulk Action
     * @Route("/bulk-action/", name="company_bulk_action")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public
    function bulkAction(
        Request $request
    ) {
        $ids = $request->get("ids", array());
        $action = $request->get("bulk_action", "delete");
        if ($action == "delete") {
            try {
                $em = $this->getDoctrine()->getManager();
                $repository = $em->getRepository('AppBundle:Company');
                foreach ($ids as $id) {
                    $company = $repository->find($id);
                    $em->remove($company);
                    $em->flush();
                }
                $this->get('session')->getFlashBag()->add('success', 'companies was deleted successfully!');

            } catch (Exception $ex) {
                $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the companies ');
            }
        }

        return $this->redirect($this->generateUrl('company'));
    }


}
