<?php

namespace AppBundle\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\View\TwitterBootstrap3View;

use AppBundle\Entity\CompanyService;

/**
 * CompanyService controller.
 *
 * @Route("backend/companyservice")
 */
class CompanyServiceController extends Controller
{
    /**
     * Lists all CompanyService entities.
     *
     * @Route("/", name="companyservice")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('AppBundle:CompanyService')->createQueryBuilder('e');

        list($filterForm, $queryBuilder) = $this->filter($queryBuilder, $request);
        list($companyServices, $pagerHtml) = $this->paginator($queryBuilder, $request);
        
        $totalOfRecordsString = $this->getTotalOfRecordsString($queryBuilder, $request);

        return $this->render('backend/companyservice/index.html.twig', array(
            'companyServices' => $companyServices,
            'pagerHtml' => $pagerHtml,
            'filterForm' => $filterForm->createView(),
            'totalOfRecordsString' => $totalOfRecordsString,

        ));
    }

    /**
    * Create filter form and process filter request.
    *
    */
    protected function filter($queryBuilder, Request $request)
    {
        $session = $request->getSession();
        $filterForm = $this->createForm('AppBundle\Form\CompanyServiceFilterType');

        // Reset filter
        if ($request->get('filter_action') == 'reset') {
            $session->remove('CompanyServiceControllerFilter');
        }

        // Filter action
        if ($request->get('filter_action') == 'filter') {
            // Bind values from the request
            $filterForm->handleRequest($request);

            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('CompanyServiceControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('CompanyServiceControllerFilter')) {
                $filterData = $session->get('CompanyServiceControllerFilter');
                
                foreach ($filterData as $key => $filter) { //fix for entityFilterType that is loaded from session
                    if (is_object($filter)) {
                        $filterData[$key] = $queryBuilder->getEntityManager()->merge($filter);
                    }
                }
                
                $filterForm = $this->createForm('AppBundle\Form\CompanyServiceFilterType', $filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }


    /**
    * Get results from paginator and get paginator view.
    *
    */
    protected function paginator($queryBuilder, Request $request)
    {
        //sorting
        $sortCol = $queryBuilder->getRootAlias().'.'.$request->get('pcg_sort_col', 'id');
        $queryBuilder->orderBy($sortCol, $request->get('pcg_sort_order', 'desc'));
        // Paginator
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage($request->get('pcg_show' , 10));

        try {
            $pagerfanta->setCurrentPage($request->get('pcg_page', 1));
        } catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $ex) {
            $pagerfanta->setCurrentPage(1);
        }
        
        $entities = $pagerfanta->getCurrentPageResults();

        // Paginator - route generator
        $me = $this;
        $routeGenerator = function($page) use ($me, $request)
        {
            $requestParams = $request->query->all();
            $requestParams['pcg_page'] = $page;
            return $me->generateUrl('companyservice', $requestParams);
        };

        // Paginator - view
        $view = new TwitterBootstrap3View();
        $pagerHtml = $view->render($pagerfanta, $routeGenerator, array(
            'proximity' => 3,
            'prev_message' => 'previous',
            'next_message' => 'next',
        ));

        return array($entities, $pagerHtml);
    }
    
    
    
    /*
     * Calculates the total of records string
     */
    protected function getTotalOfRecordsString($queryBuilder, $request) {
        $totalOfRecords = $queryBuilder->select('COUNT(e.id)')->getQuery()->getSingleScalarResult();
        $show = $request->get('pcg_show', 10);
        $page = $request->get('pcg_page', 1);

        $startRecord = ($show * ($page - 1)) + 1;
        $endRecord = $show * $page;

        if ($endRecord > $totalOfRecords) {
            $endRecord = $totalOfRecords;
        }
        return "Showing $startRecord - $endRecord of $totalOfRecords Records.";
    }
    
    

    /**
     * Displays a form to create a new CompanyService entity.
     *
     * @Route("/new", name="companyservice_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
    
        $companyService = new CompanyService();
        $form   = $this->createForm('AppBundle\Form\CompanyServiceType', $companyService);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($companyService);
            $em->flush();
            
            $editLink = $this->generateUrl('companyservice_edit', array('id' => $companyService->getId()));
            $this->get('session')->getFlashBag()->add('success', "<a href='$editLink'>New companyService was created successfully.</a>" );
            
            $nextAction=  $request->get('submit') == 'save' ? 'companyservice' : 'companyservice_new';
            return $this->redirectToRoute($nextAction);
        }
        return $this->render('backend/companyservice/new.html.twig', array(
            'companyService' => $companyService,
            'form'   => $form->createView(),
        ));
    }
    

    /**
     * Finds and displays a CompanyService entity.
     *
     * @Route("/{id}", name="companyservice_show")
     * @Method("GET")
     */
    public function showAction(CompanyService $companyService)
    {
        $deleteForm = $this->createDeleteForm($companyService);
        return $this->render('backend/companyservice/show.html.twig', array(
            'companyService' => $companyService,
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    

    /**
     * Displays a form to edit an existing CompanyService entity.
     *
     * @Route("/{id}/edit", name="companyservice_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, CompanyService $companyService)
    {
        $deleteForm = $this->createDeleteForm($companyService);
        $editForm = $this->createForm('AppBundle\Form\CompanyServiceType', $companyService);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($companyService);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', 'Edited Successfully!');
            return $this->redirectToRoute('companyservice_edit', array('id' => $companyService->getId()));
        }
        return $this->render('backend/companyservice/edit.html.twig', array(
            'companyService' => $companyService,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    

    /**
     * Deletes a CompanyService entity.
     *
     * @Route("/{id}", name="companyservice_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, CompanyService $companyService)
    {
    
        $form = $this->createDeleteForm($companyService);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($companyService);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The CompanyService was deleted successfully');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the CompanyService');
        }
        
        return $this->redirectToRoute('companyservice');
    }
    
    /**
     * Creates a form to delete a CompanyService entity.
     *
     * @param CompanyService $companyService The CompanyService entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CompanyService $companyService)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('companyservice_delete', array('id' => $companyService->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
    /**
     * Delete CompanyService by id
     *
     * @Route("/delete/{id}", name="companyservice_by_id_delete")
     * @Method("GET")
     */
    public function deleteByIdAction(CompanyService $companyService){
        $em = $this->getDoctrine()->getManager();
        
        try {
            $em->remove($companyService);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The CompanyService was deleted successfully');
        } catch (Exception $ex) {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the CompanyService');
        }

        return $this->redirect($this->generateUrl('companyservice'));

    }
    

    /**
    * Bulk Action
    * @Route("/bulk-action/", name="companyservice_bulk_action")
    * @Method("POST")
    */
    public function bulkAction(Request $request)
    {
        $ids = $request->get("ids", array());
        $action = $request->get("bulk_action", "delete");

        if ($action == "delete") {
            try {
                $em = $this->getDoctrine()->getManager();
                $repository = $em->getRepository('AppBundle:CompanyService');

                foreach ($ids as $id) {
                    $companyService = $repository->find($id);
                    $em->remove($companyService);
                    $em->flush();
                }

                $this->get('session')->getFlashBag()->add('success', 'companyServices was deleted successfully!');

            } catch (Exception $ex) {
                $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the companyServices ');
            }
        }

        return $this->redirect($this->generateUrl('companyservice'));
    }
    

}
