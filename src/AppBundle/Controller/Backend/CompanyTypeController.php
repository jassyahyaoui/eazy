<?php

namespace AppBundle\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\View\TwitterBootstrap3View;

use AppBundle\Entity\CompanyType;

/**
 * Class CompanyTypeController
 * @package AppBundle\Controller
 *
 * @Route("/backend/company_type")
 */
class CompanyTypeController extends Controller
{
    /**
     * Lists all CompanyType entities.
     * @Route("/", name="companytype")
     * @Method("GET")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('AppBundle:CompanyType')->createQueryBuilder('e');

        list($filterForm, $queryBuilder) = $this->filter($queryBuilder, $request);
        list($companyTypes, $pagerHtml) = $this->paginator($queryBuilder, $request);

        $totalOfRecordsString = $this->getTotalOfRecordsString($queryBuilder, $request);

        return $this->render(
            'backend/companytype/index.html.twig',
            array(
                'companyTypes' => $companyTypes,
                'pagerHtml' => $pagerHtml,
                'filterForm' => $filterForm->createView(),
                'totalOfRecordsString' => $totalOfRecordsString,

            )
        );
    }

    /**
     * Create filter form and process filter request.
     *
     * @param $queryBuilder
     * @param Request $request
     * @return array
     */
    protected function filter($queryBuilder, Request $request)
    {
        $session = $request->getSession();
        $filterForm = $this->createForm('AppBundle\Form\CompanyTypeFilterType');

        // Reset filter
        if ($request->get('filter_action') == 'reset') {
            $session->remove('CompanyTypeControllerFilter');
        }

        // Filter action
        if ($request->get('filter_action') == 'filter') {
            // Bind values from the request
            $filterForm->handleRequest($request);

            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('CompanyTypeControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('CompanyTypeControllerFilter')) {
                $filterData = $session->get('CompanyTypeControllerFilter');

                foreach ($filterData as $key => $filter) { //fix for entityFilterType that is loaded from session
                    if (is_object($filter)) {
                        $filterData[$key] = $queryBuilder->getEntityManager()->merge($filter);
                    }
                }

                $filterForm = $this->createForm('AppBundle\Form\CompanyTypeFilterType', $filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Get results from paginator and get paginator view.
     *
     * @param $queryBuilder
     * @param Request $request
     * @return array
     */
    protected function paginator($queryBuilder, Request $request)
    {
        //sorting
        $sortCol = $queryBuilder->getRootAlias().'.'.$request->get('pcg_sort_col', 'id');
        $queryBuilder->orderBy($sortCol, $request->get('pcg_sort_order', 'desc'));
        // Paginator
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage($request->get('pcg_show', 10));

        try {
            $pagerfanta->setCurrentPage($request->get('pcg_page', 1));
        } catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $ex) {
            $pagerfanta->setCurrentPage(1);
        }

        $entities = $pagerfanta->getCurrentPageResults();

        // Paginator - route generator
        $me = $this;
        $routeGenerator = function ($page) use ($me, $request) {
            $requestParams = $request->query->all();
            $requestParams['pcg_page'] = $page;

            return $me->generateUrl('companytype', $requestParams);
        };

        // Paginator - view
        $view = new TwitterBootstrap3View();
        $pagerHtml = $view->render(
            $pagerfanta,
            $routeGenerator,
            array(
                'proximity' => 3,
                'prev_message' => 'previous',
                'next_message' => 'next',
            )
        );

        return array($entities, $pagerHtml);
    }

    /*
     * Calculates the total of records string
     *
     * @param $queryBuilder
     * @param $request
     * @return string
     */
    protected function getTotalOfRecordsString($queryBuilder, $request)
    {
        $totalOfRecords = $queryBuilder->select('COUNT(e.id)')->getQuery()->getSingleScalarResult();
        $show = $request->get('pcg_show', 10);
        $page = $request->get('pcg_page', 1);

        $startRecord = ($show * ($page - 1)) + 1;
        $endRecord = $show * $page;

        if ($endRecord > $totalOfRecords) {
            $endRecord = $totalOfRecords;
        }

        return "Showing $startRecord - $endRecord of $totalOfRecords Records.";
    }

    /**
     * Displays a form to create a new CompanyType entity.
     * @Route("/new", name="companytype_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $companyType = new CompanyType();
        $form = $this->createForm('AppBundle\Form\CompanyTypeType', $companyType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($companyType);
            $em->flush();

            $editLink = $this->generateUrl('companytype_edit', array('id' => $companyType->getId()));
            $this->get('session')->getFlashBag()->add(
                'success',
                "<a href='$editLink'>New companyType was created successfully.</a>"
            );

            $nextAction = $request->get('submit') == 'save' ? 'companytype' : 'companytype_new';

            return $this->redirectToRoute($nextAction);
        }

        return $this->render(
            'backend/companytype/new.html.twig',
            array(
                'companyType' => $companyType,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * Finds and displays a CompanyType entity.
     * @Route("/{id}", name="companytype_show")
     * @Method("GET")
     * @param CompanyType $companyType
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(CompanyType $companyType)
    {
        $deleteForm = $this->createDeleteForm($companyType);

        return $this->render(
            'backend/companytype/show.html.twig',
            array(
                'companyType' => $companyType,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Displays a form to edit an existing CompanyType entity.
     * @Route("/{id}/edit", name="companytype_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param CompanyType $companyType
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, CompanyType $companyType)
    {
        $deleteForm = $this->createDeleteForm($companyType);
        $editForm = $this->createForm('AppBundle\Form\CompanyTypeType', $companyType);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($companyType);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Edited Successfully!');

            return $this->redirectToRoute('companytype_edit', array('id' => $companyType->getId()));
        }

        return $this->render(
            'backend/companytype/edit.html.twig',
            array(
                'companyType' => $companyType,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a CompanyType entity.
     * @Route("/{id}", name="companytype_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param CompanyType $companyType
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, CompanyType $companyType)
    {

        $form = $this->createDeleteForm($companyType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($companyType);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The CompanyType was deleted successfully');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the CompanyType');
        }

        return $this->redirectToRoute('companytype');
    }

    /**
     * Creates a form to delete a CompanyType entity.
     *
     * @param CompanyType $companyType
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(CompanyType $companyType)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('companytype_delete', array('id' => $companyType->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Delete CompanyType by id
     * @Route("/delete/{id}", name="companytype_by_id_delete")
     * @Method("GET")
     * @param CompanyType $companyType
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteByIdAction(CompanyType $companyType)
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $em->remove($companyType);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The CompanyType was deleted successfully');
        } catch (Exception $ex) {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the CompanyType');
        }

        return $this->redirect($this->generateUrl('companytype'));
    }

    /**
     * Bulk Action
     * @Route("/bulk-action/", name="companytype_bulk_action")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function bulkAction(Request $request)
    {
        $ids = $request->get("ids", array());
        $action = $request->get("bulk_action", "delete");

        if ($action == "delete") {
            try {
                $em = $this->getDoctrine()->getManager();
                $repository = $em->getRepository('AppBundle:CompanyType');

                foreach ($ids as $id) {
                    $companyType = $repository->find($id);
                    $em->remove($companyType);
                    $em->flush();
                }

                $this->get('session')->getFlashBag()->add('success', 'companyTypes was deleted successfully!');

            } catch (Exception $ex) {
                $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the companyTypes ');
            }
        }

        return $this->redirect($this->generateUrl('companytype'));
    }

}
