<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class BackController
 * @package AppBundle\Controller\Backend
 * @Route("/backend")
 */
class BackController extends Controller
{

    /**
     * @Route("", name="backend_homepage")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function homePageAction()
    {
        return $this->render('backend/homepage.html.twig');
    }
}
