<?php

namespace AppBundle\Controller\Backend;

use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\View\TwitterBootstrap3View;

/**
 * Class UserController
 * @package AppBundle\Controller\Backend
 *
 * @Route("/backend/user")
 */
class UserController extends Controller
{
    /**
     * Creates a new user entity.
     *
     * @Route("/new", name="user_new")
     * @Method({"GET", "POST"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction()
    {
        return $this->render(
            'backend/user/new.html.twig'
        );
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/{id}/edit", name="user_edit",options={"expose"=true})
     * @Method({"GET", "POST"})
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(User $user)
    {
        return $this->render(
            'backend/user/edit.html.twig',
            array(
                'user' => $user,
            )
        );
    }

    /**
     * Lists all user entities.
     *
     * @Route("/", options={"expose"=true}, name="user_index")
     * @Method("GET")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('AppBundle:User')->createQueryBuilder('e');
        list($filterForm, $queryBuilder) = $this->filter($queryBuilder, $request);
        list($users, $pagerHtml) = $this->paginator($queryBuilder, $request);
        $totalOfRecordsString = $this->getTotalOfRecordsString($queryBuilder, $request);

        return $this->render(
            'backend/user/index.html.twig',
            array(
                'users' => $users,
                'pagerHtml' => $pagerHtml,
                'filterForm' => $filterForm->createView(),
                'totalOfRecordsString' => $totalOfRecordsString,
            )
        );
    }

    /**
     * Create filter form and process filter request.
     *
     * @param $queryBuilder
     * @param Request $request
     * @return array
     */
    protected function filter($queryBuilder, Request $request)
    {
        $session = $request->getSession();
        $filterForm = $this->createForm('AppBundle\Form\UserFilterType');
        // Reset filter
        if ($request->get('filter_action') == 'reset') {
            $session->remove('UserControllerFilter');
        }
        // Filter action
        if ($request->get('filter_action') == 'filterr') {
            // Bind values from the request
            $filterForm->handleRequest($request);

            if ($filterForm->isValid()) {
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                $filterData = $filterForm->getData();
//                $filterData["office"] = null;
//                $filterData["company"] = null;
//                $filterData["service"] = null;
//                $filterData["role"] = null;
//                $filterData["firstName"] = null;
//                $filterData["lastName"] = null;
                $session->set('UserControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('UserControllerFilter')) {
                $filterData = $session->get('UserControllerFilter');
                foreach ($filterData as $key => $filter) {

                    //fix for entityFilterType that is loaded from session
                    if (is_object($filter)) {
                        $filterData[$key] = $queryBuilder->getEntityManager()->merge($filter);
                    }
                }
                $filterForm = $this->createForm('AppBundle\Form\UserFilterType', $filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        if ($request->get('office') > 0) {
            $officeId = $request->get('office');
            $queryBuilder->leftJoin('e.userCompany', 'uc')
                ->leftJoin('uc.company', 'ucc')
                ->leftJoin('ucc.office', 'ucco')
                ->andWhere('ucco.id = :officeId ')
                ->setParameter('officeId', $officeId);
        }

        if ($request->get('firstName') != "") {
            $firstName = $request->get('firstName');
            $queryBuilder
                ->andWhere('e.firstName LIKE :firstName')
                ->setParameter('firstName', '%'.$firstName.'%');
        }

        if ($request->get('lastName') != "") {
            $lastName = $request->get('lastName');
            $queryBuilder
                ->andWhere('e.lastName LIKE :lastName')
                ->setParameter('lastName', '%'.$lastName.'%');
        }

        if ($request->get('company') > 0) {
            $companyId = $request->get('company');
            $queryBuilder->leftJoin('e.userCompany', 'cuc')
                ->leftJoin('cuc.company', 'cucc')
                ->andWhere('cucc.id = :currentCompanyId ')
                ->setParameter('currentCompanyId', $companyId);
        }

        if ($request->get('sirenCompany') != "") {
            $businessIdentifier = $request->get('sirenCompany');
            $queryBuilder->leftJoin('e.userCompany', 'cuc')
                ->leftJoin('cuc.company', 'cucc')
                ->andWhere('cucc.businessIdentifier = :businessIdentifier ')
                ->setParameter('businessIdentifier', $businessIdentifier);
        }

        if ($request->get('service') > 0) {
            $serviceId = $request->get('service');
            $queryBuilder->leftJoin('e.userCompany', 'sauc')
                ->leftJoin('sauc.userCompanyService', 'saucs')
                ->leftJoin('saucs.service', 'ss')
                ->andWhere('ss.id = :serviceId ')
                ->setParameter('serviceId', $serviceId)
                ->andWhere('sauc.role LIKE :role ')
                ->setParameter('role', "%ROLE_USER%");
        }

        if ($request->get('role') != "") {
            $role = $request->get('role');
            $queryBuilder->leftJoin('e.userCompany', 'usco')
                ->AndWhere("usco.role LIKE :role")
                ->setParameter('role', '%'.$role.'%');
        }

        if ($request->get('username') != "") {
            $username = $request->get('username');
            $queryBuilder
                ->andWhere('e.username LIKE :username')
                ->setParameter('username', '%'.$username.'%');
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Get results from paginator and get paginator view.
     *
     * @param $queryBuilder
     * @param Request $request
     * @return array
     */
    protected function paginator($queryBuilder, Request $request)
    {
        $sortCol = $queryBuilder->getRootAlias().'.'.$request->get('pcg_sort_col', 'id');
        $queryBuilder->orderBy($sortCol, $request->get('pcg_sort_order', 'desc'));
        /*decrement paginator if role is ROLE_SUPER_ADMIN*/
        $this->countRole($queryBuilder);

        // Paginator
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage($request->get('pcg_show', 10));
        try {
            $pagerfanta->setCurrentPage($request->get('pcg_page', 1));
        } catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $ex) {
            $pagerfanta->setCurrentPage(1);
        }
        $entities = $pagerfanta->getCurrentPageResults();
        // Paginator - route generator
        $me = $this;
        $routeGenerator = function ($page) use ($me, $request) {
            $requestParams = $request->query->all();
            $requestParams['pcg_page'] = $page;

            return $me->generateUrl('user_index', $requestParams);
        };
        // Paginator - view
        $view = new TwitterBootstrap3View();
        $pagerHtml = $view->render(
            $pagerfanta,
            $routeGenerator,
            array(
                'proximity' => 3,
                'prev_message' => 'previous',
                'next_message' => 'next',
            )
        );

        return array($entities, $pagerHtml);
    }


    /**
     * Calculates the total of records string
     *
     * @param $queryBuilder
     * @param $request
     * @return string
     */
    protected function getTotalOfRecordsString($queryBuilder, $request)
    {
        $totalOfRecords = $queryBuilder->select('COUNT(e.id)')->getQuery()->getSingleScalarResult();
        $show = $request->get('pcg_show', 10);
        $page = $request->get('pcg_page', 1);
        $startRecord = ($show * ($page - 1)) + 1;
        $endRecord = $show * $page;
        if ($endRecord > $totalOfRecords) {
            $endRecord = $totalOfRecords;
        }

        return "Showing $startRecord - $endRecord of $totalOfRecords Records.";
    }

    /**
     * Finds and displays a User entity.
     *
     * @Route("/{id}", name="user_show")
     * @Method("GET")
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(User $user)
    {
        return $this->render(
            'backend/user/show.html.twig',
            array(
                'user' => $user,
            )
        );
    }

    /**
     * Delete User by id
     *
     * @Route("/delete/{id}", name="user_by_id_delete")
     * @Method("GET")
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteByIdAction(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $em->remove($user);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The user was deleted successfully');
        } catch (Exception $ex) {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the user');
        }

        return $this->redirect($this->generateUrl('user_index'));
    }

    private function countRole($queryBuilder)
    {
        $queryBuilder->andWhere('e.roles NOT LIKE :roles')
            ->setParameter('roles', '%"ROLE_SUPER_ADMIN"%');
    }


}
