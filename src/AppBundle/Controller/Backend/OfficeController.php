<?php

namespace AppBundle\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\View\TwitterBootstrap3View;

use AppBundle\Entity\Office;

/**
 * Class OfficeController
 * @package AppBundle\Controller\Backend
 *
 * @Route("/backend/office")
 */
class OfficeController extends Controller
{
    /**
     * Lists all Office entities.
     *
     * @Route("/", name="office")
     * @Method("GET")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('AppBundle:Office')->createQueryBuilder('e');
        $notificationForm = $this->createForm('AppBundle\Form\NotificationOfficeType');
        list($filterForm, $queryBuilder) = $this->filter($queryBuilder, $request);
        list($offices, $pagerHtml) = $this->paginator($queryBuilder, $request);
        $totalOfRecordsString = $this->getTotalOfRecordsString($queryBuilder, $request);

        return $this->render(
            'backend/office/index.html.twig',
            array(
                'offices' => $offices,
                'pagerHtml' => $pagerHtml,
                'filterForm' => $filterForm->createView(),
                'notificationForm' => $notificationForm->createView(),
                'totalOfRecordsString' => $totalOfRecordsString,
            )
        );
    }

    /**
     * Create filter form and process filter request.
     *
     * @param $queryBuilder
     * @param Request $request
     * @return array
     */
    protected function filter($queryBuilder, Request $request)
    {
        $session = $request->getSession();
        $filterForm = $this->createForm('AppBundle\Form\OfficeFilterType');
        // Reset filter
        if ($request->get('filter_action') == 'reset') {
            $session->remove('OfficeControllerFilter');
        }
        // Filter action
        if ($request->get('filter_action') == 'filter') {
            // Bind values from the request
            $filterForm->handleRequest($request);

            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('OfficeControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('OfficeControllerFilter')) {
                $filterData = $session->get('OfficeControllerFilter');
                foreach ($filterData as $key => $filter) {
                    //fix for entityFilterType that is loaded from session
                    if (is_object($filter)) {
                        $filterData[$key] = $queryBuilder->getEntityManager()->merge($filter);
                    }
                }
                $filterForm = $this->createForm('AppBundle\Form\OfficeFilterType', $filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }


    /**
     * Get results from paginator and get paginator view.
     *
     * @param $queryBuilder
     * @param Request $request
     * @return array
     */
    protected function paginator($queryBuilder, Request $request)
    {
        //sorting
        $sortCol = $queryBuilder->getRootAlias().'.'.$request->get('pcg_sort_col', 'id');
        $queryBuilder->orderBy($sortCol, $request->get('pcg_sort_order', 'desc'));
        // Paginator
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage($request->get('pcg_show', 10));

        try {
            $pagerfanta->setCurrentPage($request->get('pcg_page', 1));
        } catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $ex) {
            $pagerfanta->setCurrentPage(1);
        }

        $entities = $pagerfanta->getCurrentPageResults();

        // Paginator - route generator
        $me = $this;
        $routeGenerator = function ($page) use ($me, $request) {
            $requestParams = $request->query->all();
            $requestParams['pcg_page'] = $page;

            return $me->generateUrl('office', $requestParams);
        };

        // Paginator - view
        $view = new TwitterBootstrap3View();
        $pagerHtml = $view->render(
            $pagerfanta,
            $routeGenerator,
            array(
                'proximity' => 3,
                'prev_message' => 'previous',
                'next_message' => 'next',
            )
        );

        return array($entities, $pagerHtml);
    }


    /*
     * Calculates the total of records string
     *
     * @param $queryBuilder
     * @param $request
     * @return string
     */
    protected function getTotalOfRecordsString($queryBuilder, $request)
    {
        $totalOfRecords = $queryBuilder->select('COUNT(e.id)')->getQuery()->getSingleScalarResult();
        $show = $request->get('pcg_show', 10);
        $page = $request->get('pcg_page', 1);

        $startRecord = ($show * ($page - 1)) + 1;
        $endRecord = $show * $page;

        if ($endRecord > $totalOfRecords) {
            $endRecord = $totalOfRecords;
        }

        return "Showing $startRecord - $endRecord of $totalOfRecords Records.";
    }


    /**
     * Displays a form to create a new Office entity.
     *
     * @Route("/new", name="office_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $office = new Office();
        $form = $this->createForm('AppBundle\Form\OfficeType', $office);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($office);
            $em->flush();

            $editLink = $this->generateUrl('office_edit', array('id' => $office->getId()));
            $this->get('session')->getFlashBag()->add(
                'success',
                "<a href='$editLink'>New office was created successfully.</a>"
            );

            $nextAction = $request->get('submit') == 'save' ? 'office' : 'office_new';

            return $this->redirectToRoute($nextAction);
        }

        return $this->render(
            'backend/office/new.html.twig',
            array(
                'office' => $office,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * Finds and displays a Office entity.
     *
     * @Route("/{id}", name="office_show")
     * @Method("GET")
     * @param Office $office
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Office $office)
    {
        $deleteForm = $this->createDeleteForm($office);

        return $this->render(
            'backend/office/show.html.twig',
            array(
                'office' => $office,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Displays a form to edit an existing Office entity.
     *
     * @Route("/{id}/edit", name="office_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Office $office
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Office $office)
    {
        $deleteForm = $this->createDeleteForm($office);
        $editForm = $this->createForm('AppBundle\Form\OfficeType', $office);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($office);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Edited Successfully!');

            return $this->redirectToRoute('office_edit', array('id' => $office->getId()));
        }

        return $this->render(
            'backend/office/edit.html.twig',
            array(
                'office' => $office,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a Office entity.
     *
     * @Route("/{id}", name="office_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param Office $office
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Office $office)
    {
        $form = $this->createDeleteForm($office);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($office);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The Office was deleted successfully');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the Office');
        }

        return $this->redirectToRoute('office');
    }

    /**
     * Creates a form to delete a Office entity.
     *
     * @return \Symfony\Component\Form\Form The form
     * @param Office $office
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(Office $office)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('office_delete', array('id' => $office->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Delete Office by id
     *
     * @Route("/delete/{id}", name="office_by_id_delete")
     * @Method("GET")
     * @param Office $office
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteByIdAction(Office $office)
    {
        $em = $this->getDoctrine()->getManager();

        try {
            $em->remove($office);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The Office was deleted successfully');
        } catch (Exception $ex) {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the Office');
        }

        return $this->redirect($this->generateUrl('office'));

    }


    /**
     * Bulk Action
     * @Route("/bulk-action/", name="office_bulk_action")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function bulkAction(Request $request)
    {
        $ids = $request->get("ids", array());
        $action = $request->get("bulk_action", "delete");

        if ($action == "delete") {
            try {
                $em = $this->getDoctrine()->getManager();
                $repository = $em->getRepository('AppBundle:Office');

                foreach ($ids as $id) {
                    $office = $repository->find($id);
                    $em->remove($office);
                    $em->flush();
                }

                $this->get('session')->getFlashBag()->add('success', 'offices was deleted successfully!');

            } catch (Exception $ex) {
                $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the offices ');
            }
        }

        return $this->redirect($this->generateUrl('office'));
    }

}
