<?php

namespace AppBundle\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\View\TwitterBootstrap3View;
use AppBundle\Entity\TemplateService;

/**
 * Class TemplateServiceController
 * @package AppBundle\Controller\Backend
 *
 * @Route("/backend/templateservice")
 */
class TemplateServiceController extends Controller
{
    /**
     * Lists all TemplateService entities.
     *
     * @Route("/", name="templateservice")
     * @Method("GET")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('AppBundle:TemplateService')->createQueryBuilder('e');

        list($filterForm, $queryBuilder) = $this->filter($queryBuilder, $request);
        list($templateServices, $pagerHtml) = $this->paginator($queryBuilder, $request);

        $totalOfRecordsString = $this->getTotalOfRecordsString($queryBuilder, $request);

        return $this->render(
            'backend/templateservice/index.html.twig',
            array(
                'templateServices' => $templateServices,
                'pagerHtml' => $pagerHtml,
                'filterForm' => $filterForm->createView(),
                'totalOfRecordsString' => $totalOfRecordsString,

            )
        );
    }

    /**
     * Create filter form and process filter request.
     *
     * @param $queryBuilder
     * @param Request $request
     * @return array
     */
    protected function filter($queryBuilder, Request $request)
    {
        $session = $request->getSession();
        $filterForm = $this->createForm('AppBundle\Form\TemplateServiceFilterType');
        // Reset filter
        if ($request->get('filter_action') == 'reset') {
            $session->remove('TemplateServiceControllerFilter');
        }
        // Filter action
        if ($request->get('filter_action') == 'filter') {
            // Bind values from the request
            $filterForm->handleRequest($request);

            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('TemplateServiceControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('TemplateServiceControllerFilter')) {
                $filterData = $session->get('TemplateServiceControllerFilter');
                foreach ($filterData as $key => $filter) { //fix for entityFilterType that is loaded from session
                    if (is_object($filter)) {
                        $filterData[$key] = $queryBuilder->getEntityManager()->merge($filter);
                    }
                }
                $filterForm = $this->createForm('AppBundle\Form\TemplateServiceFilterType', $filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Get results from paginator and get paginator view.
     *
     * @param $queryBuilder
     * @param Request $request
     * @return array
     */
    protected function paginator($queryBuilder, Request $request)
    {
        //sorting
        $sortCol = $queryBuilder->getRootAlias().'.'.$request->get('pcg_sort_col', 'id');
        $queryBuilder->orderBy($sortCol, $request->get('pcg_sort_order', 'desc'));
        // Paginator
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage($request->get('pcg_show', 10));
        try {
            $pagerfanta->setCurrentPage($request->get('pcg_page', 1));
        } catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $ex) {
            $pagerfanta->setCurrentPage(1);
        }
        $entities = $pagerfanta->getCurrentPageResults();
        // Paginator - route generator
        $me = $this;
        $routeGenerator = function ($page) use ($me, $request) {
            $requestParams = $request->query->all();
            $requestParams['pcg_page'] = $page;

            return $me->generateUrl('templateservice', $requestParams);
        };
        // Paginator - view
        $view = new TwitterBootstrap3View();
        $pagerHtml = $view->render(
            $pagerfanta,
            $routeGenerator,
            array(
                'proximity' => 3,
                'prev_message' => 'previous',
                'next_message' => 'next',
            )
        );

        return array($entities, $pagerHtml);
    }

    /*
     * Calculates the total of records string
     *
     * @param $queryBuilder
     * @param $request
     * @return string
     */
    protected function getTotalOfRecordsString($queryBuilder, $request)
    {
        $totalOfRecords = $queryBuilder->select('COUNT(e.id)')->getQuery()->getSingleScalarResult();
        $show = $request->get('pcg_show', 10);
        $page = $request->get('pcg_page', 1);
        $startRecord = ($show * ($page - 1)) + 1;
        $endRecord = $show * $page;
        if ($endRecord > $totalOfRecords) {
            $endRecord = $totalOfRecords;
        }

        return "Showing $startRecord - $endRecord of $totalOfRecords Records.";
    }

    /**
     * Displays a form to create a new TemplateService entity.
     *
     * @Route("/new", name="templateservice_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $templateService = new TemplateService();
        $form = $this->createForm('AppBundle\Form\TemplateServiceType', $templateService);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($templateService);
            $em->flush();

            $editLink = $this->generateUrl('templateservice_edit', array('id' => $templateService->getId()));
            $this->get('session')->getFlashBag()->add(
                'success',
                "<a href='$editLink'>New templateService was created successfully.</a>"
            );

            $nextAction = $request->get('submit') == 'save' ? 'templateservice' : 'templateservice_new';

            return $this->redirectToRoute($nextAction);
        }

        return $this->render(
            'backend/templateservice/new.html.twig',
            array(
                'templateService' => $templateService,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * Finds and displays a TemplateService entity.
     *
     * @Route("/{id}", name="templateservice_show")
     * @Method("GET")
     * @param TemplateService $templateService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(TemplateService $templateService)
    {
        $deleteForm = $this->createDeleteForm($templateService);

        return $this->render(
            'backend/templateservice/show.html.twig',
            array(
                'templateService' => $templateService,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Displays a form to edit an existing TemplateService entity.
     *
     * @Route("/{id}/edit", name="templateservice_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param TemplateService $templateService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, TemplateService $templateService)
    {
        $deleteForm = $this->createDeleteForm($templateService);
        $editForm = $this->createForm('AppBundle\Form\TemplateServiceType', $templateService);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($templateService);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'Edited Successfully!');

            return $this->redirectToRoute('templateservice_edit', array('id' => $templateService->getId()));
        }

        return $this->render(
            'backend/templateservice/edit.html.twig',
            array(
                'templateService' => $templateService,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a TemplateService entity.
     *
     * @Route("/{id}", name="templateservice_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param TemplateService $templateService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, TemplateService $templateService)
    {
        $form = $this->createDeleteForm($templateService);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($templateService);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The TemplateService was deleted successfully');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the TemplateService');
        }

        return $this->redirectToRoute('templateservice');
    }

    /**
     * Creates a form to delete a TemplateService entity.
     *
     * @param TemplateService $templateService
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(TemplateService $templateService)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('templateservice_delete', array('id' => $templateService->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Delete TemplateService by id
     *
     * @Route("/delete/{id}", name="templateservice_by_id_delete")
     * @Method("GET")
     * @param TemplateService $templateService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteByIdAction(TemplateService $templateService)
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $em->remove($templateService);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The TemplateService was deleted successfully');
        } catch (Exception $ex) {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the TemplateService');
        }

        return $this->redirect($this->generateUrl('templateservice'));
    }

    /**
     * Bulk Action
     * @Route("/bulk-action/", name="templateservice_bulk_action")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function bulkAction(Request $request)
    {
        $ids = $request->get("ids", array());
        $action = $request->get("bulk_action", "delete");
        if ($action == "delete") {
            try {
                $em = $this->getDoctrine()->getManager();
                $repository = $em->getRepository('AppBundle:TemplateService');

                foreach ($ids as $id) {
                    $templateService = $repository->find($id);
                    $em->remove($templateService);
                    $em->flush();
                }
                $this->get('session')->getFlashBag()->add('success', 'templateServices was deleted successfully!');
            } catch (Exception $ex) {
                $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the templateServices ');
            }
        }

        return $this->redirect($this->generateUrl('templateservice'));
    }
}
