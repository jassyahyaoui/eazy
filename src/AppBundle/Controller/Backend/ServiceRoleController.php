<?php

namespace AppBundle\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\View\TwitterBootstrap3View;

use AppBundle\Entity\ServiceRole;

/**
 * Class ServiceRoleController
 * @package AppBundle\Controller
 *
 * @Route("/backend/service_role")
 */
class ServiceRoleController extends Controller
{
    /**
     * Lists all ServiceRole entities.
     *
     * @Route("/", name="servicerole")
     * @Method("GET")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('AppBundle:ServiceRole')->createQueryBuilder('e');

        list($filterForm, $queryBuilder) = $this->filter($queryBuilder, $request);
        list($serviceRoles, $pagerHtml) = $this->paginator($queryBuilder, $request);

        $totalOfRecordsString = $this->getTotalOfRecordsString($queryBuilder, $request);

        return $this->render(
            'backend/servicerole/index.html.twig',
            array(
                'serviceRoles' => $serviceRoles,
                'pagerHtml' => $pagerHtml,
                'filterForm' => $filterForm->createView(),
                'totalOfRecordsString' => $totalOfRecordsString,

            )
        );
    }

    /**
     * Create filter form and process filter request.
     *
     * @param $queryBuilder
     * @param Request $request
     * @return array
     */
    protected function filter($queryBuilder, Request $request)
    {
        $session = $request->getSession();
        $filterForm = $this->createForm('AppBundle\Form\ServiceRoleFilterType');

        // Reset filter
        if ($request->get('filter_action') == 'reset') {
            $session->remove('ServiceRoleControllerFilter');
        }

        // Filter action
        if ($request->get('filter_action') == 'filter') {
            // Bind values from the request
            $filterForm->handleRequest($request);

            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('ServiceRoleControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('ServiceRoleControllerFilter')) {
                $filterData = $session->get('ServiceRoleControllerFilter');

                foreach ($filterData as $key => $filter) { //fix for entityFilterType that is loaded from session
                    if (is_object($filter)) {
                        $filterData[$key] = $queryBuilder->getEntityManager()->merge($filter);
                    }
                }

                $filterForm = $this->createForm('AppBundle\Form\ServiceRoleFilterType', $filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }


    /**
     * Get results from paginator and get paginator view.
     *
     * @param $queryBuilder
     * @param Request $request
     * @return array
     */
    protected function paginator($queryBuilder, Request $request)
    {
        //sorting
        $sortCol = $queryBuilder->getRootAlias().'.'.$request->get('pcg_sort_col', 'id');
        $queryBuilder->orderBy($sortCol, $request->get('pcg_sort_order', 'desc'));
        // Paginator
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage($request->get('pcg_show', 10));

        try {
            $pagerfanta->setCurrentPage($request->get('pcg_page', 1));
        } catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $ex) {
            $pagerfanta->setCurrentPage(1);
        }

        $entities = $pagerfanta->getCurrentPageResults();

        // Paginator - route generator
        $me = $this;
        $routeGenerator = function ($page) use ($me, $request) {
            $requestParams = $request->query->all();
            $requestParams['pcg_page'] = $page;

            return $me->generateUrl('servicerole', $requestParams);
        };

        // Paginator - view
        $view = new TwitterBootstrap3View();
        $pagerHtml = $view->render(
            $pagerfanta,
            $routeGenerator,
            array(
                'proximity' => 3,
                'prev_message' => 'previous',
                'next_message' => 'next',
            )
        );

        return array($entities, $pagerHtml);
    }


    /*
     * Calculates the total of records string
     *
     * @param $queryBuilder
     * @param $request
     * @return string
     */
    protected function getTotalOfRecordsString($queryBuilder, $request)
    {
        $totalOfRecords = $queryBuilder->select('COUNT(e.id)')->getQuery()->getSingleScalarResult();
        $show = $request->get('pcg_show', 10);
        $page = $request->get('pcg_page', 1);

        $startRecord = ($show * ($page - 1)) + 1;
        $endRecord = $show * $page;

        if ($endRecord > $totalOfRecords) {
            $endRecord = $totalOfRecords;
        }

        return "Showing $startRecord - $endRecord of $totalOfRecords Records.";
    }


    /**
     * Displays a form to create a new ServiceRole entity.
     * @Route("/new", name="servicerole_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $serviceRole = new ServiceRole();
        $form = $this->createForm('AppBundle\Form\ServiceRoleType', $serviceRole);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($serviceRole);
            $em->flush();

            $editLink = $this->generateUrl('servicerole_edit', array('id' => $serviceRole->getId()));
            $this->get('session')->getFlashBag()->add(
                'success',
                "<a href='$editLink'>New serviceRole was created successfully.</a>"
            );

            $nextAction = $request->get('submit') == 'save' ? 'servicerole' : 'servicerole_new';

            return $this->redirectToRoute($nextAction);
        }

        return $this->render(
            'backend/servicerole/new.html.twig',
            array(
                'serviceRole' => $serviceRole,
                'form' => $form->createView(),
            )
        );
    }


    /**
     * Finds and displays a ServiceRole entity.
     *
     * @Route("/{id}", name="servicerole_show")
     * @Method("GET")
     * @param ServiceRole $serviceRole
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(ServiceRole $serviceRole)
    {
        $deleteForm = $this->createDeleteForm($serviceRole);

        return $this->render(
            'backend/servicerole/show.html.twig',
            array(
                'serviceRole' => $serviceRole,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }


    /**
     * Displays a form to edit an existing ServiceRole entity.
     *
     * @Route("/{id}/edit", name="servicerole_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param ServiceRole $serviceRole
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, ServiceRole $serviceRole)
    {
        $deleteForm = $this->createDeleteForm($serviceRole);
        $editForm = $this->createForm('AppBundle\Form\ServiceRoleType', $serviceRole);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($serviceRole);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Edited Successfully!');

            return $this->redirectToRoute('servicerole_edit', array('id' => $serviceRole->getId()));
        }

        return $this->render(
            'backend/servicerole/edit.html.twig',
            array(
                'serviceRole' => $serviceRole,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }


    /**
     * Deletes a ServiceRole entity.
     *
     * @Route("/{id}", name="servicerole_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param ServiceRole $serviceRole
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, ServiceRole $serviceRole)
    {
        $form = $this->createDeleteForm($serviceRole);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($serviceRole);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The ServiceRole was deleted successfully');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the ServiceRole');
        }

        return $this->redirectToRoute('servicerole');
    }

    /**
     * Creates a form to delete a ServiceRole entity.
     *
     * @param ServiceRole $serviceRole
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(ServiceRole $serviceRole)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('servicerole_delete', array('id' => $serviceRole->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Delete ServiceRole by id
     *
     * @Route("/delete/{id}", name="servicerole_by_id_delete")
     * @Method("GET")
     * @param ServiceRole $serviceRole
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteByIdAction(ServiceRole $serviceRole)
    {
        $em = $this->getDoctrine()->getManager();

        try {
            $em->remove($serviceRole);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The ServiceRole was deleted successfully');
        } catch (Exception $ex) {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the ServiceRole');
        }

        return $this->redirect($this->generateUrl('servicerole'));

    }


    /**
     * Bulk Action
     * @Route("/bulk-action/", name="servicerole_bulk_action")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function bulkAction(Request $request)
    {
        $ids = $request->get("ids", array());
        $action = $request->get("bulk_action", "delete");

        if ($action == "delete") {
            try {
                $em = $this->getDoctrine()->getManager();
                $repository = $em->getRepository('AppBundle:ServiceRole');

                foreach ($ids as $id) {
                    $serviceRole = $repository->find($id);
                    $em->remove($serviceRole);
                    $em->flush();
                }

                $this->get('session')->getFlashBag()->add('success', 'serviceRoles was deleted successfully!');

            } catch (Exception $ex) {
                $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the serviceRoles ');
            }
        }

        return $this->redirect($this->generateUrl('servicerole'));
    }

}
