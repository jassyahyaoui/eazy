<?php

namespace AppBundle\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\View\TwitterBootstrap3View;

use AppBundle\Entity\Thumbnail;

/**
 * Class ThumbnailController
 * @package AppBundle\Controller
 *
 * @Route("/backend/thumbnail")
 */
class ThumbnailController extends Controller
{
    /**
     * Lists all Thumbnail entities.
     *
     * @Route("/", name="thumbnail")
     * @Method("GET")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('AppBundle:Thumbnail')->createQueryBuilder('e');

        list($filterForm, $queryBuilder) = $this->filter($queryBuilder, $request);
        list($thumbnails, $pagerHtml) = $this->paginator($queryBuilder, $request);

        $totalOfRecordsString = $this->getTotalOfRecordsString($queryBuilder, $request);

        return $this->render(
            'thumbnail/index.html.twig',
            array(
                'thumbnails' => $thumbnails,
                'pagerHtml' => $pagerHtml,
                'filterForm' => $filterForm->createView(),
                'totalOfRecordsString' => $totalOfRecordsString,

            )
        );
    }

    /**
     * Create filter form and process filter request.
     *
     * @param $queryBuilder
     * @param Request $request
     * @return array
     */
    protected function filter($queryBuilder, Request $request)
    {
        $session = $request->getSession();
        $filterForm = $this->createForm('AppBundle\Form\ThumbnailFilterType');

        // Reset filter
        if ($request->get('filter_action') == 'reset') {
            $session->remove('ThumbnailControllerFilter');
        }

        // Filter action
        if ($request->get('filter_action') == 'filter') {
            // Bind values from the request
            $filterForm->handleRequest($request);

            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('ThumbnailControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('ThumbnailControllerFilter')) {
                $filterData = $session->get('ThumbnailControllerFilter');

                foreach ($filterData as $key => $filter) {
                    //fix for entityFilterType that is loaded from session
                    if (is_object($filter)) {
                        $filterData[$key] = $queryBuilder->getEntityManager()->merge($filter);
                    }
                }

                $filterForm = $this->createForm('AppBundle\Form\ThumbnailFilterType', $filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Get results from paginator and get paginator view.
     *
     * @param $queryBuilder
     * @param Request $request
     * @return array
     */
    protected function paginator($queryBuilder, Request $request)
    {
        //sorting
        $sortCol = $queryBuilder->getRootAlias().'.'.$request->get('pcg_sort_col', 'id');
        $queryBuilder->orderBy($sortCol, $request->get('pcg_sort_order', 'desc'));
        // Paginator
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage($request->get('pcg_show', 10));

        try {
            $pagerfanta->setCurrentPage($request->get('pcg_page', 1));
        } catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $ex) {
            $pagerfanta->setCurrentPage(1);
        }

        $entities = $pagerfanta->getCurrentPageResults();

        // Paginator - route generator
        $me = $this;
        $routeGenerator = function ($page) use ($me, $request) {
            $requestParams = $request->query->all();
            $requestParams['pcg_page'] = $page;

            return $me->generateUrl('thumbnail', $requestParams);
        };

        // Paginator - view
        $view = new TwitterBootstrap3View();
        $pagerHtml = $view->render(
            $pagerfanta,
            $routeGenerator,
            array(
                'proximity' => 3,
                'prev_message' => 'previous',
                'next_message' => 'next',
            )
        );

        return array($entities, $pagerHtml);
    }

    /*
     * Calculates the total of records string
     *
     * @param $queryBuilder
     * @param $request
     * @return string
     */
    protected function getTotalOfRecordsString($queryBuilder, $request)
    {
        $totalOfRecords = $queryBuilder->select('COUNT(e.id)')->getQuery()->getSingleScalarResult();
        $show = $request->get('pcg_show', 10);
        $page = $request->get('pcg_page', 1);

        $startRecord = ($show * ($page - 1)) + 1;
        $endRecord = $show * $page;

        if ($endRecord > $totalOfRecords) {
            $endRecord = $totalOfRecords;
        }

        return "Showing $startRecord - $endRecord of $totalOfRecords Records.";
    }

    /**
     * Displays a form to create a new Thumbnail entity.
     *
     * @Route("/new", name="thumbnail_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $thumbnail = new Thumbnail();
        $form = $this->createForm('AppBundle\Form\ThumbnailType', $thumbnail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($thumbnail);
            $em->flush();

            $editLink = $this->generateUrl('thumbnail_edit', array('id' => $thumbnail->getId()));
            $this->get('session')->getFlashBag()->add(
                'success',
                "<a href='$editLink'>New thumbnail was created successfully.</a>"
            );

            $nextAction = $request->get('submit') == 'save' ? 'thumbnail' : 'thumbnail_new';

            return $this->redirectToRoute($nextAction);
        }

        return $this->render(
            'thumbnail/new.html.twig',
            array(
                'thumbnail' => $thumbnail,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * Finds and displays a Thumbnail entity.
     *
     * @Route("/{id}", name="thumbnail_show")
     * @Method("GET")
     * @param Thumbnail $thumbnail
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Thumbnail $thumbnail)
    {
        $deleteForm = $this->createDeleteForm($thumbnail);

        return $this->render(
            'thumbnail/show.html.twig',
            array(
                'thumbnail' => $thumbnail,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Displays a form to edit an existing Thumbnail entity.
     *
     * @Route("/{id}/edit", name="thumbnail_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Thumbnail $thumbnail
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Thumbnail $thumbnail)
    {
        $deleteForm = $this->createDeleteForm($thumbnail);
        $editForm = $this->createForm('AppBundle\Form\ThumbnailType', $thumbnail);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($thumbnail);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Edited Successfully!');

            return $this->redirectToRoute('thumbnail_edit', array('id' => $thumbnail->getId()));
        }

        return $this->render(
            'thumbnail/edit.html.twig',
            array(
                'thumbnail' => $thumbnail,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a Thumbnail entity.
     *
     * @Route("/{id}", name="thumbnail_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param Thumbnail $thumbnail
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Thumbnail $thumbnail)
    {
        $form = $this->createDeleteForm($thumbnail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($thumbnail);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The Thumbnail was deleted successfully');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the Thumbnail');
        }

        return $this->redirectToRoute('thumbnail');
    }

    /**
     * Creates a form to delete a Thumbnail entity.
     *
     * @param Thumbnail $thumbnail
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(Thumbnail $thumbnail)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('thumbnail_delete', array('id' => $thumbnail->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Delete Thumbnail by id
     *
     * @Route("/delete/{id}", name="thumbnail_by_id_delete")
     * @Method("GET")
     * @param Thumbnail $thumbnail
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteByIdAction(Thumbnail $thumbnail)
    {
        $em = $this->getDoctrine()->getManager();

        try {
            $em->remove($thumbnail);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The Thumbnail was deleted successfully');
        } catch (Exception $ex) {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the Thumbnail');
        }

        return $this->redirect($this->generateUrl('thumbnail'));

    }

    /**
     * Bulk Action
     * @Route("/bulk-action/", name="thumbnail_bulk_action")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function bulkAction(Request $request)
    {
        $ids = $request->get("ids", array());
        $action = $request->get("bulk_action", "delete");

        if ($action == "delete") {
            try {
                $em = $this->getDoctrine()->getManager();
                $repository = $em->getRepository('AppBundle:Thumbnail');

                foreach ($ids as $id) {
                    $thumbnail = $repository->find($id);
                    $em->remove($thumbnail);
                    $em->flush();
                }

                $this->get('session')->getFlashBag()->add('success', 'thumbnails was deleted successfully!');

            } catch (Exception $ex) {
                $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the thumbnails ');
            }
        }

        return $this->redirect($this->generateUrl('thumbnail'));
    }
}
