<?php

namespace AppBundle\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\View\TwitterBootstrap3View;
use Mgilet\NotificationBundle\Entity\NotifiableNotification;

/**
 * Class NotificationController
 * @package AppBundle\Controller\Backend
 *
 * @Route("/backend/notification")
 */
class NotificationController extends Controller
{

    /**
     * @Route("/notification_list", name="notification_list")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $notifiableRepo = $this->get('doctrine.orm.entity_manager')->getRepository(
            'MgiletNotificationBundle:NotifiableNotification'
        );
        $user = $this->get('security.token_storage')->getToken()->getUser();

        return $this->render(
            'backend/notification/list.html.twig',
            array('notifiableNotifications' => $notifiableRepo->findAllForNotifiable($user, "AppBundle\Entity\User"))
        );
    }

    /**
     * @Route("/", name="notification")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $notificationForm = $this->createForm('AppBundle\Form\NotificationType');
        $queryBuilder = $em->getRepository('MgiletNotificationBundle:NotifiableNotification')->createQueryBuilder('e');
        list($filterForm, $queryBuilder) = $this->filter($queryBuilder, $request);
        $totalOfRecordsString = $this->getTotalOfRecordsString($queryBuilder, $request);
        $query = $this->getDoctrine()
            ->getRepository('MgiletNotificationBundle:NotifiableNotification')
            ->createQueryBuilder('nn')
            ->join('nn.notification', 'n')
            ->join('nn.notifiableEntity', 'ne')
            ->addSelect('n')
            ->addSelect('ne')
            ->getQuery();
        $notifiableNotifications = $query->getResult();

        return $this->render(
            'backend/notification/index.html.twig',
            array(
                'notifiableNotifications' => $notifiableNotifications,
                'notificationForm' => $notificationForm->createView(),
                'filterForm' => $filterForm->createView(),
                'totalOfRecordsString' => $totalOfRecordsString,
            )
        );
    }

    /**
     * Create filter form and process filter request.
     *
     * @param $queryBuilder
     * @param Request $request
     * @return array
     */
    protected function filter($queryBuilder, Request $request)
    {
        $session = $request->getSession();
        $filterForm = $this->createForm('AppBundle\Form\NotificationFilterType');
        // Reset filter
        if ($request->get('filter_action') == 'reset') {
            $session->remove('NotificationControllerFilter');
        }
        // Filter action
        if ($request->get('filter_action') == 'filter') {
            // Bind values from the request
            $filterForm->handleRequest($request);
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('NotificationControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('NotificationControllerFilter')) {
                $filterData = $session->get('NotificationControllerFilter');

                foreach ($filterData as $key => $filter) {
                    //fix for entityFilterType that is loaded from session
                    if (is_object($filter)) {
                        $filterData[$key] = $queryBuilder->getEntityManager()->merge($filter);
                    }
                }
                $filterForm = $this->createForm('AppBundle\Form\NotificationFilterType', $filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Get results from paginator and get paginator view.
     *
     * @param $queryBuilder
     * @param Request $request
     * @return array
     */
    protected function paginator($queryBuilder, Request $request)
    {
        //sorting
        $sortCol = $queryBuilder->getRootAlias().'.'.$request->get('pcg_sort_col', 'id');
        $queryBuilder->orderBy($sortCol, $request->get('pcg_sort_order', 'desc'));
        // Paginator
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage($request->get('pcg_show', 10));
        try {
            $pagerfanta->setCurrentPage($request->get('pcg_page', 1));
        } catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $ex) {
            $pagerfanta->setCurrentPage(1);
        }
        $entities = $pagerfanta->getCurrentPageResults();
        // Paginator - route generator
        $me = $this;
        $routeGenerator = function ($page) use ($me, $request) {
            $requestParams = $request->query->all();
            $requestParams['pcg_page'] = $page;

            return $me->generateUrl('office', $requestParams);
        };
        // Paginator - view
        $view = new TwitterBootstrap3View();
        $pagerHtml = $view->render(
            $pagerfanta,
            $routeGenerator,
            array(
                'proximity' => 3,
                'prev_message' => 'previous',
                'next_message' => 'next',
            )
        );

        return array($entities, $pagerHtml);
    }

    /*
     * Calculates the total of records string
     *
     * @param $queryBuilder
     * @param $request
     * @return string
     */
    protected function getTotalOfRecordsString($queryBuilder, $request)
    {
        $totalOfRecords = $queryBuilder->select('COUNT(e.id)')->getQuery()->getSingleScalarResult();
        $show = $request->get('pcg_show', 10);
        $page = $request->get('pcg_page', 1);

        $startRecord = ($show * ($page - 1)) + 1;
        $endRecord = $show * $page;

        if ($endRecord > $totalOfRecords) {
            $endRecord = $totalOfRecords;
        }

        return "Showing $startRecord - $endRecord of $totalOfRecords Records.";
    }

    /**
     * Finds and displays a Office entity.
     *
     * @Route("/{id}", name="notification_show")
     * @Method("GET")
     * @param NotifiableNotification $notifiableNotification
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(NotifiableNotification $notifiableNotification)
    {
        $deleteForm = $this->createDeleteForm($notifiableNotification);

        return $this->render(
            'backend/notification/show.html.twig',
            array(
                'notifiableNotification' => $notifiableNotification,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a notification entity.
     *
     * @Route("/{id}", name="notification_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param NotifiableNotification $notifiableNotification
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, NotifiableNotification $notifiableNotification)
    {

        $form = $this->createDeleteForm($notifiableNotification);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($notifiableNotification);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The notification was deleted successfully');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the notification');
        }

        return $this->redirectToRoute('notification');
    }

    /**
     * Creates a form to delete a Office entity.
     *
     * @param NotifiableNotification $notifiableNotification
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(NotifiableNotification $notifiableNotification)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('notification_delete', array('id' => $notifiableNotification->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Delete notification by id
     *
     * @Route("/delete/{id}", name="notification_by_id_delete")
     * @Method("GET")
     * @param NotifiableNotification $notifiableNotification
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteByIdAction(NotifiableNotification $notifiableNotification)
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $em->remove($notifiableNotification);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The notification was deleted successfully');
        } catch (Exception $ex) {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the notification');
        }

        return $this->redirect($this->generateUrl('notification'));
    }

    /**
     * Bulk Action
     * @Route("/bulk_action/", name="notification_bulk_action")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function bulkAction(Request $request)
    {
        $ids = $request->get("ids", array());
        $action = $request->get("bulk_action", "delete");
        if ($action == "delete") {
            try {
                $em = $this->getDoctrine()->getManager();
                $repository = $em->getRepository('MgiletNotificationBundle:NotifiableNotification');
                foreach ($ids as $id) {
                    $office = $repository->find($id);
                    $em->remove($office);
                    $em->flush();
                }
                $this->get('session')->getFlashBag()->add('success', 'notifications was deleted successfully!');
            } catch (Exception $ex) {
                $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the notification ');
            }
        }

        return $this->redirect($this->generateUrl('notification'));
    }

    /**
     * @Route("/create_notification_office", name="post_notification_office")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function postNotificationUsersFromOfficeAction(Request $request)
    {
        $notificationData = $request->request->get("notification");
        if (!is_array($notificationData)) {
            return $this->redirectToRoute('notification');
        }
        $this->container->get('notification.service')->createNotificationUsersFromOfficeAction($notificationData);

        return $this->redirectToRoute('notification');
    }

    /**
     * @Route("/create_notification_user", name="post_notification_user")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function postNotificationUserAction(Request $request)
    {
        $notificationData = $request->request->get("notification");
        $this->container->get('notification.service')->createNotificationUserAction($notificationData);

        return $this->redirectToRoute('notification');
    }

    /**
     * @Route("/create_notification_owner", name="post_notification_owner")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function postNotificationOwnerAction(Request $request)
    {
        $notificationData = $request->request->get("notification");
        $this->container->get('notification.service')->createNotificationOwnerAction($notificationData);

        return $this->redirectToRoute('notification');
    }

    /**
     * @Route("/create_notification_role", name="post_notification_role")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function postNotificationRoleAction(Request $request)
    {
        $notificationData = $request->request->get("notification");
        if (!is_array($notificationData)) {
            return $this->redirectToRoute('notification');
        }
        $this->container->get('notification.service')->createNotificationRole($notificationData);

        return $this->redirectToRoute('notification');
    }
}