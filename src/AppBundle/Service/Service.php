<?php

namespace AppBundle\Service;

use AppBundle\Entity\UserCompanyService;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\ORM\EntityManager;

class Service extends AbstractService
{

    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
    }

    /**
     * @param UserCompanyService $entity
     * @return array
     */
    public function getAllRolesByService(UserCompanyService $entity)
    {
        $userCompanyServiceRoles = $entity->getUserCompanyServiceRoles()->getValues();
        $data = [];
        for ($i = 0; $i < sizeof($userCompanyServiceRoles); $i++) {
            if (!empty($userCompanyServiceRoles[$i]->getServiceRole())) {
                $data[$i]['label'] = $userCompanyServiceRoles[$i]->getServiceRole()->getService()->getName();
                $data[$i]['value'] = array(
                    'serviceRoleId' => $userCompanyServiceRoles[$i]->getServiceRole()->getId(),
                    'serviceId' => $userCompanyServiceRoles[$i]->getServiceRole()->getService()->getId(),
                    'serviceName' => $userCompanyServiceRoles[$i]->getServiceRole()->getService()->getName(),
                );
            } else {
                $data[$i]['label'] = null;
                $data[$i]['value'] = array(
                    'serviceId' => null,
                    'serviceRoleId' => null,
                    'serviceName' => null,
                );
            }
        }

        return $data;
    }

    /**
     * @param $userCompany
     * @param $servicesEnabled
     * @return mixed
     */
    public function fetchServices($userCompany, $servicesEnabled)
    {
        $service = array();
        $userCompanyService = array();
        $data = array();
        for ($j = 0; $j < sizeof($servicesEnabled); $j++) {
            for ($i = 0; $i < count($userCompany->getUserCompanyService()); $i++) {
                if ($userCompany->getUserCompanyService()[$i]->getService()->getId() === $servicesEnabled[$j]->getId(
                    )) {
                    array_push($service, $servicesEnabled[$j]->getId());
                } else {
                    if (!in_array($servicesEnabled[$j]->getId(), $userCompanyService)) {
                        array_push($userCompanyService, $servicesEnabled[$j]->getId());
                    }
                }
            }
        }
        $key = 0;
        foreach (array_diff($userCompanyService, $service) as $value) {
            $data[$key] = array(
                'user_company_id' => $userCompany->getId(),
                'id' => $this->em->getRepository('AppBundle:Service')->findBy(["id" => $value])[0]->getId(),
                'service' => $this->em->getRepository('AppBundle:Service')->findBy(["id" => $value])[0]->getName(),
//                'service' => $this->em->getRepository('AppBundle:Service')->findById($value)[0]->getName(),
                'defaultStatus' => 'Enable',
                'enabled' => true,
            );
            $key++;
        }

        return $data;
    }

    /**
     * @param $serviceRole
     * @return mixed
     */
    public function fetchServiceRole($serviceRole)
    {
        $data = array();
        for ($i = 0; $i < sizeof($serviceRole); $i++) {
            $data[$i] = array(
                'serviceRoleId' => $serviceRole[$i]['id'],
                'serviceId' => $serviceRole[$i]['service']['id'],
                'serviceName' => $serviceRole[$i]['service']['name'],
            );
        }

        return $data;
    }

    /**
     * @param $serviceRole
     * @return mixed
     */
    public function fetchOneServiceRole($serviceRole)
    {
        $data = array();
        for ($i = 0; $i < sizeof($serviceRole); $i++) {
            $data[$i] = array(
                'serviceRoleId' => $serviceRole[$i]->getId(),
                'serviceId' => $serviceRole[$i]->getService()->getId(),
                'serviceName' => $serviceRole[$i]->getShortDesc(),
            );
        }

        return $data;
    }

    /**
     * @param $entity
     * @return mixed
     */
    public function fetchServiceRoleOptions($entity)
    {
        $data = array();
        for ($i = 0; $i < sizeof($entity); $i++) {
            $data[$i] = array(
                'ServiceId' => $entity[$i]->getService()->getId(),
                'ServiceName' => $entity[$i]->getService()->getName(),
            );
            for ($j = 0; $j < $entity[$i]->getUserCompanyServiceRoles()->count(); $j++) {
                if (!empty($entity[$i]->getUserCompanyServiceRoles()->getValues()[$j]->getServiceRole())) {
                    $data[$i]['selectedRole'][$j] = [
                        'label' => $entity[$i]->getUserCompanyServiceRoles()->getValues()[$j]->getServiceRole()
                            ->getService()->getName(),
                        'value' => [
                            'serviceId' => $entity[$i]->getUserCompanyServiceRoles()->getValues()[$j]
                                ->getServiceRole()->getService()->getId(),
                            'serviceRoleId' => $entity[$i]->getUserCompanyServiceRoles()->getValues()[$j]
                                ->getServiceRole()->getId(),
                            'serviceName' => $entity[$i]->getUserCompanyServiceRoles()->getValues()[$j]
                                ->getServiceRole()->getService()->getName(),
                        ],
                    ];
                } else {
                    $data[$i]['selectedRole'][$j] = [
                        'label' => null,
                        'value' => [
                            'serviceId' => null,
                            'serviceRoleId' => null,
                            'serviceName' => null,
                        ],
                    ];
                }
            }
        }

        return $data;
    }

    /**
     * @param $serviceId
     * @return mixed
     */
    public function enableOneService($serviceId)
    {
        $service = $this->em->getRepository('AppBundle:Service')->find($serviceId);
        $eventManager = $service->getEventManager();
        $method = "Enable".$eventManager."UserFromCompany";

        return $method;
    }

    /**
     * @param $service
     * @param $user
     * @param $company
     * @return mixed
     */
    public function disableOneService($serviceId)
    {
        $service = $this->em->getRepository('AppBundle:Service')->find($serviceId);
        $eventManager = $service->getEventManager();
        $method = "Disable".$eventManager."UserFromCompany";

        return $method;
    }


//    /**
//     * @param $userCompany
//     * @param $user
//     * @param $company
//     */
//    public function enableServices($userCompany, $user, $company)
//    {
//        $userCompanyService = $userCompany->getUserCompanyService()->getValues();
//        for ($i = 0; $i < sizeof($userCompanyService); $i++) {
//            if ($userCompanyService[$i]->getService()->getEventEnable() == true) {
//                $this->enableOneService($userCompanyService[$i]->getService()->getId(), $user, $company);
//            } else {
//                continue;
//            }
//        }
//    }
//    /**
//     * @param $userCompany
//     * @param $user
//     * @param $company
//     */
//    public function disableServices($userCompany, $user, $company)
//    {
//        $userCompanyService = $userCompany->getUserCompanyService()->getValues();
//        for ($i = 0; $i < sizeof($userCompanyService); $i++) {
//            if ($userCompanyService[$i]->getService()->getEventEnable() == true) {
//                $service = $this->em->getRepository('AppBundle:Service')->find(
//                    $userCompanyService[$i]->getService()->getId()
//                );
//                $this->disableOneService($service, $user, $company);
//            } else {
//                continue;
//            }
//        }
//    }
}
