<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Templating\EngineInterface;
use Symfony\Component\Validator\Constraints\Date;


class Mail
{
    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    private $container;
    private $mailer;
    private $templating;
    private $from;
    private $senderName;

    /**
     * Mail constructor.
     * @param $mailer
     * @param EngineInterface $templating
     * @param Container $container
     */
    public function __construct($mailer, EngineInterface $templating, Container $container)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->container = $container;
        $this->from = $this->container->getParameter('ADDRESS_MAIL');
        $this->senderName = $this->container->getParameter('SENDER_NAME');
    }

    /**
     * @param $subject
     * @param $from
     * @param $senderName
     * @param $to
     * @param $uniqPassword
     * @param $baseResettingUrl
     */
    public function SendMailPassword($subject, $from, $senderName, $to, $uniqPassword, $baseResettingUrl)
    {
        /* Send invitation Mail */
        $message = \Swift_Message::newInstance();
        $message->setSubject($subject);
        $message->setFrom(array($from => $senderName));
        $message->setTo($to);
        $message->setBody(
            $this->templating->render(
                'backend/user/email/create-account.html.twig',
                array('baseResettingUrl' => $baseResettingUrl)
            ),
            'text/html'
        );
        $this->mailer->send($message);
        /* End Send invitation Mail */
    }

    /**
     * @param $subject
     * @param $to
     * @param $data
     */
    public function sendJson($subject, $to, $data)
    {
        $message = \Swift_Message::newInstance();
        $message->setSubject($subject);
        $message->setFrom($this->from);
        $message->setTo($to);
        $message->setBody(
            $data,
            'text/html'
        );
        $this->mailer->send($message);
    }

    /**
     * @param $subject
     * @param $to
     * @param $locale
     * @param $user
     * @param $ip
     * @param $owner
     * @param $location
     */
    public function ipSecurityNotice($subject, $to, $locale, $user, $ip, $owner, $location)
    {
        $message = \Swift_Message::newInstance();
        $message->setSubject($subject);
        $message->setFrom(array($this->from => $this->senderName));
        $message->setTo($to);
        $message->setBody(
            $this->templating->render(
                'email/'.$locale.'/ip-security-notice.html.twig',
                array(
                    'owner' => $owner,
                    'user' => $user,
                    'ip' => $ip,
                    'location' => $location,
                )
            ),
            'text/html'
        );
        $this->mailer->send($message);
    }

    /**
     * @param $subject
     * @param $to
     * @param $locale
     * @param $user
     * @param $ip
     * @param $owner
     * @param $location
     */
    public function passwordSecurityNotice($subject, $to, $locale, $user, $ip, $owner, $location)
    {
        $message = \Swift_Message::newInstance();
        $message->setSubject($subject);
        $message->setFrom(array($this->from => $this->senderName));
        $message->setTo($to);
        $message->setBody(
            $this->templating->render(
                'email/'.$locale.'/password-security-notice.html.twig',
                array(
                    'owner' => $owner,
                    'user' => $user,
                    'ip' => $ip,
                    'location' => $location,
                )
            ),
            'text/html'
        );
        $this->mailer->send($message);
    }

    /**
     * @param $subject
     * @param $to
     * @param $locale
     * @param $template
     * @param $bag
     */
    public function notificationMail($subject, $to, $locale, $template, $bag)
    {
        $message = \Swift_Message::newInstance();
        $message->setSubject($subject);
        $message->setFrom(array($this->from => $this->senderName));
        $message->setTo($to);
        $message->setBody(
            $this->templating->render(
                'email/'.$locale.'/'.$template.'.html.twig',
                array(
                    'message' => $bag['message'],
                    'link' => $bag['link'],
                    'subject' => $subject,
                )
            ),
            'text/html'
        );
        $this->mailer->send($message);
    }
}
