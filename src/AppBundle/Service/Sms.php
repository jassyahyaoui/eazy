<?php

namespace AppBundle\Service;

use Authy;
use Scheb\TwoFactorBundle\Security\TwoFactor\Provider\TwoFactorProviderInterface;
use Scheb\TwoFactorBundle\Security\TwoFactor\AuthenticationContextInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface as Engine;
use Symfony\Component\HttpFoundation\RedirectResponse;


class Sms implements TwoFactorProviderInterface
{
    protected $container;
    protected $renderer;
    protected $templating;

    public function __construct(Container $container, Engine $templating)
    {
        $this->container = $container;
        $this->templating = $templating;
    }

    public function beginAuthentication(AuthenticationContextInterface $context)
    {
        // Check if user can do email authentication
        $user = $context->getUser();

        if ($user->isEmailAuthEnabled()) {
            // Generate and send a new sms code
            $this->sendSmsCode($user->getMobile(), $user->getMobileCountry(), 'sms');

            return true;
        }

        return false;
    }


    public function requestAuthenticationCode(AuthenticationContextInterface $context)
    {
        $user = $context->getUser();

        $request = $context->getRequest();
        $session = $context->getSession();

        $authCode = $request->request->get('_auth_code');
        if ($authCode !== null) {
            if ($this->verificationSmsCode($user->getMobile(), $user->getMobileCountry(), $authCode)) {

                $context->setAuthenticated(true);

                return new RedirectResponse($request->getUri());
            }

            $session->getFlashBag()->set('two_factor', 'scheb_two_factor.code_invalid');
        }

        return $this->templating->renderResponse(
            'security/2fa_form.html.twig',
            array(
                'useTrustedOption' => $context->useTrustedOption(),
            )
        );
    }

    public function sendSmsCode($mobile, $country, $method)
    {
        $key = $this->container->getParameter('AUTHY_API_KEY');
        $authy_api = new Authy\AuthyApi($key);/*Application api_key*/

        return $authy_api->phoneVerificationStart($mobile, $country, $method);
    }

    public function verificationSmsCode($mobile, $country, $code)
    {
        $key = $this->container->getParameter('AUTHY_API_KEY');
        $authy_api = new Authy\AuthyApi($key);

        $response = $authy_api->phoneVerificationCheck(
            $mobile,
            $country,
            $code
        );

        return $response->ok();
    }
}