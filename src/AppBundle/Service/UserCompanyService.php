<?php

namespace AppBundle\Service;


use Doctrine\ORM\EntityManager;
use AppBundle\Entity\UserCompanyService as EntityUserCompanyService;
use AppBundle\Entity\UserCompanyServiceRoles as EntityUserCompanyServiceRoles;

class UserCompanyService extends AbstractService
{
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
    }

    /**
     * @param $userCompanyService
     * @return JsonResponse|array
     */
    public function fetchChosenCompany($userCompanyService)
    {
        $userCompanyServiceData = array();
        $chosenCompany = array();
        foreach ($userCompanyService as $key => $value) {
            $chosenCompany = array(
                'company_id' => $value->getUserCompany()->getCompany()->getId(),
                'company_name' => $value->getUserCompany()->getCompany()->getName(),
//                'company_id' => $value->getCompany()->getId(),// remplace usercompany par company
//                'company_name' => $value->getCompany()->getName(),// remplace usercompany par company
            );
            array_push($userCompanyServiceData, $value->getUserCompany()->getUserCompanyService()->getValues());

        }

        return array('chosenCompany' => $chosenCompany, 'userCompanyService' => $userCompanyServiceData);
    }

    /**
     * @param $userCompanyServiceData
     * @param $chosenCompany
     * @return mixed
     */
    public function fetchUserCompanyService($userCompanyServiceData, $chosenCompany)
    {
        $userCompanyService = array();
        foreach ($userCompanyServiceData as $key => $value) {
            foreach ($value as $key => $data) {
                if ($data->getUserCompany()->getCompany()->getId() == $chosenCompany['company_id']) {
                    $userCompanyService[$key] = array(
                        'id' => $data->getId(),
                        'defaultStatus' => $data->getStatus(),
                        'enabled' => $data->getEnabled(),
                        'externalId' => $data->getExternalId(),
                        'user_company_id' => $data->getUserCompany()->getId(),
                        'serviceId' => $data->getService()->getId(),
                        'serviceName' => $data->getService()->getName(),
                        'eventManager' => $data->getService()->getEventManager(),
                        'roleType' => $data->getService()->getRoleType(),
                        'companyId' => $data->getUserCompany()->getCompany()->getId(),
                    );
                    if (sizeof($data->getService()->getServiceRole()) > 0) {
                        for ($i = 0; $i < sizeof($data->getService()->getServiceRole()); $i++) {
                            $userCompanyService[$key]['serviceRole'][$i] = array(
                                'label' => $data->getService()->getServiceRole()->getValues()[$i]->getShortDesc(),
                                'id' => $data->getService()->getServiceRole()->getValues()[$i]->getId(),
                            );
                        }
                    }
                    if ($data->getUserCompanyServiceRoles()->count() > 0) {
                        for ($i = 0; $i < $data->getUserCompanyServiceRoles()->count(); $i++) {

                            if (!empty($data->getUserCompanyServiceRoles()->getValues()[$i]->getServiceRole())) {

                                if ($data->getService()->getRoleType() === 'single') {

                                    $userCompanyService[$key]['selectedRole'] = array(
                                        'label' => $data->getUserCompanyServiceRoles()->getValues()[$i]->getServiceRole(
                                        )->getShortDesc(),
                                        'id' => $data->getUserCompanyServiceRoles()->getValues()[$i]->getServiceRole(
                                        )->getId(),
                                    );
                                } else {

                                    $userCompanyService[$key]['selectedRole'][$i] = array(
                                        'label' => $data->getUserCompanyServiceRoles()->getValues()[$i]->getServiceRole(
                                        )->getShortDesc(),
                                        'id' => $data->getUserCompanyServiceRoles()->getValues()[$i]->getServiceRole(
                                        )->getId(),
                                    );
                                }
                            }
                        }
                    }
                }
            }
        }

        return $userCompanyService;
    }

    /**
     * @param $userCompany
     * @return mixed
     */
    public function fetchByUserCompany($userCompany)
    {
        $data = array();
        foreach ($userCompany->getUserCompanyService()->getValues() as $key => $value) {
            $data[$key] = array(
                'id' => $value->getUserCompany()->getuserCompanyService()[0]->getId(),
                'company_id' => $value->getUserCompany()->getCompany()->getId(),
                'company_name' => $value->getUserCompany()->getCompany()->getName(),
                'user_company_id' => $value->getUserCompany()->getId(),
                'service_id' => $value->getService()->getId(),
                'serviceName' => $value->getService()->getName(),
                'eventManager' => $value->getService()->getEventManager(),
                'roleType' => $value->getService()->getRoleType(),
                'defaultStatus' => $value->getStatus(),
                'enabled' => $value->getEnabled(),
                'externalId' => $value->getExternalId(),
                'lastLogin' => $value->getLastLoginAt() ? $value->getLastLoginAt()->format('Y-m-d') : null,
            );
            if ($value->getService()->getServiceRole()->count() > 0) {
                for ($i = 0; $i < $value->getService()->getServiceRole()->count(); $i++) {
                    $data[$key]['serviceRole'][$i] = array(
                        'label' => $value->getService()->getServiceRole()->getValues()[$i]->getShortDesc(),
                        'id' => $value->getService()->getServiceRole()->getValues()[$i]->getId(),
                    );
                }
            }
            if ($value->getUserCompanyServiceRoles()->count() > 0) {
                for ($i = 0; $i < $value->getUserCompanyServiceRoles()->count(); $i++) {
                    if (!empty($value->getUserCompanyServiceRoles()->getValues()[$i]->getServiceRole())) {
                        if ($value->getService()->getRoleType() === 'single') {
                            $data[$key]['selectedRole'] = array(
                                'label' => $value->getUserCompanyServiceRoles()->getValues()[$i]->getServiceRole(
                                )->getShortDesc(),
                                'id' => $value->getUserCompanyServiceRoles()->getValues()[$i]->getServiceRole()->getId(
                                ),
                            );
                        } else {
                            $data[$key]['selectedRole'][$i] = array(
                                'label' => $value->getUserCompanyServiceRoles()->getValues()[$i]->getServiceRole(
                                )->getShortDesc(),
                                'id' => $value->getUserCompanyServiceRoles()->getValues()[$i]->getServiceRole()->getId(
                                ),
                            );
                        }
                    }
                }
            }
        }

        return $data;
    }

    /**
     * @param $request
     * @param $data
     * @param $dataUserCompanyService
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function editUserCompanyService($request, $data, $dataUserCompanyService)
    {
        if (isset($dataUserCompanyService[0]['id'])) {

            $entityIds = array();
            for ($i = 0; $i < sizeof($dataUserCompanyService); $i++) {
                $returnedIds = $this->setUserCompanyService($data, $dataUserCompanyService, $i);
                array_push($entityIds, $returnedIds);
            }
        } else {
            $entityIds = $this->addUserCompanyService($dataUserCompanyService, $data);
        }
        $this->addAdditionalService($request, $data);

        return $entityIds;
    }

    /**
     * @param $data
     * @param $dataUserCompanyService
     * @param $counter
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setUserCompanyService($data, $dataUserCompanyService, $counter)
    {
        $userCompanyService = $this->em->getRepository('AppBundle:UserCompanyService')->findBy(
            array('userCompany' => $data['value']['user_company_id'])
        );
        $entity = $this->em->getRepository('AppBundle:UserCompanyService')->find(
            $userCompanyService[$counter]->getId()
        );
        $entity->setEnabled($dataUserCompanyService[$counter]['enabled']);

        $entity->setStatus($dataUserCompanyService[$counter]['defaultStatus']);

        if (isset($dataUserCompanyService[$counter]['externalId'])) {
            $entity->setExternalId($dataUserCompanyService[$counter]['externalId']);

        }
        $this->saveEntity($entity);

        return $entity->getId();
    }

    /**
     * @param $dataUserCompanyService
     * @param $data
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addUserCompanyService($dataUserCompanyService, $data)
    {
        $entityIds = array();
        foreach ($dataUserCompanyService as $key => $service) {
            $userCompanyService = new EntityUserCompanyService();
            $userCompanyService->setUserCompany(
                $this->em->getRepository('AppBundle:UserCompany')->find($data['value']['user_company_id'])
            );
            $userCompanyService->setService(
                $this->em->getRepository('AppBundle:Service')->find($service['serviceId'])
            );
            $userCompanyService->setStartedAt(new \DateTime($data['value']['started_at']));
            $userCompanyService->setEndAt(new \DateTime($data['value']['end_at']));
            $userCompanyService->setLastLoginAt(null);
            $userCompanyService->setEnabled($service['enabled']);
            $userCompanyService->setStatus($service['defaultStatus']);
            if (isset($service['externalId'])) {
                $userCompanyService->setExternalId($service['externalId']);
            }
            $this->saveEntity($userCompanyService);
            if (!empty($userCompanyService)) {
                array_push($entityIds, $userCompanyService->getId());
            }
        }
        return $entityIds;
    }

    /**
     * @param $request
     * @param $data
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addAdditionalService($request, $data)
    {
        if ($request->request->get('restServices')) {
            $restOfServices = $request->request->get('restServices')['value'];
            $userCompanyService = new EntityUserCompanyService();
            $userCompanyService->setUserCompany(
                $this->em->getRepository('AppBundle:UserCompany')->find($restOfServices['user_company_id'])
            );
            $userCompanyService->setService(
                $this->em->getRepository('AppBundle:Service')->find($restOfServices['id'])
            );
            $userCompanyService->setStartedAt(new \DateTime($data['value']['started_at']));
            $userCompanyService->setEndAt(new \DateTime($data['value']['end_at']));
            $userCompanyService->setLastLoginAt(null);
            $userCompanyService->setEnabled($restOfServices['enabled']);
            $userCompanyService->setStatus($restOfServices['defaultStatus']);

            $this->saveEntity($userCompanyService);
        }
    }

    /**
     * @param $optionId
     */
    public function deleteRoles($optionId)
    {
        for ($i = 0; $i < sizeof($optionId); $i++) {
            if (isset($optionId[$i])) {
                $q = $this->em->createQuery(
                    'delete from AppBundle:UserCompanyServiceRoles ucsr where ucsr.userCompanyService = '.$optionId[$i]
                );
                $q->execute();
            }
        }
    }

    /**
     * @param $selectedOptions
     * @param $optionId
     * @return array
     */
    public function formatOptions($selectedOptions, $optionId)
    {
        $data = array();
        $c = 0;
        for ($a = 0; $a < sizeof($selectedOptions); $a++) {
            if (isset($selectedOptions[$a]['selectedRole'][0])) {
                for ($k = 0; $k < sizeof($selectedOptions[$a]['selectedRole']); $k++) {
                    $data[$c] = array(
                        'id' => $selectedOptions[$a]['selectedRole'][$k]['id'],
                        'user_company_service_id' => $optionId[$a],
                    );
                    $c++;
                }
            } elseif (isset($selectedOptions[$a]['selectedRole']['label'])) {
                $data[$c] = array(
                    'id' => $selectedOptions[$a]['selectedRole']['id'],
                    'user_company_service_id' => $optionId[$a],
                );
                $c++;
            } else {
                $data[$c] = array(
                    'id' => null,
                    'user_company_service_id' => $optionId[$a],
                );
                $c++;
            }
        }

        return $data;
    }

    /**
     * @param $data
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveRoles($data)
    {
        for ($i = 0; $i < sizeof($data); $i++) {
            $entity = new EntityUserCompanyServiceRoles();
            $entity->setUserCompanyService(
                $this->em->getRepository('AppBundle:UserCompanyService')->find(
                    $data[$i]['user_company_service_id']
                )
            );
            if (isset($data[$i]['id'])) {
                $entity->setServiceRole(
                    $this->em->getRepository('AppBundle:ServiceRole')->find(
                        $data[$i]['id']
                    )
                );
            }
            $this->saveEntity($entity);
        }
    }

    /**
     * @param $userCompanyServiceData
     * @param $userCompanyId
     * @return int
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveUserCompanyService($userCompanyServiceData, $userCompanyId)
    {
        foreach ($userCompanyServiceData as $key => $service) {
            $userCompanyService = new EntityUserCompanyService();
            $userCompanyService->setUserCompany(
                $this->em->getRepository('AppBundle:UserCompany')->find($userCompanyId)
            );
            $userCompanyService->setService(
                $this->em->getRepository('AppBundle:Service')->find($service['serviceId'])
            );
            $userCompanyService->setStartedAt(new \DateTime('now'));
            $userCompanyService->setEndAt(new \DateTime('now'));
            $userCompanyService->setLastLoginAt(new \DateTime('now'));
            $userCompanyService->setEnabled($service['enabled']);
            $userCompanyService->setStatus($service['defaultStatus']);
            if (isset($service['externalId'])) {
                $userCompanyService->setExternalId($service['externalId']);
            }
            $this->saveEntity($userCompanyService);
        }

        return $userCompanyService->getId();

    }

    /**
     * @param $userCompanyId
     * @return array
     */
    public function fetchId($userCompanyId)
    {
        $userCompanyServiceId = array();

        $userCompanyService = $this->em->getRepository('AppBundle:UserCompanyService')->findBy(
            array('userCompany' => $userCompanyId)
        );
        for ($i = 0; $i < sizeof($userCompanyService); $i++) {
            array_push($userCompanyServiceId, $userCompanyService[$i]->getId());
        }

        return $userCompanyServiceId;
    }

    /**
     * @param $userCompanyServiceRoles
     * @param $userCompanyServiceId
     * @return array
     */
    public function fetchServiceRoles($userCompanyServiceRoles, $userCompanyServiceId)
    {
        $data = array();
        for ($i = 0; $i < sizeof($userCompanyServiceRoles); $i++) {

            if (isset($userCompanyServiceRoles[$i]['selectedRole'])) {
                if (isset($userCompanyServiceRoles[$i]['selectedRole']['label'])) {
                    $data[$i]['serviceRoleId'][0] = $userCompanyServiceRoles[$i]['selectedRole']['id'];
                } else {
                    for ($j = 0; $j < sizeof($userCompanyServiceRoles[$i]['selectedRole']); $j++) {
                        $data[$i]['serviceRoleId'][$j] = $userCompanyServiceRoles[$i]['selectedRole'][$j]['id'];
                    }
                }
            } else {
                $data[$i]['serviceRoleId'][0] = null;
            }
            $data[$i]['userCompanyServiceId'] = $userCompanyServiceId[$i];
        }

        return $data;
    }

    /**
     * @param $data
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveServiceRole($data)
    {
        foreach ($data as $key => $value) {
            for ($i = 0; $i < sizeof($value['serviceRoleId']); $i++) {

                $entity = new EntityUserCompanyServiceRoles();
                if ($value['serviceRoleId'][$i] != null) {

                    $entity->setServiceRole(
                        $this->em->getRepository('AppBundle:ServiceRole')->find($value['serviceRoleId'][$i])
                    );
                } else {

                    $entity->setServiceRole(null);
                }
                $entity->setUserCompanyService(
                    $this->em->getRepository('AppBundle:UserCompanyService')->find(
                        $value['userCompanyServiceId']
                    )
                );
                $this->saveEntity($entity);
            }
        }
    }
}