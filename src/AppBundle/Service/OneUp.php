<?php

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;


class OneUp
{
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @param $user
     * @param $company
     * @param $service_id
     * @param $userCompany
     * @param $userCompanyService
     * @return array
     */
    public function fetchMe($user, $company, $service_id, $userCompany, $userCompanyService)
    {
        $response = array(
            'ID' => (string)$user->getId(),
            'user_login' => $user->getUsername(),
            'user_nicename' => $user->getUsernameCanonical(),
            'user_email' => $user->getEmail(),
            'user_registered' => $user->getCreatedAt()->format('Y-m-d h:i:s'),
            'user_activation' => $user->getCreatedAt()->format('Y-m-d h:i:s'),
            'user_group_role' => "",
            'user_role' => "",
            'user_status' => "",
            'company_id' => "c0".(string)$company->getId(),
            'company_name' => $company->getName(),
            'address_1' => $company->getAdress(),
            'address_2' => "",
            'city' => $company->getCity(),
            'state' => "",
            'postcode' => $company->getPostCode(),
            'country' => "",
            'configuration' => 'fr-FR',
            'phone' => $company->getPhone(),
            'fax' => $company->getFax(),
            'website' => $company->getWebsite(),
            'company_email' => $company->getEmail(),
            'display_name' => $user->getFirstName()." ".$user->getLastName(),
            'first_name' => $user->getFirstName(),
            'last_name' => $user->getLastName(),
            'siren_code' => $company->getBusinessIdentifier(),
            'siret_code' => $company->getBusinessIdentifierExtended(),
            'capital' => $company->getShareCapital(),
            'tva' => $company->getVatIdentifier(),
            'ape' => $company->getBusinessClassification(),
            'rm' => $company->getBusinessRegisterOffice(),
            'rcs' => $company->getLegalRegisterOffice(),
        );


        if (in_array("ROLE_OWNER", $userCompany->getRole())) {
            $response['user_group_role'] = 'full';
            $response['user_role'] = 'admin';
        } else {
            $response['user_group_role'] = $userCompanyService->getUserCompanyServiceRoles()->getValues(
            )[0]->getServiceRole()->getExternalId(); /* get always the first value*/
            $response['user_role'] = '';
        }

        if ($user->getEnabled() == false OR $userCompany->getEnabled() == false OR $userCompanyService->getEnabled(
            ) == false) {
            $response['user_status'] = 'disable';
        } elseif ($user->getEnabled() == true AND $userCompany->getEnabled(
            ) == true AND $userCompanyService->getEnabled() == true) {
            $response['user_status'] = 'enable';
        }

        return $response;
    }

    /**
     * @param $user
     * @param $company
     * @param $eventType
     */
    public function eventOneUp($user, $company, $eventType)
    {
        $WEBHOOK_LINK = $this->container->getParameter('WEBHOOK_LINK'); /* Get WEBHOOK_LINK from parameters.yml */
        $SHARED_SECRET = $this->container->getParameter(
            'SHARED_SECRET'
        ); /* Get SHARED_SECRET from parameters.yml */

        $curl = curl_init();

        $data = array(
            'event_type' => $eventType,
            'company_id' => "c0".(string)$company->getId(),
            'user_id' => $user->getId(),
        );

        curl_setopt($curl, CURLOPT_URL, $WEBHOOK_LINK);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->header($data, $SHARED_SECRET));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($curl);
        $all_infos = curl_getinfo($curl);
    }

    /**
     * @param $data
     * @param $SHARED_SECRET
     * @return string
     */
    public function hash($data, $SHARED_SECRET)
    {
        return base64_encode(hash_hmac('sha256', json_encode($data), $SHARED_SECRET, true));
    }

    /**
     * @param $data
     * @param $SHARED_SECRET
     * @return array
     */
    public function header($data, $SHARED_SECRET)
    {
        return array(
            'Content-Type: application/json',
            'HTTP_X_PMETI_HMAC_SHA256: '.$this->hash($data, $SHARED_SECRET),
        );
    }
}