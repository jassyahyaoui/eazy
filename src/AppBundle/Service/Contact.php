<?php

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\ORM\EntityManager;

class Contact
{

    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function getCompanyContacts($user)
    {
        $user = $this->em->getRepository('AppBundle:User')->findOneBy(array('username' => $user));
        if ($user) {
            $currentCompany = $user->getCurrentCompany();
            if ($currentCompany) {
                $companyContacts = $this->em->getRepository('AppBundle:UserCompany')->getCompanyContacts(
                    $currentCompany->getId()
                );

                return $array = (array)$companyContacts;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
