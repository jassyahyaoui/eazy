<?php

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\ORM\EntityManager;

class ServicePricing extends AbstractService
{
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
    }

    public function getServicePricing()
    {
        $servicesPricing = $this->em->getRepository('AppBundle:ServicePricing')->getServicePricingEnabled();
        if (!$servicesPricing) {
            return null;
        }

        return $servicesPricing;
    }
}
