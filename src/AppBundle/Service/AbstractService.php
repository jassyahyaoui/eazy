<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;

class AbstractService
{
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param $entity
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveEntity($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

    public function uniqPwd()
    {
        return strtoupper('$_mazars_'.substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 16).'_$');

    }
}