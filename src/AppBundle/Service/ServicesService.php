<?php

namespace AppBundle\Service;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class ServicesService {

    protected $em;

    public function __construct(EntityManager $entityManager) {
        $this->em = $entityManager;
    }

       public function getUserCompanyServices($user) {
          $user = $this->em->getRepository('AppBundle:User')->findOneByUsername($user);
       if ($user) {
               $currentCompany = $user->getCurrentCompany();
            if ($currentCompany) {
                $currentCompanyId = $currentCompany->getId();  /* Return company that user has been selected */

                /* $roles return Services From CompanyService entity by Selected Company */
                $services =  $this->em->getRepository('AppBundle:UserCompanyService')->getUserCompanyServices($currentCompanyId, $user->getId());                              
return $array =  (array) $services;
              
            } else {
                   return null;
               }
       } else
           return null;
   }
    
   
}
