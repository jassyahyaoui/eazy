<?php

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\GdprUserOptin as GdprUserOptinEntity;

class GdprUserOptin extends AbstractService
{
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
    }

    public function getGdprUserOptin($user)
    {
        if ($user->getGdprUserOptin() === true) {
            return true;
        }

        return false;
    }

    public function setGdprUserOptin($user,$ip)
    {
        $result = ['message' => null];
        if (!$user) {
            return null;
        }
        $gdprUserOptin = new GdprUserOptinEntity();
        $gdprUserOptin->setIp($ip);
        $gdprUserOptin->setTimestamp((new \DateTime('now'))->getTimestamp());
        $gdprUserOptin->setCreatedBy($user);
        $gdprUserOptin->setUpdatedBy($user);
        $user->setGdprUserOptin(true);
        $this->em->persist($gdprUserOptin);
        $this->em->persist($user);
        $this->em->flush();
        $result['message'] = "Your current company has changed";

        return $result;
    }
}
