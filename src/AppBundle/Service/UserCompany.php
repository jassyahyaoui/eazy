<?php
/**
 * Created by PhpStorm.
 * User: MAZ_USER3
 * Date: 07/06/2018
 * Time: 11:27
 */

namespace AppBundle\Service;

use AppBundle\Entity\UserCompany as EntityUserCompany;
use AppBundle\Entity\User as EntityUser;
use Doctrine\ORM\EntityManager;

class UserCompany extends AbstractService
{
  public function __construct(EntityManager $entityManager)
  {
    parent::__construct($entityManager);
  }

  /**
   * @param $userCompany
   * @return array
   */
  public function fetchSettings(EntityUserCompany $userCompany)
  {
    $response = array(
      'roles' => $userCompany->getRole(),
      'sms' => $userCompany->getNotifySms(),
      'email' => $userCompany->getNotifyEmail(),
    );

    return $response;
  }

  /**
   * @param EntityUser $user
   * @return array
   */
  public function fetchUserCompany($user)
  {
    $userCompanyService = array();

    $userCompany = array();
    if ($user->getUserCompany() == null) {
      return array('userCompany' => null, 'userCompanyService' => null);
    }
    foreach ($user->getUserCompany() as $key => $value) {
      $userCompany[$key] = array(
        'user_company_id' => $value->getId(),
        'started_at' => $value->getStartedAt()->format('Y-m-d'),
        'end_at' => $value->getEndAt()->format('Y-m-d'),
        'role' => $value->getRole(),
        'notify_sms' => $value->getNotifySms(),
        'notify_email' => $value->getNotifyEmail(),
        'enabled' => $value->getEnabled(),
        'company' => $value->getCompany()->getName(),
        'company_id' => $value->getCompany()->getId(),
        'template_id' => $value->getCompany()->getTemplate()->getId(),
      );
      array_push($userCompanyService, $value->getUserCompanyService());
    }

    return array('userCompany' => $userCompany, 'userCompanyService' => $userCompanyService);
  }

  /**
   * @param EntityUserCompany $userCompany
   * @param $data
   * @return EntityUserCompany
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  public function editSettings(EntityUserCompany $userCompany, $data)
  {
    $userCompany->setRole($data['roles']);
    $userCompany->setNotifyEmail($data['email']);
    $userCompany->setNotifySms($data['sms']);

    $this->saveEntity($userCompany);

    return $userCompany;
  }

  /**
   * @param $user
   * @param $dataUserCompany
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  public function editUserCompany(EntityUser $user, $dataUserCompany)
  {
    $userCompanyValues = array();
    foreach ($user->getUserCompany() as $value) {
      array_push($userCompanyValues, $value);
    }
    for ($i = 0; $i < sizeof($dataUserCompany); $i++) {
      if (isset($dataUserCompany[$i]['value']['user_company_id'])) {
        $this->setUserCompany($user, $userCompanyValues, $dataUserCompany, $i);
      } else {
        $this->addUserCompany($user, $dataUserCompany, $i);
      }
    }
  }

  /**
   * @param $user
   * @param $userCompanyValues
   * @param $dataUserCompany
   * @param $counter
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  public function setUserCompany(EntityUser $user, $userCompanyValues, $dataUserCompany, $counter)
  {
    $data = $userCompanyValues[$counter];
    $data->setStartedAt(
      new \DateTime($dataUserCompany[$counter]['value']['started_at'])
    );
    $data->setendAt(new \DateTime($dataUserCompany[$counter]['value']['end_at']));
    $data->setEnabled($dataUserCompany[$counter]['value']['enabled']);

    $this->saveEntity($user);
  }

  /**
   * @param $user
   * @param $dataUserCompany
   * @param $counter
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  public function addUserCompany(EntityUser $user, $dataUserCompany, $counter)
  {
    $data = $dataUserCompany[$counter]['value'];
    $userCompany = new EntityUserCompany();
    $userCompany->setStartedAt(new \DateTime($data['started_at']));
    $userCompany->setendAt(new \DateTime($data['end_at']));
    $userCompany->setEnabled($data['enabled']);
    $userCompany->setNotifySms(false);
    $userCompany->setNotifyEmail(false);
    $userCompany->setUser($this->em->getRepository('AppBundle:User')->find($user->getId()));
    $userCompany->setCompany(
      $this->em->getRepository('AppBundle:Company')->find($data['company_id'])
    );
    $userCompany->setRole([]);
    $this->saveEntity($userCompany);
  }

  /**
   * @param $userId
   * @param $userCompanyData
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  public function editCurrentCompany($userId, $userCompanyData)
  {
    $entity = $this->em->getRepository('AppBundle:User')->find($userId);
    $entity->setCurrentCompany(
      $this->em->getRepository('AppBundle:Company')->find(
        $userCompanyData['uctab'][0]['companyId']
      )
    );
    $this->saveEntity($entity);
  }

  /**
   * @param $userId
   * @param $data
   * @return mixed
   * @throws \Doctrine\ORM\ORMException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  public function saveUserCompany($userId, $data)
  {
    $response = array();
    foreach ($data as $key => $company) {
        if (is_array($company['uctab'])) {
            for ($i = 0; $i < count($company['uctab']); $i++) {
                $userCompany = new EntityUserCompany();
                $userCompany->setStartedAt(new \DateTime($company['uctab'][$i]['startedAt']));
                $userCompany->setEndAt(new \DateTime($company['uctab'][$i]['endAt']));
                $userCompany->setEnabled($company['uctab'][$i]['enabled']);
                $userCompany->setRole($company['roles']);
                $userCompany->setNotifyEmail($company['email']);
                $userCompany->setNotifySms($company['sms']);
                $userCompany->setUser($this->em->getRepository('AppBundle:User')->find($userId));
                $userCompany->setCompany(
                    $this->em->getRepository('AppBundle:Company')->find($company['uctab'][$i]['companyId'])
                );
                $this->saveEntity($userCompany);
                array_push($response, array($company['uctab'][$i]['companyId'] => $userCompany->getId()));
            }
        }
    }

    return $response;
  }

  /**
   * @param $entity
   * @return null
   */
  public function chosenCompany($entity)
  {
    $data = null;
    foreach ($entity as $val) {
      if ($val->isEmpty()) {
        continue;
      }
      $data = $val->getValues();
    }

    return $data;
  }
}
