<?php

namespace AppBundle\Service;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class UsersService {

    protected $em;
    private $container;

    public function __construct(EntityManager $entityManager, Container $container) {
        $this->em = $entityManager;
        $this->container = $container;
    }
  
   public function getCompanyUsers($user) {
       $user = $this->em->getRepository('AppBundle:User')->findOneBy(array('username'=>$user));
       if ($user) {
               $currentCompany = $user->getCurrentCompany();
               if ($currentCompany) {
                   $companyUsers =  $this->em->getRepository('AppBundle:UserCompany')->getCompanyUsers($currentCompany->getId(), $user->getId());
                   return $array =  (array) $companyUsers;
               } else {
                   return null;
               }
       } else
           return null;
   }
    
   
}
