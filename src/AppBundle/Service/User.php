<?php

namespace AppBundle\Service;

use AppBundle\Entity\User as UserEntity;
use Doctrine\ORM\EntityManager;


class User extends AbstractService
{

    /**
     * @param UserEntity $user
     * @return array
     */
    public function fetchUser(UserEntity $user)
    {
        return array(
            'id' => $user->getId(),
            'email' => $user->getEmail(),
            'first_name' => $user->getFirstName(),
            'last_name' => $user->getLastName(),
            'phone' => $user->getPhone(),
            'mobile' => $user->getMobile(),
            'mobile_country' => $user->getMobileCountry(),
            'enabled' => $user->getEnabled(),
            'title' => $user->getTitle(),
            'twoFactorAuthentication' => $user->getTwoFactorAuthentication(),
        );

    }

    /**
     * @param $user
     * @param $userData
     * @param $uniqPassword
     * @return mixed
     */
    public function editUser($user, $userData, $uniqPassword)
    {
        $user->setEmail($userData['email']);
        $user->setUsername($userData['email']);
        $user->setPlainPassword($uniqPassword);
        $user->addRole('ROLE_USER');
        $user->setEnabled(isset($userData['enabled']) ? $userData['enabled'] : '');
        $user->setFirstName(isset($userData['first_name']) ? $userData['first_name'] : '');
        $user->setLastName(isset($userData['last_name']) ? $userData['last_name'] : '');
        $user->setMobile(isset($userData['mobile']) ? $userData['mobile'] : '');
        $user->setPhone(isset($userData['phone']) ? $userData['phone'] : '');
        $user->setMobileCountry(isset($userData['mobile_country']) ? $userData['mobile_country'] : '');
        $user->setTitle(isset($userData['title']) ? $userData['title'] : '');

        return $user;
    }

    /**
     * @param $user
     * @param $data
     * @return null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function editCurrentUser($user, $data)
    {

        $companyId = $data['companyId'];
        $userCompany = $this->em->getRepository('AppBundle:UserCompany')->findOneBy(
            array('user' => $user->getId(), 'company' => $companyId)
        );

        if (!$userCompany) {
            return null;
        }
        if (isset($data['companyNotifyEmail'])) {
            $userCompany->setNotifyEmail($data['companyNotifyEmail']);
        }
        if (isset($data['companyNotifySms'])) {
            $userCompany->setNotifySms($data['companyNotifySms']);
        }
        if (isset($data['firstName'])) {
            $user->setFirstName($data['firstName']);
        }
        if (isset($data['lastName'])) {
            $user->setLastName($data['lastName']);
        }
        if (isset($data['locale'])) {
            $user->setLocale($data['locale']);
        }
        if (isset($data['mobile'])) {
            $user->setMobile($data['mobile']);
        }
        if (isset($data['mobileCountry'])) {
            $user->setMobileCountry($data['mobileCountry']);
        }
        if (isset($data['title'])) {
            $user->setTitle($data['title']);
        }

        $this->saveEntity($user);
    }

}