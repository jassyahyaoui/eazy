<?php

namespace AppBundle\Service;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;


class ipStack
{

//    private $allowedDomains;
    private $container;
    private $ip_stack_access_key;
    private $em;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->ip_stack_access_key = $this->container->getParameter('IP_STACK_ACCESS_KEY');
        $this->em = $this->container->get('doctrine.orm.default_entity_manager');
    }

    /**
     * @param $user
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function menageIpStack($user, $ip)
    {
        $userCompanies = $this->em->getRepository('AppBundle:UserCompany')->findByUser($user);
//
//        if($ip='127.0.0.1'){
//            $ip = '41.230.4.79';
//        }
        $location = $this->ipLocation($ip);

        foreach ($userCompanies as $userCompany) {
            $userCompany->setLastLoginCountryName($location['country_name']);
            $userCompany->setLastLoginRegionName($location['region_name']);
            $this->em->persist($userCompany);
            $this->em->flush($userCompany);
        }
    }

    /**
     * @param $ip
     * @return mixed
     */
    private function ipLocation($ip)
    {
        $ch = curl_init('http://api.ipstack.com/'.$ip.'?access_key='.$this->ip_stack_access_key.'');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $json = curl_exec($ch);
        curl_close($ch);

        $output = json_decode($json, true);
        $response['ip'] = $output['ip'];
        $response['country_name'] = $output['country_name'];
        $response['region_name'] = $output['region_name'] ?: $output['country_name'];


        return $response;
    }


}