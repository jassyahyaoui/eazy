<?php

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;


class UserAgent
{
    protected $em;
    private $user;

    public function __construct(EntityManager $entityManager, TokenStorageInterface $tokenStorage)
    {
        $this->em = $entityManager;
        $this->user = $tokenStorage->getToken()->getUser();
    }

    private function Device()
    {
        return $_SERVER['HTTP_USER_AGENT'];
    }

    private function BlackListDevices($serviceId)
    {
        return $this->em->find('AppBundle:Service', $serviceId)->getUserAgentDeny();
    }

    public function checkDevice($serviceId)
    {
        $isDenied = false;
        $blackList = $this->BlackListDevices($serviceId);
        if ($blackList == null) {
            return $isDenied;
        }
        $device = $this->Device();
        foreach ($blackList as $userAgent) {
            if (strpos($device, $userAgent) !== false) {
                $isDenied = true;
            }
        }

        return $isDenied;

    }
}