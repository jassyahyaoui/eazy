<?php

namespace AppBundle\Service;

use Doctrine\ORM\Query;
use AppBundle\Service\AbstractService;
use Doctrine\ORM\EntityManager;
use Mgilet\NotificationBundle\Manager\NotificationManager;
use AppBundle\Entity\User as EntityUser;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Notification
{
    protected $em;
    protected $notification;
    private $container;

    public function __construct(
        EntityManager $entityManager,
        NotificationManager $notification,
        ContainerInterface $container
    ) {
        $this->em = $entityManager;
        $this->notification = $notification;
        $this->container = $container;
    }

    /**
     * @param int $userId
     * @param int $max
     * @return object
     */
    public function notifiableNotifications($userId, $max)
    {
        return $this->em
            ->getRepository('MgiletNotificationBundle:NotifiableNotification')
            ->createQueryBuilder('nn')
            ->select('nn.seen')
            ->join('nn.notification', 'n')
            ->join('nn.notifiableEntity', 'ne')
            ->AndWhere("ne.class LIKE :class")
            ->setParameter('class', '%User%')
            ->andWhere('ne.identifier = :currentUserId ')
            ->setParameter('currentUserId', $userId)
            ->addSelect('n.link,n.message,n.subject,n.date')
            ->addSelect('ne.identifier')
            ->orderBy('nn.id', 'DESC')
            ->setMaxResults($max)
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
    }

    /**
     * @param int $userId
     * @return object
     */
    public function nbNotifiableNotifications($userId)
    {
        return $this->em
            ->getRepository('MgiletNotificationBundle:NotifiableNotification')
            ->createQueryBuilder('nn')
            ->select('count(nn ) as numberNotifications')
            ->join('nn.notification', 'n')
            ->join('nn.notifiableEntity', 'ne')
            ->AndWhere("ne.class LIKE :class")
            ->setParameter('class', '%User%')
            ->andWhere('ne.identifier = :currentUserId ')
            ->setParameter('currentUserId', $userId)
            ->andWhere('nn.seen = 0 ')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param int $userId
     * @return object
     */
    public function getNotifications($userId)
    {
        return $this->em
            ->getRepository('MgiletNotificationBundle:NotifiableNotification')
            ->createQueryBuilder('nn')
            ->leftJoin('nn.notification', 'n')
            ->leftJoin('nn.notifiableEntity', 'ne')
            ->AndWhere("ne.class LIKE :class")
            ->setParameter('class', '%User%')
            ->andWhere('ne.identifier = :currentUserId ')
            ->setParameter('currentUserId', $userId)
            ->andWhere('nn.seen = 0 ')
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
    }

    /**
     * @param array $list
     * @return object
     */
    public function setSeenByList($list)
    {
        return $this->em
            ->getRepository('MgiletNotificationBundle:NotifiableNotification')
            ->createQueryBuilder('nn')
            ->update('MgiletNotificationBundle:NotifiableNotification', 'nn')
            ->set('nn.seen', 1)
            ->where("nn.id IN ( :list )")
            ->setParameter('list', array_values(explode(',', $list)))
            ->getQuery()
            ->execute();
    }

    /**
     * @param int $id
     * @return object
     */
    public function setSeenById($id)
    {
        return $this->em
            ->getRepository('MgiletNotificationBundle:NotifiableNotification')
            ->createQueryBuilder('nn')
            ->update('MgiletNotificationBundle:NotifiableNotification', 'nn')
            ->set('nn.seen', 1)
            ->andWhere('nn.id = :notificationId ')
            ->setParameter('notificationId', $id)
            ->getQuery()
            ->execute();
    }

    /**
     * @param $notificationData
     * @return null
     */
    public function createNotificationUsersFromOfficeAction($notificationData)
    {
        $subject = $notificationData['sujet'];
        $message = $notificationData['message'];
        $link = $notificationData['link'];
        if (!$subject || !$message || !$link) {
            return null;
        }
        $officeId = $notificationData['office'];
        $office = $this->em->getRepository('AppBundle:Office')->find($officeId);
        if (!$office) {
            return null;
        }
        $users = $this->em->getRepository('AppBundle:User')->getUsersOffice($officeId);


        if (!$users) {
            return null;
        }
        /*Send notification by mail de all users office*/
        $this->container->get('notification_mail.service')->byOffice($officeId, $subject, $message, $link);

        $manager = $this->notification;
        $notification = $manager->createNotification($subject);
        $notification->setMessage($message);
        $notification->setLink($link);
        $manager->addNotification($users, $notification, true);
        $manager->addNotification(array($office), $notification, true);

    }

    /**
     * @param $notificationData
     * @return null
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createNotificationUserAction($notificationData)
    {
        $subject = $notificationData['sujet'];
        $message = $notificationData['message'];
        $link = $notificationData['link'];
        $userId = $notificationData['user'];
        if (!$subject || !$message || !$link || !$userId) {
            return null;
        }
        $user = $this->em->getRepository('AppBundle:User')->find($userId);
        if (!$user) {
            return null;
        }
        /*Send notification by mail to user*/
        $this->container->get('notification_mail.service')->byUser($user, $subject, $message, $link);

        $manager = $this->notification;
        $notification = $manager->createNotification($subject);
        $notification->setMessage($message);
        $notification->setLink($link);
        $manager->addNotification(array($user), $notification, true);
    }

    /**
     * @param $notificationData
     * @return null
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createNotificationOwnerAction($notificationData)
    {
        $subject = $notificationData['sujet'];
        $message = $notificationData['message'];
        $link = $notificationData['link'];
        $companyId = $notificationData['company'];

        if (!$subject || !$message || !$link || !$companyId) {
            return null;
        }

        if ($companyId) {
            $users = $this->em->getRepository('AppBundle:User')->getOwnerCompany($companyId);
        }

        /*Send notification by mail de all users company*/
        $this->container->get('notification_mail.service')->byCompany($companyId, $subject, $message, $link);

        $manager = $this->notification;
        $notification = $manager->createNotification($subject);
        $notification->setMessage($message);
        $notification->setLink($link);
        $manager->addNotification($users, $notification, true);
    }

    /**
     * @param $notificationData
     * @return null
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createNotificationRole($notificationData)
    {
        $subject = $notificationData['sujet'];
        $message = $notificationData['message'];
        $link = $notificationData['link'];
        $role = $notificationData['role'];

        if (!$subject || !$message || !$link || !$role) {
            return null;
        }

        $users = $this->em->getRepository('AppBundle:User')->getUsersRole($role);

        /*Send notification by mail de all users company*/
        $this->container->get('notification_mail.service')->byRole($role, $subject, $message, $link);

        $manager = $this->notification;
        $notification = $manager->createNotification($subject);
        $notification->setMessage($message);
        $notification->setLink($link);
        $manager->addNotification($users, $notification, true);
    }

    /***
     * @param EntityUser $user
     * @param $dataUserCompany
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function notificateUserNewCompany(EntityUser $user, $dataUserCompany)
    {
        $userCompanyValues = array();
        foreach ($user->getUserCompany() as $value) {
            array_push($userCompanyValues, $value);
        }
        for ($i = 0; $i < sizeof($dataUserCompany); $i++) {
            if (isset($dataUserCompany[$i]['value']['user_company_id'])) {
            } else {
                $manager = $this->notification;
                $notification = $manager->createNotification('Entreprise');

                if (array_key_exists("label", $dataUserCompany)) {
                    $companyName = $dataUserCompany[$i]['label'];
                } else {
                    $companyName = "";
                }

                $notification->setMessage("Vous êtes ajouté à une nouvelle entreprise ".$companyName);
                $notification->setLink("");
                $manager->addNotification(array($user), $notification, true);
            }
        }
    }

}