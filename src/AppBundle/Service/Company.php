<?php

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\ORM\EntityManager;

class Company extends AbstractService
{
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
    }

    /**
     * @param $username
     * @return null
     */
    public function getCurrentCompany($username)
    {
        $user = $this->em->getRepository('AppBundle:User')->findOneBy(['username' => $username]);
        if (!$user) {
            return null;
        }

        return $user->getCurrentCompany();
    }

    public function setCurrentCompany($user, $companyName)
    {
        $result = ['message' => null];
        $company = $this->em->getRepository('AppBundle:Company')->findOneBy(["name" => $companyName]);
        if (!$company) {
            return ['message' => "Error"];
        }

        $user->setCurrentCompany($company);
        $this->em->persist($company);
        $this->em->flush();

        $result['company'] = $company->getId();
        $result['message'] = "Your current company has changed";

        return $result;
    }

      public function updateCompany($company)
    {
        $result = ['message' => null];
        $companyId = 1;

        $company = $this->em->getRepository('AppBundle:Company')->find($companyId);
        if (!$company) {
            return ['message' => "Error"];
        }

        $this->em->persist($company);
        $this->em->flush();

        $result['company'] = $company->getId();
        $result['message'] = "Company has been changed";

        return $result;
    }




}
