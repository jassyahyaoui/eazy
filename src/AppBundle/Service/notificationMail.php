<?php

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;


class notificationMail
{
    private $container;
    private $em;

    /**
     * notificationMail constructor.
     * @param ContainerInterface $container
     * @param EntityManager $em
     */
    public function __construct(ContainerInterface $container, EntityManager $em)
    {
        $this->container = $container;
        $this->em = $em;
    }

    /**
     * @param $office
     * @param $subject
     * @param $message
     * @param $link
     */
    public function byOffice($office, $subject, $message, $link)
    {
        $bag = ['message' => $message, 'link' => $link];
        $users = $this->em->getRepository('AppBundle:User')->getNotifyUsersOffice($office);

        $fullList = $this->format($users);

        $this->send($this->filter($fullList), $subject, $bag, 'notification');

    }

    /**
     * @param $company
     * @param $subject
     * @param $message
     * @param $link
     */
    public function byCompany($company, $subject, $message, $link)
    {
        $bag = ['message' => $message, 'link' => $link];
        $users = $this->em->getRepository('AppBundle:User')->getNotifyOwnerCompany($company);

        $fullList = $this->format($users);

        $this->send($this->filter($fullList), $subject, $bag, 'notification');

    }

    /**
     * @param $user
     * @param $subject
     * @param $message
     * @param $link
     */
    public function byUser($user, $subject, $message, $link)
    {
        $bag = ['message' => $message, 'link' => $link];
        $userCompanies = $user->getUserCompany()->toArray();
        foreach ($userCompanies as $userCompany) {
            if ($userCompany->getNotifyEmail()) {
                $this->notify($subject, $user->getEmail(), $user->getLocale() ?: 'fr', 'notification', $bag);
            }
        }
    }

    /**
     * @param $role
     * @param $subject
     * @param $message
     * @param $link
     */
    public function byRole($role, $subject, $message, $link)
    {
        $bag = ['message' => $message, 'link' => $link];
        $users = $this->em->getRepository('AppBundle:User')->getNotifyUsersRole($role);

        $fullList = $this->format($users);

        $this->send($this->filter($fullList), $subject, $bag, 'notification');
    }

    /**
     * @param $users
     * @return mixed
     */
    private function format($users)
    {
        $fullList = [];
        foreach ($users as $key => $user) {
            $fullList[$key]['email'] = $user->getEmail();
            $fullList[$key]['locale'] = $user->getLocale() ?: 'fr';
        }

        return $fullList;
    }

    /**
     * @param $fullList
     * @return array
     */
    private function filter($fullList)
    {
        return array_map("unserialize", array_unique(array_map("serialize", $fullList)));
    }

    /**
     * @param $uniqList
     * @param $subject
     * @param $bag
     * @param $template
     */
    private function send($uniqList, $subject, $bag, $template)
    {
        foreach ($uniqList as $data) {
            $this->notify($subject, $data['email'], $data['locale'], $template, $bag);
        }
    }

    /**
     * @param $subject
     * @param $to
     * @param $locale
     * @param $template
     * @param $bag
     */
    private function notify($subject, $to, $locale, $template, $bag)
    {
        $this->container->get('mail')->notificationMail(
            'Information - '.$subject.' - Eazy by Mazars',
            $to,
            $locale,
            $template,
            $bag
        );
    }
}