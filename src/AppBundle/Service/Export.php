<?php

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\ORM\EntityManager;

class Export extends AbstractService
{
    private $mailer;

    public function __construct(EntityManager $entityManager, $mailer)
    {
        $this->mailer = $mailer;
        parent::__construct($entityManager);
    }

    public function export($email)
    {
        $tab = array();
        $services = $this->em->getRepository('AppBundle:Service')->findAll();
        $companies = $this->em->getRepository('AppBundle:Company')->findAll();
        foreach ($companies as $company) {
            $socialReason = $company->getName();
            $customerId = $company->getId();
            foreach ($services as $service) {
                $users = [];
                $date = date("m/Y");
                $serviceName = $service->getName();
                $serviceExternalId = $service->getExternalId();
                $usersDb = $this->em->getRepository('AppBundle:UserCompanyService')->getUserFromUserCompanyService(
                    $service->getId(),
                    $company->getId()
                );
                for ($j = 0; $j < sizeOf($usersDb); $j++) {
                    $r = [];
                    $roles = explode('"', $usersDb[$j]['role']);
                    for ($i = 1; $i < sizeOf($roles); $i += 2) {
                        array_push($r, $roles[$i]);
                    }
                    $users[$j]['Identifiant'] = $usersDb[$j]['id'];
                    $users[$j]['Prénom'] = $usersDb[$j]['firstName'];
                    $users[$j]['Nom'] = $usersDb[$j]['lastName'];
                    $users[$j]['Email'] = $usersDb[$j]['email'];
                    $users[$j]['Role'] = $r;
                }
                $usersQuantity = count($users);
                array_push(
                    $tab,
                    array(
                        "Date d'export" => $date,
                        "Identifiant Client" => $customerId,
                        "Raison Sociale" => $socialReason,
                        "Code Client" => null,
                        "Libellé Service" => $serviceName,
                        "Code Service" => $serviceExternalId,
                        "Quantité Utilisateur" => $usersQuantity,
                        "Utilisateur" => $users,
                    )
                );
            }
        }


        $message = \Swift_Message::newInstance();
        $message->setSubject("Export");
        $message->setFrom("contact@eazy-mazars.fr");
        $message->setTo($email)
            ->attach(\Swift_Attachment::newInstance(json_encode($tab), 'export.json', 'text/html'));
        $this->mailer->send($message);

        return true;
    }
}
