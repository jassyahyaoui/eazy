<?php
/**
 * Created by PhpStorm.
 * User: MAZ_USER3
 * Date: 24/09/2018
 * Time: 08:51
 */

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;


class AdfsMazars
{
    private $allowedDomains;
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->allowedDomains = $this->container->getParameter('ALLOWED_DOMAINS');
    }


    public function isInAllowedDomain($email)
    {
        $allowedDomains = explode(
            ',',
            str_replace(['[', ']'], ['', ''], $this->allowedDomains)
        );
        foreach ($allowedDomains as $domain) {
            if (strpos(strtolower($email), $domain) !== false) {
                return true;
            }
        }

        return false;
    }


}