<?php

namespace AppBundle\Service;


use Psr\Container\ContainerInterface;

class Notice
{
    private $container;
    private $allowedDomains;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->allowedDomains = $this->container->getParameter('ALLOWED_DOMAINS');
    }

    public function notice($user)
    {
        return $this->isUserOrOwner($user) && $this->isByPassRecaptcha($user) && !$this->isInAllowedDomain($user);
    }

    public function getOwners($user)
    {
        $owners = [];
        if (empty($this->getCompanies($user))) {
            return $owners;
        }
        foreach ($this->getCompanies($user) as $key => $company) {
            $owners[$key] = $this->container->get('doctrine.orm.default_entity_manager')
                ->getRepository('AppBundle:UserCompany')
                ->getOwners($company);
        }

        return $this->formatArray($owners);
    }

    private function isUserOrOwner($user)
    {
        if (empty($user->getUserCompany()->toArray())) {
            return false;
        }
        foreach ($user->getUserCompany()->toArray() as $userCompany) {
            if (in_array("ROLE_OWNER", $userCompany->getRole())) {
                return true;
            }
            if (in_array("ROLE_USER", $userCompany->getRole())) {
                return true;
            }
        }

        return false;
    }

    private function isByPassRecaptcha($user)
    {
        if ($user->getByPassRecaptcha() == true) {
            return false;
        }

        return true;
    }

    private function isInAllowedDomain($user)
    {
        $allowedDomains = explode(
            ',',
            str_replace(['[', ']'], ['', ''], $this->allowedDomains)
        );
        foreach ($allowedDomains as $domain) {
            if (strpos(strtolower($user->getEmail()), $domain) !== false) {
                return true;
            }
        }

        return false;
    }

    private function formatArray($array)
    {
        $response = [];
        foreach ($array as $data) {
            foreach ($data as $key => $item) {
                if (!in_array($item, $response)) {
                    array_push($response, $item);
                }
            }
        }

        return $response;

    }

    private function getCompanies($user)
    {
        $companies = [];
        if (empty($user->getUserCompany()->toArray())) {
            return $companies;
        }
        foreach ($user->getUserCompany()->toArray() as $userCompany) {
            array_push($companies, $userCompany->getCompany()->getId());
        }

        return $companies;
    }


}