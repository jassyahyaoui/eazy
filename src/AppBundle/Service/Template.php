<?php

namespace AppBundle\Service;


class Template
{
    /**
     * @param $template
     * @return array
     */
    public function fetchTemplateServices($template)
    {
        $values = array();
        $templateServiceValues = $template->getTemplateService()->getValues();
        foreach ($templateServiceValues as $key => $val) {
            $values[$key] = array(
                'serviceId' => $val->getService()->getId(),
                'serviceName' => $val->getService()->getName(),
                'eventManager' => $val->getService()->getEventManager(),
                'roleType' => $val->getService()->getRoleType(),
                'templateServiceId' => $val->getId(),
                'defaultStatus' => $val->getDefaultStatus(),
                'position' => $val->getPosition(),
                'enabled' => true,
            );
            for ($i = 0; $i < sizeof($val->getService()->getServiceRole()); $i++) {
                $values[$key]['serviceRole'][$i] = array(
                    'label' => $val->getService()->getServiceRole()->getValues()[$i]->getShortDesc(),
                    'id' => $val->getService()->getServiceRole()->getValues()[$i]->getId(),
                );
            }
        }

        return $values;
    }

}