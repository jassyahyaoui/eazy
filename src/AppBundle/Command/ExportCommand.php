<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:export')
            ->setDescription('Export Consommation des services par les clients [--email=...]')
            ->addOption(
                'email',
                null,
                InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
                'The e-mail to send to exported json file',
                null
            )
            ->setHelp(
                <<<EOT
                    The <info>%command.name%</info>command Export Consommation des services par les clients.
<info>php %command.full_name% [--email=...]</info>
EOT
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $email = $input->getOption('email');
        $export = $this->getContainer()->get('export.service');
        $export->export($email);
        $output->writeln(
            sprintf(
                'Export Consommation des services par les clients'
            )
        );
    }
}