<?php

namespace AppBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

trait UserProtectedFct
{

    /**
     * Get enabled.
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
}