<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class UserFilterType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'service',
                Filters\EntityFilterType::class,
                array(
                    'class' => 'AppBundle\Entity\Service',
                    'choice_label' => 'name',
                )
            )
            ->add(
                'company',
                Filters\EntityFilterType::class,
                array(
                    'class' => 'AppBundle\Entity\Company',
                    'choice_label' => 'name',
                )
            )
            ->add(
                'sirenCompany',
                EntityType::class,
                array(
                    'label' => 'SIREN entreprise',
                    'required' => false,
                    'class' => 'AppBundle\Entity\Company',
                    'choice_value' => 'businessIdentifier',
                    'choice_label' => 'businessIdentifier',
                    'query_builder' => function (EntityRepository $er) use ($options) {
                        return $er->createQueryBuilder('u')
                            ->orderBy('u.businessIdentifier', 'ASC');
                    },
                )
            )
            ->add(
                'office',
                Filters\EntityFilterType::class,
                array(
                    'class' => 'AppBundle\Entity\Office',
                    'choice_label' => 'name',
                )
            )
            ->add(
                'role',
                Filters\ChoiceFilterType::class,
                array(
                    'choices' => array(
                        'Role Owner' => 'ROLE_OWNER',
                        'Role Administrateur ' => 'ROLE_ADMIN',
                        'Role Super Admin' => "ROLE_SUPER_ADMIN",
                        'Role Contact' => "ROLE_CONTACT",
                        'Role User' => "ROLE_USER",
                    ),
                )
            )
            ->add(
                'firstName',
                Filters\NumberFilterType::class
            )
            ->add(
                'lastName',
                Filters\NumberFilterType::class
            )
            ->add(
                'username',
                Filters\NumberFilterType::class
            );
        $builder->setMethod("GET");
    }

    public function getBlockPrefix()
    {
        return null;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'allow_extra_fields' => true,
                'csrf_protection' => false,
                'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
            )
        );
    }

}
