<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class CompanyType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name')
                ->add('vatIdentifier')
                ->add('locked')
                ->add('adress')
                ->add('businessIdentifier')
                ->add('businessIdentifierExtended')
                ->add('businessClassification')
                ->add('businessRegisterOffice')
                ->add('legalRegisterOffice')
                ->add('shareCapital')
                ->add('city')
                ->add('postCode')
                ->add('fax')
                ->add('phone')
                ->add('website')
                ->add('email')
                ->add('template', EntityType::class, array(
                    'class' => 'AppBundle\Entity\Template',
                    'choice_label' => 'name',
                    'placeholder' => 'Please choose',
                    'empty_data' => null,
                    'required' => true
                ))
                ->add('type', EntityType::class, array(
                    'class' => 'AppBundle\Entity\companyType',
                    'choice_label' => 'name',
                    'placeholder' => 'Please choose',
                    'empty_data' => null,
                    'required' => false
                ))
                ->add('office', EntityType::class, array(
                    'class' => 'AppBundle\Entity\Office',
                    'choice_label' => 'name',
                    'placeholder' => 'Please choose',
                    'empty_data' => null,
                    'required' => true
                ))
                ->add('thumbnail', EntityType::class, array(
                    'class' => 'AppBundle\Entity\Thumbnail',
                    'choice_label' => 'name',
                    'placeholder' => 'Please choose',
                    'empty_data' => null,
                    'required' => false
                ))
                ->add('imageFile', VichFileType::class)
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Company'
        ));
    }

}
