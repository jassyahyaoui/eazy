<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;


class ServiceFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', Filters\NumberFilterType::class)
            ->add('name', Filters\TextFilterType::class)
            ->add('shortDesc', Filters\TextFilterType::class)
            ->add('longDesc', Filters\TextFilterType::class)
            ->add('helpLink', Filters\TextFilterType::class)
            ->add('connectionType', Filters\TextFilterType::class)
            ->add('enable', Filters\BooleanFilterType::class)
            ->add('isDefault', Filters\BooleanFilterType::class)
          ->add('eventManager', Filters\NumberFilterType::class) 
            ->add('eventEnable', Filters\BooleanFilterType::class)
            ->add('position', Filters\NumberFilterType::class)
            ->add('thumbnail', Filters\EntityFilterType::class, array(
                    'class' => 'AppBundle\Entity\Thumbnail',
                    'choice_label' => 'name',
            )) 
            ->add('companyService', Filters\EntityFilterType::class, array(
                    'class' => 'AppBundle\Entity\CompanyService',
                    'choice_label' => 'status',
            )) 
            ->add('templateService', Filters\EntityFilterType::class, array(
                    'class' => 'AppBundle\Entity\TemplateService',
                    'choice_label' => 'defaultStatus',
            )) 
            ->add('userCompanyService', Filters\EntityFilterType::class, array(
                    'class' => 'AppBundle\Entity\UserCompanyService',
                    'choice_label' => 'status',
            )) 
        ;
        $builder->setMethod("GET");


    }

    public function getBlockPrefix()
    {
        return null;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'allow_extra_fields' => true,
            'csrf_protection' => false,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }
}
