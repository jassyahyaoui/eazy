<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;


class TemplateFilterType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('id', Filters\NumberFilterType::class)
                ->add('name', Filters\TextFilterType::class)
                ->add('enable', Filters\BooleanFilterType::class)
                ->add('isDefault', Filters\BooleanFilterType::class)
                ->add('position', Filters\NumberFilterType::class)
                ->add('company', Filters\EntityFilterType::class, array(
                    'class' => 'AppBundle\Entity\Company',
                    'choice_label' => 'name',
                ))
                ->add('templateService', Filters\EntityFilterType::class, array(
                    'class' => 'AppBundle\Entity\TemplateService',
                    'choice_label' => 'defaultStatus',
                ))
        ;
        $builder->setMethod("GET");
    }

    public function getBlockPrefix() {
        return null;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'allow_extra_fields' => true,
            'csrf_protection' => false,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }

}
