<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;


class ThumbnailFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', Filters\NumberFilterType::class)
            ->add('name', Filters\TextFilterType::class)
            ->add('link', Filters\TextFilterType::class)
            ->add('updatedAt', Filters\DateTimeFilterType::class)
            ->add('createdAt', Filters\DateTimeFilterType::class)
        
            ->add('createdBy', Filters\EntityFilterType::class, array(
                    'class' => 'AppBundle\Entity\User',
                    'choice_label' => 'firstName',
            )) 
            ->add('updatedBy', Filters\EntityFilterType::class, array(
                    'class' => 'AppBundle\Entity\User',
                    'choice_label' => 'firstName',
            )) 
            ->add('company', Filters\EntityFilterType::class, array(
                    'class' => 'AppBundle\Entity\Company',
                    'choice_label' => 'name',
            )) 
            ->add('office', Filters\EntityFilterType::class, array(
                    'class' => 'AppBundle\Entity\Office',
                    'choice_label' => 'name',
            )) 
            ->add('service', Filters\EntityFilterType::class, array(
                    'class' => 'AppBundle\Entity\Service',
                    'choice_label' => 'name',
            )) 
            ->add('user', Filters\EntityFilterType::class, array(
                    'class' => 'AppBundle\Entity\User',
                    'choice_label' => 'firstName',
            )) 
        ;
        $builder->setMethod("GET");


    }

    public function getBlockPrefix()
    {
        return null;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'allow_extra_fields' => true,
            'csrf_protection' => false,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }
}
