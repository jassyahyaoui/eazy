<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class NotificationEditType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
//                ->add('type', ChoiceType::class, array(
//                    'choices' => array(
//                        'Office' => 'Office',
//                        'Propriétaire ' => 'Propriétaire',
//                        'Utilisateur' => "Utilisateur",
//                    ),
//                ))
//                ->add('office', EntityType::class, array(
//                    'class' => 'AppBundle:Office',
//                    'choice_label' => 'name'))
//                ->add('user', EntityType::class, array(
//                    'class' => 'AppBundle:User',
//                    'choice_label' => 'username'))
//                ->add('userCompany', EntityType::class, array(
//                    'class' => 'AppBundle:UserCompany',
//                    'choice_label' => 'company.name'))
//                ->add('subject')
//                ->add('message')
//                ->add('link')
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => null,
//            'office' => null,
        ));
    }

}
