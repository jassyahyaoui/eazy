<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;


class CompanyFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', Filters\NumberFilterType::class)
            ->add('name',Filters\TextFilterType::class)
                
            ->add('businessIdentifier', Filters\TextFilterType::class)
            ->add('vatIdentifier', Filters\TextFilterType::class)
            ->add('locked', Filters\BooleanFilterType::class)
            ->add('adress', Filters\TextFilterType::class)
           ->add('businessIdentifierExtended', Filters\textFilterType::class)
           ->add('businessClassification', Filters\textFilterType::class)
           ->add('businessRegisterOffice', Filters\textFilterType::class)
           ->add('legalRegisterOffice', Filters\textFilterType::class)
           ->add('city', Filters\textFilterType::class)
           ->add('postCode', Filters\textFilterType::class)
           ->add('createdAt', Filters\DateTimeFilterType::class)
           ->add('updatedAt', Filters\DateTimeFilterType::class)

            ->add('template', Filters\EntityFilterType::class, array(
                    'class' => 'AppBundle\Entity\Template',
                    'choice_label' => 'name',
                
                
            )) 
            ->add('office', Filters\EntityFilterType::class, array(
                    'class' => 'AppBundle\Entity\Office',
                    'choice_label' => 'name',
                
            )) 
            ->add('type', Filters\EntityFilterType::class, array(
                    'class' => 'AppBundle\Entity\companyType',
                    'choice_label' => 'name',
            )) 
            ->add('thumbnail', Filters\EntityFilterType::class, array(
                    'class' => 'AppBundle\Entity\Thumbnail',
                    'choice_label' => 'name',
            )) 
            ->add('userCompany', Filters\EntityFilterType::class, array(
                    'class' => 'AppBundle\Entity\UserCompany',
                    'choice_label' => 'id',
            )) 
        ;
        $builder->setMethod("GET");


    }

    public function getBlockPrefix()
    {
        return null;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'allow_extra_fields' => true,
            'csrf_protection' => false,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }
}
