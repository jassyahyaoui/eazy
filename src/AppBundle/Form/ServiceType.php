<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Form\ThumbnailType;

class ServiceType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name')
                ->add('shortDesc')
                ->add('longDesc')
                ->add('helpLink')
                ->add('enable')
                ->add('isDefault')
                ->add('position')
                ->add('eventEnable')
                ->add('externalId')
                ->add('connectionIdentifier')
                ->add('connectionTarget')
                ->add('thumbnail', ThumbnailType::class)
                ->add('connectionType', ChoiceType::class, array(
                    'choices' => array(
                        'Oauth' => 'Oauth'
                    ),
                ))
                ->add('eventManager', ChoiceType::class, array(
                    'choices' => array(
                        'ONEUP' => 'ONEUP',
                        'CEGID ' => 'CEGID'
                    ),
                ))
                ->add('roleType', ChoiceType::class, array(
                    'choices' => array(
                        'not applicable' => true,
                        'single' => true,
                        'multiple' => false), 'required' => true, 'placeholder' => 'Please choose'))
        ;
    }
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Service'
        ));
    }

}
