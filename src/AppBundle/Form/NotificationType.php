<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class NotificationType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('type', ChoiceType::class, array(
                    'choices' => array(
                        'Bureau' => 'Bureau',
                        'Companie' => 'Companie',
                        'Utilisateur' => "Utilisateur",
                        'Role' => "Role"
                    ),
                ))
                ->add('office', EntityType::class, array(
                    'class' => 'AppBundle:Office',
                    'choice_label' => 'name'))
                ->add('user', EntityType::class, array(
                    'class' => 'AppBundle:User',
                    'choice_label' => 'username'))
                ->add('company', EntityType::class, array(
                    'class' => 'AppBundle:Company',
                    'choice_label' => 'name'))
                ->add('role', ChoiceType::class, array(
                    'choices' => array(
                        'Contact' => 'ROLE_CONTACT',
                        'Utilisateur' => 'ROLE_USER',
                        'Propriétaire' => 'ROLE_OWNER'
                    ),
                ))
                ->add('service', EntityType::class, array(
                    'class' => 'AppBundle:Service',
                    'choice_label' => 'name'))
                ->add('sujet', ChoiceType::class, array("label" => "Catégorie",
                    'choices' => array(
                        'Entreprise' => 'Entreprise',
                        'Actualité ' => 'Actualité',
                        'Nouveauté' => "Nouveauté"
                    )
                ))
                ->add('message', TextareaType::class, array("attr"=>array("maxlength"  => 250)))
                ->add('link', TextType::class)
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => null,
//            'office' => null,
        ));
    }

}
