<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyServiceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('externalId')
            ->add('startedAt')
            ->add('endAt')
            ->add(
                'status',
                ChoiceType::class,
                array(
                    'choices' => array(
                        'Mandatory' => 'Mandatory',
                        'Enable ' => 'Enable',
                        'Disable' => "Disable",
                    ),
                )
            )
            ->add(
                'company',
                EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\company',
                    'choice_label' => 'name',
                    'placeholder' => 'Please choose',
                    'empty_data' => null,
                    'required' => true,

                )
            )
            ->add(
                'service',
                EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\Service',
                    'choice_label' => 'name',
                    'placeholder' => 'Please choose',
                    'empty_data' => null,
                    'required' => true,

                )
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'AppBundle\Entity\CompanyService',
            )
        );
    }
}
