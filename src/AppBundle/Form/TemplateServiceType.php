<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class TemplateServiceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//             ->add('defaultStatus')   
            
            ->add('position')
            ->add('createdBy', EntityType::class, array(
                'class' => 'AppBundle\Entity\User',
                'choice_label' => 'firstName',
                'placeholder' => 'Please choose',
                'empty_data' => null,
                'required' => false
 
            )) 
            ->add('updatedBy', EntityType::class, array(
                'class' => 'AppBundle\Entity\User',
                'choice_label' => 'firstName',
                'placeholder' => 'Please choose',
                'empty_data' => null,
                'required' => false
 
            )) 
            ->add('template', EntityType::class, array(
                'class' => 'AppBundle\Entity\Template',
                'choice_label' => 'name',
                'placeholder' => 'Please choose',
                'empty_data' => null,
                'required' => true
 
            )) 
            ->add('service', EntityType::class, array(
                'class' => 'AppBundle\Entity\Service',
                'choice_label' => 'name',
                'placeholder' => 'Please choose',
                'empty_data' => null,
                'required' => true
 
            )) 
                ->add('defaultStatus',ChoiceType::class, array(
                'placeholder' => 'Please choose',
                'empty_data' => null,
                'required' => true,
                'choices'  => array(
     
            'Mandatory' => 'Mandatory',
            'Enable' => 'Enable',
              'Disable' => 'Disable', 

                      )))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\TemplateService'
        ));
    }
}
