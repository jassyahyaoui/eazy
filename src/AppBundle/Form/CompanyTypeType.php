<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyTypeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('shortDesc')
            ->add('longDesc')
            ->add('enable')
            ->add('isDefault')
//            ->add('createdAt')
//            ->add('updatedAt')
            ->add('createdBy', EntityType::class, array(
                'class' => 'AppBundle\Entity\User',
                'choice_label' => 'firstName',
                'placeholder' => 'Please choose',
                'empty_data' => null,
                'required' => false
 
            )) 
            ->add('updatedBy', EntityType::class, array(
                'class' => 'AppBundle\Entity\User',
                'choice_label' => 'firstName',
                'placeholder' => 'Please choose',
                'empty_data' => null,
                'required' => false
 
            )) 
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\CompanyType'
        ));
    }
}
