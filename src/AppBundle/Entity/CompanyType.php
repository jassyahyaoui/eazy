<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Traits\Timestampable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CompanyType
 *
 * @ORM\Table(name="company_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CompanyTypeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CompanyType
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min=4, minMessage="NAME field '{{ value }}' is not valid") 
     * @Assert\Length(max=4, maxMessage="NAME field '{{ value }}' is not valid") 
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="shortDesc", type="text",nullable=true)
     * @Assert\NotBlank()
     * @Assert\Length(min=10, minMessage="Short description fied min length must be at least {{limit}} characters.") 
     * @Assert\Length(max=30, maxMessage="Short descriptionfield an not exceed {{limit}} characters.") 
     */
    private $shortDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="longDesc", type="text",nullable=true)
     * @Assert\NotBlank()
     * @Assert\Length(min=20, minMessage="LONGDESC fied min length must be at least {{limit}} characters.") 
     * @Assert\Length(max=255, maxMessage="LONGDESC field an not exceed {{limit}} characters.") 
     */
    private $longDesc;

    /**
     * @var bool
     *
     * @ORM\Column(name="enable", type="boolean",nullable=true)
     */
    private $enable;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_default", type="boolean",nullable=true)
     */
    private $isDefault;

    /**
     * One Created person has One User.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="companyTypeCreatedtedBy", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * One Updated person has One User.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="companyTypeUpdatedBy", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="Company", mappedBy="type", cascade={"persist", "remove"})
     */
    private $company;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return CompanyType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set shortDesc.
     *
     * @param string $shortDesc
     *
     * @return CompanyType
     */
    public function setShortDesc($shortDesc)
    {
        $this->shortDesc = $shortDesc;

        return $this;
    }

    /**
     * Get shortDesc.
     *
     * @return string
     */
    public function getShortDesc()
    {
        return $this->shortDesc;
    }

    /**
     * Set longDesc.
     *
     * @param string $longDesc
     *
     * @return CompanyType
     */
    public function setLongDesc($longDesc)
    {
        $this->longDesc = $longDesc;

        return $this;
    }

    /**
     * Get longDesc.
     *
     * @return string
     */
    public function getLongDesc()
    {
        return $this->longDesc;
    }

    /**
     * Set enable.
     *
     * @param bool $enable
     *
     * @return CompanyType
     */
    public function setEnable($enable)
    {
        $this->enable = $enable;

        return $this;
    }

    /**
     * Get enable.
     *
     * @return bool
     */
    public function getEnable()
    {
        return $this->enable;
    }

    /**
     * Set isDefault.
     *
     * @param bool $isDefault
     *
     * @return CompanyType
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    /**
     * Get isDefault.
     *
     * @return bool
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }

    /**
     * Set createdBy.
     *
     * @param \AppBundle\Entity\User|null $createdBy
     *
     * @return CompanyType
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy.
     *
     * @param \AppBundle\Entity\User|null $updatedBy
     *
     * @return CompanyType
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->company = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add company.
     *
     * @param \AppBundle\Entity\Company $company
     *
     * @return CompanyType
     */
    public function addCompany(\AppBundle\Entity\Company $company)
    {
        $this->company[] = $company;

        return $this;
    }

    /**
     * Remove company.
     *
     * @param \AppBundle\Entity\Company $company
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCompany(\AppBundle\Entity\Company $company)
    {
        return $this->company->removeElement($company);
    }

    /**
     * Get company.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompany()
    {
        return $this->company;
    }
}
