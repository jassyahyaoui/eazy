<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Mgilet\NotificationBundle\Annotation\Notifiable;
use Mgilet\NotificationBundle\NotifiableInterface;
use AppBundle\Traits\Timestampable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Office
 *
 * @ORM\Table(name="office")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OfficeRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @ORM\HasLifecycleCallbacks()
 * @Notifiable(name="user_notification")
 */
class Office implements NotifiableInterface
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(max=131, maxMessage="NAME field can not exceed {{limit}} characters.")
     */
    private $name;

    /**
     * One Office has One Thumbnail.
     * @ORM\OneToOne(targetEntity="Thumbnail", inversedBy="office", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="thumbnail_id", referencedColumnName="id" ,nullable=true)
     */
    private $thumbnail;

    /**
     * @var string
     *
     * @ORM\Column(name="business_identifier", type="string", length=255)
     * @Assert\Length(min=9, minMessage="Business identifier field '{{ value }}' is not valid.The field must be 9 characters length.")
     * @Assert\Length(max=9, maxMessage="Business identifier field '{{ value }}' is not valid.The field must be 9 characters length.")
     */
    private $businessIdentifier;

    /**
     * @var string
     *
     * @ORM\Column(name="adress", type="text",nullable=true)
     */
    private $adress;

    /**
     * @var bool
     *
     * @ORM\Column(name="enable", type="boolean",nullable=true)
     */
    private $enable;

    /**
     * @var string
     *
     * @ORM\Column(name="vat_identifier", type="string", length=255,nullable=true)
     */
    private $vatIdentifier;

    /**
     * @var string
     *
     * @ORM\Column(name="url_link", type="string", length=255,nullable=true)
     * @Assert\NotBlank()
     * @Assert\Url(message = "The url link field '{{ value }}' is not a valid url.")
     */
    private $urlLink;

    /**
     * One Created person has One User.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="officeCreatedtedBy", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * One Updated person has One User.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="officeUpdatedBy", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="Company", mappedBy="office", cascade={"persist", "remove"})
     */
    private $company;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->company = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Office
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set businessIdentifier.
     *
     * @param string $businessIdentifier
     *
     * @return Office
     */
    public function setBusinessIdentifier($businessIdentifier)
    {
        $this->businessIdentifier = $businessIdentifier;

        return $this;
    }

    /**
     * Get businessIdentifier.
     *
     * @return string
     */
    public function getBusinessIdentifier()
    {
        return $this->businessIdentifier;
    }

    /**
     * Set vatIdentifier.
     *
     * @param string $vatIdentifier
     *
     * @return Office
     */
    public function setVatIdentifier($vatIdentifier)
    {
        $this->vatIdentifier = $vatIdentifier;

        return $this;
    }

    /**
     * Get vatIdentifier.
     *
     * @return string
     */
    public function getVatIdentifier()
    {
        return $this->vatIdentifier;
    }

    /**
     * Set adress.
     *
     * @param string $adress
     *
     * @return Office
     */
    public function setAdress($adress)
    {
        $this->adress = $adress;

        return $this;
    }

    /**
     * Get adress.
     *
     * @return string
     */
    public function getAdress()
    {
        return $this->adress;
    }

    /**
     * Set enable.
     *
     * @param bool $enable
     *
     * @return Office
     */
    public function setEnable($enable)
    {
        $this->enable = $enable;

        return $this;
    }

    /**
     * Get enable.
     *
     * @return bool
     */
    public function getEnable()
    {
        return $this->enable;
    }

    /**
     * Set createdBy.
     *
     * @param \AppBundle\Entity\User|null $createdBy
     *
     * @return Office
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy.
     *
     * @param \AppBundle\Entity\User|null $updatedBy
     *
     * @return Office
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Add company.
     *
     * @param \AppBundle\Entity\Company $company
     *
     * @return Office
     */
    public function addCompany(\AppBundle\Entity\Company $company)
    {
        $this->company[] = $company;

        return $this;
    }

    /**
     * Remove company.
     *
     * @param \AppBundle\Entity\Company $company
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCompany(\AppBundle\Entity\Company $company)
    {
        return $this->company->removeElement($company);
    }

    /**
     * Get company.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set thumbnail.
     *
     * @param \AppBundle\Entity\Thumbnail|null $thumbnail
     *
     * @return Office
     */
    public function setThumbnail(\AppBundle\Entity\Thumbnail $thumbnail = null)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail.
     *
     * @return \AppBundle\Entity\Thumbnail|null
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set urlLink.
     *
     * @param string|null $urlLink
     *
     * @return Office
     */
    public function setUrlLink($urlLink = null)
    {
        $this->urlLink = $urlLink;

        return $this;
    }

    /**
     * Get urlLink.
     *
     * @return string|null
     */
    public function getUrlLink()
    {
        return $this->urlLink;
    }

    /**
     * Set deletedAt.
     *
     * @param \DateTime|null $deletedAt
     *
     * @return Office
     */
    public function setDeletedAt($deletedAt = null)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt.
     *
     * @return \DateTime|null
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
