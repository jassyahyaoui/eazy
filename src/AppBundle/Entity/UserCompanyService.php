<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Traits\Timestampable;

/**
 * UserCompanyService
 *
 * @ORM\Table(name="user_company_service")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserCompanyServiceRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class UserCompanyService
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Many UserCompanies have One Company.
     * @ORM\ManyToOne(targetEntity="UserCompany", inversedBy="userCompanyService", cascade={"all"})
     * @ORM\JoinColumn(name="user_company_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $userCompany;

    /**
     * Many UserCompanies have One Company.
     * @ORM\ManyToOne(targetEntity="Service", inversedBy="userCompanyService", cascade={"persist"})
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id")
     */
    private $service;

    /**
     * @var string
     *
     * @ORM\Column(name="external_id", type="string", length=255,nullable=true)
     */
    private $externalId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="started_at", type="datetime")
     */
    private $startedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_at", type="datetime")
     */
    private $endAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_login_at", type="datetime",nullable=true)
     */
    private $lastLoginAt;


    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
    private $status;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled;

    /**
     * One Created person has One User.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userCompanyServiceCreatedtedBy", cascade={"persist"})
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * One Updated person has One User.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userCompanyServiceUpdatedBy", cascade={"persist"})
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="UserCompanyServiceRoles", mappedBy="userCompanyService", cascade={"persist", "remove"})
     */
    private $userCompanyServiceRoles;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startedAt.
     *
     * @param \DateTime $startedAt
     *
     * @return UserCompanyService
     */
    public function setStartedAt($startedAt)
    {
        $this->startedAt = $startedAt;

        return $this;
    }

    /**
     * Get startedAt.
     *
     * @return \DateTime
     */
    public function getStartedAt()
    {
        return $this->startedAt;
    }

    /**
     * Set endAt.
     *
     * @param \DateTime $endAt
     *
     * @return UserCompanyService
     */
    public function setEndAt($endAt)
    {
        $this->endAt = $endAt;

        return $this;
    }

    /**
     * Get endAt.
     *
     * @return \DateTime
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * Set lastLoginAt.
     *
     * @param \DateTime $lastLoginAt
     *
     * @return UserCompanyService
     */
    public function setLastLoginAt($lastLoginAt)
    {
        $this->lastLoginAt = $lastLoginAt;

        return $this;
    }

    /**
     * Get lastLoginAt.
     *
     * @return \DateTime
     */
    public function getLastLoginAt()
    {
        return $this->lastLoginAt;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return UserCompanyService
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdBy.
     *
     * @param \AppBundle\Entity\User|null $createdBy
     *
     * @return UserCompanyService
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy.
     *
     * @param \AppBundle\Entity\User|null $updatedBy
     *
     * @return UserCompanyService
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set userCompany.
     *
     * @param \AppBundle\Entity\UserCompany|null $userCompany
     *
     * @return UserCompanyService
     */
    public function setUserCompany(\AppBundle\Entity\UserCompany $userCompany = null)
    {
        $this->userCompany = $userCompany;

        return $this;
    }

    /**
     * Get userCompany.
     *
     * @return \AppBundle\Entity\UserCompany|null
     */
    public function getUserCompany()
    {
        return $this->userCompany;
    }

    /**
     * Set service.
     *
     * @param \AppBundle\Entity\Service|null $service
     *
     * @return UserCompanyService
     */
    public function setService(\AppBundle\Entity\Service $service = null)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service.
     *
     * @return \AppBundle\Entity\Service|null
     */
    public function getService()
    {
        return $this->service;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userCompanyServiceRoles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set externalId.
     *
     * @param string $externalId
     *
     * @return UserCompanyService
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * Get externalId.
     *
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * Add userCompanyServiceRole.
     *
     * @param \AppBundle\Entity\UserCompanyServiceRoles $userCompanyServiceRole
     *
     * @return UserCompanyService
     */
    public function addUserCompanyServiceRole(\AppBundle\Entity\UserCompanyServiceRoles $userCompanyServiceRole)
    {
        $this->userCompanyServiceRoles[] = $userCompanyServiceRole;

        return $this;
    }

    /**
     * Remove userCompanyServiceRole.
     *
     * @param \AppBundle\Entity\UserCompanyServiceRoles $userCompanyServiceRole
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUserCompanyServiceRole(\AppBundle\Entity\UserCompanyServiceRoles $userCompanyServiceRole)
    {
        return $this->userCompanyServiceRoles->removeElement($userCompanyServiceRole);
    }

    /**
     * Get userCompanyServiceRoles.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserCompanyServiceRoles()
    {
        return $this->userCompanyServiceRoles;
    }

    /**
     * Set enabled.
     *
     * @param bool $enabled
     *
     * @return UserCompanyService
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled.
     *
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
}
