<?php

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use AppBundle\Traits\Timestampable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ServiceRole
 *
 * @ORM\Table(name="service_role")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ServiceRoleRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ServiceRole
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Many Offices have One Company.
     * @ORM\ManyToOne(targetEntity="Service", inversedBy="serviceRole", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id")
     */
    private $service;

    /**
     * @var string
     *
     * @ORM\Column(name="ShortDesc", type="string", length=255,nullable=true)
     * @Assert\NotBlank()
     * @Assert\Length(min=5, minMessage="Short description fied min length must be at least {{limit}} characters.") 
     * @Assert\Length(max=30, maxMessage="Short description field an not exceed {{limit}} characters.") 
     */
    private $shortDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="LongDesc", type="text")
     */
    private $longDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="ExternalId", type="string", length=255,nullable=true)
     * @Assert\NotBlank()
     */
    private $externalId;

    /**
     * @var bool
     *
     * @ORM\Column(name="isDefault", type="boolean",nullable=true)
     */
    private $isDefault;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer",nullable=true)
     */
    private $position;

    /**
     * One Created person has One User.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="serviceRoleCreatedtedBy", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id" )
     */
    private $createdBy;

    /**
     * One Updated person has One User.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="serviceRoleUpdatedBy", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="UserCompanyServiceRoles", mappedBy="serviceRole", cascade={"persist", "remove"})
     */
    private $userCompanyServiceRoles;
    
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set shortDesc.
     *
     * @param string $shortDesc
     *
     * @return ServiceRole
     */
    public function setShortDesc($shortDesc)
    {
        $this->shortDesc = $shortDesc;

        return $this;
    }

    /**
     * Get shortDesc.
     *
     * @return string
     */
    public function getShortDesc()
    {
        return $this->shortDesc;
    }

    /**
     * Set longDesc.
     *
     * @param string $longDesc
     *
     * @return ServiceRole
     */
    public function setLongDesc($longDesc)
    {
        $this->longDesc = $longDesc;

        return $this;
    }

    /**
     * Get longDesc.
     *
     * @return string
     */
    public function getLongDesc()
    {
        return $this->longDesc;
    }

    /**
     * Set externalId.
     *
     * @param string $externalId
     *
     * @return ServiceRole
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * Get externalId.
     *
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * Set isDefault.
     *
     * @param bool $isDefault
     *
     * @return ServiceRole
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    /**
     * Get isDefault.
     *
     * @return bool
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }

    /**
     * Set position.
     *
     * @param int $position
     *
     * @return ServiceRole
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set service.
     *
     * @param \AppBundle\Entity\Service|null $service
     *
     * @return ServiceRole
     */
    public function setService(\AppBundle\Entity\Service $service = null)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service.
     *
     * @return \AppBundle\Entity\Service|null
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set createdBy.
     *
     * @param \AppBundle\Entity\User|null $createdBy
     *
     * @return ServiceRole
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy.
     *
     * @param \AppBundle\Entity\User|null $updatedBy
     *
     * @return ServiceRole
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userCompanyServiceRoles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add userCompanyServiceRole.
     *
     * @param \AppBundle\Entity\UserCompanyServiceRoles $userCompanyServiceRole
     *
     * @return ServiceRole
     */
    public function addUserCompanyServiceRole(\AppBundle\Entity\UserCompanyServiceRoles $userCompanyServiceRole)
    {
        $this->userCompanyServiceRoles[] = $userCompanyServiceRole;

        return $this;
    }

    /**
     * Remove userCompanyServiceRole.
     *
     * @param \AppBundle\Entity\UserCompanyServiceRoles $userCompanyServiceRole
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUserCompanyServiceRole(\AppBundle\Entity\UserCompanyServiceRoles $userCompanyServiceRole)
    {
        return $this->userCompanyServiceRoles->removeElement($userCompanyServiceRole);
    }

    /**
     * Get userCompanyServiceRoles.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserCompanyServiceRoles()
    {
        return $this->userCompanyServiceRoles;
    }
}
