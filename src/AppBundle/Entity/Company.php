<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Traits\Timestampable;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Company
 *
 * @ORM\Table(name="company")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CompanyRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable
 */
class Company {

    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(max=131, maxMessage="NAME field can not exceed {{limit}} characters.") 
     */
    private $name;

    /**
     * Many Templates have One Company.
     * @ORM\ManyToOne(targetEntity="Template", inversedBy="company", cascade={"persist"})
     * @ORM\JoinColumn(name="template_id", referencedColumnName="id")
     */
    private $template;

    /**
     * Many Offices have One Company.
     * @ORM\ManyToOne(targetEntity="Office", inversedBy="company", cascade={"persist"})
     * @ORM\JoinColumn(name="office_id", referencedColumnName="id")
     */
    private $office;

    /**
     * One Company has One Thumbnail.
     * @ORM\OneToOne(targetEntity="Thumbnail", inversedBy="company", cascade={"persist"})
     * @ORM\JoinColumn(name="thumbnail_id", referencedColumnName="id")
     */
    private $thumbnail;

    /**
     * @var string
     *
     * @ORM\Column(name="business_identifier", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min=9, minMessage="BUSINESSIDENTIFIER field '{{ value }}' is not valid.The field must be 14 characters length") 
     * @Assert\Length(max=9, maxMessage="BUSINESSIDENTIFIER field '{{ value }}' is not valid.The field must be 14 characters length") 
     */
    private $businessIdentifier;

    /**
     * @var string
     *
     * @ORM\Column(name="business_identifier_extended", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min=14, minMessage="BUSINESSIDENTIFIEREXTENDED field '{{ value }}' is not valid. The field must be 14 characters length.") 
     * @Assert\Length(max=14, maxMessage="BUSINESSIDENTIFIEREXTENDED field '{{ value }}' is not valid.The field must be 14 characters length") 
     */
    private $businessIdentifierExtended;

    /**
     * @var string
     *
     * @ORM\Column(name="business_classification", type="string", length=255,nullable=true)
     * @Assert\Length(min=5, minMessage="BUSINESSCLASSIFICATION field '{{ value }}' is not valid.The field must be 14 characters length") 
     * @Assert\Length(max=5, maxMessage="BUSINESSCLASSIFICATION field '{{ value }}' is not valid.The field must be 14 characters length") 
     */
    private $businessClassification;

    /**
     * @var string
     *
     * @ORM\Column(name="business_register_office", type="string", length=255,nullable=true)
     */
    private $businessRegisterOffice;

    /**
     * @var string
     *
     * @ORM\Column(name="legal_register_office", type="string", length=255,nullable=true)
     * @Assert\NotBlank()
     */
    private $legalRegisterOffice;

    /**
     * @var string
     *
     * @ORM\Column(name="vat_identifier", type="string", length=255,nullable=true)
     * @Assert\Length(max=13, maxMessage="VATIDENTIFIER field can not exceed {{limit}} characters.")
     */
    private $vatIdentifier;

    /**
     * @var int
     *
     * @ORM\Column(name="share_capital", type="integer")
     * @Assert\NotBlank()
     * @Assert\Type(type="integer", message="SHARECAPITAL field must be integer.")
     */
    private $shareCapital;

    /**
     * @var bool
     *
     * @ORM\Column(name="locked", type="boolean" ,nullable=true)
     */
    private $locked;

    /**
     * @var string
     *
     * @ORM\Column(name="adress", type="text" )
     * @Assert\NotBlank() 
     */
    private $adress;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(max=32, maxMessage="CITY field can not exceed {{limit}} characters.") 
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="post_code", type="string", length=255 ,nullable=true)
     * @Assert\NotBlank()
     * @Assert\Length(min=5, minMessage="POSTCODE field '{{ value }}' is not valid.") 
     * @Assert\Length(max=5, maxMessage="POSTCODE field '{{ value }}' is not valid.") 
     */
    private $postCode;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255 ,nullable=true)
     * @Assert\NotBlank()
     * @Assert\Length(max=14, maxMessage="PHONE field can not exceed {{limit}} characters.") 
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=255 ,nullable=true)
     * @Assert\Length(max=14, maxMessage="FAX field can not exceed {{limit}} characters.") 
     */
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=255 ,nullable=true)
     * @Assert\Url(message = "The WEBSITE field '{{ value }}' is not a valid url")
     */
    private $website;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255 ,nullable=true)
     * @Assert\NotBlank()
     * @Assert\Email(message = "The EMAIL field '{{ value }}' is not a valid email.", checkMX = true )
     */
    private $email;

    /**
     * One Created person has One User.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="companyCreatedtedBy", cascade={"persist"})
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * One Updated person has One User.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="companyUpdatedBy", cascade={"persist"})
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="UserCompany", mappedBy="company", cascade={"persist"})
     */
    private $userCompany;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="User", mappedBy="currentCompany", cascade={"persist"})
     */
    private $user;

    /**
     * Many Templates have One Company.
     * @ORM\ManyToOne(targetEntity="CompanyType", inversedBy="company", cascade={"persist"})
     * @ORM\JoinColumn(name="company_type_id", referencedColumnName="id",nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Constructor
     */
    public function __construct() {
        $this->userCompany = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="CompanyService", mappedBy="company", cascade={"persist"})
     */
    private $companyService;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Company
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set businessIdentifier.
     *
     * @param string $businessIdentifier
     *
     * @return Company
     */
    public function setBusinessIdentifier($businessIdentifier) {
        $this->businessIdentifier = $businessIdentifier;

        return $this;
    }

    /**
     * Get businessIdentifier.
     *
     * @return string
     */
    public function getBusinessIdentifier() {
        return $this->businessIdentifier;
    }

    /**
     * Set vatIdentifier.
     *
     * @param string $vatIdentifier
     *
     * @return Company
     */
    public function setVatIdentifier($vatIdentifier) {
        $this->vatIdentifier = $vatIdentifier;

        return $this;
    }

    /**
     * Get vatIdentifier.
     *
     * @return string
     */
    public function getVatIdentifier() {
        return $this->vatIdentifier;
    }

    /**
     * Set locked.
     *
     * @param bool $locked
     *
     * @return Company
     */
    public function setLocked($locked) {
        $this->locked = $locked;

        return $this;
    }

    /**
     * Get locked.
     *
     * @return bool
     */
    public function getLocked() {
        return $this->locked;
    }

    /**
     * Set adress.
     *
     * @param string $adress
     *
     * @return Company
     */
    public function setAdress($adress) {
        $this->adress = $adress;

        return $this;
    }

    /**
     * Get adress.
     *
     * @return string
     */
    public function getAdress() {
        return $this->adress;
    }

    /**
     * Set createdBy.
     *
     * @param \AppBundle\Entity\User|null $createdBy
     *
     * @return Company
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null) {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getCreatedBy() {
        return $this->createdBy;
    }

    /**
     * Set updatedBy.
     *
     * @param \AppBundle\Entity\User|null $updatedBy
     *
     * @return Company
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null) {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getUpdatedBy() {
        return $this->updatedBy;
    }

    /**
     * Set template.
     *
     * @param \AppBundle\Entity\Template|null $template
     *
     * @return Company
     */
    public function setTemplate(\AppBundle\Entity\Template $template = null) {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template.
     *
     * @return \AppBundle\Entity\Template|null
     */
    public function getTemplate() {
        return $this->template;
    }

    /**
     * Set office.
     *
     * @param \AppBundle\Entity\Office|null $office
     *
     * @return Company
     */
    public function setOffice(\AppBundle\Entity\Office $office = null) {
        $this->office = $office;

        return $this;
    }

    /**
     * Get office.
     *
     * @return \AppBundle\Entity\Office|null
     */
    public function getOffice() {
        return $this->office;
    }

    /**
     * Set thumbnail.
     *
     * @param \AppBundle\Entity\Thumbnail|null $thumbnail
     *
     * @return Company
     */
    public function setThumbnail(\AppBundle\Entity\Thumbnail $thumbnail = null) {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail.
     *
     * @return \AppBundle\Entity\Thumbnail|null
     */
    public function getThumbnail() {
        return $this->thumbnail;
    }

    /**
     * Add userCompany.
     *
     * @param \AppBundle\Entity\UserCompany $userCompany
     *
     * @return Company
     */
    public function addUserCompany(\AppBundle\Entity\UserCompany $userCompany) {
        $this->userCompany[] = $userCompany;

        return $this;
    }

    /**
     * Remove userCompany.
     *
     * @param \AppBundle\Entity\UserCompany $userCompany
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUserCompany(\AppBundle\Entity\UserCompany $userCompany) {
        return $this->userCompany->removeElement($userCompany);
    }

    /**
     * Get userCompany.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserCompany() {
        return $this->userCompany;
    }

    /**
     * Add user.
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Company
     */
    public function addUser(\AppBundle\Entity\User $user) {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Remove user.
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUser(\AppBundle\Entity\User $user) {
        return $this->user->removeElement($user);
    }

    /**
     * Get user.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set companyType.
     *
     * @param \AppBundle\Entity\CompanyType|null $companyType
     *
     * @return Company
     */
    public function setCompanyType(\AppBundle\Entity\CompanyType $companyType = null) {
        $this->companyType = $companyType;

        return $this;
    }

    /**
     * Get companyType.
     *
     * @return \AppBundle\Entity\CompanyType|null
     */
    public function getCompanyType() {
        return $this->companyType;
    }

    /**
     * Set businessIdentifierExtended.
     *
     * @param string $businessIdentifierExtended
     *
     * @return Company
     */
    public function setBusinessIdentifierExtended($businessIdentifierExtended) {
        $this->businessIdentifierExtended = $businessIdentifierExtended;

        return $this;
    }

    /**
     * Get businessIdentifierExtended.
     *
     * @return string
     */
    public function getBusinessIdentifierExtended() {
        return $this->businessIdentifierExtended;
    }

    /**
     * Set businessClassification.
     *
     * @param string $businessClassification
     *
     * @return Company
     */
    public function setBusinessClassification($businessClassification) {
        $this->businessClassification = $businessClassification;

        return $this;
    }

    /**
     * Get businessClassification.
     *
     * @return string
     */
    public function getBusinessClassification() {
        return $this->businessClassification;
    }

    /**
     * Set businessRegisterOffice.
     *
     * @param string $businessRegisterOffice
     *
     * @return Company
     */
    public function setBusinessRegisterOffice($businessRegisterOffice) {
        $this->businessRegisterOffice = $businessRegisterOffice;

        return $this;
    }

    /**
     * Get businessRegisterOffice.
     *
     * @return string
     */
    public function getBusinessRegisterOffice() {
        return $this->businessRegisterOffice;
    }

    /**
     * Set legalRegisterOffice.
     *
     * @param string $legalRegisterOffice
     *
     * @return Company
     */
    public function setLegalRegisterOffice($legalRegisterOffice) {
        $this->legalRegisterOffice = $legalRegisterOffice;

        return $this;
    }

    /**
     * Get legalRegisterOffice.
     *
     * @return string
     */
    public function getLegalRegisterOffice() {
        return $this->legalRegisterOffice;
    }

    /**
     * Set shareCapital.
     *
     * @param int $shareCapital
     *
     * @return Company
     */
    public function setShareCapital($shareCapital) {
        $this->shareCapital = $shareCapital;

        return $this;
    }

    /**
     * Get shareCapital.
     *
     * @return int
     */
    public function getShareCapital() {
        return $this->shareCapital;
    }

    /**
     * Set city.
     *
     * @param string $city
     *
     * @return Company
     */
    public function setCity($city) {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string
     */
    public function getCity() {
        return $this->city;
    }

    /**
     * Set postCode.
     *
     * @param string $postCode
     *
     * @return Company
     */
    public function setPostCode($postCode) {
        $this->postCode = $postCode;

        return $this;
    }

    /**
     * Get postCode.
     *
     * @return string
     */
    public function getPostCode() {
        return $this->postCode;
    }

    /**
     * Set phone.
     *
     * @param string $phone
     *
     * @return Company
     */
    public function setPhone($phone) {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return string
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     * Set fax.
     *
     * @param string $fax
     *
     * @return Company
     */
    public function setFax($fax) {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax.
     *
     * @return string
     */
    public function getFax() {
        return $this->fax;
    }

    /**
     * Set website.
     *
     * @param string $website
     *
     * @return Company
     */
    public function setWebsite($website) {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website.
     *
     * @return string
     */
    public function getWebsite() {
        return $this->website;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return Company
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set type.
     *
     * @param \AppBundle\Entity\CompanyType|null $type
     *
     * @return Company
     */
    public function setType(\AppBundle\Entity\CompanyType $type = null) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return \AppBundle\Entity\CompanyType|null
     */
    public function getType() {
        return $this->type;
    }

      /**
     * @Vich\UploadableField(mapping="company_image", fileNameProperty="imageName")
     * 
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     *
     * @var string
     */
    private $imageName;
    
//    public function setImageFile(File $imageName = null) {
//        $this->imageFile = $imageName;
//
//        // VERY IMPORTANT:
//        // It is required that at least one field changes if you are using Doctrine,
//        // otherwise the event listeners won't be called and the file is lost
//        if ($image) {
//            // if 'updatedAt' is not defined in your entity, use another property
//            $this->updatedAt = new \DateTime('now');
//        }
//    }

    public function getImageFile() {
        return $this->imageFile;
    }

    public function setImageName($imageName) {
        $this->imageName = $imageName;
    }

    public function getImageName() {
        return $this->imageName;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imageName
     *
     * @return Devis
     */
    public function setImageFile(File $imageName = null) {
        $this->imageFile = $imageName;
        if ($imageName)
            $this->updatedAt = new \DateTimeImmutable();
        return $this;
    }

    /**
     * Set deletedAt.
     *
     * @param \DateTime|null $deletedAt
     *
     * @return Company
     */
    public function setDeletedAt($deletedAt = null)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt.
     *
     * @return \DateTime|null
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Add companyService.
     *
     * @param \AppBundle\Entity\CompanyService $companyService
     *
     * @return Company
     */
    public function addCompanyService(\AppBundle\Entity\CompanyService $companyService)
    {
        $this->companyService[] = $companyService;

        return $this;
    }

    /**
     * Remove companyService.
     *
     * @param \AppBundle\Entity\CompanyService $companyService
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCompanyService(\AppBundle\Entity\CompanyService $companyService)
    {
        return $this->companyService->removeElement($companyService);
    }

    /**
     * Get companyService.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompanyService()
    {
        return $this->companyService;
    }
}
