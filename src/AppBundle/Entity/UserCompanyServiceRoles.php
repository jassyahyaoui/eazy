<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Traits\Timestampable;

/**
 * UserCompanyServiceRoles
 *
 * @ORM\Table(name="user_company_service_roles")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserCompanyServiceRolesRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class UserCompanyServiceRoles
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Many Offices have One Company.
     * @ORM\ManyToOne(targetEntity="UserCompanyService", inversedBy="userCompanyServiceRoles", cascade={"all"})
     * @ORM\JoinColumn(name="user_company_service_id", referencedColumnName="id")
     */
    private $userCompanyService;

    /**
     * Many Offices have One Company.
     * @ORM\ManyToOne(targetEntity="ServiceRole", inversedBy="userCompanyServiceRoles", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="service_role_id", referencedColumnName="id")
     */
    private $serviceRole;

    /**
     * One Created person has One User.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userCompanyServiceRolesCreatedtedBy", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * One Updated person has One User.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userCompanyServiceRolesUpdatedBy", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userCompanyService.
     *
     * @param \AppBundle\Entity\UserCompanyService|null $userCompanyService
     *
     * @return UserCompanyServiceRoles
     */
    public function setUserCompanyService(\AppBundle\Entity\UserCompanyService $userCompanyService = null)
    {
        $this->userCompanyService = $userCompanyService;

        return $this;
    }

    /**
     * Get userCompanyService.
     *
     * @return \AppBundle\Entity\UserCompanyService|null
     */
    public function getUserCompanyService()
    {
        return $this->userCompanyService;
    }


    /**
     * Set createdBy.
     *
     * @param \AppBundle\Entity\User|null $createdBy
     *
     * @return UserCompanyServiceRoles
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy.
     *
     * @param \AppBundle\Entity\User|null $updatedBy
     *
     * @return UserCompanyServiceRoles
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set serviceRole.
     *
     * @param \AppBundle\Entity\ServiceRole|null $serviceRole
     *
     * @return UserCompanyServiceRoles
     */
    public function setServiceRole(\AppBundle\Entity\ServiceRole $serviceRole = null)
    {
        $this->serviceRole = $serviceRole;

        return $this;
    }

    /**
     * Get serviceRole.
     *
     * @return \AppBundle\Entity\ServiceRole|null
     */
    public function getServiceRole()
    {
        return $this->serviceRole;
    }
}
