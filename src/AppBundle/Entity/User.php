<?php

// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Traits\UserProtectedFct;
use Mgilet\NotificationBundle\Annotation\Notifiable;
use Mgilet\NotificationBundle\NotifiableInterface;
use AppBundle\Traits\Timestampable;
use Symfony\Component\Validator\Constraints as Assert;
use Scheb\TwoFactorBundle\Model\Email\TwoFactorInterface;
use Scheb\TwoFactorBundle\Model\TrustedComputerInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @Notifiable(name="user_notification")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser implements NotifiableInterface, TwoFactorInterface, TrustedComputerInterface
{

    use UserProtectedFct;
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
        $this->trusted = true;
        // your own logic
    }

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255,nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255,nullable=true)
     */
    private $lastName;

    /**
     * One User has One Thumbnail.
     * @ORM\OneToOne(targetEntity="Thumbnail", inversedBy="user", cascade={"persist"})
     * @ORM\JoinColumn(name="thumbnail_id", referencedColumnName="id",nullable=true)
     */
    private $thumbnail;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string",nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", length=255,nullable=true)
     */
    private $mobile;
    /**
     * @var integer
     *
     * @ORM\Column(name="mobile_country", type="integer",nullable=true)
     */
    private $mobileCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255,nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="im_identifier", type="string", length=255,nullable=true)
     */
    private $imIdentifier;

    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string", length=255,nullable=true)
     */
    private $locale;

    /**
     * One Created person has One User.
     * @ORM\ManyToOne(targetEntity="User", cascade={"persist"})
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id",nullable=true)
     */
    private $createdBy;

    /**
     * One Updated person has One User.
     * @ORM\ManyToOne(targetEntity="User", cascade={"persist"})
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id",nullable=true)
     */
    private $updatedBy;

    /**
     * Many Features have One Product.
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="user", cascade={"persist"})
     * @ORM\JoinColumn(name="current_company_id", referencedColumnName="id")
     */
    private $currentCompany;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="UserCompany", mappedBy="user", cascade={"persist"})
     */
    private $userCompany;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="UserCompanyService", mappedBy="updatedBy", cascade={"persist"})
     */
    private $userCompanyServiceUpdatedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="UserCompanyService", mappedBy="createdBy", cascade={"persist"})
     */
    private $userCompanyServiceCreatedtedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="Thumbnail", mappedBy="updatedBy", cascade={"persist"})
     */
    private $thumbnailUpdatedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="Thumbnail", mappedBy="createdBy", cascade={"persist"})
     */
    private $thumbnailCreatedtedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="TemplateService", mappedBy="updatedBy", cascade={"persist"})
     */
    private $templateServiceUpdatedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="TemplateService", mappedBy="createdBy", cascade={"persist"})
     */
    private $templateServiceCreatedtedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="Template", mappedBy="updatedBy", cascade={"persist"})
     */
    private $templateUpdatedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="Template", mappedBy="createdBy", cascade={"persist"})
     */
    private $templateCreatedtedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="Service", mappedBy="updatedBy", cascade={"persist"})
     */
    private $serviceUpdatedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="Service", mappedBy="createdBy", cascade={"persist"})
     */
    private $serviceCreatedtedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="CompanyService", mappedBy="updatedBy", cascade={"persist"})
     */
    private $companyServiceUpdatedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="CompanyService", mappedBy="createdBy", cascade={"persist"})
     */
    private $companyServiceCreatedtedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="Office", mappedBy="updatedBy", cascade={"persist"})
     */
    private $officeUpdatedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="Office", mappedBy="createdBy", cascade={"persist"})
     */
    private $officeCreatedtedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="Company", mappedBy="updatedBy", cascade={"persist"})
     */
    private $companyUpdatedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="Company", mappedBy="createdBy", cascade={"persist"})
     */
    private $companyCreatedtedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="UserCompany", mappedBy="updatedBy", cascade={"persist"})
     */
    private $userCompanyUpdatedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="UserCompany", mappedBy="createdBy", cascade={"persist"})
     */
    private $userCompanyCreatedtedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="CompanyType", mappedBy="updatedBy", cascade={"persist"})
     */
    private $companyTypeUpdatedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="CompanyType", mappedBy="createdBy", cascade={"persist"})
     */
    private $companyTypeCreatedtedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="ServiceRole", mappedBy="updatedBy", cascade={"persist"})
     */
    private $serviceRoleUpdatedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="ServiceRole", mappedBy="createdBy", cascade={"persist"})
     */
    private $serviceRoleCreatedtedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="UserCompanyServiceRoles", mappedBy="updatedBy", cascade={"persist"})
     */
    private $userCompanyServiceRolesUpdatedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="UserCompanyServiceRoles", mappedBy="createdBy", cascade={"persist"})
     */
    private $userCompanyServiceRolesCreatedtedBy;

    /**
     * @var string
     */
    protected $email;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="twoFactorAuthentication", type="boolean",nullable=true)
     */
    private $twoFactorAuthentication;

    /**
     * @var bool
     *
     * @ORM\Column(name="gdpr_user_optin", type="boolean",nullable=true)
     */
    private $gdprUserOptin;

    /**
     * @var string
     *
     * @ORM\Column(name="authCode", type="string", length=255,nullable=true)
     */
    private $authCode;

    /**
     * @var bool
     *
     * @ORM\Column(name="byPassRecaptcha", type="boolean",nullable=true)
     */
    private $byPassRecaptcha;

    /**
     * @ORM\Column(type="json_array",nullable=true)
     */
    private $trusted;

    public function addTrustedComputer($token, \DateTime $validUntil)
    {
        $this->trusted[$token] = $validUntil->format("r");
    }

    public function isTrustedComputer($token)
    {
        if (isset($this->trusted[$token])) {
            $now = new \DateTime();
            $validUntil = new \DateTime($this->trusted[$token]);

            return $now < $validUntil;
        }

        return false;
    }

    public function isEmailAuthEnabled()
    {
        return $this->getTwoFactorAuthentication();
    }

    public function getEmailAuthReceiver()
    {
        return $this->email;
    }

    public function getEmailAuthCode()
    {
        return $this->authCode;
    }

    public function setEmailAuthCode($authCode)
    {
        $this->authCode = $authCode;
    }

    /**
     * {@inheritdoc}
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * {@inheritdoc}
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Set firstName.
     *
     * @param string|null $firstName
     *
     * @return User
     */
    public function setFirstName($firstName = null)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName.
     *
     * @return string|null
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName.
     *
     * @param string|null $lastName
     *
     * @return User
     */
    public function setLastName($lastName = null)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName.
     *
     * @return string|null
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set mobile.
     *
     * @param string|null $mobile
     *
     * @return User
     */
    public function setMobile($mobile = null)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile.
     *
     * @return string|null
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set phone.
     *
     * @param string|null $phone
     *
     * @return User
     */
    public function setPhone($phone = null)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set imIdentifier.
     *
     * @param string|null $imIdentifier
     *
     * @return User
     */
    public function setImIdentifier($imIdentifier = null)
    {
        $this->imIdentifier = $imIdentifier;

        return $this;
    }

    /**
     * Get imIdentifier.
     *
     * @return string|null
     */
    public function getImIdentifier()
    {
        return $this->imIdentifier;
    }

    /**
     * Set locale.
     *
     * @param string|null $locale
     *
     * @return User
     */
    public function setLocale($locale = null)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale.
     *
     * @return string|null
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set createdBy.
     *
     * @param \AppBundle\Entity\User|null $createdBy
     *
     * @return User
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy.
     *
     * @param \AppBundle\Entity\User|null $updatedBy
     *
     * @return User
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set thumbnail.
     *
     * @param \AppBundle\Entity\Thumbnail|null $thumbnail
     *
     * @return User
     */
    public function setThumbnail(\AppBundle\Entity\Thumbnail $thumbnail = null)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail.
     *
     * @return \AppBundle\Entity\Thumbnail|null
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set currentCompany.
     *
     * @param \AppBundle\Entity\Company|null $currentCompany
     *
     * @return User
     */
    public function setCurrentCompany(\AppBundle\Entity\Company $currentCompany = null)
    {
        $this->currentCompany = $currentCompany;

        return $this;
    }

    /**
     * Get currentCompany.
     *
     * @return \AppBundle\Entity\Company|null
     */
    public function getCurrentCompany()
    {
        return $this->currentCompany;
    }

    /**
     * Add userCompany.
     *
     * @param \AppBundle\Entity\UserCompany $userCompany
     *
     * @return User
     */
    public function addUserCompany(\AppBundle\Entity\UserCompany $userCompany)
    {
        $this->userCompany[] = $userCompany;

        return $this;
    }

    /**
     * Remove userCompany.
     *
     * @param \AppBundle\Entity\UserCompany $userCompany
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUserCompany(\AppBundle\Entity\UserCompany $userCompany)
    {
        return $this->userCompany->removeElement($userCompany);
    }

    /**
     * Get userCompany.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserCompany()
    {
        return $this->userCompany;
    }

    /**
     * Set userCompanyServiceUpdatedBy.
     *
     * @param \AppBundle\Entity\UserCompanyService|null $userCompanyServiceUpdatedBy
     *
     * @return User
     */
    public function setUserCompanyServiceUpdatedBy(
        \AppBundle\Entity\UserCompanyService $userCompanyServiceUpdatedBy = null
    ) {
        $this->userCompanyServiceUpdatedBy = $userCompanyServiceUpdatedBy;

        return $this;
    }

    /**
     * Get userCompanyServiceUpdatedBy.
     *
     * @return \AppBundle\Entity\UserCompanyService|null
     */
    public function getUserCompanyServiceUpdatedBy()
    {
        return $this->userCompanyServiceUpdatedBy;
    }

    /**
     * Set userCompanyServiceCreatedtedBy.
     *
     * @param \AppBundle\Entity\UserCompanyService|null $userCompanyServiceCreatedtedBy
     *
     * @return User
     */
    public function setUserCompanyServiceCreatedtedBy(
        \AppBundle\Entity\UserCompanyService $userCompanyServiceCreatedtedBy = null
    ) {
        $this->userCompanyServiceCreatedtedBy = $userCompanyServiceCreatedtedBy;

        return $this;
    }

    /**
     * Get userCompanyServiceCreatedtedBy.
     *
     * @return \AppBundle\Entity\UserCompanyService|null
     */
    public function getUserCompanyServiceCreatedtedBy()
    {
        return $this->userCompanyServiceCreatedtedBy;
    }

    /**
     * Set thumbnailUpdatedBy.
     *
     * @param \AppBundle\Entity\Thumbnail|null $thumbnailUpdatedBy
     *
     * @return User
     */
    public function setThumbnailUpdatedBy(\AppBundle\Entity\Thumbnail $thumbnailUpdatedBy = null)
    {
        $this->thumbnailUpdatedBy = $thumbnailUpdatedBy;

        return $this;
    }

    /**
     * Get thumbnailUpdatedBy.
     *
     * @return \AppBundle\Entity\Thumbnail|null
     */
    public function getThumbnailUpdatedBy()
    {
        return $this->thumbnailUpdatedBy;
    }

    /**
     * Set thumbnailCreatedtedBy.
     *
     * @param \AppBundle\Entity\Thumbnail|null $thumbnailCreatedtedBy
     *
     * @return User
     */
    public function setThumbnailCreatedtedBy(\AppBundle\Entity\Thumbnail $thumbnailCreatedtedBy = null)
    {
        $this->thumbnailCreatedtedBy = $thumbnailCreatedtedBy;

        return $this;
    }

    /**
     * Get thumbnailCreatedtedBy.
     *
     * @return \AppBundle\Entity\Thumbnail|null
     */
    public function getThumbnailCreatedtedBy()
    {
        return $this->thumbnailCreatedtedBy;
    }

    /**
     * Set templateServiceUpdatedBy.
     *
     * @param \AppBundle\Entity\TemplateService|null $templateServiceUpdatedBy
     *
     * @return User
     */
    public function setTemplateServiceUpdatedBy(\AppBundle\Entity\TemplateService $templateServiceUpdatedBy = null)
    {
        $this->templateServiceUpdatedBy = $templateServiceUpdatedBy;

        return $this;
    }

    /**
     * Get templateServiceUpdatedBy.
     *
     * @return \AppBundle\Entity\TemplateService|null
     */
    public function getTemplateServiceUpdatedBy()
    {
        return $this->templateServiceUpdatedBy;
    }

    /**
     * Set templateServiceCreatedtedBy.
     *
     * @param \AppBundle\Entity\TemplateService|null $templateServiceCreatedtedBy
     *
     * @return User
     */
    public function setTemplateServiceCreatedtedBy(\AppBundle\Entity\TemplateService $templateServiceCreatedtedBy = null
    ) {
        $this->templateServiceCreatedtedBy = $templateServiceCreatedtedBy;

        return $this;
    }

    /**
     * Get templateServiceCreatedtedBy.
     *
     * @return \AppBundle\Entity\TemplateService|null
     */
    public function getTemplateServiceCreatedtedBy()
    {
        return $this->templateServiceCreatedtedBy;
    }

    /**
     * Set templateUpdatedBy.
     *
     * @param \AppBundle\Entity\Template|null $templateUpdatedBy
     *
     * @return User
     */
    public function setTemplateUpdatedBy(\AppBundle\Entity\Template $templateUpdatedBy = null)
    {
        $this->templateUpdatedBy = $templateUpdatedBy;

        return $this;
    }

    /**
     * Get templateUpdatedBy.
     *
     * @return \AppBundle\Entity\Template|null
     */
    public function getTemplateUpdatedBy()
    {
        return $this->templateUpdatedBy;
    }

    /**
     * Set templateCreatedtedBy.
     *
     * @param \AppBundle\Entity\Template|null $templateCreatedtedBy
     *
     * @return User
     */
    public function setTemplateCreatedtedBy(\AppBundle\Entity\Template $templateCreatedtedBy = null)
    {
        $this->templateCreatedtedBy = $templateCreatedtedBy;

        return $this;
    }

    /**
     * Get templateCreatedtedBy.
     *
     * @return \AppBundle\Entity\Template|null
     */
    public function getTemplateCreatedtedBy()
    {
        return $this->templateCreatedtedBy;
    }

    /**
     * Set serviceUpdatedBy.
     *
     * @param \AppBundle\Entity\Service|null $serviceUpdatedBy
     *
     * @return User
     */
    public function setServiceUpdatedBy(\AppBundle\Entity\Service $serviceUpdatedBy = null)
    {
        $this->serviceUpdatedBy = $serviceUpdatedBy;

        return $this;
    }

    /**
     * Get serviceUpdatedBy.
     *
     * @return \AppBundle\Entity\Service|null
     */
    public function getServiceUpdatedBy()
    {
        return $this->serviceUpdatedBy;
    }

    /**
     * Set serviceCreatedtedBy.
     *
     * @param \AppBundle\Entity\Service|null $serviceCreatedtedBy
     *
     * @return User
     */
    public function setServiceCreatedtedBy(\AppBundle\Entity\Service $serviceCreatedtedBy = null)
    {
        $this->serviceCreatedtedBy = $serviceCreatedtedBy;

        return $this;
    }

    /**
     * Get serviceCreatedtedBy.
     *
     * @return \AppBundle\Entity\Service|null
     */
    public function getServiceCreatedtedBy()
    {
        return $this->serviceCreatedtedBy;
    }

    /**
     * Set companyServiceUpdatedBy.
     *
     * @param \AppBundle\Entity\CompanyService|null $companyServiceUpdatedBy
     *
     * @return User
     */
    public function setCompanyServiceUpdatedBy(\AppBundle\Entity\CompanyService $companyServiceUpdatedBy = null)
    {
        $this->companyServiceUpdatedBy = $companyServiceUpdatedBy;

        return $this;
    }

    /**
     * Get companyServiceUpdatedBy.
     *
     * @return \AppBundle\Entity\CompanyService|null
     */
    public function getCompanyServiceUpdatedBy()
    {
        return $this->companyServiceUpdatedBy;
    }

    /**
     * Set companyServiceCreatedtedBy.
     *
     * @param \AppBundle\Entity\CompanyService|null $companyServiceCreatedtedBy
     *
     * @return User
     */
    public function setCompanyServiceCreatedtedBy(\AppBundle\Entity\CompanyService $companyServiceCreatedtedBy = null)
    {
        $this->companyServiceCreatedtedBy = $companyServiceCreatedtedBy;

        return $this;
    }

    /**
     * Get companyServiceCreatedtedBy.
     *
     * @return \AppBundle\Entity\CompanyService|null
     */
    public function getCompanyServiceCreatedtedBy()
    {
        return $this->companyServiceCreatedtedBy;
    }

    /**
     * Set officeUpdatedBy.
     *
     * @param \AppBundle\Entity\Office|null $officeUpdatedBy
     *
     * @return User
     */
    public function setOfficeUpdatedBy(\AppBundle\Entity\Office $officeUpdatedBy = null)
    {
        $this->officeUpdatedBy = $officeUpdatedBy;

        return $this;
    }

    /**
     * Get officeUpdatedBy.
     *
     * @return \AppBundle\Entity\Office|null
     */
    public function getOfficeUpdatedBy()
    {
        return $this->officeUpdatedBy;
    }

    /**
     * Set officeCreatedtedBy.
     *
     * @param \AppBundle\Entity\Office|null $officeCreatedtedBy
     *
     * @return User
     */
    public function setOfficeCreatedtedBy(\AppBundle\Entity\Office $officeCreatedtedBy = null)
    {
        $this->officeCreatedtedBy = $officeCreatedtedBy;

        return $this;
    }

    /**
     * Get officeCreatedtedBy.
     *
     * @return \AppBundle\Entity\Office|null
     */
    public function getOfficeCreatedtedBy()
    {
        return $this->officeCreatedtedBy;
    }

    /**
     * Set companyUpdatedBy.
     *
     * @param \AppBundle\Entity\Company|null $companyUpdatedBy
     *
     * @return User
     */
    public function setCompanyUpdatedBy(\AppBundle\Entity\Company $companyUpdatedBy = null)
    {
        $this->companyUpdatedBy = $companyUpdatedBy;

        return $this;
    }

    /**
     * Get companyUpdatedBy.
     *
     * @return \AppBundle\Entity\Company|null
     */
    public function getCompanyUpdatedBy()
    {
        return $this->companyUpdatedBy;
    }

    /**
     * Set companyCreatedtedBy.
     *
     * @param \AppBundle\Entity\Company|null $companyCreatedtedBy
     *
     * @return User
     */
    public function setCompanyCreatedtedBy(\AppBundle\Entity\Company $companyCreatedtedBy = null)
    {
        $this->companyCreatedtedBy = $companyCreatedtedBy;

        return $this;
    }

    /**
     * Get companyCreatedtedBy.
     *
     * @return \AppBundle\Entity\Company|null
     */
    public function getCompanyCreatedtedBy()
    {
        return $this->companyCreatedtedBy;
    }

    /**
     * Set userCompanyUpdatedBy.
     *
     * @param \AppBundle\Entity\UserCompany|null $userCompanyUpdatedBy
     *
     * @return User
     */
    public function setUserCompanyUpdatedBy(\AppBundle\Entity\UserCompany $userCompanyUpdatedBy = null)
    {
        $this->userCompanyUpdatedBy = $userCompanyUpdatedBy;

        return $this;
    }

    /**
     * Get userCompanyUpdatedBy.
     *
     * @return \AppBundle\Entity\UserCompany|null
     */
    public function getUserCompanyUpdatedBy()
    {
        return $this->userCompanyUpdatedBy;
    }

    /**
     * Set userCompanyCreatedtedBy.
     *
     * @param \AppBundle\Entity\UserCompany|null $userCompanyCreatedtedBy
     *
     * @return User
     */
    public function setUserCompanyCreatedtedBy(\AppBundle\Entity\UserCompany $userCompanyCreatedtedBy = null)
    {
        $this->userCompanyCreatedtedBy = $userCompanyCreatedtedBy;

        return $this;
    }

    /**
     * Get userCompanyCreatedtedBy.
     *
     * @return \AppBundle\Entity\UserCompany|null
     */
    public function getUserCompanyCreatedtedBy()
    {
        return $this->userCompanyCreatedtedBy;
    }

    /**
     * Set companyTypeUpdatedBy.
     *
     * @param \AppBundle\Entity\CompanyType|null $companyTypeUpdatedBy
     *
     * @return User
     */
    public function setCompanyTypeUpdatedBy(\AppBundle\Entity\CompanyType $companyTypeUpdatedBy = null)
    {
        $this->companyTypeUpdatedBy = $companyTypeUpdatedBy;

        return $this;
    }

    /**
     * Get companyTypeUpdatedBy.
     *
     * @return \AppBundle\Entity\CompanyType|null
     */
    public function getCompanyTypeUpdatedBy()
    {
        return $this->companyTypeUpdatedBy;
    }

    /**
     * Set companyTypeCreatedtedBy.
     *
     * @param \AppBundle\Entity\CompanyType|null $companyTypeCreatedtedBy
     *
     * @return User
     */
    public function setCompanyTypeCreatedtedBy(\AppBundle\Entity\CompanyType $companyTypeCreatedtedBy = null)
    {
        $this->companyTypeCreatedtedBy = $companyTypeCreatedtedBy;

        return $this;
    }

    /**
     * Get companyTypeCreatedtedBy.
     *
     * @return \AppBundle\Entity\CompanyType|null
     */
    public function getCompanyTypeCreatedtedBy()
    {
        return $this->companyTypeCreatedtedBy;
    }



    /**
     * Set serviceRoleUpdatedBy.
     *
     * @param \AppBundle\Entity\ServiceRole|null $serviceRoleUpdatedBy
     *
     * @return User
     */
    public function setServiceRoleUpdatedBy(\AppBundle\Entity\ServiceRole $serviceRoleUpdatedBy = null)
    {
        $this->serviceRoleUpdatedBy = $serviceRoleUpdatedBy;

        return $this;
    }

    /**
     * Get serviceRoleUpdatedBy.
     *
     * @return \AppBundle\Entity\ServiceRole|null
     */
    public function getServiceRoleUpdatedBy()
    {
        return $this->serviceRoleUpdatedBy;
    }

    /**
     * Set serviceRoleCreatedtedBy.
     *
     * @param \AppBundle\Entity\ServiceRole|null $serviceRoleCreatedtedBy
     *
     * @return User
     */
    public function setServiceRoleCreatedtedBy(\AppBundle\Entity\ServiceRole $serviceRoleCreatedtedBy = null)
    {
        $this->serviceRoleCreatedtedBy = $serviceRoleCreatedtedBy;

        return $this;
    }

    /**
     * Get serviceRoleCreatedtedBy.
     *
     * @return \AppBundle\Entity\ServiceRole|null
     */
    public function getServiceRoleCreatedtedBy()
    {
        return $this->serviceRoleCreatedtedBy;
    }

    /**
     * Set userCompanyServiceRolesUpdatedBy.
     *
     * @param \AppBundle\Entity\UserCompanyServiceRoles|null $userCompanyServiceRolesUpdatedBy
     *
     * @return User
     */
    public function setUserCompanyServiceRolesUpdatedBy(
        \AppBundle\Entity\UserCompanyServiceRoles $userCompanyServiceRolesUpdatedBy = null
    ) {
        $this->userCompanyServiceRolesUpdatedBy = $userCompanyServiceRolesUpdatedBy;

        return $this;
    }

    /**
     * Get userCompanyServiceRolesUpdatedBy.
     *
     * @return \AppBundle\Entity\UserCompanyServiceRoles|null
     */
    public function getUserCompanyServiceRolesUpdatedBy()
    {
        return $this->userCompanyServiceRolesUpdatedBy;
    }

    /**
     * Set userCompanyServiceRolesCreatedtedBy.
     *
     * @param \AppBundle\Entity\UserCompanyServiceRoles|null $userCompanyServiceRolesCreatedtedBy
     *
     * @return User
     */
    public function setUserCompanyServiceRolesCreatedtedBy(
        \AppBundle\Entity\UserCompanyServiceRoles $userCompanyServiceRolesCreatedtedBy = null
    ) {
        $this->userCompanyServiceRolesCreatedtedBy = $userCompanyServiceRolesCreatedtedBy;

        return $this;
    }

    /**
     * Get userCompanyServiceRolesCreatedtedBy.
     *
     * @return \AppBundle\Entity\UserCompanyServiceRoles|null
     */
    public function getUserCompanyServiceRolesCreatedtedBy()
    {
        return $this->userCompanyServiceRolesCreatedtedBy;
    }

    /**
     * Add userCompanyServiceUpdatedBy.
     *
     * @param \AppBundle\Entity\UserCompanyService $userCompanyServiceUpdatedBy
     *
     * @return User
     */
    public function addUserCompanyServiceUpdatedBy(\AppBundle\Entity\UserCompanyService $userCompanyServiceUpdatedBy)
    {
        $this->userCompanyServiceUpdatedBy[] = $userCompanyServiceUpdatedBy;

        return $this;
    }

    /**
     * Remove userCompanyServiceUpdatedBy.
     *
     * @param \AppBundle\Entity\UserCompanyService $userCompanyServiceUpdatedBy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUserCompanyServiceUpdatedBy(\AppBundle\Entity\UserCompanyService $userCompanyServiceUpdatedBy)
    {
        return $this->userCompanyServiceUpdatedBy->removeElement($userCompanyServiceUpdatedBy);
    }

    /**
     * Add userCompanyServiceCreatedtedBy.
     *
     * @param \AppBundle\Entity\UserCompanyService $userCompanyServiceCreatedtedBy
     *
     * @return User
     */
    public function addUserCompanyServiceCreatedtedBy(
        \AppBundle\Entity\UserCompanyService $userCompanyServiceCreatedtedBy
    ) {
        $this->userCompanyServiceCreatedtedBy[] = $userCompanyServiceCreatedtedBy;

        return $this;
    }

    /**
     * Remove userCompanyServiceCreatedtedBy.
     *
     * @param \AppBundle\Entity\UserCompanyService $userCompanyServiceCreatedtedBy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUserCompanyServiceCreatedtedBy(
        \AppBundle\Entity\UserCompanyService $userCompanyServiceCreatedtedBy
    ) {
        return $this->userCompanyServiceCreatedtedBy->removeElement($userCompanyServiceCreatedtedBy);
    }

    /**
     * Add thumbnailUpdatedBy.
     *
     * @param \AppBundle\Entity\Thumbnail $thumbnailUpdatedBy
     *
     * @return User
     */
    public function addThumbnailUpdatedBy(\AppBundle\Entity\Thumbnail $thumbnailUpdatedBy)
    {
        $this->thumbnailUpdatedBy[] = $thumbnailUpdatedBy;

        return $this;
    }

    /**
     * Remove thumbnailUpdatedBy.
     *
     * @param \AppBundle\Entity\Thumbnail $thumbnailUpdatedBy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeThumbnailUpdatedBy(\AppBundle\Entity\Thumbnail $thumbnailUpdatedBy)
    {
        return $this->thumbnailUpdatedBy->removeElement($thumbnailUpdatedBy);
    }

    /**
     * Add thumbnailCreatedtedBy.
     *
     * @param \AppBundle\Entity\Thumbnail $thumbnailCreatedtedBy
     *
     * @return User
     */
    public function addThumbnailCreatedtedBy(\AppBundle\Entity\Thumbnail $thumbnailCreatedtedBy)
    {
        $this->thumbnailCreatedtedBy[] = $thumbnailCreatedtedBy;

        return $this;
    }

    /**
     * Remove thumbnailCreatedtedBy.
     *
     * @param \AppBundle\Entity\Thumbnail $thumbnailCreatedtedBy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeThumbnailCreatedtedBy(\AppBundle\Entity\Thumbnail $thumbnailCreatedtedBy)
    {
        return $this->thumbnailCreatedtedBy->removeElement($thumbnailCreatedtedBy);
    }

    /**
     * Add templateServiceUpdatedBy.
     *
     * @param \AppBundle\Entity\TemplateService $templateServiceUpdatedBy
     *
     * @return User
     */
    public function addTemplateServiceUpdatedBy(\AppBundle\Entity\TemplateService $templateServiceUpdatedBy)
    {
        $this->templateServiceUpdatedBy[] = $templateServiceUpdatedBy;

        return $this;
    }

    /**
     * Remove templateServiceUpdatedBy.
     *
     * @param \AppBundle\Entity\TemplateService $templateServiceUpdatedBy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTemplateServiceUpdatedBy(\AppBundle\Entity\TemplateService $templateServiceUpdatedBy)
    {
        return $this->templateServiceUpdatedBy->removeElement($templateServiceUpdatedBy);
    }

    /**
     * Add templateServiceCreatedtedBy.
     *
     * @param \AppBundle\Entity\TemplateService $templateServiceCreatedtedBy
     *
     * @return User
     */
    public function addTemplateServiceCreatedtedBy(\AppBundle\Entity\TemplateService $templateServiceCreatedtedBy)
    {
        $this->templateServiceCreatedtedBy[] = $templateServiceCreatedtedBy;

        return $this;
    }

    /**
     * Remove templateServiceCreatedtedBy.
     *
     * @param \AppBundle\Entity\TemplateService $templateServiceCreatedtedBy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTemplateServiceCreatedtedBy(\AppBundle\Entity\TemplateService $templateServiceCreatedtedBy)
    {
        return $this->templateServiceCreatedtedBy->removeElement($templateServiceCreatedtedBy);
    }

    /**
     * Add templateUpdatedBy.
     *
     * @param \AppBundle\Entity\Template $templateUpdatedBy
     *
     * @return User
     */
    public function addTemplateUpdatedBy(\AppBundle\Entity\Template $templateUpdatedBy)
    {
        $this->templateUpdatedBy[] = $templateUpdatedBy;

        return $this;
    }

    /**
     * Remove templateUpdatedBy.
     *
     * @param \AppBundle\Entity\Template $templateUpdatedBy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTemplateUpdatedBy(\AppBundle\Entity\Template $templateUpdatedBy)
    {
        return $this->templateUpdatedBy->removeElement($templateUpdatedBy);
    }

    /**
     * Add templateCreatedtedBy.
     *
     * @param \AppBundle\Entity\Template $templateCreatedtedBy
     *
     * @return User
     */
    public function addTemplateCreatedtedBy(\AppBundle\Entity\Template $templateCreatedtedBy)
    {
        $this->templateCreatedtedBy[] = $templateCreatedtedBy;

        return $this;
    }

    /**
     * Remove templateCreatedtedBy.
     *
     * @param \AppBundle\Entity\Template $templateCreatedtedBy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTemplateCreatedtedBy(\AppBundle\Entity\Template $templateCreatedtedBy)
    {
        return $this->templateCreatedtedBy->removeElement($templateCreatedtedBy);
    }

    /**
     * Add serviceUpdatedBy.
     *
     * @param \AppBundle\Entity\Service $serviceUpdatedBy
     *
     * @return User
     */
    public function addServiceUpdatedBy(\AppBundle\Entity\Service $serviceUpdatedBy)
    {
        $this->serviceUpdatedBy[] = $serviceUpdatedBy;

        return $this;
    }

    /**
     * Remove serviceUpdatedBy.
     *
     * @param \AppBundle\Entity\Service $serviceUpdatedBy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeServiceUpdatedBy(\AppBundle\Entity\Service $serviceUpdatedBy)
    {
        return $this->serviceUpdatedBy->removeElement($serviceUpdatedBy);
    }

    /**
     * Add serviceCreatedtedBy.
     *
     * @param \AppBundle\Entity\Service $serviceCreatedtedBy
     *
     * @return User
     */
    public function addServiceCreatedtedBy(\AppBundle\Entity\Service $serviceCreatedtedBy)
    {
        $this->serviceCreatedtedBy[] = $serviceCreatedtedBy;

        return $this;
    }

    /**
     * Remove serviceCreatedtedBy.
     *
     * @param \AppBundle\Entity\Service $serviceCreatedtedBy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeServiceCreatedtedBy(\AppBundle\Entity\Service $serviceCreatedtedBy)
    {
        return $this->serviceCreatedtedBy->removeElement($serviceCreatedtedBy);
    }

    /**
     * Add companyServiceUpdatedBy.
     *
     * @param \AppBundle\Entity\CompanyService $companyServiceUpdatedBy
     *
     * @return User
     */
    public function addCompanyServiceUpdatedBy(\AppBundle\Entity\CompanyService $companyServiceUpdatedBy)
    {
        $this->companyServiceUpdatedBy[] = $companyServiceUpdatedBy;

        return $this;
    }

    /**
     * Remove companyServiceUpdatedBy.
     *
     * @param \AppBundle\Entity\CompanyService $companyServiceUpdatedBy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCompanyServiceUpdatedBy(\AppBundle\Entity\CompanyService $companyServiceUpdatedBy)
    {
        return $this->companyServiceUpdatedBy->removeElement($companyServiceUpdatedBy);
    }

    /**
     * Add companyServiceCreatedtedBy.
     *
     * @param \AppBundle\Entity\CompanyService $companyServiceCreatedtedBy
     *
     * @return User
     */
    public function addCompanyServiceCreatedtedBy(\AppBundle\Entity\CompanyService $companyServiceCreatedtedBy)
    {
        $this->companyServiceCreatedtedBy[] = $companyServiceCreatedtedBy;

        return $this;
    }

    /**
     * Remove companyServiceCreatedtedBy.
     *
     * @param \AppBundle\Entity\CompanyService $companyServiceCreatedtedBy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCompanyServiceCreatedtedBy(\AppBundle\Entity\CompanyService $companyServiceCreatedtedBy)
    {
        return $this->companyServiceCreatedtedBy->removeElement($companyServiceCreatedtedBy);
    }

    /**
     * Add officeUpdatedBy.
     *
     * @param \AppBundle\Entity\Office $officeUpdatedBy
     *
     * @return User
     */
    public function addOfficeUpdatedBy(\AppBundle\Entity\Office $officeUpdatedBy)
    {
        $this->officeUpdatedBy[] = $officeUpdatedBy;

        return $this;
    }

    /**
     * Remove officeUpdatedBy.
     *
     * @param \AppBundle\Entity\Office $officeUpdatedBy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeOfficeUpdatedBy(\AppBundle\Entity\Office $officeUpdatedBy)
    {
        return $this->officeUpdatedBy->removeElement($officeUpdatedBy);
    }

    /**
     * Add officeCreatedtedBy.
     *
     * @param \AppBundle\Entity\Office $officeCreatedtedBy
     *
     * @return User
     */
    public function addOfficeCreatedtedBy(\AppBundle\Entity\Office $officeCreatedtedBy)
    {
        $this->officeCreatedtedBy[] = $officeCreatedtedBy;

        return $this;
    }

    /**
     * Remove officeCreatedtedBy.
     *
     * @param \AppBundle\Entity\Office $officeCreatedtedBy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeOfficeCreatedtedBy(\AppBundle\Entity\Office $officeCreatedtedBy)
    {
        return $this->officeCreatedtedBy->removeElement($officeCreatedtedBy);
    }

    /**
     * Add companyUpdatedBy.
     *
     * @param \AppBundle\Entity\Company $companyUpdatedBy
     *
     * @return User
     */
    public function addCompanyUpdatedBy(\AppBundle\Entity\Company $companyUpdatedBy)
    {
        $this->companyUpdatedBy[] = $companyUpdatedBy;

        return $this;
    }

    /**
     * Remove companyUpdatedBy.
     *
     * @param \AppBundle\Entity\Company $companyUpdatedBy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCompanyUpdatedBy(\AppBundle\Entity\Company $companyUpdatedBy)
    {
        return $this->companyUpdatedBy->removeElement($companyUpdatedBy);
    }

    /**
     * Add companyCreatedtedBy.
     *
     * @param \AppBundle\Entity\Company $companyCreatedtedBy
     *
     * @return User
     */
    public function addCompanyCreatedtedBy(\AppBundle\Entity\Company $companyCreatedtedBy)
    {
        $this->companyCreatedtedBy[] = $companyCreatedtedBy;

        return $this;
    }

    /**
     * Remove companyCreatedtedBy.
     *
     * @param \AppBundle\Entity\Company $companyCreatedtedBy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCompanyCreatedtedBy(\AppBundle\Entity\Company $companyCreatedtedBy)
    {
        return $this->companyCreatedtedBy->removeElement($companyCreatedtedBy);
    }

    /**
     * Add userCompanyUpdatedBy.
     *
     * @param \AppBundle\Entity\UserCompany $userCompanyUpdatedBy
     *
     * @return User
     */
    public function addUserCompanyUpdatedBy(\AppBundle\Entity\UserCompany $userCompanyUpdatedBy)
    {
        $this->userCompanyUpdatedBy[] = $userCompanyUpdatedBy;

        return $this;
    }

    /**
     * Remove userCompanyUpdatedBy.
     *
     * @param \AppBundle\Entity\UserCompany $userCompanyUpdatedBy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUserCompanyUpdatedBy(\AppBundle\Entity\UserCompany $userCompanyUpdatedBy)
    {
        return $this->userCompanyUpdatedBy->removeElement($userCompanyUpdatedBy);
    }

    /**
     * Add userCompanyCreatedtedBy.
     *
     * @param \AppBundle\Entity\UserCompany $userCompanyCreatedtedBy
     *
     * @return User
     */
    public function addUserCompanyCreatedtedBy(\AppBundle\Entity\UserCompany $userCompanyCreatedtedBy)
    {
        $this->userCompanyCreatedtedBy[] = $userCompanyCreatedtedBy;

        return $this;
    }

    /**
     * Remove userCompanyCreatedtedBy.
     *
     * @param \AppBundle\Entity\UserCompany $userCompanyCreatedtedBy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUserCompanyCreatedtedBy(\AppBundle\Entity\UserCompany $userCompanyCreatedtedBy)
    {
        return $this->userCompanyCreatedtedBy->removeElement($userCompanyCreatedtedBy);
    }

    /**
     * Add companyTypeUpdatedBy.
     *
     * @param \AppBundle\Entity\CompanyType $companyTypeUpdatedBy
     *
     * @return User
     */
    public function addCompanyTypeUpdatedBy(\AppBundle\Entity\CompanyType $companyTypeUpdatedBy)
    {
        $this->companyTypeUpdatedBy[] = $companyTypeUpdatedBy;

        return $this;
    }

    /**
     * Remove companyTypeUpdatedBy.
     *
     * @param \AppBundle\Entity\CompanyType $companyTypeUpdatedBy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCompanyTypeUpdatedBy(\AppBundle\Entity\CompanyType $companyTypeUpdatedBy)
    {
        return $this->companyTypeUpdatedBy->removeElement($companyTypeUpdatedBy);
    }

    /**
     * Add companyTypeCreatedtedBy.
     *
     * @param \AppBundle\Entity\CompanyType $companyTypeCreatedtedBy
     *
     * @return User
     */
    public function addCompanyTypeCreatedtedBy(\AppBundle\Entity\CompanyType $companyTypeCreatedtedBy)
    {
        $this->companyTypeCreatedtedBy[] = $companyTypeCreatedtedBy;

        return $this;
    }

    /**
     * Remove companyTypeCreatedtedBy.
     *
     * @param \AppBundle\Entity\CompanyType $companyTypeCreatedtedBy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCompanyTypeCreatedtedBy(\AppBundle\Entity\CompanyType $companyTypeCreatedtedBy)
    {
        return $this->companyTypeCreatedtedBy->removeElement($companyTypeCreatedtedBy);
    }

    /**
     * Add serviceRoleUpdatedBy.
     *
     * @param \AppBundle\Entity\ServiceRole $serviceRoleUpdatedBy
     *
     * @return User
     */
    public function addServiceRoleUpdatedBy(\AppBundle\Entity\ServiceRole $serviceRoleUpdatedBy)
    {
        $this->serviceRoleUpdatedBy[] = $serviceRoleUpdatedBy;

        return $this;
    }

    /**
     * Remove serviceRoleUpdatedBy.
     *
     * @param \AppBundle\Entity\ServiceRole $serviceRoleUpdatedBy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeServiceRoleUpdatedBy(\AppBundle\Entity\ServiceRole $serviceRoleUpdatedBy)
    {
        return $this->serviceRoleUpdatedBy->removeElement($serviceRoleUpdatedBy);
    }

    /**
     * Add serviceRoleCreatedtedBy.
     *
     * @param \AppBundle\Entity\ServiceRole $serviceRoleCreatedtedBy
     *
     * @return User
     */
    public function addServiceRoleCreatedtedBy(\AppBundle\Entity\ServiceRole $serviceRoleCreatedtedBy)
    {
        $this->serviceRoleCreatedtedBy[] = $serviceRoleCreatedtedBy;

        return $this;
    }

    /**
     * Remove serviceRoleCreatedtedBy.
     *
     * @param \AppBundle\Entity\ServiceRole $serviceRoleCreatedtedBy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeServiceRoleCreatedtedBy(\AppBundle\Entity\ServiceRole $serviceRoleCreatedtedBy)
    {
        return $this->serviceRoleCreatedtedBy->removeElement($serviceRoleCreatedtedBy);
    }

    /**
     * Add userCompanyServiceRolesUpdatedBy.
     *
     * @param \AppBundle\Entity\UserCompanyServiceRoles $userCompanyServiceRolesUpdatedBy
     *
     * @return User
     */
    public function addUserCompanyServiceRolesUpdatedBy(
        \AppBundle\Entity\UserCompanyServiceRoles $userCompanyServiceRolesUpdatedBy
    ) {
        $this->userCompanyServiceRolesUpdatedBy[] = $userCompanyServiceRolesUpdatedBy;

        return $this;
    }

    /**
     * Remove userCompanyServiceRolesUpdatedBy.
     *
     * @param \AppBundle\Entity\UserCompanyServiceRoles $userCompanyServiceRolesUpdatedBy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUserCompanyServiceRolesUpdatedBy(
        \AppBundle\Entity\UserCompanyServiceRoles $userCompanyServiceRolesUpdatedBy
    ) {
        return $this->userCompanyServiceRolesUpdatedBy->removeElement($userCompanyServiceRolesUpdatedBy);
    }

    /**
     * Add userCompanyServiceRolesCreatedtedBy.
     *
     * @param \AppBundle\Entity\UserCompanyServiceRoles $userCompanyServiceRolesCreatedtedBy
     *
     * @return User
     */
    public function addUserCompanyServiceRolesCreatedtedBy(
        \AppBundle\Entity\UserCompanyServiceRoles $userCompanyServiceRolesCreatedtedBy
    ) {
        $this->userCompanyServiceRolesCreatedtedBy[] = $userCompanyServiceRolesCreatedtedBy;

        return $this;
    }

    /**
     * Remove userCompanyServiceRolesCreatedtedBy.
     *
     * @param \AppBundle\Entity\UserCompanyServiceRoles $userCompanyServiceRolesCreatedtedBy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUserCompanyServiceRolesCreatedtedBy(
        \AppBundle\Entity\UserCompanyServiceRoles $userCompanyServiceRolesCreatedtedBy
    ) {
        return $this->userCompanyServiceRolesCreatedtedBy->removeElement($userCompanyServiceRolesCreatedtedBy);
    }


    /**
     * Set deletedAt.
     *
     * @param \DateTime|null $deletedAt
     *
     * @return User
     */
    public function setDeletedAt($deletedAt = null)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt.
     *
     * @return \DateTime|null
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return User
     */
    public function setTitle($title = null)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set twoFactorAuthentication.
     *
     * @param bool|null $twoFactorAuthentication
     *
     * @return User
     */
    public function setTwoFactorAuthentication($twoFactorAuthentication = null)
    {
        $this->twoFactorAuthentication = $twoFactorAuthentication;

        return $this;
    }

    /**
     * Get twoFactorAuthentication.
     *
     * @return bool|null
     */
    public function getTwoFactorAuthentication()
    {
        return $this->twoFactorAuthentication;
    }

    /**
     * Set gdprUserOptin.
     *
     * @param bool|null $gdprUserOptin
     *
     * @return User
     */
    public function setGdprUserOptin($gdprUserOptin = null)
    {
        $this->gdprUserOptin = $gdprUserOptin;
    }

    /**
     * Set mobileCountry.
     *
     * @param int|null $mobileCountry
     *
     * @return User
     */
    public function setMobileCountry($mobileCountry = null)
    {
        $this->mobileCountry = $mobileCountry;

        return $this;
    }

    /**
     * Get gdprUserOptin.
     *
     * @return bool|null
     */
    public function getGdprUserOptin()
    {
        return $this->gdprUserOptin;
    }

    /**
     * Get mobileCountry.
     *
     * @return int|null
     */
    public function getMobileCountry()
    {
        return $this->mobileCountry;
    }

    /**
     * Set authCode.
     *
     * @param string|null $authCode
     *
     * @return User
     */
    public function setAuthCode($authCode = null)
    {
        $this->authCode = $authCode;

        return $this;
    }

    /**
     * Get authCode.
     *
     * @return string|null
     */
    public function getAuthCode()
    {
        return $this->authCode;
    }

    /**
     * Set byPassRecaptcha.
     *
     * @param bool|null $byPassRecaptcha
     *
     * @return User
     */
    public function setByPassRecaptcha($byPassRecaptcha = null)
    {
        $this->byPassRecaptcha = $byPassRecaptcha;

        return $this;
    }

    /**
     * Get byPassRecaptcha.
     *
     * @return bool|null
     */
    public function getByPassRecaptcha()
    {
        return $this->byPassRecaptcha;
    }

    /**
     * Set trusted.
     *
     * @param array $trusted
     *
     * @return User
     */
    public function setTrusted($trusted)
    {
        $this->trusted = $trusted;

        return $this;
    }

    /**
     * Get trusted.
     *
     * @return array
     */
    public function getTrusted()
    {
        return $this->trusted;
    }
}
