<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Translatable;
use AppBundle\Traits\Timestampable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Service
 *
 * @ORM\Table(name="service")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ServiceRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @ORM\HasLifecycleCallbacks()
 */
class Service implements Translatable
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(max=131, maxMessage="NAME field can not exceed {{limit}} characters.")
     */
    private $name;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="short_desc", type="string", length=255,nullable=true)
     * @Assert\NotBlank()
     * @Assert\Length(min=10, minMessage="Short description fied min length must be at least {{limit}} characters.")
     * @Assert\Length(max=30, maxMessage="Short description field an not exceed {{limit}} characters.")
     */
    private $shortDesc;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="long_desc", type="text",nullable=true)
     * @Assert\NotBlank()
     * @Assert\Length(min=20, minMessage="Long description fied min length must be at least {{limit}} characters.")
     * @Assert\Length(max=255, maxMessage="Long description field an not exceed {{limit}} characters.")
     */
    private $longDesc;

    /**
     * One Service has One Thumbnail.
     * @ORM\OneToOne(targetEntity="Thumbnail", inversedBy="service", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="thumbnail_id", referencedColumnName="id")
     */
    private $thumbnail;

    /**
     * @var string
     *
     * @ORM\Column(name="help_link", type="string", length=255,nullable=true)
     * @Assert\Url(message = "The WEBSITE field '{{ value }}' is not a valid url.")
     */
    private $helpLink;

    /**
     * @var bool
     *
     * @ORM\Column(name="enable", type="boolean",nullable=true)
     */
    private $enable;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_default", type="boolean",nullable=true)
     */
    private $isDefault;

    /**
     * @var bool
     *
     * @ORM\Column(name="role_type", type="string", length=255,nullable=true)
     * @Assert\Choice({"not applicable", "single", "multiple"},message="Choose a valid role type")
     */
    private $roleType;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer",nullable=true)
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="connection_type", type="string", length=255,nullable=true)
     * @Assert\Choice({"Oauth"},message="Choose a valid connection type")
     */
    private $connectionType;

    /**
     * @var string
     *
     * @ORM\Column(name="connection_identifier", type="string", length=255,nullable=true)
     */
    private $connectionIdentifier;

    /**
     * @var string
     *
     * @ORM\Column(name="connection_target", type="string", length=255,nullable=true)
     * @Assert\Url(message = "The CONNECTIONTARGET field '{{ value }}' is not a valid url.")
     */
    private $connectionTarget;

    /**
     * @var bool
     *
     * @ORM\Column(name="event_enable", type="boolean",nullable=true)
     */
    private $eventEnable;

    /**
     * @var string
     *
     * @ORM\Column(name="event_manager", type="string", length=255,nullable=true)
     * @Assert\Choice({"ONEUP", "CEGID"},message="Choose a valid event manager")
     */
    private $eventManager;

    /**
     * One Created person has One User.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="serviceCreatedtedBy", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * One Updated person has One User.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="serviceUpdatedBy", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="CompanyService", mappedBy="service", cascade={"persist", "remove"})
     */
    private $companyService;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="TemplateService", mappedBy="service", cascade={"persist", "remove"})
     */
    private $templateService;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="UserCompanyService", mappedBy="service", cascade={"persist", "remove"})
     */
    private $userCompanyService;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ServiceRole", mappedBy="service", cascade={"persist", "remove"})
     */
    private $serviceRole;

    /**
     * @var string
     *
     * @ORM\Column(name="external_id", type="string", length=255,nullable=true)
     */
    private $externalId;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     * and it is not necessary because globally locale can be set in listener
     */
    private $locale;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="user_agent_deny",type="text")
     */
    private $userAgentDeny;

    /**
     * @var string
     *
     * @ORM\Column(name="connection_target_attribute",type="text")
     */
    private $connectionTargetAttribute;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->companyService = new \Doctrine\Common\Collections\ArrayCollection();
        $this->templateService = new \Doctrine\Common\Collections\ArrayCollection();
        $this->userCompanyService = new \Doctrine\Common\Collections\ArrayCollection();
        $this->connectionTargetAttribute = "_self";
        $this->userAgentDeny = [];
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Service
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set shortDesc.
     *
     * @param string $shortDesc
     *
     * @return Service
     */
    public function setShortDesc($shortDesc)
    {
        $this->shortDesc = $shortDesc;

        return $this;
    }

    /**
     * Get shortDesc.
     *
     * @return string
     */
    public function getShortDesc()
    {
        return $this->shortDesc;
    }

    /**
     * Set longDesc.
     *
     * @param string $longDesc
     *
     * @return Service
     */
    public function setLongDesc($longDesc)
    {
        $this->longDesc = $longDesc;

        return $this;
    }

    /**
     * Get longDesc.
     *
     * @return string
     */
    public function getLongDesc()
    {
        return $this->longDesc;
    }

    /**
     * Set helpLink.
     *
     * @param string $helpLink
     *
     * @return Service
     */
    public function setHelpLink($helpLink)
    {
        $this->helpLink = $helpLink;

        return $this;
    }

    /**
     * Get helpLink.
     *
     * @return string
     */
    public function getHelpLink()
    {
        return $this->helpLink;
    }

    /**
     * Set enable.
     *
     * @param bool $enable
     *
     * @return Service
     */
    public function setEnable($enable)
    {
        $this->enable = $enable;

        return $this;
    }

    /**
     * Get enable.
     *
     * @return bool
     */
    public function getEnable()
    {
        return $this->enable;
    }

    /**
     * Set isDefault.
     *
     * @param bool $isDefault
     *
     * @return Service
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    /**
     * Get isDefault.
     *
     * @return bool
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }

    /**
     * Set position.
     *
     * @param int $position
     *
     * @return Service
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set createdBy.
     *
     * @param \AppBundle\Entity\User|null $createdBy
     *
     * @return Service
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy.
     *
     * @param \AppBundle\Entity\User|null $updatedBy
     *
     * @return Service
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set thumbnail.
     *
     * @param \AppBundle\Entity\Thumbnail|null $thumbnail
     *
     * @return Service
     */
    public function setThumbnail(\AppBundle\Entity\Thumbnail $thumbnail = null)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail.
     *
     * @return \AppBundle\Entity\Thumbnail|null
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Add companyService.
     *
     * @param \AppBundle\Entity\CompanyService $companyService
     *
     * @return Service
     */
    public function addCompanyService(\AppBundle\Entity\CompanyService $companyService)
    {
        $this->companyService[] = $companyService;

        return $this;
    }

    /**
     * Remove companyService.
     *
     * @param \AppBundle\Entity\CompanyService $companyService
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCompanyService(\AppBundle\Entity\CompanyService $companyService)
    {
        return $this->companyService->removeElement($companyService);
    }

    /**
     * Get companyService.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompanyService()
    {
        return $this->companyService;
    }

    /**
     * Add templateService.
     *
     * @param \AppBundle\Entity\TemplateService $templateService
     *
     * @return Service
     */
    public function addTemplateService(\AppBundle\Entity\TemplateService $templateService)
    {
        $this->templateService[] = $templateService;

        return $this;
    }

    /**
     * Remove templateService.
     *
     * @param \AppBundle\Entity\TemplateService $templateService
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTemplateService(\AppBundle\Entity\TemplateService $templateService)
    {
        return $this->templateService->removeElement($templateService);
    }

    /**
     * Get templateService.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTemplateService()
    {
        return $this->templateService;
    }

    /**
     * Add userCompanyService.
     *
     * @param \AppBundle\Entity\UserCompanyService $userCompanyService
     *
     * @return Service
     */
    public function addUserCompanyService(\AppBundle\Entity\UserCompanyService $userCompanyService)
    {
        $this->userCompanyService[] = $userCompanyService;

        return $this;
    }

    /**
     * Remove userCompanyService.
     *
     * @param \AppBundle\Entity\UserCompanyService $userCompanyService
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUserCompanyService(\AppBundle\Entity\UserCompanyService $userCompanyService)
    {
        return $this->userCompanyService->removeElement($userCompanyService);
    }

    /**
     * Get userCompanyService.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserCompanyService()
    {
        return $this->userCompanyService;
    }

    /**
     * Set roleType.
     *
     * @param bool $roleType
     *
     * @return Service
     */
    public function setRoleType($roleType)
    {
        $this->roleType = $roleType;

        return $this;
    }

    /**
     * Get roleType.
     *
     * @return bool
     */
    public function getRoleType()
    {
        return $this->roleType;
    }

    /**
     * Set connectionType.
     *
     * @param string $connectionType
     *
     * @return Service
     */
    public function setConnectionType($connectionType)
    {
        $this->connectionType = $connectionType;

        return $this;
    }

    /**
     * Get connectionType.
     *
     * @return string
     */
    public function getConnectionType()
    {
        return $this->connectionType;
    }

    /**
     * Set connectionIdentifier.
     *
     * @param string $connectionIdentifier
     *
     * @return Service
     */
    public function setConnectionIdentifier($connectionIdentifier)
    {
        $this->connectionIdentifier = $connectionIdentifier;

        return $this;
    }

    /**
     * Get connectionIdentifier.
     *
     * @return string
     */
    public function getConnectionIdentifier()
    {
        return $this->connectionIdentifier;
    }

    /**
     * Set connectionTarget.
     *
     * @param string $connectionTarget
     *
     * @return Service
     */
    public function setConnectionTarget($connectionTarget)
    {
        $this->connectionTarget = $connectionTarget;

        return $this;
    }

    /**
     * Get connectionTarget.
     *
     * @return string
     */
    public function getConnectionTarget()
    {
        return $this->connectionTarget;
    }

    /**
     * Add serviceRole.
     *
     * @param \AppBundle\Entity\ServiceRole $serviceRole
     *
     * @return Service
     */
    public function addServiceRole(\AppBundle\Entity\ServiceRole $serviceRole)
    {
        $this->serviceRole[] = $serviceRole;

        return $this;
    }

    /**
     * Remove serviceRole.
     *
     * @param \AppBundle\Entity\ServiceRole $serviceRole
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeServiceRole(\AppBundle\Entity\ServiceRole $serviceRole)
    {
        return $this->serviceRole->removeElement($serviceRole);
    }

    /**
     * Get serviceRole.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServiceRole()
    {
        return $this->serviceRole;
    }

    /**
     * Set eventEnable.
     *
     * @param bool|null $eventEnable
     *
     * @return Service
     */
    public function setEventEnable($eventEnable = null)
    {
        $this->eventEnable = $eventEnable;

        return $this;
    }

    /**
     * Get eventEnable.
     *
     * @return bool|null
     */
    public function getEventEnable()
    {
        return $this->eventEnable;
    }

    /**
     * Set eventManager.
     *
     * @param string|null $eventManager
     *
     * @return Service
     */
    public function setEventManager($eventManager = null)
    {
        $this->eventManager = $eventManager;

        return $this;
    }

    /**
     * Get eventManager.
     *
     * @return string|null
     */
    public function getEventManager()
    {
        return $this->eventManager;
    }

    /**
     * Set externalId.
     *
     * @param string $externalId
     *
     * @return Service
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * Get externalId.
     *
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }


    /**
     * Set deletedAt.
     *
     * @param \DateTime|null $deletedAt
     *
     * @return Service
     */
    public function setDeletedAt($deletedAt = null)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt.
     *
     * @return \DateTime|null
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set userAgentDeny.
     *
     * @param string $userAgentDeny
     *
     * @return Service
     */
    public function setUserAgentDeny($userAgentDeny)
    {
        $this->userAgentDeny = serialize($userAgentDeny);

        return $this;
    }

    /**
     * Get userAgentDeny.
     *
     * @return string
     */
    public function getUserAgentDeny()
    {
        return !empty($this->userAgentDeny) ? unserialize($this->userAgentDeny) : null;
    }

    /**
     * Set connectionTargetAttribute.
     *
     * @param string $connectionTargetAttribute
     *
     * @return Service
     */
    public function setConnectionTargetAttribute($connectionTargetAttribute)
    {
        $this->connectionTargetAttribute = $connectionTargetAttribute;

        return $this;
    }

    /**
     * Get connectionTargetAttribute.
     *
     * @return string
     */
    public function getConnectionTargetAttribute()
    {
        return $this->connectionTargetAttribute;
    }
}
