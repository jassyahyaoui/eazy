<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Traits\Timestampable;

/**
 * UserCompany
 *
 * @ORM\Table(name="user_company")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserCompanyRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class UserCompany
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Many UserCompanies have One User.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userCompany", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",nullable=true,onDelete="SET NULL")
     */
    private $user;

    /**
     * Many UserCompanies have One Company.
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="userCompany", cascade={"persist"})
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", nullable=true,onDelete="SET NULL")
     */
    private $company;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="started_at", type="datetime")
     */
    private $startedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_at", type="datetime")
     */
    private $endAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled;

    /**
     * @var string
     *
     * @ORM\Column(name="role",type="text")
     */
    private $role;

    /**
     * @var bool
     *
     * @ORM\Column(name="notify_sms", type="boolean")
     */
    private $notifySms;

    /**
     * @var bool
     *
     * @ORM\Column(name="notify_email", type="boolean")
     */
    private $notifyEmail;

    /**
     * One Created person has One User.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userCompanyCreatedtedBy", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="created_by", nullable=true, referencedColumnName="id")
     */
    private $createdBy;

    /**
     * One Updated person has One User.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userCompanyUpdatedBy", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="updated_by", nullable=true, referencedColumnName="id")
     */
    private $updatedBy;


    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="UserCompanyService", mappedBy="userCompany", cascade={"persist"})
     */
    private $userCompanyService;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_login_date",nullable=true, type="datetime")
     */
    private $lastLoginDate;

    /**
     * @var string
     *
     * @ORM\Column(name="last_login_ip",nullable=true,type="text")
     */
    private $lastLoginIp;
    /**
     * @var string
     *
     * @ORM\Column(name="last_login_country_name",nullable=true,type="text")
     */
    private $lastLoginCountryName;
    /**
     * @var string
     *
     * @ORM\Column(name="last_login_region_name",nullable=true,type="text")
     */
    private $lastLoginRegionName;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->companyService = new \Doctrine\Common\Collections\ArrayCollection();
        $this->role = array();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startedAt.
     *
     * @param \DateTime $startedAt
     *
     * @return UserCompany
     */
    public function setStartedAt($startedAt)
    {
        $this->startedAt = $startedAt;

        return $this;
    }

    /**
     * Get startedAt.
     *
     * @return \DateTime
     */
    public function getStartedAt()
    {
        return $this->startedAt;
    }

    /**
     * Set endAt.
     *
     * @param \DateTime $endAt
     *
     * @return UserCompany
     */
    public function setEndAt($endAt)
    {
        $this->endAt = $endAt;

        return $this;
    }

    /**
     * Get endAt.
     *
     * @return \DateTime
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * Set role.
     *
     * @param array $role
     *
     * @return UserCompany
     */
    public function setRole($role)
    {
        $this->role = serialize($role);

        return $this;
    }

    /**
     * Get role.
     *
     * @return array
     */
    public function getRole()
    {
        return !empty($this->role) ? unserialize($this->role) : null;
    }

    /**
     * Set notifySms.
     *
     * @param bool $notifySms
     *
     * @return UserCompany
     */
    public function setNotifySms($notifySms)
    {
        $this->notifySms = $notifySms;

        return $this;
    }

    /**
     * Get notifySms.
     *
     * @return bool
     */
    public function getNotifySms()
    {
        return $this->notifySms;
    }

    /**
     * Set notifyEmail.
     *
     * @param bool $notifyEmail
     *
     * @return UserCompany
     */
    public function setNotifyEmail($notifyEmail)
    {
        $this->notifyEmail = $notifyEmail;

        return $this;
    }

    /**
     * Get notifyEmail.
     *
     * @return bool
     */
    public function getNotifyEmail()
    {
        return $this->notifyEmail;
    }

    /**
     * Set createdBy.
     *
     * @param \AppBundle\Entity\User|null $createdBy
     *
     * @return UserCompany
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy.
     *
     * @param \AppBundle\Entity\User|null $updatedBy
     *
     * @return UserCompany
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set user.
     *
     * @param \AppBundle\Entity\User|null $user
     *
     * @return UserCompany
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set company.
     *
     * @param \AppBundle\Entity\Company|null $company
     *
     * @return UserCompany
     */
    public function setCompany(\AppBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company.
     *
     * @return \AppBundle\Entity\Company|null
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Get companyService.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompanyService()
    {
        return $this->companyService;
    }

    /**
     * Add userCompanyService.
     *
     * @param \AppBundle\Entity\UserCompanyService $userCompanyService
     *
     * @return UserCompany
     */
    public function addUserCompanyService(\AppBundle\Entity\UserCompanyService $userCompanyService)
    {
        $this->userCompanyService[] = $userCompanyService;

        return $this;
    }

    /**
     * Remove userCompanyService.
     *
     * @param \AppBundle\Entity\UserCompanyService $userCompanyService
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUserCompanyService(\AppBundle\Entity\UserCompanyService $userCompanyService)
    {
        return $this->userCompanyService->removeElement($userCompanyService);
    }

    /**
     * Get userCompanyService.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserCompanyService()
    {
        return $this->userCompanyService;
    }

    /**
     * Set enabled.
     *
     * @param bool $enabled
     *
     * @return UserCompany
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled.
     *
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set lastLoginDate.
     *
     * @param \DateTime $lastLoginDate
     *
     * @return UserCompany
     */
    public function setLastLoginDate($lastLoginDate)
    {
        $this->lastLoginDate = $lastLoginDate;

        return $this;
    }

    /**
     * Get lastLoginDate.
     *
     * @return \DateTime
     */
    public function getLastLoginDate()
    {
        return $this->lastLoginDate;
    }

    /**
     * Set lastLoginIp.
     *
     * @param string $lastLoginIp
     *
     * @return UserCompany
     */
    public function setLastLoginIp($lastLoginIp)
    {
        $this->lastLoginIp = $lastLoginIp;

        return $this;
    }

    /**
     * Get lastLoginIp.
     *
     * @return string
     */
    public function getLastLoginIp()
    {
        return $this->lastLoginIp;
    }

    /**
     * Set lastLoginCountryName.
     *
     * @param string|null $lastLoginCountryName
     *
     * @return UserCompany
     */
    public function setLastLoginCountryName($lastLoginCountryName = null)
    {
        $this->lastLoginCountryName = $lastLoginCountryName;

        return $this;
    }

    /**
     * Get lastLoginCountryName.
     *
     * @return string|null
     */
    public function getLastLoginCountryName()
    {
        return $this->lastLoginCountryName;
    }

    /**
     * Set lastLoginRegionName.
     *
     * @param string|null $lastLoginRegionName
     *
     * @return UserCompany
     */
    public function setLastLoginRegionName($lastLoginRegionName = null)
    {
        $this->lastLoginRegionName = $lastLoginRegionName;

        return $this;
    }

    /**
     * Get lastLoginRegionName.
     *
     * @return string|null
     */
    public function getLastLoginRegionName()
    {
        return $this->lastLoginRegionName;
    }
}
