<?php

namespace AppBundle\DataFixtures\ORM\Prod;

use Hautelook\AliceBundle\Doctrine\DataFixtures\AbstractLoader;
//use Mgilet\NotificationBundle\Entity\Notification;
use Mgilet\NotificationBundle\Entity\NotifiableNotification;

class DataLoader extends AbstractLoader
{
    /**
     * {@inheritdoc}
     */
    public function getFixtures()
    {
        return [
            '@AppBundle/Resources/fixtures/orm/Service.yml',
            '@AppBundle/Resources/fixtures/orm/Template.yml',
            '@AppBundle/Resources/fixtures/orm/TemplateService.yml',
            '@AppBundle/Resources/fixtures/orm/Office.yml',
            '@AppBundle/Resources/fixtures/orm/CompanyType.yml',
        ];
    }
    public function concat()
    {
        $result = '';

        foreach (func_get_args() as $string) {
            $result .= $string;
        }

        return $result;
    }
}
