<?php

namespace AppBundle\DependencyInjection\Compiler;

use AppBundle\Service\AuditReaderService;
use AppBundle\Factory\AuditManager;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class OverrideAuditReaderCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition('simplethings_entityaudit.reader');
        $definition->setClass(AuditReaderService::class);
        $definition->setFactory(array(new Reference('app_entityaudit.manager'), 'createAuditReader'));
    }
}
