<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180920083838 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user ADD trusted JSON DEFAULT NULL COMMENT \'(DC2Type:json_array)\'');
        $this->addSql('ALTER TABLE revisions_user ADD trusted JSON DEFAULT NULL COMMENT \'(DC2Type:json_array)\'');
        $this->addSql('ALTER TABLE company_service DROP FOREIGN KEY FK_C1CF800530FCDC3A');
        $this->addSql('DROP INDEX IDX_C1CF800530FCDC3A ON company_service');
        $this->addSql('ALTER TABLE company_service CHANGE user_company_id company_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE company_service ADD CONSTRAINT FK_C1CF8005979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('CREATE INDEX IDX_C1CF8005979B1AD6 ON company_service (company_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company_service DROP FOREIGN KEY FK_C1CF8005979B1AD6');
        $this->addSql('DROP INDEX IDX_C1CF8005979B1AD6 ON company_service');
        $this->addSql('ALTER TABLE company_service CHANGE company_id user_company_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE company_service ADD CONSTRAINT FK_C1CF800530FCDC3A FOREIGN KEY (user_company_id) REFERENCES user_company (id)');
        $this->addSql('CREATE INDEX IDX_C1CF800530FCDC3A ON company_service (user_company_id)');
        $this->addSql('ALTER TABLE revisions_user DROP trusted');
        $this->addSql('ALTER TABLE user DROP trusted');
    }
}
