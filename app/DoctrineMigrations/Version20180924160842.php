<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180924160842 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE service ADD connection_target_attribute LONGTEXT NOT NULL, CHANGE user_agent_deny user_agent_deny LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE revisions_service ADD connection_target_attribute LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE revisions_service DROP connection_target_attribute');
        $this->addSql('ALTER TABLE service DROP connection_target_attribute, CHANGE user_agent_deny user_agent_deny LONGTEXT NOT NULL COLLATE utf8_unicode_ci');
    }
}
