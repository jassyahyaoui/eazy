<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180912100354 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE service ADD user_agent_deny LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE revisions_service ADD user_agent_deny LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_company ADD last_login_date DATETIME DEFAULT NULL, ADD last_login_ip LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE revisions_user_company ADD last_login_date DATETIME DEFAULT NULL, ADD last_login_ip LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE revisions_service DROP user_agent_deny');
        $this->addSql('ALTER TABLE revisions_user_company DROP last_login_date, DROP last_login_ip');
        $this->addSql('ALTER TABLE service DROP user_agent_deny');
        $this->addSql('ALTER TABLE user_company DROP last_login_date, DROP last_login_ip');
    }
}
