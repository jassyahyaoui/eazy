// var default_user_profile_image = $('#show-modalUser').data('url');


var default_user_profile_image = document.getElementById('show-modalUser').dataset.url


import MultiLanguage from 'vue-multilanguage'
import CxltToastr from 'cxlt-vue2-toastr'
import VueSession from 'vue-session'
import Cropper from 'cropperjs'
import 'vue-upload-component/dist/vue-upload-component.part.css'
import FileUpload from 'vue-upload-component'
import VModal from 'vue-js-modal'
import VueTelInput from 'vue-tel-input'
/*import VueIntro from 'vue-introjs';
Vue.use(VueIntro);
 import 'intro.js/introjs.css';*/

Vue.use(VueTelInput)
import {
    VueMasonryPlugin
} from 'vue-masonry'
import {
    BasicSelect
} from 'vue-search-select'

var toastrConfigs = {
    position: 'top right',
    showDuration: 5000,
    type: 'success',
    timeOut: 5000
};

Vue.use(FileUpload)
Vue.use(VueSession)
Vue.use(CxltToastr, toastrConfigs)
Vue.use(MultiLanguage, translate)
Vue.use(VModal)
Vue.use(VueMasonryPlugin)
Vue.use(BasicSelect)
Vue.use(VModal, {
    dialog: true
})

let app = new Vue({
    el: '#main',
    components: {
        'file-upload': FileUpload,
        'basic-select': BasicSelect,
        // 'v-intro' : VueIntro
        // 'tel': tel
    },
    data: {
        autostart: false,
        files: [],
        edit: false,
        cropper: false,
        services: [],
        message: '',
        userCompanies: [],
        selectedCompany: {},
        CompanyContacts: [],
        user: [],
        companyNotification: "",
        users: [],
        company: [],
        notifications: [],
        notifiableNotification: [],
        notification: [],
        checkedNames: "",
        numberNotifications: 0,
        pageNotifications: 1,
        isUnseenNotifications: false,
        messages: [],
        image: "",
        searchQuery: '',
        data: [],
        columns: [],
        filterKey: "",
        selectedfilter: "",
        currentCompanyUrlLink: "",
        filterAll: true,
        filterCompany: false,
        filterActuality: false,
        filterNovelty: false,
        filteredData: [],
        nbFilter: 0,
        menuFilter: false,
        code: "",
        isValidCode: "",
        serviceDescription: ""
    },
    mounted() {
//      this.startTour();
//this.$intro().start(); // start the guide
//        this.$intro().showHints(); // show hints
        this.getUserCompanyServices();
        this.getCompanyContacts();
        this.getUserCompanies();
        this.getCurrentCompany();
        this.getUser();
        this.getCompanyUsers();
        this.getNotifications();
    },
    methods: {
        // startTour() {
        //                  this.$intro().start();
        //              },
        //              showHints() {
        //                  this.$intro().showHints();
        //              },
        //              toggleAutostart() {
        //                  this.autostart = !this.autostart;
        //              },
        initMobilePhone() {
            if (this.$refs.phone != undefined) {
                this.$refs.phone._data.phone = this.user.mobile;
                for (var key in this.$refs.phone._data.allCountries) {
                    if (this.$refs.phone._data.allCountries[key].dialCode == this.user.mobileCountry) {
                        this.$refs.phone._data.activeCountry = this.$refs.phone._data.allCountries[key];
                    }
                }
            }
        },
        onInput() {
            this.user.mobileCountry = this.$refs.phone._data.activeCountry.dialCode;
            this.user.mobile = this.$refs.phone._data.phone;
            if (this.user.mobile.indexOf(this.user.mobileCountry) >= 0) {
                var tmp = this.user.mobile.replace(/\s/g, '');
                this.user.mobile = tmp.replace('+' + this.user.mobileCountry, '');
            }
        },
        showModalSmsValidation() {
            this.$modal.show('modal-sms-validation');
        },
        hideModalSmsValidation() {
            this.$modal.hide('modal-sms-validation');
        },
        showModalNotifications() {
            this.$modal.show('modal-notifications');
        },
        showModalUser() {
            this.$modal.show('modal-user');
        },
        showModalHelp() {
            this.$modal.show('modal-help');
        },
        hideModalNotifications() {
            this.$modal.hide('modal-notifications');
        },
        hideModalUser() {
            this.$modal.hide('modal-user');
        },
        hideModalHelp() {
            this.$modal.hide('modal-help');
        },
        showModalEditImageUser() {
            this.$modal.show('modalEditImageUser');
        },
        hideModalEditImageUser() {
            this.$modal.hide('modalEditImageUser');
        },
        showModalEditImageCompany() {
            this.$modal.show('modalEditImageCompany');
        },
        hideModalEditImageCompany() {
            this.$modal.hide('modalEditImageCompany');
        },
        showModalServiceDescription(text) {
            this.serviceDescription = text;
            this.$modal.show('modal-service-description');
        },
        hideModalServiceDescription() {
            this.serviceDescription = "";
            this.$modal.hide('modal-service-description');
        },
        /*  User */
        getUserCompanyServices() {
            this.$getUserCompanyServices = this.$resource(Routing.generate('get_user_company_services'))
            this.$getUserCompanyServices.query().then(
                (response) => { /*Success block*/
                    this.services = response.data.services;
                },
                (response) => { /*Error block*/
                }
            );
        },
        getCompanyContacts() {
            this.$getCompanyContacts = this.$resource(Routing.generate('get_company_contacts'))
            this.$getCompanyContacts.query().then(
                (response) => { /*Success block*/
                    this.CompanyContacts = response.data.CompanyContacts;
                },
                (response) => { /*Error block*/
                }
            );
        },
        getUserCompanies() {
            this.$getUserCompanies = this.$resource(Routing.generate('get_user_companies'))
            this.$getUserCompanies.query().then(
                (response) => { /*Success block*/
                    var userCompanies = response.data.userCompanies;
                    var list = [];
                    userCompanies.forEach(function (value) {
                        list.push({
                            value: value.name,
                            text: value.name
                        });
                    });
                    this.userCompanies = list;
                },
                (response) => { /*Error block*/
                }
            );
        },
        getCurrentCompany() {
            this.$getCurrentCompany = this.$resource(Routing.generate('get_current_company'));
            this.$getCurrentCompany.query().then(
                (response) => { /*Success block*/
                    var selectedCompany = {};
                    selectedCompany.text = response.data.currentCompany.name;
                    selectedCompany.value = response.data.currentCompany.name;
                    this.selectedCompany = selectedCompany;
                    this.company = response.data.currentCompany;
                    this.currentCompanyUrlLink = response.data.currentCompany.officeUrlLink;
                },
                (response) => { /*Error block*/
                }
            );
        },
        getUser() {
            this.$getUser = this.$resource(Routing.generate('get_user'));
            this.$getUser.query().then(
                (response) => { /*Success block*/
                    this.user = response.data.user;

            console.log("user.thumbnail",this.user.thumbnail)
            console.log("default_user_profile_image",default_user_profile_image);

            // if (this.user.thumbnail == null){
            //     this.user.thumbnail = default_user_profile_image ;
            // }
            // else{
            //     this.user.thumbnail =  /uploads/' + this.user.thumbnail;
            // }


            this.user.thumbnail = (this.user.thumbnail != null) ? '/uploads/' + this.user.thumbnail : default_user_profile_image



                    this.user.companyId =  this.user.currentCompany.id;
                    this.initMobilePhone();
                    this.companyNotification = this.user.currentCompany.id;
                    this.loadCompanyNotification(this.user.currentCompany.id);
                    if (this.user.locale === "fr") {
                        this.language = "fr";
                    } else if (this.user.locale === "en") {
                        this.language = "en";
                    } else if (this.user.locale === "de") {
                        this.language = "de";
                    } else {
                        this.language = "fr";
                    }
                    //Recuperer et afficher les messages depuis la sesions
                    this.showMessages();
                },
                (response) => { /*Error block*/
                }
            );
        },
        postUser() {
            this.$postUser = this.$resource(Routing.generate('post_current_user'));
            this.$postUser.update(this.user).then(
                (response) => { /*Success block*/
                    this.$session.set("messages", "Success");
                    window.location.replace(Routing.generate('frontend_homepage'));

                },
                (response) => { /*Error block*/
                    this.showNotification("Error");
                }
            );
        },
        postUserCompanyImage() {
            this.$postUser = this.$resource(Routing.generate('set_current_company_image'));
            this.$postUser.update(this.company).then(
                (response) => { /*Success block*/
                },
                (response) => { /*Error block*/
                    this.showNotification("Error");
                }
            );
        },
        loadCompanyNotification(id) {
            var notifySms;
            var notifyEmail;
            this.user.userCompany.forEach(function (value) {
                if (value.company.id === id) {
                    notifySms = value.notifySms;
                    notifyEmail = value.notifyEmail;
                }
            });
            this.user.companyNotifyEmail = notifyEmail;
            this.user.companyNotifySms = notifySms;
        },
        getCompanyUsers() {
            this.$getCompanyUsers = this.$resource(Routing.generate('get_company_users'));
            this.$getCompanyUsers.query().then(
                (response) => { /*Success block*/
                    this.users = response.data.users;
                },
                (response) => { /*Error block*/
                }
            );
        },
        connectLink(id, status, longDesc, connectionTargetAttribute) {
            if (status === "Disable") {
                this.showModalServiceDescription(longDesc);
                return;
            }
            this.$getLink = this.$resource(Routing.generate('connect_service_user_from_company', {
                id: id
            }));
            this.$getLink.query().then(
                (response) => {
                    var url = response.data.link;
                    if (connectionTargetAttribute != "") {
                        var win = window.open(url, connectionTargetAttribute);
                    }
                    else {
                        var win = window.open(url);
                    }
                    if (win) {
                        win.focus();
                    }
                    if (win === undefined || win === null) {

                        if (this.user.locale === "fr")
                        {
                            alert("Erreur ! S'il vous plaît configurer votre navigateur Web pour autoriser les fenêtres pop-up");
                        }
                        else if (this.user.locale === "en")
                        {
                           alert('Error ! Please configure your web browser to allow pop-up windows')
                        }
                        else if (this.user.locale === "de")
                        {
                           alert('Fehler! Bitte konfigurieren Sie Ihren Webbrowser, um Pop-ups zuzulassen')
                        }

                    }


                },
                (response) => {
                }
            );
        },
        /*  Notification                 */
        getNotifications() {
            this.$getNotificationUser = this.$resource(Routing.generate('get_notification_user', {
                p: this.pageNotifications
            }));
            this.$getNotificationUser.query().then(
                (response) => { /*Success block*/
                    this.notifications = response.data.notifications;
                    this.numberNotifications = response.data.numberNotifications;
                    this.filterNotification();
                    if (response.data.numberNotifications > 0)
                        this.isUnseenNotifications = true;
                    else
                        this.isUnseenNotifications = false;
                },
                (response) => { /*Error block*/
                }
            );
        },
        markAllSeen() {
            var id = 0
            this.$postUser = this.$resource(Routing.generate('set_notification_user_all_seen'));
            this.$postUser.update({
                'id': id
            }).then(
                (response) => { /*Success block*/
                    this.getNotifications();
                },
                (response) => { /*Error block*/
                }
            );
        },
        markNotificationSeen(id) {
            this.$postUser = this.$resource(Routing.generate('set_notification_user_seen'));
            this.$postUser.update({
                'id': id
            }).then(
                (response) => { /*Success block*/
                    this.getNotifications();
                },
                (response) => { /*Error block*/
                }
            );
        },
        loadMoreNotification() {
            this.pageNotifications = this.pageNotifications + 1;
            this.getNotifications();
        },
        showNotification(type) {
            if (this.language === "fr") {
                if (type === "Success") {
                    this.$toast.success({
                        title: 'Succès',
                        message: "Les données ont été correctement sauvegardées."
                    });
                } else if (type === "Error ") {
                    this.$toast.success({
                        title: 'Erreur',
                        message: "Une erreur inattendue a été rencontrée. Nos équipes ont été notifiées. Merci de votre compréhension."
                    });
                }
            } else if (this.language === "en") {
                if (type === "Success") {
                    this.$toast.success({
                        title: 'Success',
                        message: "Data have been successfully saved."
                    });
                } else if (type === "Error ") {
                    this.$toast.success({
                        title: 'Error',
                        message: "An unknown error has occurred. The team has been notified. Thanks for your understanding."
                    });
                }
            }else if (this.language === "de") {
                if (type === "Success") {
                    this.$toast.success({
                        title: 'Erfolg',
                        message: "Die Daten wurden korrekt gespeichert."
                    });
                } else if (type === "Error ") {
                    this.$toast.success({
                        title: 'Fehler',
                        message: "Ein unerwarteter Fehler ist aufgetreten. Unsere Teams wurden benachrichtigt. Vielen Dank für Ihr Verständnis."
                    });
                }
            }
        },
        filterNotification() {
            var filterCompany = this.filterCompany;
            var filterActuality = this.filterActuality;
            var filterNovelty = this.filterNovelty;
            var filteredData = [];
            if (this.filterAll === true) {
                this.filterCompany = true;
                this.filterActuality = true;
                this.filterNovelty = true;
                this.notifications.forEach(function (n) {
                    filteredData.push(n);
                });
            } else {
                this.notifications.forEach(function (n) {
                    if (n.subject === "Entreprise") {
                        if (filterCompany === true)
                            filteredData.push(n);
                    }
                    if (n.subject === "Actualité") {
                        if (filterActuality === true)
                            filteredData.push(n);
                    }
                    if (n.subject === "Nouveauté") {
                        if (filterNovelty === true)
                            filteredData.push(n);
                    }
                });
            }
            this.filteredData = filteredData;
            this.nbFilter = 0;
            this.nbFilter += (filterCompany === true) ? 1 : 0;
            this.nbFilter += (filterActuality === true) ? 1 : 0;
            this.nbFilter += (filterNovelty === true) ? 1 : 0;
            this.menuFilter = false;
        },
        filterNotificationCancel() {
            this.nbFilter = 0;
            this.filterAll = false;
            this.filterCompany = false;
            this.filterActuality = false;
            this.filterNovelty = false;
            var filteredData = [];
            this.notifications.forEach(function (n) {
                filteredData.push(n);
            });
            this.filteredData = filteredData;
            this.menuFilter = false;
        },
        // Show the filter menu after click at filter button
        showFilter() {
            this.menuFilter = (this.menuFilter === true) ? false : true;
        },
        getLink(link) {
            return "uploads/".link;
        },
        showMessages() {
            this.messages = this.$session.get("messages");
            if (this.$session.get("messages")) {
                this.showNotification(this.$session.get("messages"));
                this.$session.remove("messages");
            }
        },
        selectCompany(company) {
            this.selectedCompany = company
            this.$setCompany = this.$resource(Routing.generate('set_current_company'))
            this.$setCompany.save({
                company: company.value
            }).then(
                (response) => { /*Success block*/
                    this.getUserCompanyServices();
                    this.getCompanyContacts();
                    this.companyNotification = response.data.company;
                    this.loadCompanyNotification(response.data.company);
                    this.getCurrentCompany();
                },
                (response) => { /*Error block*/
                }
            );
        },
        resetPassword() {
            this.$resetPassword = this.$resource(Routing.generate('resetting_password'));//user_reset_password
            this.$resetPassword.save({
                'username': this.user.email
            }).then(
                (response) => {
                    if (this.language === "fr") {
                        this.$toast.success({
                            title: 'Succès',
                            message: "Un e-mail a été envoyé. \n Il contient un lien sur lequel il vous faudra cliquer " +
                            "pour réinitialiser votre mot de passe. \n" +
                            "Remarque : Vous ne pouvez demander un nouveau mot de passe que toutes les 5 minutes. "
                        });
                    }
                    else if (this.language === "en") {
                        this.$toast.success({
                            title: 'Success', message: "An email has been sent. It contains a link " +
                            "you must click to reset your password. Note: You can only request a new password once " +
                            "within 5 minutes. If you don't get an email check your spam folder or try again."
                        });
                    }
                    else if (this.language === "de") {
                        this.$toast.success({
                            title: 'Success', message: "Eine E-Mail wurde gesendet \n. Es enthält einen Link," +
                            " auf den Sie klicken müssen um dein Passwort zurückzusetzen. \n" +
                            "Hinweis: Sie können nur alle 5 Minuten ein neues Passwort anfordern."
                        });
                    }
                },
                (response) => {
                    if (this.language === "fr") {
                        this.$toast.success({
                            title: 'Erreur',
                            message: "Vous ne pouvez demander un nouveau mot de passe que toutes les 5 minutes."
                        });
                    }
                    else if (this.success === "en") {
                        this.$toast.success({
                            title: 'Erreur', message: "You can only request a new password once within 5 minutes."
                        });
                    }
                    else if (this.success === "de") {
                        this.$toast.success({
                            title: 'Erreur', message: "Sie können nur alle 5 Minuten ein neues Passwort anfordern."
                        });
                    }
                }
            );
        },
        // Images handling
        onChanged() {
            if (this.$refs.pictureInput.file) {
                this.image = this.$refs.pictureInput.file;
            } else {
            }
        },
        onRemoved() {
            this.image = '';
        },
        attemptUpload() {
            if (this.image) {
                FormDataPost(this.$resource(Routing.generate('set_current_company_image')), this.image)
                    .then(response => {
                        if (response.data.success) {
                            this.image = '';
                        }
                    })
                    .catch(err => {
                    });
            }
        },
        editSave() {
            this.edit = false;
            let oldFile = this.files[0];
            let binStr = atob(this.cropper.getCroppedCanvas().toDataURL(oldFile.type).split(',')[1]);
            let arr = new Uint8Array(binStr.length);
            for (let i = 0; i < binStr.length; i++) {
                arr[i] = binStr.charCodeAt(i);
            }
            let file;
            try {
                file = new File([arr], oldFile.name, {
                    type: oldFile.type
                });
            } catch (err) {
                file = new Blob([arr], {
                    type: oldFile.type
                });
            }
            this.$refs.upload.update(oldFile.id, {
                file,
                type: file.type,
                size: file.size,
                active: true
            });
        },
        inputFile(newFile, oldFile, prevent) {
            if (newFile) {
                if (newFile.error) {
                    // @TODO : Gérer l'erreur
                    alert('Erreur lors de l\'upload!')
                }
                if (newFile.success) {
                    this.getCurrentCompany();
                    this.getUser();
                }
            }
            if (newFile && !oldFile) {
                this.$nextTick(function () {
                    this.edit = true;
                });
            }

            if (!newFile && oldFile) {
                this.edit = false;
            }
        },
        inputFilter(newFile, oldFile, prevent) {
            if (newFile && !oldFile) {
                if (!/\.(jpg|jpeg|png)$/i.test(newFile.name)) {
                    alert('Your choice is not a picture')
                    return prevent()
                }
                if (newFile.size > 204800) {
                    alert('The maximum upload file size is set to 200 KO')
                    return prevent()
                }
            }
            if (newFile && (!oldFile || newFile.file !== oldFile.file)) {
                newFile.url = ''
                let URL = window.URL || window.webkitURL
                if (URL && URL.createObjectURL) {
                    newFile.url = URL.createObjectURL(newFile.file)
                }
            }
        },
        remove(file) {
            this.$refs.upload.remove(file)
        },
        update: function () {
            this.$postUser = this.$resource(Routing.generate('company_update_logo'));
            this.$postUser.update(this.user).then(
                (response) => { /*Success block*/
                },
                (response) => { /*Error block*/
                    this.showNotification("Error");
                }
            );
        },
        updateCompany: function () {
            this.$resource(Routing.generate('post_frontend_edit_company', {
                'id': this.company.id,
                'company': this.company
            })).update(this.company).then(
                (response) => { /*Success block*/
                    this.showNotification("Success");
                    window.location.href = Routing.generate('frontend_homepage');
                },
                (response) => { /*Error block*/
                    this.showNotification("Error");
                }
            );
        },
        setTwoFA: function (TwoFA) {

            this.$setTwoFA = this.$resource(Routing.generate('user_set_TwoFA', {TwoFA: TwoFA}))
            this.$setTwoFA.update().then(
                (response) => {/*Success block*/
                },
                (response) => {/*Error block*/
                }
            )
        },
        sendSmsCode: function () {
            this.$setTwoFA = this.$resource(Routing.generate('send_sms_code'))
            this.$setTwoFA.save().then(
                (response) => {/*Success block*/
                },
                (response) => {/*Error block*/
                }
            )
        },
        cancelSmsCode: function () {
            this.hideModalSmsValidation();
            this.user.twoFactorAuthentication = false;
            this.code = "";
            if (this.language === "fr") {
                this.$toast.success({
                    title: 'Echec',
                    message: "Le code d’activation est erroné ." +
                    " \nVeuillez recommencez votre activation ou contacter notre support si nécessaire ."
                });
            }
            else if (this.success === "en") {
                this.$toast.success({
                    title: 'Erreur', message: "\n" +
                    "The activation code is wrong. Please restart your activation or contact our support if necessary"
                });
            }
            else if (this.success === "de") {
                this.$toast.success({
                    title: 'Fehler',
                    message: "Der Aktivierungscode ist falsch Bitte " +
                    " \n starten Sie Ihre Aktivierung neu oder kontaktieren Sie unseren Support ."
                });
            }
        },
        validateSmsCode: function (code) {
            this.$validateSmsCode = this.$resource(Routing.generate('validate_sms_code', {code: code}))
            this.$validateSmsCode.save().then(
                (response) => {/*Success block*/
                    this.isValidCode = response.data;
                    if (this.isValidCode == true) {
                        this.setTwoFA(true);
                        this.hideModalSmsValidation();
                        this.code = "";
                        if (this.language === "fr") {
                            this.$toast.success({
                                title: 'Echec',
                                message: "Votre téléphone portable est maintenant validé." +
                                " \nL’authentification à deux facteurs est active."
                            });
                        }
                        else if (this.success === "en") {
                            this.$toast.success({
                                title: 'Success', message: "\n" +
                                "The activation code is wrong. Please restart your activation or contact our support if necessary"
                            });
                        }
                        else if (this.success === "de") {
                            this.$toast.success({
                                title: 'Succès',
                                message: "Ihr Mobiltelefon ist jetzt validiert. Die Zwei-Faktor-Authentifizierung ist aktiv."
                            });
                        }
                    } else {
                        this.cancelSmsCode();
                    }
                },
                (response) => {/*Error block*/
                }
            )
        },
        phoneValidation: function (twoFactorAuthentication) {
            if (twoFactorAuthentication == false) {
                this.setTwoFA(false);
            } else {
                this.sendSmsCode();
                this.showModalSmsValidation();
            }
        },
        /*       startTour() {
                   this.$intro().start();
               },
               showHints() {
                   this.$intro().showHints();
               },
               toggleAutostart() {
                   this.autostart = !this.autostart;
               },*/
    },
    watch: {
        /* Upload and edit Image */
        edit(value) {
            if (value) {
                this.$nextTick(function () {
                    if (!this.$refs.editImage) {
                        return
                    }
                    let cropper = new Cropper(this.$refs.editImage, {
                        aspectRatio: 1 / 1,
                        viewMode: 1,
                    })
                    this.cropper = cropper
                })
            } else {
                if (this.cropper) {
                    this.cropper.destroy()
                    this.cropper = false
                }
            }
        },
        // Recharger les donnes pour chaque changement de variable de la sociéte curante
        companyNotification: function (value) {
            this.user.companyId = value;
            this.loadCompanyNotification(value);
        },

    }
});
