Vue.filter('DD/MM/YY', function (value) {
    if (value) {
        return moment(String(value)).format('DD/MM/YYYY')
    }
})

Vue.filter('formatDate', function (value, locale) {
    if (value) {
        if (locale == 'en') {
            return moment(String(value)).format('MM/DD/YYYY hh:mm A')
        }
        return moment(String(value)).format('DD/MM/YYYY HH:mm')
    }
})

Vue.filter('notificationtype', function (value) {


    if (value) {
        return moment(String(value)).format('DD/MM/YYYY hh:mm')
    }
})