var user_id = $('#some-link').data('id');

new Vue({
    el: '#userEdit',
    data:
        {
            seen: false,
            user: {
                id: '',
                username: '',
                username_canonical: '',
                email: '',
                email_canonical: '',
                enabled: true,
                password: '',
                role: [],
                created_at: '',
                updated_at: '',
                last_name: '',
                first_name: '',
                mobile: '',
            },
            options: [],
            ServiceRoleOptions: [],
            selectedCompany: [],
            SavedSelectedCompany: [],
            RestOfServices: [],
            AppearRestOfServices: '',
            selectedRestOfService: '',
            userCompany: {
                roles: [],
                email: true,
                sms: true,
                uctab: [],
            },
            selected: null,
            services: [],
            roles: [],
            email: true,
            sms: true,
            clone_services: [],
            template_id_tab: [],
            template_id: null,
            appear: false,
            company_id: null,
            user_company_id: null,
            data: {label: 'Ajouter un company service', value: ''},
            new_user_company_id: null,
            new_company_id: null,
            user_id: null,
            role_type: '',
            selectedRoleOptions: '',
            user_company_service_id_option: [],
            selectOptionsResult: '',
            first_password :'',
            second_password : '',
            password_strength : ''
        }
    ,
    mounted() {
        this.fetchInfos(user_id);
        this.fetchCompanies();
        this.fetchServiceRoleAll();
    },
    watch: {
        data: function () {
            if (this.data.value) {
                this.fetchUserCompanyServicesByUserCompany_ID(this.data.value.user_company_id)
                this.fetchRestOfServices(this.data.value.user_company_id);
                this.fetchOptions(this.data.value.user_company_id);
                this.getUserCompanySettings(this.data.value.user_company_id);
            }
        },
        selectedCompany: function () {
            this.userCompany.uctab = this.selectedCompany
        },
        first_password: function () {
            var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
            var mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
            var enoughRegex = new RegExp("(?=.{6,}).*", "g");

            if (this.first_password == 0) {
                this.password_strength = 'Tapez votre mot de passe';
            } else if (false == enoughRegex.test(this.first_password)) {
                this.password_strength = 'More Characters';
            } else if (strongRegex.test(this.first_password)) {
                this.strength = 'Très fort';
            } else if (mediumRegex.test(this.first_password)) {
                this.password_strength = 'Bon';
            } else {
                this.password_strength = 'Très faible';
            }
        }
    },
    computed: {},
    methods: {
        fetchOptions(id) {
            this.$getselectedOptions = this.$resource(Routing.generate('get_service_role_options', {id: id}))
            this.$getselectedOptions.query().then(
                (response) => {/*Success block*/
                    this.selectOptionsResult = response.data;
                },
                (response) => {/*Error block*/
                    console.log('erreur', response)
                }
            )
        },
        selectInfos(role_type, user_company_service_id, service_id) {
            this.user_company_service_id_option = user_company_service_id;
            if (role_type === null) {
                this.role_type = null;
                this.selectedRoleOptions = null;
            } else {
                for (var value of this.selectOptionsResult) {
                    if (value.ServiceId === service_id) {
                        if (role_type === 'single') {
                            this.role_type = 'single';
                            this.selectedRoleOptions = '';
                            if (value.selectedRole != null) {
                                this.selectedRoleOptions = value.selectedRole[0];
                            }
                        }
                        if (role_type === 'multiple') {
                            this.role_type = 'multiple';
                            this.selectedRoleOptions = [];
                            if (value.selectedRole != null) {
                                this.selectedRoleOptions = value.selectedRole;
                            }
                        }
                    }
                }
            }
        },
        show(param) {
            this.seen = param;
        },
        checkServicesAction(entity) {
            /* if toggle passed from False to True*/
            if (entity.enabled == true) {
                this.$EnableServicesUserFromCompany = this.$resource(Routing.generate('enable_services_user_from_company', {
                    user_id: user_id,
                    company_id: entity.company_id
                }))
                this.$EnableServicesUserFromCompany.update().then(
                    (response) => {/*Success block*/
                    },
                    (response) => {/*Error block*/
                        console.log('erreur', response)
                    }
                )
            }
            /* if toggle passed from True to False*/
            if (entity.enabled == false) {
                this.$DisableServicesUserFromCompany = this.$resource(Routing.generate('disable_services_user_from_company', {
                    user_id: user_id,
                    company_id: entity.company_id
                }))
                this.$DisableServicesUserFromCompany.update().then(
                    (response) => {/*Success block*/
                    },
                    (response) => {/*Error block*/
                        console.log('erreur', response)
                    }
                )
            }
        },
        checkServiceAction(entity) {
            /* if toggle passed from False to True*/
            if (entity.enabled == true) {
                this.$EnableServiceUserFromCompany = this.$resource(Routing.generate('enable_service_user_from_company', {
                    service_id: entity.serviceId,
                    user_id: user_id,
                    company_id: entity.companyId
                }))
                this.$EnableServiceUserFromCompany.update().then(
                    (response) => {/*Success block*/
                    },
                    (response) => {/*Error block*/
                        console.log('erreur', response)
                    }
                )
            }
            /* if toggle passed from True to False*/
            if (entity.enabled == false) {
                this.$DisableServiceUserFromCompany = this.$resource(Routing.generate('disable_service_user_from_company', {
                    service_id: entity.serviceId,
                    user_id: user_id,
                    company_id: entity.companyId
                }))
                this.$DisableServiceUserFromCompany.update().then(
                    (response) => {/*Success block*/
                    },
                    (response) => {/*Error block*/
                        console.log('erreur', response)
                    }
                )
            }
        },
        setUserCompanyService(id) {
            this.$setUser = this.$resource(Routing.generate('set_user_company_service'))
            this.$setUser.update({
                userCompanyService: this.services,
                selectInfos: this.data,
                restServices: this.selectedRestOfService
            }).then(
                (response) => {/*Success block*/
                    this.user_company_service_id_option = response.data;
                    this.setUserCompanySettings(this.data['value']['user_company_id']);
                },
                (response) => {/*Error block*/
                    console.log('erreur', response)
                }
            )
        },
        setUserCompanyServiceRoles() {
            this.$setUserCompanyServiceRoles = this.$resource(Routing.generate('set_user_company_service_roles'))
            this.$setUserCompanyServiceRoles.update({
                user_company_service_id_option: this.user_company_service_id_option,
                selectedRoleOptions: this.services,
                userCompanyId: this.services[0]['user_company_id'],
            }).then(
                (response) => {/*Success block*/
                    $(function () {
                        $(".addproduct").click();
                    });
                },
                (response) => {/*Error block*/
                    console.log('erreur', response)
                    $(function () {
                        $('#exampleModal').modal('hide');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();
                    });
                }
            )
        },

        setUserCompany(id) {
            this.$setUser = this.$resource(Routing.generate('set_user_company', {id: id}))
            this.$setUser.update({userCompany: this.userCompany.uctab}).then(
                (response) => {/*Success block*/
                    location.reload();
                },
                (response) => {/*Error block*/
                    console.log('erreur', response)
                }
            )
        },
        setUserCompanySettings(id) {
            this.$setUserCompanySettings = this.$resource(Routing.generate('set_user_company_settings', {id: id}))
            this.$setUserCompanySettings.update({roles: this.roles, email: this.email, sms: this.sms}).then(
                (response) => {/*Success block*/
                    this.setUserCompanyServiceRoles();
                    location.reload();
                },
                (response) => {/*Error block*/
                    console.log('erreur', response)
                }
            )
        },
        getUserCompanySettings(id) {
            this.$getUserCompanySettings = this.$resource(Routing.generate('get_user_company_settings', {id: id}))
            this.$getUserCompanySettings.query().then(
                (response) => {/*Success block*/
                    this.roles = response.data['roles']
                    this.sms = response.data['sms']
                    this.email = response.data['email']
                },
                (response) => {/*Error block*/
                    console.log('erreur', response)
                }
            )
        },
        setUser(id) {
            this.$setUser = this.$resource(Routing.generate('set_user', {id: id}))
            this.$setUser.update({user: this.user}).then(
                (response) => {/*Success block*/
                    this.setUserCompany(id)
                },
                (response) => {/*Error block*/
                    console.log('erreur', response)
                }
            )
        },
        fetchUserCompanyServicesByUserCompany_ID(id) {
            this.$getUserCompanyServices = this.$resource(Routing.generate('get_user_company_service_by_UserCompany_id', {id: id}))
            this.$getUserCompanyServices.query().then(
                (response) => {/*Success block*/
                    this.services = response.data
                    if (this.services.length === 0) {
                        this.fetchServices(this.data.value.template_id)
                    }
                },
                (response) => {/*Error block*/
                    console.log('erreur', response)
                }
            )
        },
        fetchServices(id) {
            this.$getTemplateServices = this.$resource(Routing.generate('get_backend_template_services', {id: id}))
            this.$getTemplateServices.query().then(
                (response) => {/*Success block*/
                    this.services = response.data
                },
                (response) => {/*Error block*/
                    console.log('erreur', response)
                }
            )
        },
        fetchCompanies() {
            this.$getCompanies = this.$resource(Routing.generate('get_backend_companies_all'))
            this.$getCompanies.query().then(
                (response) => {/*Success block*/
                    for (var value of response.data) {
                        if (value.template && value.template.templateService[0]) {
                            this.options.push({
                                label: value.name,
                                value: {
                                    company_id: value.id,
                                    company: value.name,
                                    enabled: true,
                                    started_at: moment(String(new Date())).format('YYYY-MM-DD'),
                                    end_at: null,
                                    template_id: value.template.id,
                                },
                            })
                        }
                    }
                },
                (response) => {/*Error block*/
                    console.log('erreur', response)
                }
            )
        },
        fetchServiceRoleAll() {
            this.$getServiceRoleEditAll = this.$resource(Routing.generate('get_service_role_all'))
            this.$getServiceRoleEditAll.query().then(
                (response) => {/*Success block*/
                    for (var value of response.data) {

                        this.ServiceRoleOptions.push({
                            label: value.serviceName,
                            value: value
                        })
                    }
                },
                (response) => {/*Error block*/
                    console.log('erreur', response)
                }
            )
        },
        fetchRestOfServices(id) {
            this.RestOfServices = [];
            this.$getRestOfServices = this.$resource(Routing.generate('get_rest_of_services', {id: id}))
            this.$getRestOfServices.query().then(
                (response) => {/*Success block*/
                    if (response.data.length > 0) {
                        this.AppearRestOfServices = true;
                        for (var value of response.data) {
                            /*init second v-select*/
                            this.RestOfServices.push({
                                label: value.service,
                                value: value,
                            })
                        }
                    } else {
                        this.AppearRestOfServices = false;
                    }
                },
                (response) => {/*Error block*/
                    console.log('erreur', response)
                }
            )
        },
        fetchInfos(id) {
            this.$getTemplateServices = this.$resource(Routing.generate('get_backend_user_by_id', {id: id}))
            this.$getTemplateServices.query().then(
                (response) => {/*Success block*/
                    /* Get settings Roles+email+sms by user_company_id*/
                    if (response.data['user_company_service'] != null) {
                        this.getUserCompanySettings(response.data['user_company_service'][0]['user_company_id']);
                    }
                    /*Init section*/
                    this.user = response.data['user']
                    if (response.data['choosen_company'] != null) {
                        this.company_id = response.data['choosen_company']['company_id']
                        this.data.label = response.data['choosen_company']['company_name']
                    }
                    this.services = response.data['user_company_service']
                    if (response.data['user_company'] != null) {
                        for (var value of response.data['user_company']) {
                            /*Assign selected company with values*/
                            if (value.company_id === this.company_id) {
                                this.data.value = value
                                this.fetchRestOfServices(this.data.value.user_company_id);
                                this.fetchOptions(this.data.value.user_company_id);
                            }
                            /*init first v-select*/
                            this.selectedCompany.push({
                                label: value.company,
                                value: value,
                            })
                            /*init second v-select*/
                            this.SavedSelectedCompany.push({
                                label: value.company,
                                value: value,
                            })
                        }
                    }
                },
                (response) => {/*Error block*/
                    console.log('erreur', response)
                }
            )
        },
        disabledTwoFA: function (id, TwoFA) {
            this.$setTwoFA = this.$resource(Routing.generate('disable_user_TwoFA', {id: id, TwoFA: TwoFA}))
            this.$setTwoFA.update().then(
                (response) => {/*Success block*/
                },
                (response) => {/*Error block*/
                    console.log('erreur', response)
                }
            )
        },
        deleteUserCompany(id) {/*Delete UserCompany from ID*/
            this.$deleteUserCompany = this.$resource(Routing.generate('delete_user_company', {id: id}))
            this.$deleteUserCompany.delete().then(
                (response) => {/*Success block*/
                    location.reload();
                },
                (response) => {/*Error block*/
                    console.log('erreur', response)
                }
            )
        },
        confirmDelete: function (index) {//index is passed by the button
            var self = this;
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                closeOnConfirm: true,
            }).then(function () {
                self.deleteUserCompany(index);
                swal(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                );
            })
        },
        changePassword() {
            if (this.first_password != this.second_password) {
                alert("les deux mot de passe ne sont pas identique")
                return;
            }

            var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
            var mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
            var enoughRegex = new RegExp("(?=.{6,}).*", "g");

            if (this.first_password == 0) {
                this.password_strength = 'Tapez votre mot de passe';
            } else if (false == enoughRegex.test(this.first_password)) {
                this.password_strength = 'More Characters';
            } else if (strongRegex.test(this.first_password)) {
                this.strength = 'Très fort';
            } else if (mediumRegex.test(this.first_password)) {
                this.password_strength = 'Bon';
            } else {
                this.password_strength = 'Très faible';
            }

            if ((this.password_strength === 'Bon' || this.strength === 'Très fort') === false)
            {
                alert('Mot de passe Très faible')
                return;
            }
            this.$resetPassword = this.$resource(Routing.generate('user_change_password', {id: this.user.id}))
            this.$resetPassword.update({
                'password':this.first_password
            }).then(
                (response) => { /*Success block*/
                    this.first_password = ""
                    this.second_password = ""
                    document.getElementById("close_change_pwd").click();
                    alert("Password has been changed");
                },
                    (response) => { /*Error block*/
                        alert("Error");
                    }
                );
        },
    }
})
