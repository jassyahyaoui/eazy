new Vue({
    el: '#backend',
    data: {
        seen: false,
        user: {
            id: '',
            username: '',
            username_canonical: '',
            email: '',
            email_canonical: '',
            enabled: true,
            password: '',
            role: [],
            created_at: '',
            updated_at: '',
            last_name: '',
            first_name: '',
            mobile: '',
        },
        roles: [],
        ServiceRoleOptions: [],
        companies: [],
        services: [],
        selectedCompany: [],
        data: null,
        userCompanyId: null,
        options: [],
        userCompanyArrayOfId: [],
        userCompany: {
            roles: [],
            email: true,
            sms: true,
            uctab: [],
        },
        user_company_service_id: '',
    },
    mounted() {
        this.fetchCompanies();
        this.fetchServiceRoleAll();
    },
    watch: {
        data: function () {
            this.fetchServices(this.data.templateId)
        },
        selectedCompany: function () {
            this.userCompany.uctab = this.selectedCompany
        }
    },
    methods: {
        show(param) {
            this.seen = param;
        },
        fetchServices(param) {
            this.$getTemplateServices = this.$resource(Routing.generate('get_backend_template_services', {id: param}))
            this.$getTemplateServices.query().then(
                (response) => {/*Success block*/
                    this.services = response.data
                },
                (response) => {/*Error block*/
                    console.log('erreur', response)
                }
            )
        },
        fetchServiceRoleAll() {
            this.$getServiceRoleAll = this.$resource(Routing.generate('get_service_role_all'))
            this.$getServiceRoleAll.query().then(
                (response) => {/*Success block*/
                    for (var value of response.data) {

                        this.ServiceRoleOptions.push({
                            label: value.serviceName,
                            value: value
                        })
                    }

                },
                (response) => {/*Error block*/
                    console.log('erreur', response)
                }
            )
        },
        fetchCompanies() {
            this.$getCompanies = this.$resource(Routing.generate('get_backend_companies_all'))
            this.$getCompanies.query().then(
                (response) => {/*Success block*/

                    for (var value of response.data) {
                        if (value.template) {
                            this.options.push({
                                label: value.name,
                                companyId: value.id,
                                templateId: value.template.id,
                                companyName: value.name,
                                templateService: [{
                                    ts: value.template.templateService,
                                    enabled: false,
                                    statut: value.template.templateService.defaultStatus
                                }],
                                userCompany: value.userCompany,
                                enabled: true,
                                startedAt: moment(String(new Date())).format('YYYY-MM-DD'),
                                endAt: ''
                            })
                        }
                    }

                },
                (response) => {/*Error block*/
                    console.log('erreur', response)
                }
            )
        },
        addUser() {
            this.$addUser = this.$resource(Routing.generate('post_backend_user'))
            this.$addUser.save({user: this.user, selectedCompany: this.selectedCompany[0].companyId}).then(
                (response) => {/*Success block*/
                    this.user = JSON.parse(response.data)
                    this.addUserCompany(this.user.id)

                },
                (response) => {/*Error block*/
                    var choice = confirm('L\'utilisateur ' + response.body['email']
                        + ' existe déjà avec le nom : '
                        + response.body['firstName']
                        + ' '
                        + response.body['lastName'] + '.\n' +
                        'Veuillez le modifier ???');
                    if (choice == true) {
                        window.location.href = Routing.generate('user_edit', {id: response.body['id']});
                    }
                    // else {
                    //     location.reload();
                    // }
                }
            )
        },
        addUserCompany(param) {
            this.$addUserCompany = this.$resource(Routing.generate('post_backend_user_company'))
            this.$addUserCompany.save({userCompany: this.userCompany, userId: param}).then(
                (response) => {/*Success block*/
                    this.userCompanyArrayOfId = JSON.parse(response.data)
                    var i = 0;
                    this.userCompanyId = null
                    for (i = 0; i < this.userCompanyArrayOfId.length; i++) {
                        if (this.data.companyId == Object.keys(this.userCompanyArrayOfId[i])) {
                            this.userCompanyId = this.userCompanyArrayOfId[i][this.data.companyId]
                        }
                    }
                    this.addUserCompanyService()
                },
                (response) => {/*Error block*/
                    console.log('erreur', response)
                }
            )
        },
        addUserCompanyService() {
            this.$addUserCompanyService = this.$resource(Routing.generate('post_backend_user_company_service'))
            this.$addUserCompanyService.save({
                userCompanyService: this.services,
                userCompanyId: this.userCompanyId
            }).then(
                (response) => {/*Success block*/
                    this.user_company_service_id = response.data;
                    this.addUserCompanyServiceRoles();
                },
                (response) => {/*Error block*/
                    console.log('erreur', response)
                }
            )
        },
        addUserCompanyServiceRoles() {
            this.$addUserCompanyServiceRoles = this.$resource(Routing.generate('post_backend_user_company_service_roles'))
            this.$addUserCompanyServiceRoles.save({
                userCompanyServicRoles: this.services,
                userCompanyId: this.userCompanyId
            }).then(
                (response) => {/*Success block*/
                    window.location.href = Routing.generate('user_index');
                },
                (response) => {/*Error block*/
                    console.log('erreur', response)
                }
            )
        }

    }
})