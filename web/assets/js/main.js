'use strict';

import '../css/custom-bootstrap.scss';
import 'font-awesome/css/font-awesome.css';
import '../css/main.css';
import '../css/mazar-icon.css';
import '../css/sidebar.css';
import '../css/responsive.css';
import 'vue-tel-input/dist/vue-tel-input.css';
