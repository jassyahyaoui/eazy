AppEazyByMazars
========================

## Requirement environment

1. Clean server

	```
	$ sudo apt-get autoremove & sudo apt-get upgrade & sudo apt-get dist-upgrade & sudo apt-get update
	```
	
2. PHP version>7.1

	```
	$ sudo apt-get install -y python-software-properties
	$ sudo add-apt-repository -y ppa:ondrej/php
	$ sudo apt-get update & sudo apt-get install php7.1
	```
	
	look php version `$ php -v`
3. Extention PHP required 

	```
	$ sudo apt-get install php7.1-curl php7.1-mysql php7.1-memcached php7.1-xml php7.1-mbstring php7.1-zip php7.1-json php7.1-gd php7.1-common php7.1-intl php7.1-mcrypt php7.1-sqlite3
	```
	
	Mode dev for debuging or on preprod we need x-debug to generate phpunit report
	
	```
	$ sudo apt-get install php-xdebug
	```
	
4. CURL
	Install CURL
	
	```
	$ sudo apt-get install curl
	```
	
5. UNZIP
	Install CURL
	
	```
	$ sudo apt-get install unzip
	```
	
6. Composer
	Install composer
	
	```
	$ curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
	```
	
7. MySql version 5.7
	Install server
	
	```
	$ sudo apt install mysql-server-5.6
	```
	
	Install client
	
	```
	$ sudo apt install mysql-client-5.6
	```
	
8. Apache version 2.4
	Install Apache2.4
	
	```
	$ sudo apt install apache2
	```
	
9. Node version 8
	Install node 
	
	```
	$ curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
	$ sudo apt-get install -y nodejs
	```
	
10. Yarn version 1.7
	Install Yarn 
	
	```
	$ curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
	$ echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
	$ sudo apt-get update && sudo apt-get install yarn
	```
	
## Doctrine Migration

#### Initialize Migration

1. Drop existing database
    ```
    $ mysqladmin -u[username] -p[password] drop [database]
    ```

2. Create new DataBase
    ```
    $ bin/console doctrine:database:create
    ```
    
3. Generate init version to this database
    ```
    $ bin/console doctrine:migration:diff
    ```
    In this step Verison Calss will be create in `app/DoctrineMigrations` 

4. Update database
    ```
    $ bin/console doctrine:migration:migrate
    ```
    In this step, the database will be updated and conform to Version created

5. Load data fixture
    ```
    $ bin/console hautelook_alice:fixtures:load -n
    ```
#### Use Doctrine migration version

To update our database compared a doctrine Entities version without use `doctrine:schema:update --force`

You need use Doctrine migration

1. Generate a version to this model
    ```
    $ bin/console doctrine:migration:diff
    ```
    In this step Verison Calss will be create in `app/DoctrineMigrations` 

2. Update database
    ```
    $ bin/console doctrine:migration:migrate
    ```

## ExaKat

TO DO