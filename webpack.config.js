var Encore = require('@symfony/webpack-encore');
const CopyWebpackPlugin = require('copy-webpack-plugin');
Encore
    .setOutputPath('web/vue/v-frontend/compiled/')
    .setPublicPath('/vue/v-frontend/compiled')
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .addEntry('js/app', './web/vue/v-frontend/app.js')
    .addEntry('login', './web/assets/js/login.js')
    .addEntry('main', './web/assets/js/main.js')


//    .createSharedEntry('vendor', ['babel-polyfill'])
    .enableVueLoader()
    .enableSassLoader()
    .enableBuildNotifications()
    .addPlugin(new CopyWebpackPlugin([
        { from: './web/assets/static', to: 'static' }
    ]))
;

module.exports = Encore.getWebpackConfig();