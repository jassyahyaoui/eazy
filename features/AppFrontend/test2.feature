#features/comany/create.feature
Feature: A.2 App
@javascript
Scenario: 1.2 CREATION/VALIDATION D'UN OBJET PAR UN UTILISATEUR(userDoeEntrerprise1@pmeti.hosting)
#0 - Se connecter 
Given I am logged using "userDoeEntrerprise1@pmeti.hosting" and "init"
When I wait for ajax
Given I am logged using "userDoeEntrerprise1@pmeti.hosting" and "init"
When I wait for ajax
#1 - Contrôler dans le portail que 
#Then I should see current company of user "userDoeEntrerprise1@pmeti.hosting"
#Then I should see services of user "userDeoEntreprise2@pmeti.hosting"
When I click on the link "menu"
Then I should see translateLabel
#Then I should see contacts of user "userDeoEntreprise2@pmeti.hosting"
Then I should see link menu
#2 - Aller sur le formulaire "Paramètres de l'entreprise"
When I click on the link "company_setting"
#3 - Sur le formulaire  "Paramètres de l'entreprise"contrôler :
#Then I should see entreprise setting of user "userDeoEntreprise2@pmeti.hosting"
#4 - Aller sur le formulaire "Liste des Utilisateurs" 
#5 - Sur le formulaire  "Liste des Utilisateurs" 
When I click on the link "menu" 
When I click on the link "user_list"
When I wait for ajax
Then I should see list of user "userDeoEntreprise2@pmeti.hosting"
#6 - Aller sur le formulaire "Préférences Utilisateur"
#When I click on the link "modaluser"
#Then I click on the link "usersetting"
#7 - Sur le formulaire modifier les champs et sauvegarder 
#Given I fill
#8-contrôler la présence des nouvelles valeurs des champs 
#Then I should see new usersetting of user "userDeoEntreprise2@pmeti.hosting"
#9 - Valider la connexion vers ONEUP
#When I click on the link "box"
#Then  I should see the devis link
#10 - Valider la connexion vers CEGID
#11 - Contrôler dans la modale Help Menu
When I click on the link "help"
Then  I should see support number
Then  I should see the guide link
Then  I should see the requestlink
Then  I click on the link "close_modal  close thick"
When I wait for ajax
#12 - Contrôler dans la modale Notification que 
When I click on the link "show-modalNotification"
Then I should see notification
Then I should see message
Then  I click on the link "closenotification"
When I wait for ajax
When I click on the link "modaluser"
#13 - Contrôler dans la modale User Menu 
Then I should see conforme user of user "userDeoEntreprise2@pmeti.hosting"
Then  I should see the presentation link
Then  I should see the mazars link2
#14 - Se déconnecter et controler que l'Utilisateur n'accède pas au Portail
Then I click on the link "logout"

