#features/comany/create.feature
Feature: A.3 App
@javascript
Scenario: 1.3 CREATION/VALIDATION D'UN OBJET PAR UN COLLABORATEUR (usermzbdx1@pmeti.hosting)
#0 - Se connecter 
Given I am logged using "usermzbdx1@pmeti.hosting" and "init"
When I wait for ajax
Given I am logged using "usermzbdx1@pmeti.hosting" and "init"
When I wait for ajax
When I wait for ajax
#1 - Contrôler dans le portail que 
Then  I should see current company of user "usermzbdx1@pmeti.hosting"
Then I should see services of user "usermzbdx1@pmeti.hosting"

When I click on the link "menu"
Then I should see translateLabel
Then I should see contacts of user "usermzbdx1@pmeti.hosting"
Then I should see link menu
#2 - Aller sur le formulaire "Paramètres de l'entreprise"
When I click on the link "company_setting"
#3 - Sur le formulaire  "Paramètres de l'entreprise" contrôler :
  When I wait for ajax
  When I wait for ajax
Then I should see entreprise setting of user "usermzbdx1@pmeti.hosting"
#4 - Aller sur le formulaire "Liste des Utilisateurs" 
#5 - Sur le formulaire  "Liste des Utilisateurs" 
When I click on the link "menu"
When I click on the link "user_list"
Then I should see list of user "usermzbdx1@pmeti.hosting"
When I wait for ajax
When I click on the link "menu"
When I click on the link "homepage"
When I wait for ajax
When I wait for ajax
#6- Sélectionner l'entreprise "DoeConsulting" dans le Sélecteur d'entreprise
When I click on the link "slc"
When I wait for ajax
#And I select "Durand Formation" from "selectedCompany"
When I wait for ajax

#7 - Contrôler dans le portail que :
Then I should see services of user "usermzbdx1@pmeti.hosting"
When I wait for ajax
When I click on the link "menu"
Then I should see translateLabel
Then I should see contacts of user "usermzbdx1@pmeti.hosting"
When I wait for ajax
#When I click on the link "modaluser"
#8 - Contrôler dans la modale User Menu 
#Then I should see the mazars link
When I click on the link "menu"
When I click on the link "menu"
When I click on the link "homepage"
When I wait for ajax
When I wait for ajax
#9 - Aller sur le formulaire "Paramètres de l'entreprise"
When I click on the link "menu"
When I click on the link "company_setting"
#10- Sur le formulaire  "Paramètres de l'entreprise"contrôler :
  When I wait for ajax
  When I wait for ajax
Then I should see entreprise setting of user "usermzbdx1@pmeti.hosting"
#11 - Aller sur le formulaire "Liste des Utilisateurs" 
#12 - Sur le formulaire  "Liste des Utilisateurs" 
When I click on the link "menu"
When I click on the link "user_list"
Then I should see list of user "usermzbdx1@pmeti.hosting"
When I wait for ajax
#13 - Sur le formulaire modifier les champs et sauvegarder
#When I click on the link "modaluser"
#When I wait for ajax
#Then I click on the link "usersetting"
#Given I fill
When I wait for ajax
#14-contrôler la présence des nouvelles valeurs des champs 
#Then I should see new usersetting of user "usermzbdx1@pmeti.hosting"
#15 - Contrôler dans la modale Help Menu
When I click on the link "help"
Then  I should see support number

Then  I should see the guide link

Then  I should see the requestlink

Then  I click on the link "close_modal  close thick"
When I wait for ajax
#16 - Contrôler dans la modale Notification que 
When I click on the link "show-modalNotification"
Then I should see notification
Then I should see message
Then  I click on the link "closenotification"
When I wait for ajax
When I click on the link "modaluser"
#17 - Contrôler dans la modale User Menu 
Then I should see conforme user of user "usermzbdx1@pmeti.hosting"
Then  I should see the presentation link
Then  I should see the mazars link 
#18 - Se déconnecter et controler que l'Utilisateur n'accède pas au Portail
Then I click on the link "logout"
