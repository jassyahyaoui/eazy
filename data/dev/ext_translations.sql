-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  mar. 17 juil. 2018 à 10:29
-- Version du serveur :  5.7.20-18-log
-- Version de PHP :  7.1.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `preprod-www.pmeti.hosting`
--

--
-- Déchargement des données de la table `ext_translations`
--

INSERT INTO `ext_translations` (`id`, `locale`, `object_class`, `field`, `foreign_key`, `content`) VALUES
(1, 'en', 'AppBundle\\Entity\\Service', 'name', '1', 'INVOICING'),
(2, 'en', 'AppBundle\\Entity\\Service', 'shortDesc', '1', 'Your quotes and sales invoices in 1 click'),
(3, 'en', 'AppBundle\\Entity\\Service', 'name', '2', 'POS Software'),
(4, 'en', 'AppBundle\\Entity\\Service', 'shortDesc', '2', 'Your POS Software certified and synchronized with your eCommerce website'),
(5, 'en', 'AppBundle\\Entity\\Service', 'name', '3', 'eCommerce'),
(6, 'en', 'AppBundle\\Entity\\Service', 'shortDesc', '3', 'Your simple and easy-to-setup eCommerce website'),
(7, 'en', 'AppBundle\\Entity\\Service', 'name', '4', 'Payroll Entry'),
(8, 'en', 'AppBundle\\Entity\\Service', 'shortDesc', '4', 'Your payroll setup : easy and simple'),
(9, 'en', 'AppBundle\\Entity\\Service', 'name', '5', 'DOCUMENTS SCAN'),
(10, 'en', 'AppBundle\\Entity\\Service', 'shortDesc', '5', 'Automatic recording of your expenses or purchase invoices'),
(11, 'en', 'AppBundle\\Entity\\Service', 'name', '6', 'DOCUMENTS'),
(12, 'en', 'AppBundle\\Entity\\Service', 'shortDesc', '6', 'Secure exchange of your documents with your accountant'),
(13, 'en', 'AppBundle\\Entity\\Service', 'name', '7', 'ACCOUNTING'),
(14, 'en', 'AppBundle\\Entity\\Service', 'shortDesc', '7', 'Your online accounting');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
